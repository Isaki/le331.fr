/**
 * Created by romaintiger on 20/10/15.
 */

$(document).ready(function () {

    $(".custom-input-file").each(function () {
        var id = $(this).attr("id"),
            name = $(this).attr("name"),
            title = $(this).data("title"),
            accept = $(this).attr("accept"),
            placeholder = $(this).attr("placeholder"),
            classVerif = "",
            template = "";

        if ($(this).hasClass("verif")) classVerif = "verif";

        template += "<div class='button_border'>";
        template += "<input type='file' name='" + name + "' id='" + id + "' accept='" + accept + "' class='file-input " + classVerif + "'>";
        template += "<span>" + placeholder + "</span>";
        template += "<button type='button' class='big grey input-file-trigger'>" + title + "</button>";
        template += "</div></div>";

        //$(this).wrap('<div class="custom-select-wrapper"></div>');
        $(this).after(template);
        $(this).remove();
    });

    $(".input-file-trigger").click(function (event) {
        var parent = $(this).parent(".button_border"),
            input = parent.find(".file-input");
        input.trigger("click");
    });

    $(".file-input").change(function () {
        var parent = $(this).parent(".button_border"),
            text = parent.find("span"),
            names = $(this).val().split("\\"),
            name = names[names.length - 1];

        text.html(name);
        parent.removeClass("rejected");
        parent.addClass("passed");
    });

});