$(document).ready(function () {
    $("ul.selector li").click(function () {
        var index = $(this).index();
        var content = $("ul.content li:eq(" + index + ")");
        $("ul.selector li").removeClass('active');
        $(this).addClass('active');
        $("ul.content li").show();
        setTimeout(function () {
            $("ul.content li").removeClass('active');
            $("ul.content li").addClass('disactive');
            content.removeClass('disactive');
            content.addClass('active');
            $("ul.content li").one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend',
                function () {
                    $("ul.content li.disactive").hide();
                    $("ul.content li.active").show();
                });
        }, 1);
    });
});