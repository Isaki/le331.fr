function initMap() {
    // Create a map object and specify the DOM element for display.
    var myLatLng = {lat: 48.8740668, lng: 2.3479677};
    var title = $('header h1').text();

    var map = new google.maps.Map(document.getElementById('map'), {
        scrollwheel: false,
        zoom: 6,
        center: myLatLng
    });

    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: title
    });

    var styles = [
        {
            stylers: [
                {hue: "#7BC9E5"},
                {saturation: -20},
            ]
        }, {
            featureType: "road",
            elementType: "geometry",
            stylers: [
                {lightness: 100},
                {visibility: "simplified"}
            ]
        }, {
            featureType: "road",
            elementType: "labels",
            stylers: [
                {visibility: "off"}
            ]
        }
    ];

    map.setOptions({styles: styles});
}