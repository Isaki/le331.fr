/**
 * Created by Romain on 11/02/2016.
 */

$(document).ready(function () {

    var search_type = $('#search_type').val();

    var types = {
        students: ['promo', 'city', 'profile'],
        companies: ['industry', 'workforce', 'city']
    };

    var filters = {
        promo: null,
        city: null,
        profile: null,
        industry: null
    };

    // les selects
    $('.promo-filter').change(function () {
        filters.promo = $(this).val() == 'ras' ? null : 'promo-' + $(this).val();
        filter();
    });
    $('.city-filter').change(function () {
        filters.city = $(this).val() == 'ras' ? null : 'city-' + $(this).val();
        filter();
    });
    $('.profile-filter').change(function () {
        filters.profile = $(this).val() == 'ras' ? null : 'profile-' + $(this).val();
        filter();
    });
    $('.industry-filter').change(function () {
        filters.industry = $(this).val() == 'ras' ? null : 'industry-' + $(this).val();
        filter();
    });

    function filter() {
        $('.tile').each(function () {
            var show = true;

            for (var i = 0; i < types[search_type].length; i++) {
                var filterType = types[search_type][i];

                if (filters[filterType] != null) {
                    if ($(this).hasClass(filters[filterType]) == false) {
                        show = false;
                    }
                }
            }

            if (show) $(this).show();
            else $(this).hide();
        });
    }
});