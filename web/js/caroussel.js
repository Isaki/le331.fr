/**
 * Created by dorianlods on 20/10/15.
 */

//$(function(){
//    $('.caroussel img:gt(0)').hide();
//    setInterval(function(){$('.caroussel :first-child').fadeOut().next('img').caroussel().end().appendTo('.caroussel');}, 1000);
//});​
$(document).ready(function () {

    $(window).load(function () {


        function redimensionnement() {

            var $image = $('#caroussel img');
            var image_width = $image.width();
            var image_height = $image.height();

            var over = image_width / image_height;
            var under = image_height / image_width;

            var body_width = $('#caroussel').width();
            var body_height = $('#caroussel').height();

            if (body_width / body_height >= over) {
                $image.css({
                    'width': body_width + 'px',
                    'height': Math.ceil(under * body_width) + 'px',
                    'left': '0px',
                    'top': Math.abs((under * body_width) - body_height) / -2 + 'px'
                });
            }

            else {
                $image.css({
                    'width': Math.ceil(over * body_height) + 'px',
                    'height': body_height + 'px',
                    'top': '0px',
                    'left': Math.abs((over * body_height) - body_width) / -2 + 'px'
                });
            }
        }


        // initialisation des images
        $('#caroussel img').hide();
        $('#caroussel img:first-of-type').show();

        // initialisation du contenu textuel
        $("#caroussel_text h2").hide();
        $("#caroussel_text p").hide();
        $("#caroussel_text > div:first-child > h2").fadeIn(2000);
        $("#caroussel_text > div:first-child > p").fadeIn(2000);
        $("#caroussel_text > div:first-child > h2").addClass("translateIn");
        $("#caroussel_text > div:first-child > p").addClass("translateIn");

        var index = 1;
        setInterval(function () {
            // changement de l'image
            $('#caroussel :first-child').fadeOut(2000).next('img').fadeIn(2000).end().appendTo('#caroussel');

            // changement du texte
            $("#caroussel_text > div:nth-child(" + index + ") > h2").fadeOut(2000, function () {
                $(this).removeClass("translateOut");
            });
            $("#caroussel_text > div:nth-child(" + index + ") > p").fadeOut(2000, function () {
                $(this).removeClass("translateOut");
            });
            ;
            $("#caroussel_text > div:nth-child(" + index + ") > h2").removeClass("translateIn").addClass("translateOut");
            $("#caroussel_text > div:nth-child(" + index + ") > p").removeClass("translateIn").addClass("translateOut");

            if (index < $("#caroussel > img").length) index++;
            else index = 1;
            $("#caroussel_text > div:nth-child(" + index + ") > h2").fadeIn(2000);
            $("#caroussel_text > div:nth-child(" + index + ") > p").fadeIn(2000);
            $("#caroussel_text > div:nth-child(" + index + ") > h2").addClass("translateIn");
            $("#caroussel_text > div:nth-child(" + index + ") > p").addClass("translateIn");
        }, 5000);


        // Au chargement initial
        redimensionnement();

        // En cas de redimensionnement de la fenêtre
        $(window).resize(function () {
            redimensionnement();
        });

    });
});
