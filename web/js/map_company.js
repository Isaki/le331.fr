/**
 * Created by dorianlods on 27/11/15.
 */

function initMap() {
    // Create a map object and specify the DOM element for display.
    var myLatLng = {lat: 48.8740668, lng: 2.3479677};
    var title = $('header h1').text();

    var map = new google.maps.Map(document.getElementById('map'), {
        scrollwheel: false,
        zoom: 15,
        center: myLatLng
    });

    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: title
    });

    var styles = [
        {
            stylers: [
                {hue: "#7BC9E5"},
                {saturation: -20},
            ]
        }, {
            featureType: "road",
            elementType: "geometry",
            stylers: [
                {lightness: 100},
                {visibility: "simplified"}
            ]
        }, {
            featureType: "road",
            elementType: "labels",
            stylers: [
                {visibility: "off"}
            ]
        }
    ];

    map.setOptions({styles: styles});
}

function toggleBounce() {
    if (marker.getAnimation() !== null) {
        marker.setAnimation(null);
    } else {
        marker.setAnimation(google.maps.Animation.BOUNCE);
    }
}


var active = false;


function toggleMap() {
    var content = $('main>div:first-of-type');
    var mapContainer = $('main>section');

    console.log(active);

    if (active == false) {
        content.animate({
            opacity: 0
        }, 500, function () {
            //Fin de l'anim
            content.css("z-index", -1);
            mapContainer.css("z-index", 0);
            active = true;
        });
    }
    else {
        content.css("z-index", 0);
        mapContainer.css("z-index", -1);
        content.animate({
            opacity: 1
        }, 500, function () {
            //Fin de l'anim
            active = false;
        });
    }
}

$(document).ready(function () {
    $('#maptoggle').click(function () {
        $(this).toggleClass('active');
        toggleMap();
    });
});