/**
 * Created by dorianlods on 11/11/15.
 */

$(document).ready(function () {

    //scrollTo

    $('.targeted').click(function (event) {
        event.preventDefault();

        var target = $(this).data('target');
        console.log($(target));
        $(window).scrollTo($(target), 1000, {easing: 'easeInOutCirc'});
    });

    //transition entre les pages

    //var popStat = false;


    $(window).load(function () {

        $("#loader").addClass("dissmissed");

        //$("a.transition").click(function (event) {
        //    event.preventDefault();
        //    linkLocation = this.href;
        //    $("#loader").removeClass("dissmissed");
        //    setTimeout(redirectPage, 1000);
        //    popStat = true;
        //});
        //
        //function redirectPage() {
        //    window.location = linkLocation;
        //}
    });

    //$(window).bind("popstate", function () {
    //    if (popStat == true) {
    //        $("#loader").addClass("dissmissed");
    //    }
    //});

});