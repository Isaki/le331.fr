$(document).ready(function () {

    var selectExp = $('select.experiences');
    var selectTyp = $('select.type');
    var addUpdateBtn = $('form.experience > button.addUpdate');
    var deleteBtn = $('form.experience > button.delete').hide();

    ///////////// EVENTS

    // lorsque l'on passe en ajout d'expérience || modification d'une exp
    selectExp.change(function () {
        // auto-completion du champs
        var value = selectExp.val();

        // afficher/cacher le bouton du type d'ajout
        if (value == 'add') {
            addUpdateBtn.html('Ajouter');
            displayTheRightForm(null);
        }
        else {
            addUpdateBtn.html('Modifier');

            var data = $(this).val().split(':');
            displayTheRightForm(data[0]);
            ajaxGetExperience(data[0], data[1]);
        }
    });

    // lorsque l'on veux ajouter une exp et que l'on change le type d'exp
    selectTyp.change(function () {
        displayTheRightForm(null);
    });

    deleteBtn.click(function () {
        var data = selectExp.val().split(':');

        // on vire l'entré dans la bdd
        var url = window.location.href;
        var p = url.split('/');

        base_url = '';
        for (var i = 0; i < p.length - 1; i++) {
            if (p[i] == 'student') break;
            base_url += p[i] + '/';
        }

        $.ajax({
            url: base_url + 'ajax_delete_experience/' + data[0] + '/' + data[1],
            asynch: 'false',
            type: 'post',
            datatype: 'json',
            success: function (data) {
            }
        });

        // on vire les donnée de la vue
        $('.' + data[0] + '_' + data[1] + '').each(function () {
            $(this).remove();
        });

        // on réinitialise la séléction
        selectExp.val('add');
        displayTheRightForm(null);
    });

    ///////////// FUNCTIONS

    // affiche le bon formulaire
    function displayTheRightForm(value) {
        if (value == null) value = selectTyp.val();

        if (selectExp.val() == 'add') {
            selectTyp.css('display', 'inline-block');
            deleteBtn.hide();
        }
        else {
            selectTyp.css('display', 'none');
            deleteBtn.show();
        }

        // on vide les formulaires
        setDataJob(null);
        setDataDegree(null);
        setDataProject(null);

        // afficher/cacher le bon formulaire
        $('form.experience').css('display', 'none');
        $('form.' + value).css('display', 'block');
    }

    displayTheRightForm(null);

    // ajax : récupération des données (type d'expérience + son id)
    function ajaxGetExperience(type, id) {

        var url = window.location.href;
        var p = url.split('/');

        base_url = '';
        for (var i = 0; i < p.length - 1; i++) {
            if (p[i] == 'student') break;
            base_url += p[i] + '/';
        }

        $.ajax({
            url: base_url + 'ajax_get_experience/' + type + '/' + id,
            asynch: 'false',
            type: 'post',
            datatype: 'json',
            success: function (data) {
                switch (type) {
                    case 'Job':
                        setDataJob(data);
                        break;

                    case 'Degree':
                        setDataDegree(data);
                        break;

                    case 'Project':
                        setDataProject(data);
                        break;
                }
            }
        });
    }

    function setDataJob(data) {
        $('#UserAddJobType_profession_id').val(data == null ? null : data['profession']);
        $('#UserAddJobType_company_id').val(data == null ? null : data['company']);
        $('#UserAddJobType_description').val(data == null ? null : data['description']);
        $('#UserAddJobType_yearFrom').val(data == null ? null : data['yearFrom']);
        $('#UserAddJobType_yearTo').val(data == null ? null : data['yearTo']);

        $('#UserAddJobType_job_id').val(data == null ? null : data['id']);
    }

    function setDataDegree(data) {
        $('#UserAddDegreeType_degree_id').val(data == null ? null : data['degree']);
        $('#UserAddDegreeType_yearFrom').val(data == null ? null : data['yearFrom']);
        $('#UserAddDegreeType_yearTo').val(data == null ? null : data['yearTo']);

        $('#UserAddDegreeType_training_id').val(data == null ? null : data['id']);
    }

    function setDataProject(data) {
        $('#UserAddProjectType_unipro_id').val(data == null ? null : data['project']);
        $('#UserAddProjectType_profession_id').val(data == null ? null : data['role']);
        $('#UserAddProjectType_yearFrom').val(data == null ? null : data['yearFrom']);
        $('#UserAddProjectType_yearTo').val(data == null ? null : data['yearTo']);

        $('#UserAddProjectType_project_id').val(data == null ? null : data['id']);
    }

});