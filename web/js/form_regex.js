/**
 * Created by romaintiger on 21/10/15.
 */

$(document).ready(function () {

    // fonction de vérification de la saisie
    function check_input_value(input) {
        var regexType = {
                "nom": "^([A-Z][a-zàèéïëäâîêç]{1,})((-| )[A-Z][a-z]+){0,}$",
                "email": "^[A-Za-z0-9._-]+@[A-Za-z0-9.-]{2,}\\.[a-z]{2,4}$",
                "lien": "^((http|https)://)?(www)?([a-z0-9]{2,}){1}((\\.|-)[a-z0-9]{2,})+\\.[a-z0-9]{2,3}(/[a-z0-9+%&=?\\-.]+){0,}$",
                "date": "^[0-3][0-9]/[0-1][0-2]/(19|20)[0-9]{2}$"
            },
            regexKey = input.data("regex"),
            pswLength = 8,
            result = 0,
            el = input;

        switch (input.attr("type")) {
            case "text":
                // On charge dans "regex" l'expression associée
                var regex = regexType[regexKey];

                if (regexKey == "nom") {
                    // On met une majuscule à la première lettre de chaques mots, le reste en minuscules
                    var caretPosition = input.getCursorPosition();

                    var separator = "-";
                    if (input.val().indexOf(separator) == -1) separator = " ";

                    var words = input.val().split(separator);
                    var newVal = "";
                    for (var i = 0; i < words.length; i++) {
                        if (i > 0) newVal += separator;
                        newVal += words[i].charAt(0).toUpperCase() + words[i].slice(1).toLowerCase();
                    }
                    input.val(newVal);
                    input.selectRange(caretPosition);
                }

                if (input.val().match(regex) == null) result = 1;
                break;

            case "password":
                if (input.val().length < pswLength) result = 1;
                break;

            case "file":
                if (input.val() == "") result = 1;
                el = input.parent();
                break;
        }

        if (result == 0) {
            el.removeClass("rejected");
            el.addClass("passed");
        }
        else if (result == 1) {
            el.removeClass("passed");
            el.addClass("rejected");
        }

        return result;
    }

    // validation de la saisie des champs
    $("form.verif_regex input.verif").on("input", function () {
        check_input_value($(this));
    });

    // tentative de validation d'un formulaire
    $("form.verif_regex").submit(function (event) {
        var inputs = $(this).find(".verif");
        var result = 0;

        inputs.each(function () {
            if ($(this).hasClass("optional") == false) result += check_input_value($(this));
        });

        if (result > 0) {
            event.preventDefault();
            $("#plane").removeClass("active");
        }
    });

    // pour obtenir la position du curseur
    (function ($, undefined) {
        $.fn.getCursorPosition = function () {
            var el = $(this).get(0);
            var pos = 0;
            if ('selectionStart' in el) {
                pos = el.selectionStart;
            } else if ('selection' in document) {
                el.focus();
                var Sel = document.selection.createRange();
                var SelLength = document.selection.createRange().text.length;
                Sel.moveStart('character', -el.value.length);
                pos = Sel.text.length - SelLength;
            }
            return pos;
        }
    })(jQuery);

    // pour définir la position du curseur
    $.fn.selectRange = function (start, end) {
        if (typeof end === 'undefined') {
            end = start;
        }
        return this.each(function () {
            if ('selectionStart' in this) {
                this.selectionStart = start;
                this.selectionEnd = end;
            } else if (this.setSelectionRange) {
                this.setSelectionRange(start, end);
            } else if (this.createTextRange) {
                var range = this.createTextRange();
                range.collapse(true);
                range.moveEnd('character', end);
                range.moveStart('character', start);
                range.select();
            }
        });
    };

});