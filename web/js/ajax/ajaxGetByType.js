function ajaxListGetByType(type, inputElement) {

    var url = window.location.href;
    var p = url.split('/');

    base_url = '';
    for (var i = 0; i < p.length - 1; i++) {
        if (p[i] == 'student') break;
        base_url += p[i] + '/';
    }

    $.ajax({
        url: base_url + 'ajax_get_list_by_type/' + type,
        asynch: 'false',
        type: 'post',
        datatype: 'json',
        success: function (data) {
            inputElement.autocomplete({
                source: data['list']
            });
        }
    });
}