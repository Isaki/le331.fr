<?php

namespace MMIBundle\Model;

use MMIBundle\Model\om\BaseRolePeer;

class RolePeer extends BaseRolePeer
{
    public static function get($id){
        return RoleQuery::create()
            ->findOneById($id);
    }

    public static function getUsersById($id) {
        $role = RoleQuery::create()
            ->filterById($id)
            ->findOne();

        return $role->getUsers();
    }
}
