<?php

namespace MMIBundle\Model;

use MMIBundle\Model\om\BaseCityPeer;

class CityPeer extends BaseCityPeer
{
    public static function getAll(){
        return CityQuery::create()
            ->orderByName("ASC")
            ->find();
    }

    public static function getLike($search){
        return CityQuery::create()
            ->orderByName("ASC")
            ->filterByName("%".$search."%")
            ->find();
    }

    public static function getByName($name){
        return CityQuery::create()
            ->filterByName($name)
            ->findOne();
    }

    public static function getByCp($cp){
        return CityQuery::create()
            ->filterByCp($cp)
            ->findOne();
    }
}
