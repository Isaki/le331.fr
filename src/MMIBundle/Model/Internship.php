<?php

namespace MMIBundle\Model;

use MMIBundle\Model\om\BaseInternship;
use \PropelPDO;

class Internship extends BaseInternship
{
    protected $aTutor = null;

    public function getUser(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aUser === null && $this->user_id !== null && $doQuery) {
            $this->aUser = UserQuery::create()
                ->filterById($this->getUserId())
                ->findOne($con);
        }

        return $this->aUser;
    }

    public function getTutor(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aTutor === null && $this->tutor_id !== null && $doQuery) {
            $this->aTutor = UserQuery::create()
                ->filterById($this->getTutorId())
                ->findOne($con);
        }

        return $this->aTutor;
    }

    public function setUser(User $v = null)
    {
        return $this->setUserId(null !== $v ? $v->getId() : null);
    }

    public function setTutor(User $v = null)
    {
        return $this->setTutorId(null !== $v ? $v->getId() : null);
    }

    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aOfferinternshipRelatedByOfferId !== null) {
                if ($this->aOfferinternshipRelatedByOfferId->isModified() || $this->aOfferinternshipRelatedByOfferId->isNew()) {
                    $affectedRows += $this->aOfferinternshipRelatedByOfferId->save($con);
                }
                $this->setOfferinternshipRelatedByOfferId($this->aOfferinternshipRelatedByOfferId);
            }

            if ($this->aUser !== null) {
                if ($this->aUser->isModified() || $this->aUser->isNew()) {
                    $affectedRows += $this->aUser->save($con);
                }
                $this->setUser($this->aUser);
            }

            if ($this->aTutor !== null) {
                if ($this->aTutor->isModified() || $this->aTutor->isNew()) {
                    $affectedRows += $this->aTutor->save($con);
                }
                $this->setUser($this->aUser);
            }

            if ($this->aCompany !== null) {
                if ($this->aCompany->isModified() || $this->aCompany->isNew()) {
                    $affectedRows += $this->aCompany->save($con);
                }
                $this->setCompany($this->aCompany);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->offerinternshipsRelatedByInternshipIdScheduledForDeletion !== null) {
                if (!$this->offerinternshipsRelatedByInternshipIdScheduledForDeletion->isEmpty()) {
                    OfferinternshipQuery::create()
                        ->filterByPrimaryKeys($this->offerinternshipsRelatedByInternshipIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->offerinternshipsRelatedByInternshipIdScheduledForDeletion = null;
                }
            }

            if ($this->collOfferinternshipsRelatedByInternshipId !== null) {
                foreach ($this->collOfferinternshipsRelatedByInternshipId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    }
}
