<?php

namespace MMIBundle\Model;

use MMIBundle\Model\om\BaseUserPeer;

class UserPeer extends BaseUserPeer
{
    public static function getOneById($id)
    {
        return UserQuery::create()
            ->filterById($id)
            ->findOne();
    }

    public static function getOneByName($name)
    {
        return CompanyQuery::create()
            ->filterByName($name)
            ->findOne();
    }

    public static function userExists($email)
    {
        return UserQuery::create()
            ->filterByEmail($email)
            ->count() > 0;
    }

    public static function getByEmail($email)
    {
        return UserQuery::create()
            ->filterByEmail($email)
            ->findOne();
    }

    public static function isValidEmailPassword(User $user, $pass)
    {
        return $user->getPassword() === $pass;
    }

    public static function getPages($id)
    {
        $pagesQuery = PageQuery::create()
            ->useRolePageQuery()
            ->useRoleQuery()
            ->useUserRoleQuery()
            ->useUserQuery()
            ->filterById($id)
            ->endUse()
            ->endUse()
            ->endUse()
            ->endUse()
            ->find();
        $pages = [];
        foreach ($pagesQuery as $page) $pages[$page->getRoute()] = $page->getRoute();

        return $pages;
    }

    public static function isAdmin($id)
    {
        $result = RoleQuery::create()
            ->filterById(10)
            ->useUserRoleQuery()
            ->useUserQuery()
            ->filterById($id)
            ->endUse()
            ->endUse()
            ->findOne();

        return $result != null ? true : false;
    }

    public static function getExperiences($id)
    {
        $experiences = [];

        // les professions
        $jobs = UserJobQuery::create()
            ->orderByYearFrom('ASC')
            ->findByUserId($id);

        for ($i = 0; $i < count($jobs); $i++) {
            $job = $jobs[$i];
            $comp = $job->getCompany();
            $prof = $job->getProfession();
            $exp = [];

            $exp['type'] = 'Job';
            $exp['display'] = 'Emploi';
            $exp['id'] = $job->getId();
            $exp['from'] = $job->getYearFrom();
            $exp['to'] = $job->getYearTo();
            $exp['name'] = $comp->getName();
            $exp['desc'] = $prof->getName();
            $exp['image'] = $comp->getLogoFile();

            $object = [];
            $object['id'] = $comp->getId();
            $object['name'] = $comp->getName();
            $object['objects'][] = $object;

            $object = [];
            $object['id'] = $prof->getId();
            $object['name'] = $prof->getName();
            $exp['objects'][] = $object;

            $experiences[] = $exp;
        }

        // les diplômes
        $degrees = UserObtainedDegreeQuery::create()
            ->orderByYearFrom('ASC')
            ->findByUserId($id);

        for ($i = 0; $i < count($degrees); $i++) {
            $degree = $degrees[$i];
            $deg = DegreeQuery::create()
                ->filterById($degree->getDegreeId())
                ->findOne();
            $exp = [];

            $exp['type'] = 'Degree';
            $exp['display'] = 'Formation';
            $exp['id'] = $degree->getId();
            $exp['from'] = $degree->getYearFrom();
            $exp['to'] = $degree->getYearTo();
            $exp['name'] = $deg->getName();
            $exp['desc'] = $deg->getOption();

            $object = [];
            $object['id'] = $deg->getId();
            $object['name'] = $deg->getName();
            $exp['objects'][] = $object;

            $experiences[] = $exp;
        }

        // les projets
        $projects = UserMemberUniproQuery::create()
            ->orderByYearFrom('ASC')
            ->findByUserId($id);

        for ($i = 0; $i < count($projects); $i++) {
            $project = $projects[$i];
            $pro = UnivProjectQuery::create()
                ->filterById($project->getUniproId())
                ->findOne();
            $prof = $project->getProfession();
            $exp = [];

            $exp['type'] = 'Project';
            $exp['display'] = 'Projet';
            $exp['id'] = $project->getId();
            $exp['from'] = $project->getYearFrom();
            $exp['to'] = $project->getYearTo();
            $exp['name'] = $pro->getName();
            $exp['desc'] = $prof->getName();

            $object = [];
            $object['id'] = $pro->getId();
            $object['name'] = $pro->getName();
            $exp['objects'][] = $object;

            $experiences[] = $exp;
        }

        return array_sort($experiences, 'from', SORT_DESC);
    }

    public static function getMenuItems($id = null)
    {
        if ($id != null) {
            return ItemmenuQuery::create()
                ->filterByLogstate(['login', 'both'])
                ->orderByOrder('ASC')
                ->useRoleMenuQuery()
                ->useRoleQuery()
                ->useUserRoleQuery()
                ->useUserQuery()
                ->filterById($id)
                ->endUse()
                ->endUse()
                ->endUse()
                ->endUse()
                ->find();
        } else {
            return ItemmenuQuery::create()
                ->filterByLogstate(['logout', 'both'])
                ->orderByOrder('ASC')
                ->find();
        }
    }
}

function array_sort($array, $on, $order = SORT_ASC)
{
    $new_array = array();
    $sortable_array = array();

    if (count($array) > 0) {
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $k2 => $v2) {
                    if ($k2 == $on) {
                        $sortable_array[$k] = $v2;
                    }
                }
            } else {
                $sortable_array[$k] = $v;
            }
        }

        switch ($order) {
            case SORT_ASC:
                asort($sortable_array);
                break;
            case SORT_DESC:
                arsort($sortable_array);
                break;
        }

        foreach ($sortable_array as $k => $v) {
            $new_array[$k] = $array[$k];
        }
    }

    return $new_array;
}