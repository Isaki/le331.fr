<?php

namespace MMIBundle\Model;

use MMIBundle\Model\om\BasePagePeer;

class PagePeer extends BasePagePeer
{
    public static function isMenuBlack($routeName)
    {
        $route = PageQuery::create()
            ->findOneByRoute($routeName);

        return $route != null ? $route->getIsmenublack() : false;
    }
}
