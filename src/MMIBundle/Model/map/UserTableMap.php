<?php

namespace MMIBundle\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'user' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.MMIBundle.Model.map
 */
class UserTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.MMIBundle.Model.map.UserTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('user');
        $this->setPhpName('User');
        $this->setClassname('MMIBundle\\Model\\User');
        $this->setPackage('src.MMIBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('city_id', 'CityId', 'INTEGER', 'city', 'id', true, null, null);
        $this->addForeignKey('profile_id', 'ProfileId', 'INTEGER', 'profession', 'id', true, null, null);
        $this->addForeignKey('promo_id', 'PromoId', 'INTEGER', 'promo', 'id', true, null, null);
        $this->addColumn('email', 'Email', 'VARCHAR', false, 64, null);
        $this->addColumn('password', 'Password', 'VARCHAR', false, 128, null);
        $this->addColumn('lastname', 'Lastname', 'VARCHAR', false, 32, null);
        $this->addColumn('firstname', 'Firstname', 'VARCHAR', false, 32, null);
        $this->addColumn('description', 'Description', 'VARCHAR', false, 140, null);
        $this->addColumn('newsletter', 'Newsletter', 'BOOLEAN', false, 1, true);
        $this->addColumn('birthday', 'Birthday', 'DATE', false, null, null);
        $this->addColumn('url_portfolio', 'UrlPortfolio', 'VARCHAR', false, 64, null);
        $this->addColumn('url_linkedin', 'UrlLinkedin', 'VARCHAR', false, 64, null);
        $this->addColumn('url_facebook', 'UrlFacebook', 'VARCHAR', false, 64, null);
        $this->addColumn('url_google', 'UrlGoogle', 'VARCHAR', false, 64, null);
        $this->addColumn('url_twitter', 'UrlTwitter', 'VARCHAR', false, 64, null);
        $this->addColumn('url_tumblr', 'UrlTumblr', 'VARCHAR', false, 64, null);
        $this->addColumn('url_instagram', 'UrlInstagram', 'VARCHAR', false, 64, null);
        $this->addColumn('url_blog', 'UrlBlog', 'VARCHAR', false, 64, null);
        $this->addColumn('cv_file', 'CvFile', 'VARCHAR', false, 64, null);
        $this->addColumn('image_profil_file', 'ImageProfilFile', 'VARCHAR', false, 64, 'default.svg');
        $this->addColumn('image_couv_file', 'ImageCouvFile', 'VARCHAR', false, 64, 'default.jpg');
        $this->addColumn('telephone', 'Telephone', 'VARCHAR', false, 32, null);
        $this->addColumn('first_connection', 'FirstConnection', 'BOOLEAN', false, 1, false);
        $this->addColumn('validated', 'Validated', 'BOOLEAN', false, 1, false);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('City', 'MMIBundle\\Model\\City', RelationMap::MANY_TO_ONE, array('city_id' => 'id', ), null, null);
        $this->addRelation('Profession', 'MMIBundle\\Model\\Profession', RelationMap::MANY_TO_ONE, array('profile_id' => 'id', ), null, null);
        $this->addRelation('Promo', 'MMIBundle\\Model\\Promo', RelationMap::MANY_TO_ONE, array('promo_id' => 'id', ), null, null);
        $this->addRelation('UserRole', 'MMIBundle\\Model\\UserRole', RelationMap::ONE_TO_MANY, array('id' => 'user_id', ), null, null, 'UserRoles');
        $this->addRelation('Internship', 'MMIBundle\\Model\\Internship', RelationMap::ONE_TO_MANY, array('id' => 'user_id', ), null, null, 'Internships');
        $this->addRelation('UserJob', 'MMIBundle\\Model\\UserJob', RelationMap::ONE_TO_MANY, array('id' => 'user_id', ), null, null, 'UserJobs');
        $this->addRelation('UserObtainedDegree', 'MMIBundle\\Model\\UserObtainedDegree', RelationMap::ONE_TO_MANY, array('id' => 'user_id', ), null, null, 'UserObtainedDegrees');
        $this->addRelation('UnivProject', 'MMIBundle\\Model\\UnivProject', RelationMap::ONE_TO_MANY, array('id' => 'user_id', ), null, null, 'UnivProjects');
        $this->addRelation('UserMemberUnipro', 'MMIBundle\\Model\\UserMemberUnipro', RelationMap::ONE_TO_MANY, array('id' => 'user_id', ), null, null, 'UserMemberUnipros');
        $this->addRelation('Validation', 'MMIBundle\\Model\\Validation', RelationMap::ONE_TO_MANY, array('id' => 'user_id', ), null, null, 'Validations');
        $this->addRelation('Role', 'MMIBundle\\Model\\Role', RelationMap::MANY_TO_MANY, array(), null, null, 'Roles');
    } // buildRelations()

} // UserTableMap
