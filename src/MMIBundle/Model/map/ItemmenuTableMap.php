<?php

namespace MMIBundle\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'itemMenu' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.MMIBundle.Model.map
 */
class ItemmenuTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.MMIBundle.Model.map.ItemmenuTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('itemMenu');
        $this->setPhpName('Itemmenu');
        $this->setClassname('MMIBundle\\Model\\Itemmenu');
        $this->setPackage('src.MMIBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('page_id', 'PageId', 'INTEGER', 'page', 'id', true, null, null);
        $this->addColumn('order', 'Order', 'INTEGER', false, null, null);
        $this->addColumn('logState', 'Logstate', 'VARCHAR', false, 10, 'both');
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Page', 'MMIBundle\\Model\\Page', RelationMap::MANY_TO_ONE, array('page_id' => 'id', ), null, null);
        $this->addRelation('RoleMenu', 'MMIBundle\\Model\\RoleMenu', RelationMap::ONE_TO_MANY, array('id' => 'menu_id', ), null, null, 'RoleMenus');
        $this->addRelation('Role', 'MMIBundle\\Model\\Role', RelationMap::MANY_TO_MANY, array(), null, null, 'Roles');
    } // buildRelations()

} // ItemmenuTableMap
