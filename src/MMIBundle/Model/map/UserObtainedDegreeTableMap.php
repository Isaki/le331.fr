<?php

namespace MMIBundle\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'user_obtained_degree' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.MMIBundle.Model.map
 */
class UserObtainedDegreeTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.MMIBundle.Model.map.UserObtainedDegreeTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('user_obtained_degree');
        $this->setPhpName('UserObtainedDegree');
        $this->setClassname('MMIBundle\\Model\\UserObtainedDegree');
        $this->setPackage('src.MMIBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('user_id', 'UserId', 'INTEGER', 'user', 'id', true, null, null);
        $this->addForeignKey('degree_id', 'DegreeId', 'INTEGER', 'promo', 'id', true, null, null);
        $this->addColumn('year_from', 'YearFrom', 'INTEGER', false, 4, null);
        $this->addColumn('year_to', 'YearTo', 'INTEGER', false, 4, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('User', 'MMIBundle\\Model\\User', RelationMap::MANY_TO_ONE, array('user_id' => 'id', ), null, null);
        $this->addRelation('Promo', 'MMIBundle\\Model\\Promo', RelationMap::MANY_TO_ONE, array('degree_id' => 'id', ), null, null);
    } // buildRelations()

} // UserObtainedDegreeTableMap
