<?php

namespace MMIBundle\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'profession' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.MMIBundle.Model.map
 */
class ProfessionTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.MMIBundle.Model.map.ProfessionTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('profession');
        $this->setPhpName('Profession');
        $this->setClassname('MMIBundle\\Model\\Profession');
        $this->setPackage('src.MMIBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('name', 'Name', 'VARCHAR', false, 64, null);
        $this->addColumn('description', 'Description', 'LONGVARCHAR', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('User', 'MMIBundle\\Model\\User', RelationMap::ONE_TO_MANY, array('id' => 'profile_id', ), null, null, 'Users');
        $this->addRelation('Contact', 'MMIBundle\\Model\\Contact', RelationMap::ONE_TO_MANY, array('id' => 'profession_id', ), null, null, 'Contacts');
        $this->addRelation('UserJob', 'MMIBundle\\Model\\UserJob', RelationMap::ONE_TO_MANY, array('id' => 'profession_id', ), null, null, 'UserJobs');
        $this->addRelation('UserMemberUnipro', 'MMIBundle\\Model\\UserMemberUnipro', RelationMap::ONE_TO_MANY, array('id' => 'profession_id', ), null, null, 'UserMemberUnipros');
    } // buildRelations()

} // ProfessionTableMap
