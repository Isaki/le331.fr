<?php

namespace MMIBundle\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'address' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.MMIBundle.Model.map
 */
class AddressTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.MMIBundle.Model.map.AddressTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('address');
        $this->setPhpName('Address');
        $this->setClassname('MMIBundle\\Model\\Address');
        $this->setPackage('src.MMIBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('city_id', 'CityId', 'INTEGER', true, null, null);
        $this->addForeignKey('geoloc_id', 'GeolocId', 'INTEGER', 'geoloc', 'id', true, null, null);
        $this->addColumn('street', 'Street', 'VARCHAR', false, 64, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Geoloc', 'MMIBundle\\Model\\Geoloc', RelationMap::MANY_TO_ONE, array('geoloc_id' => 'id', ), null, null);
        $this->addRelation('Company', 'MMIBundle\\Model\\Company', RelationMap::ONE_TO_MANY, array('id' => 'address_id', ), null, null, 'Companies');
    } // buildRelations()

} // AddressTableMap
