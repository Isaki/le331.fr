<?php

namespace MMIBundle\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'promo' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.MMIBundle.Model.map
 */
class PromoTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.MMIBundle.Model.map.PromoTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('promo');
        $this->setPhpName('Promo');
        $this->setClassname('MMIBundle\\Model\\Promo');
        $this->setPackage('src.MMIBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('degree_id', 'DegreeId', 'INTEGER', 'degree', 'id', true, null, null);
        $this->addColumn('year', 'Year', 'INTEGER', true, 4, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Degree', 'MMIBundle\\Model\\Degree', RelationMap::MANY_TO_ONE, array('degree_id' => 'id', ), null, null);
        $this->addRelation('User', 'MMIBundle\\Model\\User', RelationMap::ONE_TO_MANY, array('id' => 'promo_id', ), null, null, 'Users');
        $this->addRelation('UserObtainedDegree', 'MMIBundle\\Model\\UserObtainedDegree', RelationMap::ONE_TO_MANY, array('id' => 'degree_id', ), null, null, 'UserObtainedDegrees');
    } // buildRelations()

} // PromoTableMap
