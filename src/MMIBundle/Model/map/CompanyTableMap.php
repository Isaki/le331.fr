<?php

namespace MMIBundle\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'company' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.MMIBundle.Model.map
 */
class CompanyTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.MMIBundle.Model.map.CompanyTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('company');
        $this->setPhpName('Company');
        $this->setClassname('MMIBundle\\Model\\Company');
        $this->setPackage('src.MMIBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('address_id', 'AddressId', 'INTEGER', 'address', 'id', true, null, null);
        $this->addForeignKey('contact_id', 'ContactId', 'INTEGER', 'contact', 'id', true, null, null);
        $this->addForeignKey('industry_id', 'IndustryId', 'INTEGER', 'industry', 'id', true, null, null);
        $this->addColumn('name', 'Name', 'VARCHAR', false, 32, null);
        $this->addColumn('siret', 'Siret', 'INTEGER', false, 32, null);
        $this->addColumn('password', 'Password', 'VARCHAR', false, 128, null);
        $this->addColumn('email', 'Email', 'VARCHAR', false, 64, null);
        $this->addColumn('telephone', 'Telephone', 'VARCHAR', false, 16, null);
        $this->addColumn('description', 'Description', 'LONGVARCHAR', false, null, null);
        $this->addColumn('workforce', 'Workforce', 'VARCHAR', false, 16, null);
        $this->addColumn('logo_file', 'LogoFile', 'VARCHAR', false, 64, null);
        $this->addColumn('url_website', 'UrlWebsite', 'VARCHAR', false, 64, null);
        $this->addColumn('first_connection', 'FirstConnection', 'BOOLEAN', false, 1, false);
        $this->addColumn('validated', 'Validated', 'BOOLEAN', false, 1, false);
        $this->addColumn('newsletter', 'Newsletter', 'BOOLEAN', false, 1, true);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Address', 'MMIBundle\\Model\\Address', RelationMap::MANY_TO_ONE, array('address_id' => 'id', ), null, null);
        $this->addRelation('Contact', 'MMIBundle\\Model\\Contact', RelationMap::MANY_TO_ONE, array('contact_id' => 'id', ), null, null);
        $this->addRelation('Industry', 'MMIBundle\\Model\\Industry', RelationMap::MANY_TO_ONE, array('industry_id' => 'id', ), null, null);
        $this->addRelation('Internship', 'MMIBundle\\Model\\Internship', RelationMap::ONE_TO_MANY, array('id' => 'company_id', ), null, null, 'Internships');
        $this->addRelation('UserJob', 'MMIBundle\\Model\\UserJob', RelationMap::ONE_TO_MANY, array('id' => 'company_id', ), null, null, 'UserJobs');
        $this->addRelation('Validation', 'MMIBundle\\Model\\Validation', RelationMap::ONE_TO_MANY, array('id' => 'company_id', ), null, null, 'Validations');
    } // buildRelations()

} // CompanyTableMap
