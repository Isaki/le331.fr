<?php

namespace MMIBundle\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'role_menu' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.MMIBundle.Model.map
 */
class RoleMenuTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.MMIBundle.Model.map.RoleMenuTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('role_menu');
        $this->setPhpName('RoleMenu');
        $this->setClassname('MMIBundle\\Model\\RoleMenu');
        $this->setPackage('src.MMIBundle.Model');
        $this->setUseIdGenerator(true);
        $this->setIsCrossRef(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('role_id', 'RoleId', 'INTEGER', 'role', 'id', true, null, null);
        $this->addForeignKey('menu_id', 'MenuId', 'INTEGER', 'itemMenu', 'id', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Role', 'MMIBundle\\Model\\Role', RelationMap::MANY_TO_ONE, array('role_id' => 'id', ), null, null);
        $this->addRelation('Itemmenu', 'MMIBundle\\Model\\Itemmenu', RelationMap::MANY_TO_ONE, array('menu_id' => 'id', ), null, null);
    } // buildRelations()

} // RoleMenuTableMap
