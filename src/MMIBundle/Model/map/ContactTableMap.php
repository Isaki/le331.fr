<?php

namespace MMIBundle\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'contact' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.MMIBundle.Model.map
 */
class ContactTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.MMIBundle.Model.map.ContactTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('contact');
        $this->setPhpName('Contact');
        $this->setClassname('MMIBundle\\Model\\Contact');
        $this->setPackage('src.MMIBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('firstname', 'Firstname', 'VARCHAR', false, 32, null);
        $this->addColumn('lastname', 'Lastname', 'VARCHAR', false, 32, null);
        $this->addForeignKey('profession_id', 'ProfessionId', 'VARCHAR', 'profession', 'id', false, 32, null);
        $this->addColumn('telephone', 'Telephone', 'VARCHAR', false, 32, null);
        $this->addColumn('email', 'Email', 'VARCHAR', false, 32, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Profession', 'MMIBundle\\Model\\Profession', RelationMap::MANY_TO_ONE, array('profession_id' => 'id', ), null, null);
        $this->addRelation('Company', 'MMIBundle\\Model\\Company', RelationMap::ONE_TO_MANY, array('id' => 'contact_id', ), null, null, 'Companies');
    } // buildRelations()

} // ContactTableMap
