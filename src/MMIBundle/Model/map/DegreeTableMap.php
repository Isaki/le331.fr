<?php

namespace MMIBundle\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'degree' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.MMIBundle.Model.map
 */
class DegreeTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.MMIBundle.Model.map.DegreeTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('degree');
        $this->setPhpName('Degree');
        $this->setClassname('MMIBundle\\Model\\Degree');
        $this->setPackage('src.MMIBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('name', 'Name', 'VARCHAR', false, 64, null);
        $this->addColumn('option', 'Option', 'VARCHAR', false, 64, null);
        $this->addColumn('summary', 'Summary', 'LONGVARCHAR', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Promo', 'MMIBundle\\Model\\Promo', RelationMap::ONE_TO_MANY, array('id' => 'degree_id', ), null, null, 'Promos');
    } // buildRelations()

} // DegreeTableMap
