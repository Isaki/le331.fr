<?php

namespace MMIBundle\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'internship' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.MMIBundle.Model.map
 */
class InternshipTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.MMIBundle.Model.map.InternshipTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('internship');
        $this->setPhpName('Internship');
        $this->setClassname('MMIBundle\\Model\\Internship');
        $this->setPackage('src.MMIBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('offer_id', 'OfferId', 'INTEGER', 'offerInternship', 'id', true, null, null);
        $this->addForeignKey('user_id', 'UserId', 'INTEGER', 'user', 'id', true, null, null);
        $this->addForeignKey('company_id', 'CompanyId', 'INTEGER', 'company', 'id', true, null, null);
        $this->addColumn('tutor_id', 'TutorId', 'INTEGER', true, null, null);
        $this->addColumn('work_function', 'WorkFunction', 'VARCHAR', false, 32, null);
        $this->addColumn('description', 'Description', 'LONGVARCHAR', false, null, null);
        $this->addColumn('information', 'Information', 'LONGVARCHAR', false, null, null);
        $this->addColumn('url_website_diffusion', 'UrlWebsiteDiffusion', 'VARCHAR', false, 64, null);
        $this->addColumn('signatory_lastname', 'SignatoryLastname', 'VARCHAR', false, 32, null);
        $this->addColumn('signatory_firstname', 'SignatoryFirstname', 'VARCHAR', false, 32, null);
        $this->addColumn('signatory_function', 'SignatoryFunction', 'VARCHAR', false, 64, null);
        $this->addColumn('master_lastname', 'MasterLastname', 'VARCHAR', false, 32, null);
        $this->addColumn('master_firstname', 'MasterFirstname', 'VARCHAR', false, 32, null);
        $this->addColumn('master_email', 'MasterEmail', 'VARCHAR', false, 64, null);
        $this->addColumn('master_telephone', 'MasterTelephone', 'VARCHAR', false, 32, null);
        $this->addColumn('valide', 'Valide', 'SMALLINT', false, null, null);
        $this->addColumn('etabli', 'Etabli', 'SMALLINT', false, null, null);
        $this->addColumn('date_visite', 'DateVisite', 'DATE', false, null, null);
        $this->addColumn('date_soutenance', 'DateSoutenance', 'DATE', false, null, null);
        $this->addColumn('internship_begining', 'InternshipBegining', 'DATE', false, null, null);
        $this->addColumn('internship_ending', 'InternshipEnding', 'DATE', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('OfferinternshipRelatedByOfferId', 'MMIBundle\\Model\\Offerinternship', RelationMap::MANY_TO_ONE, array('offer_id' => 'id', ), null, null);
        $this->addRelation('User', 'MMIBundle\\Model\\User', RelationMap::MANY_TO_ONE, array('user_id' => 'id', ), null, null);
        $this->addRelation('Company', 'MMIBundle\\Model\\Company', RelationMap::MANY_TO_ONE, array('company_id' => 'id', ), null, null);
        $this->addRelation('OfferinternshipRelatedByInternshipId', 'MMIBundle\\Model\\Offerinternship', RelationMap::ONE_TO_MANY, array('id' => 'internship_id', ), null, null, 'OfferinternshipsRelatedByInternshipId');
        $this->addRelation('Diaryweek', 'MMIBundle\\Model\\Diaryweek', RelationMap::ONE_TO_MANY, array('id' => 'internship_id', ), null, null, 'Diaryweeks');
    } // buildRelations()

} // InternshipTableMap
