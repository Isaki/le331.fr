<?php

namespace MMIBundle\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'geoloc' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.MMIBundle.Model.map
 */
class GeolocTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.MMIBundle.Model.map.GeolocTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('geoloc');
        $this->setPhpName('Geoloc');
        $this->setClassname('MMIBundle\\Model\\Geoloc');
        $this->setPackage('src.MMIBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('latitude', 'Latitude', 'FLOAT', false, null, null);
        $this->addColumn('longitude', 'Longitude', 'FLOAT', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Address', 'MMIBundle\\Model\\Address', RelationMap::ONE_TO_MANY, array('id' => 'geoloc_id', ), null, null, 'Addresses');
        $this->addRelation('City', 'MMIBundle\\Model\\City', RelationMap::ONE_TO_MANY, array('id' => 'geoloc_id', ), null, null, 'Cities');
    } // buildRelations()

} // GeolocTableMap
