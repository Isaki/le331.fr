<?php

namespace MMIBundle\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'page' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.MMIBundle.Model.map
 */
class PageTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.MMIBundle.Model.map.PageTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('page');
        $this->setPhpName('Page');
        $this->setClassname('MMIBundle\\Model\\Page');
        $this->setPackage('src.MMIBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('name', 'Name', 'VARCHAR', false, 32, null);
        $this->addColumn('route', 'Route', 'VARCHAR', false, 32, null);
        $this->addColumn('isMenuBlack', 'Ismenublack', 'BOOLEAN', false, 1, false);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Itemmenu', 'MMIBundle\\Model\\Itemmenu', RelationMap::ONE_TO_MANY, array('id' => 'page_id', ), null, null, 'Itemmenus');
        $this->addRelation('RolePage', 'MMIBundle\\Model\\RolePage', RelationMap::ONE_TO_MANY, array('id' => 'page_id', ), null, null, 'RolePages');
        $this->addRelation('Role', 'MMIBundle\\Model\\Role', RelationMap::MANY_TO_MANY, array(), null, null, 'Roles');
    } // buildRelations()

} // PageTableMap
