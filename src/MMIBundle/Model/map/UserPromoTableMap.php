<?php

namespace MMIBundle\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'user_promo' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.src.MMIBundle.Model.map
 */
class UserPromoTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.MMIBundle.Model.map.UserPromoTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('user_promo');
        $this->setPhpName('UserPromo');
        $this->setClassname('MMIBundle\\Model\\UserPromo');
        $this->setPackage('src.MMIBundle.Model');
        $this->setUseIdGenerator(false);
        $this->setIsCrossRef(true);
        // columns
        $this->addForeignPrimaryKey('id_user', 'IdUser', 'INTEGER' , 'user', 'id', true, null, null);
        $this->addForeignPrimaryKey('id_promo', 'IdPromo', 'INTEGER' , 'promo', 'id', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('User', 'MMIBundle\\Model\\User', RelationMap::MANY_TO_ONE, array('id_user' => 'id', ), null, null);
        $this->addRelation('Promo', 'MMIBundle\\Model\\Promo', RelationMap::MANY_TO_ONE, array('id_promo' => 'id', ), null, null);
    } // buildRelations()

} // UserPromoTableMap
