<?php

namespace MMIBundle\Model;

use MMIBundle\Model\om\BaseCompanyPeer;

class CompanyPeer extends BaseCompanyPeer
{

    public static function getOneByName($name)
    {
        return CompanyQuery::create()
            ->filterByName($name)
            ->findOne();
    }

    public static function getOneById($id)
    {
        return CompanyQuery::create()
            ->filterById($id)
            ->findOne();
    }

    public static function companyExists($email)
    {
        return CompanyQuery::create()
            ->filterByEmail($email)
            ->count() > 0;
    }

    public static function getByEmail($email)
    {
        return CompanyQuery::create()
            ->filterByEmail($email)
            ->findOne();
    }

    public static function isValidEmailPassword(Company $company, $pass)
    {
        return $company->getPassword() === $pass;
    }

    public static function getMenuItems()
    {
        return ItemmenuQuery::create()
            ->filterByLogstate(['login', 'both'])
            ->orderByOrder('ASC')
            ->useRoleMenuQuery()
            ->useRoleQuery()
            ->filterById([1, 5])
            ->endUse()
            ->endUse()
            ->find();
    }

    public static function getPages()
    {
        $pagesQuery = PageQuery::create()
            ->useRolePageQuery()
            ->useRoleQuery()
            ->filterById([1, 5])
            ->endUse()
            ->endUse()
            ->find();
        $pages = [];
        foreach ($pagesQuery as $page) $pages[$page->getRoute()] = $page->getRoute();

        return $pages;
    }
}
