<?php

namespace MMIBundle\Model;

use Doctrine\Common\Collections\Criteria;
use MMIBundle\Model\om\BaseDegreePeer;

class DegreePeer extends BaseDegreePeer
{
    public static function getOneLike($search)
    {
        return DegreeQuery::create()
            ->orderByName(Criteria::ASC)
            ->filterByName(trim(explode('option', $search)[0]))
            ->findOne();
    }
}
