<?php

namespace MMIBundle\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use MMIBundle\Model\Itemmenu;
use MMIBundle\Model\ItemmenuQuery;
use MMIBundle\Model\Page;
use MMIBundle\Model\PagePeer;
use MMIBundle\Model\PageQuery;
use MMIBundle\Model\Role;
use MMIBundle\Model\RolePage;
use MMIBundle\Model\RolePageQuery;
use MMIBundle\Model\RoleQuery;

abstract class BasePage extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'MMIBundle\\Model\\PagePeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        PagePeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the name field.
     * @var        string
     */
    protected $name;

    /**
     * The value for the route field.
     * @var        string
     */
    protected $route;

    /**
     * The value for the ismenublack field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $ismenublack;

    /**
     * @var        PropelObjectCollection|Itemmenu[] Collection to store aggregation of Itemmenu objects.
     */
    protected $collItemmenus;
    protected $collItemmenusPartial;

    /**
     * @var        PropelObjectCollection|RolePage[] Collection to store aggregation of RolePage objects.
     */
    protected $collRolePages;
    protected $collRolePagesPartial;

    /**
     * @var        PropelObjectCollection|Role[] Collection to store aggregation of Role objects.
     */
    protected $collRoles;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rolesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $itemmenusScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rolePagesScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->ismenublack = false;
    }

    /**
     * Initializes internal state of BasePage object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [name] column value.
     *
     * @return string
     */
    public function getName()
    {

        return $this->name;
    }

    /**
     * Get the [route] column value.
     *
     * @return string
     */
    public function getRoute()
    {

        return $this->route;
    }

    /**
     * Get the [ismenublack] column value.
     *
     * @return boolean
     */
    public function getIsmenublack()
    {

        return $this->ismenublack;
    }

    /**
     * Set the value of [id] column.
     *
     * @param  int $v new value
     * @return Page The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = PagePeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [name] column.
     *
     * @param  string $v new value
     * @return Page The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->name !== $v) {
            $this->name = $v;
            $this->modifiedColumns[] = PagePeer::NAME;
        }


        return $this;
    } // setName()

    /**
     * Set the value of [route] column.
     *
     * @param  string $v new value
     * @return Page The current object (for fluent API support)
     */
    public function setRoute($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->route !== $v) {
            $this->route = $v;
            $this->modifiedColumns[] = PagePeer::ROUTE;
        }


        return $this;
    } // setRoute()

    /**
     * Sets the value of the [ismenublack] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return Page The current object (for fluent API support)
     */
    public function setIsmenublack($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->ismenublack !== $v) {
            $this->ismenublack = $v;
            $this->modifiedColumns[] = PagePeer::ISMENUBLACK;
        }


        return $this;
    } // setIsmenublack()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->ismenublack !== false) {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->name = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->route = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->ismenublack = ($row[$startcol + 3] !== null) ? (boolean) $row[$startcol + 3] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 4; // 4 = PagePeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Page object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PagePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = PagePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collItemmenus = null;

            $this->collRolePages = null;

            $this->collRoles = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PagePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = PageQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(PagePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                PagePeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->rolesScheduledForDeletion !== null) {
                if (!$this->rolesScheduledForDeletion->isEmpty()) {
                    $pks = array();
                    $pk = $this->getPrimaryKey();
                    foreach ($this->rolesScheduledForDeletion->getPrimaryKeys(false) as $remotePk) {
                        $pks[] = array($remotePk, $pk);
                    }
                    RolePageQuery::create()
                        ->filterByPrimaryKeys($pks)
                        ->delete($con);
                    $this->rolesScheduledForDeletion = null;
                }

                foreach ($this->getRoles() as $role) {
                    if ($role->isModified()) {
                        $role->save($con);
                    }
                }
            } elseif ($this->collRoles) {
                foreach ($this->collRoles as $role) {
                    if ($role->isModified()) {
                        $role->save($con);
                    }
                }
            }

            if ($this->itemmenusScheduledForDeletion !== null) {
                if (!$this->itemmenusScheduledForDeletion->isEmpty()) {
                    ItemmenuQuery::create()
                        ->filterByPrimaryKeys($this->itemmenusScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->itemmenusScheduledForDeletion = null;
                }
            }

            if ($this->collItemmenus !== null) {
                foreach ($this->collItemmenus as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rolePagesScheduledForDeletion !== null) {
                if (!$this->rolePagesScheduledForDeletion->isEmpty()) {
                    RolePageQuery::create()
                        ->filterByPrimaryKeys($this->rolePagesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rolePagesScheduledForDeletion = null;
                }
            }

            if ($this->collRolePages !== null) {
                foreach ($this->collRolePages as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = PagePeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . PagePeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(PagePeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(PagePeer::NAME)) {
            $modifiedColumns[':p' . $index++]  = '`name`';
        }
        if ($this->isColumnModified(PagePeer::ROUTE)) {
            $modifiedColumns[':p' . $index++]  = '`route`';
        }
        if ($this->isColumnModified(PagePeer::ISMENUBLACK)) {
            $modifiedColumns[':p' . $index++]  = '`isMenuBlack`';
        }

        $sql = sprintf(
            'INSERT INTO `page` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`name`':
                        $stmt->bindValue($identifier, $this->name, PDO::PARAM_STR);
                        break;
                    case '`route`':
                        $stmt->bindValue($identifier, $this->route, PDO::PARAM_STR);
                        break;
                    case '`isMenuBlack`':
                        $stmt->bindValue($identifier, (int) $this->ismenublack, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = PagePeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collItemmenus !== null) {
                    foreach ($this->collItemmenus as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRolePages !== null) {
                    foreach ($this->collRolePages as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = PagePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getName();
                break;
            case 2:
                return $this->getRoute();
                break;
            case 3:
                return $this->getIsmenublack();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Page'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Page'][$this->getPrimaryKey()] = true;
        $keys = PagePeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getName(),
            $keys[2] => $this->getRoute(),
            $keys[3] => $this->getIsmenublack(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collItemmenus) {
                $result['Itemmenus'] = $this->collItemmenus->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRolePages) {
                $result['RolePages'] = $this->collRolePages->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = PagePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setName($value);
                break;
            case 2:
                $this->setRoute($value);
                break;
            case 3:
                $this->setIsmenublack($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = PagePeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setName($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setRoute($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setIsmenublack($arr[$keys[3]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(PagePeer::DATABASE_NAME);

        if ($this->isColumnModified(PagePeer::ID)) $criteria->add(PagePeer::ID, $this->id);
        if ($this->isColumnModified(PagePeer::NAME)) $criteria->add(PagePeer::NAME, $this->name);
        if ($this->isColumnModified(PagePeer::ROUTE)) $criteria->add(PagePeer::ROUTE, $this->route);
        if ($this->isColumnModified(PagePeer::ISMENUBLACK)) $criteria->add(PagePeer::ISMENUBLACK, $this->ismenublack);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(PagePeer::DATABASE_NAME);
        $criteria->add(PagePeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Page (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setName($this->getName());
        $copyObj->setRoute($this->getRoute());
        $copyObj->setIsmenublack($this->getIsmenublack());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getItemmenus() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addItemmenu($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRolePages() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRolePage($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Page Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return PagePeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new PagePeer();
        }

        return self::$peer;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Itemmenu' == $relationName) {
            $this->initItemmenus();
        }
        if ('RolePage' == $relationName) {
            $this->initRolePages();
        }
    }

    /**
     * Clears out the collItemmenus collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Page The current object (for fluent API support)
     * @see        addItemmenus()
     */
    public function clearItemmenus()
    {
        $this->collItemmenus = null; // important to set this to null since that means it is uninitialized
        $this->collItemmenusPartial = null;

        return $this;
    }

    /**
     * reset is the collItemmenus collection loaded partially
     *
     * @return void
     */
    public function resetPartialItemmenus($v = true)
    {
        $this->collItemmenusPartial = $v;
    }

    /**
     * Initializes the collItemmenus collection.
     *
     * By default this just sets the collItemmenus collection to an empty array (like clearcollItemmenus());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initItemmenus($overrideExisting = true)
    {
        if (null !== $this->collItemmenus && !$overrideExisting) {
            return;
        }
        $this->collItemmenus = new PropelObjectCollection();
        $this->collItemmenus->setModel('Itemmenu');
    }

    /**
     * Gets an array of Itemmenu objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Page is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Itemmenu[] List of Itemmenu objects
     * @throws PropelException
     */
    public function getItemmenus($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collItemmenusPartial && !$this->isNew();
        if (null === $this->collItemmenus || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collItemmenus) {
                // return empty collection
                $this->initItemmenus();
            } else {
                $collItemmenus = ItemmenuQuery::create(null, $criteria)
                    ->filterByPage($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collItemmenusPartial && count($collItemmenus)) {
                      $this->initItemmenus(false);

                      foreach ($collItemmenus as $obj) {
                        if (false == $this->collItemmenus->contains($obj)) {
                          $this->collItemmenus->append($obj);
                        }
                      }

                      $this->collItemmenusPartial = true;
                    }

                    $collItemmenus->getInternalIterator()->rewind();

                    return $collItemmenus;
                }

                if ($partial && $this->collItemmenus) {
                    foreach ($this->collItemmenus as $obj) {
                        if ($obj->isNew()) {
                            $collItemmenus[] = $obj;
                        }
                    }
                }

                $this->collItemmenus = $collItemmenus;
                $this->collItemmenusPartial = false;
            }
        }

        return $this->collItemmenus;
    }

    /**
     * Sets a collection of Itemmenu objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $itemmenus A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Page The current object (for fluent API support)
     */
    public function setItemmenus(PropelCollection $itemmenus, PropelPDO $con = null)
    {
        $itemmenusToDelete = $this->getItemmenus(new Criteria(), $con)->diff($itemmenus);


        $this->itemmenusScheduledForDeletion = $itemmenusToDelete;

        foreach ($itemmenusToDelete as $itemmenuRemoved) {
            $itemmenuRemoved->setPage(null);
        }

        $this->collItemmenus = null;
        foreach ($itemmenus as $itemmenu) {
            $this->addItemmenu($itemmenu);
        }

        $this->collItemmenus = $itemmenus;
        $this->collItemmenusPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Itemmenu objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Itemmenu objects.
     * @throws PropelException
     */
    public function countItemmenus(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collItemmenusPartial && !$this->isNew();
        if (null === $this->collItemmenus || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collItemmenus) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getItemmenus());
            }
            $query = ItemmenuQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPage($this)
                ->count($con);
        }

        return count($this->collItemmenus);
    }

    /**
     * Method called to associate a Itemmenu object to this object
     * through the Itemmenu foreign key attribute.
     *
     * @param    Itemmenu $l Itemmenu
     * @return Page The current object (for fluent API support)
     */
    public function addItemmenu(Itemmenu $l)
    {
        if ($this->collItemmenus === null) {
            $this->initItemmenus();
            $this->collItemmenusPartial = true;
        }

        if (!in_array($l, $this->collItemmenus->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddItemmenu($l);

            if ($this->itemmenusScheduledForDeletion and $this->itemmenusScheduledForDeletion->contains($l)) {
                $this->itemmenusScheduledForDeletion->remove($this->itemmenusScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	Itemmenu $itemmenu The itemmenu object to add.
     */
    protected function doAddItemmenu($itemmenu)
    {
        $this->collItemmenus[]= $itemmenu;
        $itemmenu->setPage($this);
    }

    /**
     * @param	Itemmenu $itemmenu The itemmenu object to remove.
     * @return Page The current object (for fluent API support)
     */
    public function removeItemmenu($itemmenu)
    {
        if ($this->getItemmenus()->contains($itemmenu)) {
            $this->collItemmenus->remove($this->collItemmenus->search($itemmenu));
            if (null === $this->itemmenusScheduledForDeletion) {
                $this->itemmenusScheduledForDeletion = clone $this->collItemmenus;
                $this->itemmenusScheduledForDeletion->clear();
            }
            $this->itemmenusScheduledForDeletion[]= clone $itemmenu;
            $itemmenu->setPage(null);
        }

        return $this;
    }

    /**
     * Clears out the collRolePages collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Page The current object (for fluent API support)
     * @see        addRolePages()
     */
    public function clearRolePages()
    {
        $this->collRolePages = null; // important to set this to null since that means it is uninitialized
        $this->collRolePagesPartial = null;

        return $this;
    }

    /**
     * reset is the collRolePages collection loaded partially
     *
     * @return void
     */
    public function resetPartialRolePages($v = true)
    {
        $this->collRolePagesPartial = $v;
    }

    /**
     * Initializes the collRolePages collection.
     *
     * By default this just sets the collRolePages collection to an empty array (like clearcollRolePages());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRolePages($overrideExisting = true)
    {
        if (null !== $this->collRolePages && !$overrideExisting) {
            return;
        }
        $this->collRolePages = new PropelObjectCollection();
        $this->collRolePages->setModel('RolePage');
    }

    /**
     * Gets an array of RolePage objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Page is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RolePage[] List of RolePage objects
     * @throws PropelException
     */
    public function getRolePages($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRolePagesPartial && !$this->isNew();
        if (null === $this->collRolePages || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRolePages) {
                // return empty collection
                $this->initRolePages();
            } else {
                $collRolePages = RolePageQuery::create(null, $criteria)
                    ->filterByPage($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRolePagesPartial && count($collRolePages)) {
                      $this->initRolePages(false);

                      foreach ($collRolePages as $obj) {
                        if (false == $this->collRolePages->contains($obj)) {
                          $this->collRolePages->append($obj);
                        }
                      }

                      $this->collRolePagesPartial = true;
                    }

                    $collRolePages->getInternalIterator()->rewind();

                    return $collRolePages;
                }

                if ($partial && $this->collRolePages) {
                    foreach ($this->collRolePages as $obj) {
                        if ($obj->isNew()) {
                            $collRolePages[] = $obj;
                        }
                    }
                }

                $this->collRolePages = $collRolePages;
                $this->collRolePagesPartial = false;
            }
        }

        return $this->collRolePages;
    }

    /**
     * Sets a collection of RolePage objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rolePages A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Page The current object (for fluent API support)
     */
    public function setRolePages(PropelCollection $rolePages, PropelPDO $con = null)
    {
        $rolePagesToDelete = $this->getRolePages(new Criteria(), $con)->diff($rolePages);


        $this->rolePagesScheduledForDeletion = $rolePagesToDelete;

        foreach ($rolePagesToDelete as $rolePageRemoved) {
            $rolePageRemoved->setPage(null);
        }

        $this->collRolePages = null;
        foreach ($rolePages as $rolePage) {
            $this->addRolePage($rolePage);
        }

        $this->collRolePages = $rolePages;
        $this->collRolePagesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RolePage objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RolePage objects.
     * @throws PropelException
     */
    public function countRolePages(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRolePagesPartial && !$this->isNew();
        if (null === $this->collRolePages || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRolePages) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getRolePages());
            }
            $query = RolePageQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPage($this)
                ->count($con);
        }

        return count($this->collRolePages);
    }

    /**
     * Method called to associate a RolePage object to this object
     * through the RolePage foreign key attribute.
     *
     * @param    RolePage $l RolePage
     * @return Page The current object (for fluent API support)
     */
    public function addRolePage(RolePage $l)
    {
        if ($this->collRolePages === null) {
            $this->initRolePages();
            $this->collRolePagesPartial = true;
        }

        if (!in_array($l, $this->collRolePages->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRolePage($l);

            if ($this->rolePagesScheduledForDeletion and $this->rolePagesScheduledForDeletion->contains($l)) {
                $this->rolePagesScheduledForDeletion->remove($this->rolePagesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	RolePage $rolePage The rolePage object to add.
     */
    protected function doAddRolePage($rolePage)
    {
        $this->collRolePages[]= $rolePage;
        $rolePage->setPage($this);
    }

    /**
     * @param	RolePage $rolePage The rolePage object to remove.
     * @return Page The current object (for fluent API support)
     */
    public function removeRolePage($rolePage)
    {
        if ($this->getRolePages()->contains($rolePage)) {
            $this->collRolePages->remove($this->collRolePages->search($rolePage));
            if (null === $this->rolePagesScheduledForDeletion) {
                $this->rolePagesScheduledForDeletion = clone $this->collRolePages;
                $this->rolePagesScheduledForDeletion->clear();
            }
            $this->rolePagesScheduledForDeletion[]= clone $rolePage;
            $rolePage->setPage(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Page is new, it will return
     * an empty collection; or if this Page has previously
     * been saved, it will retrieve related RolePages from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Page.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RolePage[] List of RolePage objects
     */
    public function getRolePagesJoinRole($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RolePageQuery::create(null, $criteria);
        $query->joinWith('Role', $join_behavior);

        return $this->getRolePages($query, $con);
    }

    /**
     * Clears out the collRoles collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Page The current object (for fluent API support)
     * @see        addRoles()
     */
    public function clearRoles()
    {
        $this->collRoles = null; // important to set this to null since that means it is uninitialized
        $this->collRolesPartial = null;

        return $this;
    }

    /**
     * Initializes the collRoles collection.
     *
     * By default this just sets the collRoles collection to an empty collection (like clearRoles());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @return void
     */
    public function initRoles()
    {
        $this->collRoles = new PropelObjectCollection();
        $this->collRoles->setModel('Role');
    }

    /**
     * Gets a collection of Role objects related by a many-to-many relationship
     * to the current object by way of the role_page cross-reference table.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Page is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria Optional query object to filter the query
     * @param PropelPDO $con Optional connection object
     *
     * @return PropelObjectCollection|Role[] List of Role objects
     */
    public function getRoles($criteria = null, PropelPDO $con = null)
    {
        if (null === $this->collRoles || null !== $criteria) {
            if ($this->isNew() && null === $this->collRoles) {
                // return empty collection
                $this->initRoles();
            } else {
                $collRoles = RoleQuery::create(null, $criteria)
                    ->filterByPage($this)
                    ->find($con);
                if (null !== $criteria) {
                    return $collRoles;
                }
                $this->collRoles = $collRoles;
            }
        }

        return $this->collRoles;
    }

    /**
     * Sets a collection of Role objects related by a many-to-many relationship
     * to the current object by way of the role_page cross-reference table.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $roles A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Page The current object (for fluent API support)
     */
    public function setRoles(PropelCollection $roles, PropelPDO $con = null)
    {
        $this->clearRoles();
        $currentRoles = $this->getRoles(null, $con);

        $this->rolesScheduledForDeletion = $currentRoles->diff($roles);

        foreach ($roles as $role) {
            if (!$currentRoles->contains($role)) {
                $this->doAddRole($role);
            }
        }

        $this->collRoles = $roles;

        return $this;
    }

    /**
     * Gets the number of Role objects related by a many-to-many relationship
     * to the current object by way of the role_page cross-reference table.
     *
     * @param Criteria $criteria Optional query object to filter the query
     * @param boolean $distinct Set to true to force count distinct
     * @param PropelPDO $con Optional connection object
     *
     * @return int the number of related Role objects
     */
    public function countRoles($criteria = null, $distinct = false, PropelPDO $con = null)
    {
        if (null === $this->collRoles || null !== $criteria) {
            if ($this->isNew() && null === $this->collRoles) {
                return 0;
            } else {
                $query = RoleQuery::create(null, $criteria);
                if ($distinct) {
                    $query->distinct();
                }

                return $query
                    ->filterByPage($this)
                    ->count($con);
            }
        } else {
            return count($this->collRoles);
        }
    }

    /**
     * Associate a Role object to this object
     * through the role_page cross reference table.
     *
     * @param  Role $role The RolePage object to relate
     * @return Page The current object (for fluent API support)
     */
    public function addRole(Role $role)
    {
        if ($this->collRoles === null) {
            $this->initRoles();
        }

        if (!$this->collRoles->contains($role)) { // only add it if the **same** object is not already associated
            $this->doAddRole($role);
            $this->collRoles[] = $role;

            if ($this->rolesScheduledForDeletion and $this->rolesScheduledForDeletion->contains($role)) {
                $this->rolesScheduledForDeletion->remove($this->rolesScheduledForDeletion->search($role));
            }
        }

        return $this;
    }

    /**
     * @param	Role $role The role object to add.
     */
    protected function doAddRole(Role $role)
    {
        // set the back reference to this object directly as using provided method either results
        // in endless loop or in multiple relations
        if (!$role->getPages()->contains($this)) { $rolePage = new RolePage();
            $rolePage->setRole($role);
            $this->addRolePage($rolePage);

            $foreignCollection = $role->getPages();
            $foreignCollection[] = $this;
        }
    }

    /**
     * Remove a Role object to this object
     * through the role_page cross reference table.
     *
     * @param Role $role The RolePage object to relate
     * @return Page The current object (for fluent API support)
     */
    public function removeRole(Role $role)
    {
        if ($this->getRoles()->contains($role)) {
            $this->collRoles->remove($this->collRoles->search($role));
            if (null === $this->rolesScheduledForDeletion) {
                $this->rolesScheduledForDeletion = clone $this->collRoles;
                $this->rolesScheduledForDeletion->clear();
            }
            $this->rolesScheduledForDeletion[]= $role;
        }

        return $this;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->name = null;
        $this->route = null;
        $this->ismenublack = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collItemmenus) {
                foreach ($this->collItemmenus as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRolePages) {
                foreach ($this->collRolePages as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRoles) {
                foreach ($this->collRoles as $o) {
                    $o->clearAllReferences($deep);
                }
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collItemmenus instanceof PropelCollection) {
            $this->collItemmenus->clearIterator();
        }
        $this->collItemmenus = null;
        if ($this->collRolePages instanceof PropelCollection) {
            $this->collRolePages->clearIterator();
        }
        $this->collRolePages = null;
        if ($this->collRoles instanceof PropelCollection) {
            $this->collRoles->clearIterator();
        }
        $this->collRoles = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(PagePeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
