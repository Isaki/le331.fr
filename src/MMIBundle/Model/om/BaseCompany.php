<?php

namespace MMIBundle\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use MMIBundle\Model\Address;
use MMIBundle\Model\AddressQuery;
use MMIBundle\Model\Company;
use MMIBundle\Model\CompanyPeer;
use MMIBundle\Model\CompanyQuery;
use MMIBundle\Model\Contact;
use MMIBundle\Model\ContactQuery;
use MMIBundle\Model\Industry;
use MMIBundle\Model\IndustryQuery;
use MMIBundle\Model\Internship;
use MMIBundle\Model\InternshipQuery;
use MMIBundle\Model\UserJob;
use MMIBundle\Model\UserJobQuery;
use MMIBundle\Model\Validation;
use MMIBundle\Model\ValidationQuery;

abstract class BaseCompany extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'MMIBundle\\Model\\CompanyPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CompanyPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the address_id field.
     * @var        int
     */
    protected $address_id;

    /**
     * The value for the contact_id field.
     * @var        int
     */
    protected $contact_id;

    /**
     * The value for the industry_id field.
     * @var        int
     */
    protected $industry_id;

    /**
     * The value for the name field.
     * @var        string
     */
    protected $name;

    /**
     * The value for the siret field.
     * @var        int
     */
    protected $siret;

    /**
     * The value for the password field.
     * @var        string
     */
    protected $password;

    /**
     * The value for the email field.
     * @var        string
     */
    protected $email;

    /**
     * The value for the telephone field.
     * @var        string
     */
    protected $telephone;

    /**
     * The value for the description field.
     * @var        string
     */
    protected $description;

    /**
     * The value for the workforce field.
     * @var        string
     */
    protected $workforce;

    /**
     * The value for the logo_file field.
     * @var        string
     */
    protected $logo_file;

    /**
     * The value for the url_website field.
     * @var        string
     */
    protected $url_website;

    /**
     * The value for the first_connection field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $first_connection;

    /**
     * The value for the validated field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $validated;

    /**
     * The value for the newsletter field.
     * Note: this column has a database default value of: true
     * @var        boolean
     */
    protected $newsletter;

    /**
     * @var        Address
     */
    protected $aAddress;

    /**
     * @var        Contact
     */
    protected $aContact;

    /**
     * @var        Industry
     */
    protected $aIndustry;

    /**
     * @var        PropelObjectCollection|Internship[] Collection to store aggregation of Internship objects.
     */
    protected $collInternships;
    protected $collInternshipsPartial;

    /**
     * @var        PropelObjectCollection|UserJob[] Collection to store aggregation of UserJob objects.
     */
    protected $collUserJobs;
    protected $collUserJobsPartial;

    /**
     * @var        PropelObjectCollection|Validation[] Collection to store aggregation of Validation objects.
     */
    protected $collValidations;
    protected $collValidationsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $internshipsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $userJobsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $validationsScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->first_connection = false;
        $this->validated = false;
        $this->newsletter = true;
    }

    /**
     * Initializes internal state of BaseCompany object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [address_id] column value.
     *
     * @return int
     */
    public function getAddressId()
    {

        return $this->address_id;
    }

    /**
     * Get the [contact_id] column value.
     *
     * @return int
     */
    public function getContactId()
    {

        return $this->contact_id;
    }

    /**
     * Get the [industry_id] column value.
     *
     * @return int
     */
    public function getIndustryId()
    {

        return $this->industry_id;
    }

    /**
     * Get the [name] column value.
     *
     * @return string
     */
    public function getName()
    {

        return $this->name;
    }

    /**
     * Get the [siret] column value.
     *
     * @return int
     */
    public function getSiret()
    {

        return $this->siret;
    }

    /**
     * Get the [password] column value.
     *
     * @return string
     */
    public function getPassword()
    {

        return $this->password;
    }

    /**
     * Get the [email] column value.
     *
     * @return string
     */
    public function getEmail()
    {

        return $this->email;
    }

    /**
     * Get the [telephone] column value.
     *
     * @return string
     */
    public function getTelephone()
    {

        return $this->telephone;
    }

    /**
     * Get the [description] column value.
     *
     * @return string
     */
    public function getDescription()
    {

        return $this->description;
    }

    /**
     * Get the [workforce] column value.
     *
     * @return string
     */
    public function getWorkforce()
    {

        return $this->workforce;
    }

    /**
     * Get the [logo_file] column value.
     *
     * @return string
     */
    public function getLogoFile()
    {

        return $this->logo_file;
    }

    /**
     * Get the [url_website] column value.
     *
     * @return string
     */
    public function getUrlWebsite()
    {

        return $this->url_website;
    }

    /**
     * Get the [first_connection] column value.
     *
     * @return boolean
     */
    public function getFirstConnection()
    {

        return $this->first_connection;
    }

    /**
     * Get the [validated] column value.
     *
     * @return boolean
     */
    public function getValidated()
    {

        return $this->validated;
    }

    /**
     * Get the [newsletter] column value.
     *
     * @return boolean
     */
    public function getNewsletter()
    {

        return $this->newsletter;
    }

    /**
     * Set the value of [id] column.
     *
     * @param  int $v new value
     * @return Company The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = CompanyPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [address_id] column.
     *
     * @param  int $v new value
     * @return Company The current object (for fluent API support)
     */
    public function setAddressId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->address_id !== $v) {
            $this->address_id = $v;
            $this->modifiedColumns[] = CompanyPeer::ADDRESS_ID;
        }

        if ($this->aAddress !== null && $this->aAddress->getId() !== $v) {
            $this->aAddress = null;
        }


        return $this;
    } // setAddressId()

    /**
     * Set the value of [contact_id] column.
     *
     * @param  int $v new value
     * @return Company The current object (for fluent API support)
     */
    public function setContactId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->contact_id !== $v) {
            $this->contact_id = $v;
            $this->modifiedColumns[] = CompanyPeer::CONTACT_ID;
        }

        if ($this->aContact !== null && $this->aContact->getId() !== $v) {
            $this->aContact = null;
        }


        return $this;
    } // setContactId()

    /**
     * Set the value of [industry_id] column.
     *
     * @param  int $v new value
     * @return Company The current object (for fluent API support)
     */
    public function setIndustryId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->industry_id !== $v) {
            $this->industry_id = $v;
            $this->modifiedColumns[] = CompanyPeer::INDUSTRY_ID;
        }

        if ($this->aIndustry !== null && $this->aIndustry->getId() !== $v) {
            $this->aIndustry = null;
        }


        return $this;
    } // setIndustryId()

    /**
     * Set the value of [name] column.
     *
     * @param  string $v new value
     * @return Company The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->name !== $v) {
            $this->name = $v;
            $this->modifiedColumns[] = CompanyPeer::NAME;
        }


        return $this;
    } // setName()

    /**
     * Set the value of [siret] column.
     *
     * @param  int $v new value
     * @return Company The current object (for fluent API support)
     */
    public function setSiret($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->siret !== $v) {
            $this->siret = $v;
            $this->modifiedColumns[] = CompanyPeer::SIRET;
        }


        return $this;
    } // setSiret()

    /**
     * Set the value of [password] column.
     *
     * @param  string $v new value
     * @return Company The current object (for fluent API support)
     */
    public function setPassword($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->password !== $v) {
            $this->password = $v;
            $this->modifiedColumns[] = CompanyPeer::PASSWORD;
        }


        return $this;
    } // setPassword()

    /**
     * Set the value of [email] column.
     *
     * @param  string $v new value
     * @return Company The current object (for fluent API support)
     */
    public function setEmail($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->email !== $v) {
            $this->email = $v;
            $this->modifiedColumns[] = CompanyPeer::EMAIL;
        }


        return $this;
    } // setEmail()

    /**
     * Set the value of [telephone] column.
     *
     * @param  string $v new value
     * @return Company The current object (for fluent API support)
     */
    public function setTelephone($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->telephone !== $v) {
            $this->telephone = $v;
            $this->modifiedColumns[] = CompanyPeer::TELEPHONE;
        }


        return $this;
    } // setTelephone()

    /**
     * Set the value of [description] column.
     *
     * @param  string $v new value
     * @return Company The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->description !== $v) {
            $this->description = $v;
            $this->modifiedColumns[] = CompanyPeer::DESCRIPTION;
        }


        return $this;
    } // setDescription()

    /**
     * Set the value of [workforce] column.
     *
     * @param  string $v new value
     * @return Company The current object (for fluent API support)
     */
    public function setWorkforce($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->workforce !== $v) {
            $this->workforce = $v;
            $this->modifiedColumns[] = CompanyPeer::WORKFORCE;
        }


        return $this;
    } // setWorkforce()

    /**
     * Set the value of [logo_file] column.
     *
     * @param  string $v new value
     * @return Company The current object (for fluent API support)
     */
    public function setLogoFile($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->logo_file !== $v) {
            $this->logo_file = $v;
            $this->modifiedColumns[] = CompanyPeer::LOGO_FILE;
        }


        return $this;
    } // setLogoFile()

    /**
     * Set the value of [url_website] column.
     *
     * @param  string $v new value
     * @return Company The current object (for fluent API support)
     */
    public function setUrlWebsite($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->url_website !== $v) {
            $this->url_website = $v;
            $this->modifiedColumns[] = CompanyPeer::URL_WEBSITE;
        }


        return $this;
    } // setUrlWebsite()

    /**
     * Sets the value of the [first_connection] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return Company The current object (for fluent API support)
     */
    public function setFirstConnection($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->first_connection !== $v) {
            $this->first_connection = $v;
            $this->modifiedColumns[] = CompanyPeer::FIRST_CONNECTION;
        }


        return $this;
    } // setFirstConnection()

    /**
     * Sets the value of the [validated] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return Company The current object (for fluent API support)
     */
    public function setValidated($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->validated !== $v) {
            $this->validated = $v;
            $this->modifiedColumns[] = CompanyPeer::VALIDATED;
        }


        return $this;
    } // setValidated()

    /**
     * Sets the value of the [newsletter] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return Company The current object (for fluent API support)
     */
    public function setNewsletter($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->newsletter !== $v) {
            $this->newsletter = $v;
            $this->modifiedColumns[] = CompanyPeer::NEWSLETTER;
        }


        return $this;
    } // setNewsletter()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->first_connection !== false) {
                return false;
            }

            if ($this->validated !== false) {
                return false;
            }

            if ($this->newsletter !== true) {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->address_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->contact_id = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->industry_id = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
            $this->name = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->siret = ($row[$startcol + 5] !== null) ? (int) $row[$startcol + 5] : null;
            $this->password = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->email = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->telephone = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->description = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->workforce = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->logo_file = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->url_website = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->first_connection = ($row[$startcol + 13] !== null) ? (boolean) $row[$startcol + 13] : null;
            $this->validated = ($row[$startcol + 14] !== null) ? (boolean) $row[$startcol + 14] : null;
            $this->newsletter = ($row[$startcol + 15] !== null) ? (boolean) $row[$startcol + 15] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 16; // 16 = CompanyPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Company object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aAddress !== null && $this->address_id !== $this->aAddress->getId()) {
            $this->aAddress = null;
        }
        if ($this->aContact !== null && $this->contact_id !== $this->aContact->getId()) {
            $this->aContact = null;
        }
        if ($this->aIndustry !== null && $this->industry_id !== $this->aIndustry->getId()) {
            $this->aIndustry = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CompanyPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CompanyPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aAddress = null;
            $this->aContact = null;
            $this->aIndustry = null;
            $this->collInternships = null;

            $this->collUserJobs = null;

            $this->collValidations = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CompanyPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CompanyQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CompanyPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CompanyPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aAddress !== null) {
                if ($this->aAddress->isModified() || $this->aAddress->isNew()) {
                    $affectedRows += $this->aAddress->save($con);
                }
                $this->setAddress($this->aAddress);
            }

            if ($this->aContact !== null) {
                if ($this->aContact->isModified() || $this->aContact->isNew()) {
                    $affectedRows += $this->aContact->save($con);
                }
                $this->setContact($this->aContact);
            }

            if ($this->aIndustry !== null) {
                if ($this->aIndustry->isModified() || $this->aIndustry->isNew()) {
                    $affectedRows += $this->aIndustry->save($con);
                }
                $this->setIndustry($this->aIndustry);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->internshipsScheduledForDeletion !== null) {
                if (!$this->internshipsScheduledForDeletion->isEmpty()) {
                    InternshipQuery::create()
                        ->filterByPrimaryKeys($this->internshipsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->internshipsScheduledForDeletion = null;
                }
            }

            if ($this->collInternships !== null) {
                foreach ($this->collInternships as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->userJobsScheduledForDeletion !== null) {
                if (!$this->userJobsScheduledForDeletion->isEmpty()) {
                    UserJobQuery::create()
                        ->filterByPrimaryKeys($this->userJobsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->userJobsScheduledForDeletion = null;
                }
            }

            if ($this->collUserJobs !== null) {
                foreach ($this->collUserJobs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->validationsScheduledForDeletion !== null) {
                if (!$this->validationsScheduledForDeletion->isEmpty()) {
                    foreach ($this->validationsScheduledForDeletion as $validation) {
                        // need to save related object because we set the relation to null
                        $validation->save($con);
                    }
                    $this->validationsScheduledForDeletion = null;
                }
            }

            if ($this->collValidations !== null) {
                foreach ($this->collValidations as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CompanyPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CompanyPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CompanyPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(CompanyPeer::ADDRESS_ID)) {
            $modifiedColumns[':p' . $index++]  = '`address_id`';
        }
        if ($this->isColumnModified(CompanyPeer::CONTACT_ID)) {
            $modifiedColumns[':p' . $index++]  = '`contact_id`';
        }
        if ($this->isColumnModified(CompanyPeer::INDUSTRY_ID)) {
            $modifiedColumns[':p' . $index++]  = '`industry_id`';
        }
        if ($this->isColumnModified(CompanyPeer::NAME)) {
            $modifiedColumns[':p' . $index++]  = '`name`';
        }
        if ($this->isColumnModified(CompanyPeer::SIRET)) {
            $modifiedColumns[':p' . $index++]  = '`siret`';
        }
        if ($this->isColumnModified(CompanyPeer::PASSWORD)) {
            $modifiedColumns[':p' . $index++]  = '`password`';
        }
        if ($this->isColumnModified(CompanyPeer::EMAIL)) {
            $modifiedColumns[':p' . $index++]  = '`email`';
        }
        if ($this->isColumnModified(CompanyPeer::TELEPHONE)) {
            $modifiedColumns[':p' . $index++]  = '`telephone`';
        }
        if ($this->isColumnModified(CompanyPeer::DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = '`description`';
        }
        if ($this->isColumnModified(CompanyPeer::WORKFORCE)) {
            $modifiedColumns[':p' . $index++]  = '`workforce`';
        }
        if ($this->isColumnModified(CompanyPeer::LOGO_FILE)) {
            $modifiedColumns[':p' . $index++]  = '`logo_file`';
        }
        if ($this->isColumnModified(CompanyPeer::URL_WEBSITE)) {
            $modifiedColumns[':p' . $index++]  = '`url_website`';
        }
        if ($this->isColumnModified(CompanyPeer::FIRST_CONNECTION)) {
            $modifiedColumns[':p' . $index++]  = '`first_connection`';
        }
        if ($this->isColumnModified(CompanyPeer::VALIDATED)) {
            $modifiedColumns[':p' . $index++]  = '`validated`';
        }
        if ($this->isColumnModified(CompanyPeer::NEWSLETTER)) {
            $modifiedColumns[':p' . $index++]  = '`newsletter`';
        }

        $sql = sprintf(
            'INSERT INTO `company` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`address_id`':
                        $stmt->bindValue($identifier, $this->address_id, PDO::PARAM_INT);
                        break;
                    case '`contact_id`':
                        $stmt->bindValue($identifier, $this->contact_id, PDO::PARAM_INT);
                        break;
                    case '`industry_id`':
                        $stmt->bindValue($identifier, $this->industry_id, PDO::PARAM_INT);
                        break;
                    case '`name`':
                        $stmt->bindValue($identifier, $this->name, PDO::PARAM_STR);
                        break;
                    case '`siret`':
                        $stmt->bindValue($identifier, $this->siret, PDO::PARAM_INT);
                        break;
                    case '`password`':
                        $stmt->bindValue($identifier, $this->password, PDO::PARAM_STR);
                        break;
                    case '`email`':
                        $stmt->bindValue($identifier, $this->email, PDO::PARAM_STR);
                        break;
                    case '`telephone`':
                        $stmt->bindValue($identifier, $this->telephone, PDO::PARAM_STR);
                        break;
                    case '`description`':
                        $stmt->bindValue($identifier, $this->description, PDO::PARAM_STR);
                        break;
                    case '`workforce`':
                        $stmt->bindValue($identifier, $this->workforce, PDO::PARAM_STR);
                        break;
                    case '`logo_file`':
                        $stmt->bindValue($identifier, $this->logo_file, PDO::PARAM_STR);
                        break;
                    case '`url_website`':
                        $stmt->bindValue($identifier, $this->url_website, PDO::PARAM_STR);
                        break;
                    case '`first_connection`':
                        $stmt->bindValue($identifier, (int) $this->first_connection, PDO::PARAM_INT);
                        break;
                    case '`validated`':
                        $stmt->bindValue($identifier, (int) $this->validated, PDO::PARAM_INT);
                        break;
                    case '`newsletter`':
                        $stmt->bindValue($identifier, (int) $this->newsletter, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aAddress !== null) {
                if (!$this->aAddress->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aAddress->getValidationFailures());
                }
            }

            if ($this->aContact !== null) {
                if (!$this->aContact->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aContact->getValidationFailures());
                }
            }

            if ($this->aIndustry !== null) {
                if (!$this->aIndustry->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aIndustry->getValidationFailures());
                }
            }


            if (($retval = CompanyPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collInternships !== null) {
                    foreach ($this->collInternships as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collUserJobs !== null) {
                    foreach ($this->collUserJobs as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collValidations !== null) {
                    foreach ($this->collValidations as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CompanyPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getAddressId();
                break;
            case 2:
                return $this->getContactId();
                break;
            case 3:
                return $this->getIndustryId();
                break;
            case 4:
                return $this->getName();
                break;
            case 5:
                return $this->getSiret();
                break;
            case 6:
                return $this->getPassword();
                break;
            case 7:
                return $this->getEmail();
                break;
            case 8:
                return $this->getTelephone();
                break;
            case 9:
                return $this->getDescription();
                break;
            case 10:
                return $this->getWorkforce();
                break;
            case 11:
                return $this->getLogoFile();
                break;
            case 12:
                return $this->getUrlWebsite();
                break;
            case 13:
                return $this->getFirstConnection();
                break;
            case 14:
                return $this->getValidated();
                break;
            case 15:
                return $this->getNewsletter();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Company'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Company'][$this->getPrimaryKey()] = true;
        $keys = CompanyPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getAddressId(),
            $keys[2] => $this->getContactId(),
            $keys[3] => $this->getIndustryId(),
            $keys[4] => $this->getName(),
            $keys[5] => $this->getSiret(),
            $keys[6] => $this->getPassword(),
            $keys[7] => $this->getEmail(),
            $keys[8] => $this->getTelephone(),
            $keys[9] => $this->getDescription(),
            $keys[10] => $this->getWorkforce(),
            $keys[11] => $this->getLogoFile(),
            $keys[12] => $this->getUrlWebsite(),
            $keys[13] => $this->getFirstConnection(),
            $keys[14] => $this->getValidated(),
            $keys[15] => $this->getNewsletter(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aAddress) {
                $result['Address'] = $this->aAddress->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aContact) {
                $result['Contact'] = $this->aContact->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aIndustry) {
                $result['Industry'] = $this->aIndustry->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collInternships) {
                $result['Internships'] = $this->collInternships->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUserJobs) {
                $result['UserJobs'] = $this->collUserJobs->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collValidations) {
                $result['Validations'] = $this->collValidations->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CompanyPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setAddressId($value);
                break;
            case 2:
                $this->setContactId($value);
                break;
            case 3:
                $this->setIndustryId($value);
                break;
            case 4:
                $this->setName($value);
                break;
            case 5:
                $this->setSiret($value);
                break;
            case 6:
                $this->setPassword($value);
                break;
            case 7:
                $this->setEmail($value);
                break;
            case 8:
                $this->setTelephone($value);
                break;
            case 9:
                $this->setDescription($value);
                break;
            case 10:
                $this->setWorkforce($value);
                break;
            case 11:
                $this->setLogoFile($value);
                break;
            case 12:
                $this->setUrlWebsite($value);
                break;
            case 13:
                $this->setFirstConnection($value);
                break;
            case 14:
                $this->setValidated($value);
                break;
            case 15:
                $this->setNewsletter($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CompanyPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setAddressId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setContactId($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setIndustryId($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setName($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setSiret($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setPassword($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setEmail($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setTelephone($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setDescription($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setWorkforce($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setLogoFile($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setUrlWebsite($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setFirstConnection($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setValidated($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setNewsletter($arr[$keys[15]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CompanyPeer::DATABASE_NAME);

        if ($this->isColumnModified(CompanyPeer::ID)) $criteria->add(CompanyPeer::ID, $this->id);
        if ($this->isColumnModified(CompanyPeer::ADDRESS_ID)) $criteria->add(CompanyPeer::ADDRESS_ID, $this->address_id);
        if ($this->isColumnModified(CompanyPeer::CONTACT_ID)) $criteria->add(CompanyPeer::CONTACT_ID, $this->contact_id);
        if ($this->isColumnModified(CompanyPeer::INDUSTRY_ID)) $criteria->add(CompanyPeer::INDUSTRY_ID, $this->industry_id);
        if ($this->isColumnModified(CompanyPeer::NAME)) $criteria->add(CompanyPeer::NAME, $this->name);
        if ($this->isColumnModified(CompanyPeer::SIRET)) $criteria->add(CompanyPeer::SIRET, $this->siret);
        if ($this->isColumnModified(CompanyPeer::PASSWORD)) $criteria->add(CompanyPeer::PASSWORD, $this->password);
        if ($this->isColumnModified(CompanyPeer::EMAIL)) $criteria->add(CompanyPeer::EMAIL, $this->email);
        if ($this->isColumnModified(CompanyPeer::TELEPHONE)) $criteria->add(CompanyPeer::TELEPHONE, $this->telephone);
        if ($this->isColumnModified(CompanyPeer::DESCRIPTION)) $criteria->add(CompanyPeer::DESCRIPTION, $this->description);
        if ($this->isColumnModified(CompanyPeer::WORKFORCE)) $criteria->add(CompanyPeer::WORKFORCE, $this->workforce);
        if ($this->isColumnModified(CompanyPeer::LOGO_FILE)) $criteria->add(CompanyPeer::LOGO_FILE, $this->logo_file);
        if ($this->isColumnModified(CompanyPeer::URL_WEBSITE)) $criteria->add(CompanyPeer::URL_WEBSITE, $this->url_website);
        if ($this->isColumnModified(CompanyPeer::FIRST_CONNECTION)) $criteria->add(CompanyPeer::FIRST_CONNECTION, $this->first_connection);
        if ($this->isColumnModified(CompanyPeer::VALIDATED)) $criteria->add(CompanyPeer::VALIDATED, $this->validated);
        if ($this->isColumnModified(CompanyPeer::NEWSLETTER)) $criteria->add(CompanyPeer::NEWSLETTER, $this->newsletter);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CompanyPeer::DATABASE_NAME);
        $criteria->add(CompanyPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Company (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setAddressId($this->getAddressId());
        $copyObj->setContactId($this->getContactId());
        $copyObj->setIndustryId($this->getIndustryId());
        $copyObj->setName($this->getName());
        $copyObj->setSiret($this->getSiret());
        $copyObj->setPassword($this->getPassword());
        $copyObj->setEmail($this->getEmail());
        $copyObj->setTelephone($this->getTelephone());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setWorkforce($this->getWorkforce());
        $copyObj->setLogoFile($this->getLogoFile());
        $copyObj->setUrlWebsite($this->getUrlWebsite());
        $copyObj->setFirstConnection($this->getFirstConnection());
        $copyObj->setValidated($this->getValidated());
        $copyObj->setNewsletter($this->getNewsletter());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getInternships() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addInternship($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUserJobs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUserJob($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getValidations() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addValidation($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Company Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CompanyPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CompanyPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a Address object.
     *
     * @param                  Address $v
     * @return Company The current object (for fluent API support)
     * @throws PropelException
     */
    public function setAddress(Address $v = null)
    {
        if ($v === null) {
            $this->setAddressId(NULL);
        } else {
            $this->setAddressId($v->getId());
        }

        $this->aAddress = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Address object, it will not be re-added.
        if ($v !== null) {
            $v->addCompany($this);
        }


        return $this;
    }


    /**
     * Get the associated Address object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Address The associated Address object.
     * @throws PropelException
     */
    public function getAddress(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aAddress === null && ($this->address_id !== null) && $doQuery) {
            $this->aAddress = AddressQuery::create()->findPk($this->address_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aAddress->addCompanies($this);
             */
        }

        return $this->aAddress;
    }

    /**
     * Declares an association between this object and a Contact object.
     *
     * @param                  Contact $v
     * @return Company The current object (for fluent API support)
     * @throws PropelException
     */
    public function setContact(Contact $v = null)
    {
        if ($v === null) {
            $this->setContactId(NULL);
        } else {
            $this->setContactId($v->getId());
        }

        $this->aContact = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Contact object, it will not be re-added.
        if ($v !== null) {
            $v->addCompany($this);
        }


        return $this;
    }


    /**
     * Get the associated Contact object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Contact The associated Contact object.
     * @throws PropelException
     */
    public function getContact(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aContact === null && ($this->contact_id !== null) && $doQuery) {
            $this->aContact = ContactQuery::create()->findPk($this->contact_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aContact->addCompanies($this);
             */
        }

        return $this->aContact;
    }

    /**
     * Declares an association between this object and a Industry object.
     *
     * @param                  Industry $v
     * @return Company The current object (for fluent API support)
     * @throws PropelException
     */
    public function setIndustry(Industry $v = null)
    {
        if ($v === null) {
            $this->setIndustryId(NULL);
        } else {
            $this->setIndustryId($v->getId());
        }

        $this->aIndustry = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Industry object, it will not be re-added.
        if ($v !== null) {
            $v->addCompany($this);
        }


        return $this;
    }


    /**
     * Get the associated Industry object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Industry The associated Industry object.
     * @throws PropelException
     */
    public function getIndustry(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aIndustry === null && ($this->industry_id !== null) && $doQuery) {
            $this->aIndustry = IndustryQuery::create()->findPk($this->industry_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aIndustry->addCompanies($this);
             */
        }

        return $this->aIndustry;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Internship' == $relationName) {
            $this->initInternships();
        }
        if ('UserJob' == $relationName) {
            $this->initUserJobs();
        }
        if ('Validation' == $relationName) {
            $this->initValidations();
        }
    }

    /**
     * Clears out the collInternships collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Company The current object (for fluent API support)
     * @see        addInternships()
     */
    public function clearInternships()
    {
        $this->collInternships = null; // important to set this to null since that means it is uninitialized
        $this->collInternshipsPartial = null;

        return $this;
    }

    /**
     * reset is the collInternships collection loaded partially
     *
     * @return void
     */
    public function resetPartialInternships($v = true)
    {
        $this->collInternshipsPartial = $v;
    }

    /**
     * Initializes the collInternships collection.
     *
     * By default this just sets the collInternships collection to an empty array (like clearcollInternships());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initInternships($overrideExisting = true)
    {
        if (null !== $this->collInternships && !$overrideExisting) {
            return;
        }
        $this->collInternships = new PropelObjectCollection();
        $this->collInternships->setModel('Internship');
    }

    /**
     * Gets an array of Internship objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Company is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Internship[] List of Internship objects
     * @throws PropelException
     */
    public function getInternships($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collInternshipsPartial && !$this->isNew();
        if (null === $this->collInternships || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collInternships) {
                // return empty collection
                $this->initInternships();
            } else {
                $collInternships = InternshipQuery::create(null, $criteria)
                    ->filterByCompany($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collInternshipsPartial && count($collInternships)) {
                      $this->initInternships(false);

                      foreach ($collInternships as $obj) {
                        if (false == $this->collInternships->contains($obj)) {
                          $this->collInternships->append($obj);
                        }
                      }

                      $this->collInternshipsPartial = true;
                    }

                    $collInternships->getInternalIterator()->rewind();

                    return $collInternships;
                }

                if ($partial && $this->collInternships) {
                    foreach ($this->collInternships as $obj) {
                        if ($obj->isNew()) {
                            $collInternships[] = $obj;
                        }
                    }
                }

                $this->collInternships = $collInternships;
                $this->collInternshipsPartial = false;
            }
        }

        return $this->collInternships;
    }

    /**
     * Sets a collection of Internship objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $internships A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Company The current object (for fluent API support)
     */
    public function setInternships(PropelCollection $internships, PropelPDO $con = null)
    {
        $internshipsToDelete = $this->getInternships(new Criteria(), $con)->diff($internships);


        $this->internshipsScheduledForDeletion = $internshipsToDelete;

        foreach ($internshipsToDelete as $internshipRemoved) {
            $internshipRemoved->setCompany(null);
        }

        $this->collInternships = null;
        foreach ($internships as $internship) {
            $this->addInternship($internship);
        }

        $this->collInternships = $internships;
        $this->collInternshipsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Internship objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Internship objects.
     * @throws PropelException
     */
    public function countInternships(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collInternshipsPartial && !$this->isNew();
        if (null === $this->collInternships || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collInternships) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getInternships());
            }
            $query = InternshipQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCompany($this)
                ->count($con);
        }

        return count($this->collInternships);
    }

    /**
     * Method called to associate a Internship object to this object
     * through the Internship foreign key attribute.
     *
     * @param    Internship $l Internship
     * @return Company The current object (for fluent API support)
     */
    public function addInternship(Internship $l)
    {
        if ($this->collInternships === null) {
            $this->initInternships();
            $this->collInternshipsPartial = true;
        }

        if (!in_array($l, $this->collInternships->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddInternship($l);

            if ($this->internshipsScheduledForDeletion and $this->internshipsScheduledForDeletion->contains($l)) {
                $this->internshipsScheduledForDeletion->remove($this->internshipsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	Internship $internship The internship object to add.
     */
    protected function doAddInternship($internship)
    {
        $this->collInternships[]= $internship;
        $internship->setCompany($this);
    }

    /**
     * @param	Internship $internship The internship object to remove.
     * @return Company The current object (for fluent API support)
     */
    public function removeInternship($internship)
    {
        if ($this->getInternships()->contains($internship)) {
            $this->collInternships->remove($this->collInternships->search($internship));
            if (null === $this->internshipsScheduledForDeletion) {
                $this->internshipsScheduledForDeletion = clone $this->collInternships;
                $this->internshipsScheduledForDeletion->clear();
            }
            $this->internshipsScheduledForDeletion[]= clone $internship;
            $internship->setCompany(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Company is new, it will return
     * an empty collection; or if this Company has previously
     * been saved, it will retrieve related Internships from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Company.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Internship[] List of Internship objects
     */
    public function getInternshipsJoinOfferinternshipRelatedByOfferId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = InternshipQuery::create(null, $criteria);
        $query->joinWith('OfferinternshipRelatedByOfferId', $join_behavior);

        return $this->getInternships($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Company is new, it will return
     * an empty collection; or if this Company has previously
     * been saved, it will retrieve related Internships from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Company.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Internship[] List of Internship objects
     */
    public function getInternshipsJoinUser($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = InternshipQuery::create(null, $criteria);
        $query->joinWith('User', $join_behavior);

        return $this->getInternships($query, $con);
    }

    /**
     * Clears out the collUserJobs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Company The current object (for fluent API support)
     * @see        addUserJobs()
     */
    public function clearUserJobs()
    {
        $this->collUserJobs = null; // important to set this to null since that means it is uninitialized
        $this->collUserJobsPartial = null;

        return $this;
    }

    /**
     * reset is the collUserJobs collection loaded partially
     *
     * @return void
     */
    public function resetPartialUserJobs($v = true)
    {
        $this->collUserJobsPartial = $v;
    }

    /**
     * Initializes the collUserJobs collection.
     *
     * By default this just sets the collUserJobs collection to an empty array (like clearcollUserJobs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUserJobs($overrideExisting = true)
    {
        if (null !== $this->collUserJobs && !$overrideExisting) {
            return;
        }
        $this->collUserJobs = new PropelObjectCollection();
        $this->collUserJobs->setModel('UserJob');
    }

    /**
     * Gets an array of UserJob objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Company is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|UserJob[] List of UserJob objects
     * @throws PropelException
     */
    public function getUserJobs($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collUserJobsPartial && !$this->isNew();
        if (null === $this->collUserJobs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collUserJobs) {
                // return empty collection
                $this->initUserJobs();
            } else {
                $collUserJobs = UserJobQuery::create(null, $criteria)
                    ->filterByCompany($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collUserJobsPartial && count($collUserJobs)) {
                      $this->initUserJobs(false);

                      foreach ($collUserJobs as $obj) {
                        if (false == $this->collUserJobs->contains($obj)) {
                          $this->collUserJobs->append($obj);
                        }
                      }

                      $this->collUserJobsPartial = true;
                    }

                    $collUserJobs->getInternalIterator()->rewind();

                    return $collUserJobs;
                }

                if ($partial && $this->collUserJobs) {
                    foreach ($this->collUserJobs as $obj) {
                        if ($obj->isNew()) {
                            $collUserJobs[] = $obj;
                        }
                    }
                }

                $this->collUserJobs = $collUserJobs;
                $this->collUserJobsPartial = false;
            }
        }

        return $this->collUserJobs;
    }

    /**
     * Sets a collection of UserJob objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $userJobs A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Company The current object (for fluent API support)
     */
    public function setUserJobs(PropelCollection $userJobs, PropelPDO $con = null)
    {
        $userJobsToDelete = $this->getUserJobs(new Criteria(), $con)->diff($userJobs);


        $this->userJobsScheduledForDeletion = $userJobsToDelete;

        foreach ($userJobsToDelete as $userJobRemoved) {
            $userJobRemoved->setCompany(null);
        }

        $this->collUserJobs = null;
        foreach ($userJobs as $userJob) {
            $this->addUserJob($userJob);
        }

        $this->collUserJobs = $userJobs;
        $this->collUserJobsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related UserJob objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related UserJob objects.
     * @throws PropelException
     */
    public function countUserJobs(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collUserJobsPartial && !$this->isNew();
        if (null === $this->collUserJobs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUserJobs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUserJobs());
            }
            $query = UserJobQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCompany($this)
                ->count($con);
        }

        return count($this->collUserJobs);
    }

    /**
     * Method called to associate a UserJob object to this object
     * through the UserJob foreign key attribute.
     *
     * @param    UserJob $l UserJob
     * @return Company The current object (for fluent API support)
     */
    public function addUserJob(UserJob $l)
    {
        if ($this->collUserJobs === null) {
            $this->initUserJobs();
            $this->collUserJobsPartial = true;
        }

        if (!in_array($l, $this->collUserJobs->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddUserJob($l);

            if ($this->userJobsScheduledForDeletion and $this->userJobsScheduledForDeletion->contains($l)) {
                $this->userJobsScheduledForDeletion->remove($this->userJobsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	UserJob $userJob The userJob object to add.
     */
    protected function doAddUserJob($userJob)
    {
        $this->collUserJobs[]= $userJob;
        $userJob->setCompany($this);
    }

    /**
     * @param	UserJob $userJob The userJob object to remove.
     * @return Company The current object (for fluent API support)
     */
    public function removeUserJob($userJob)
    {
        if ($this->getUserJobs()->contains($userJob)) {
            $this->collUserJobs->remove($this->collUserJobs->search($userJob));
            if (null === $this->userJobsScheduledForDeletion) {
                $this->userJobsScheduledForDeletion = clone $this->collUserJobs;
                $this->userJobsScheduledForDeletion->clear();
            }
            $this->userJobsScheduledForDeletion[]= clone $userJob;
            $userJob->setCompany(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Company is new, it will return
     * an empty collection; or if this Company has previously
     * been saved, it will retrieve related UserJobs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Company.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|UserJob[] List of UserJob objects
     */
    public function getUserJobsJoinUser($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = UserJobQuery::create(null, $criteria);
        $query->joinWith('User', $join_behavior);

        return $this->getUserJobs($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Company is new, it will return
     * an empty collection; or if this Company has previously
     * been saved, it will retrieve related UserJobs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Company.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|UserJob[] List of UserJob objects
     */
    public function getUserJobsJoinProfession($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = UserJobQuery::create(null, $criteria);
        $query->joinWith('Profession', $join_behavior);

        return $this->getUserJobs($query, $con);
    }

    /**
     * Clears out the collValidations collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Company The current object (for fluent API support)
     * @see        addValidations()
     */
    public function clearValidations()
    {
        $this->collValidations = null; // important to set this to null since that means it is uninitialized
        $this->collValidationsPartial = null;

        return $this;
    }

    /**
     * reset is the collValidations collection loaded partially
     *
     * @return void
     */
    public function resetPartialValidations($v = true)
    {
        $this->collValidationsPartial = $v;
    }

    /**
     * Initializes the collValidations collection.
     *
     * By default this just sets the collValidations collection to an empty array (like clearcollValidations());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initValidations($overrideExisting = true)
    {
        if (null !== $this->collValidations && !$overrideExisting) {
            return;
        }
        $this->collValidations = new PropelObjectCollection();
        $this->collValidations->setModel('Validation');
    }

    /**
     * Gets an array of Validation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Company is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Validation[] List of Validation objects
     * @throws PropelException
     */
    public function getValidations($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collValidationsPartial && !$this->isNew();
        if (null === $this->collValidations || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collValidations) {
                // return empty collection
                $this->initValidations();
            } else {
                $collValidations = ValidationQuery::create(null, $criteria)
                    ->filterByCompany($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collValidationsPartial && count($collValidations)) {
                      $this->initValidations(false);

                      foreach ($collValidations as $obj) {
                        if (false == $this->collValidations->contains($obj)) {
                          $this->collValidations->append($obj);
                        }
                      }

                      $this->collValidationsPartial = true;
                    }

                    $collValidations->getInternalIterator()->rewind();

                    return $collValidations;
                }

                if ($partial && $this->collValidations) {
                    foreach ($this->collValidations as $obj) {
                        if ($obj->isNew()) {
                            $collValidations[] = $obj;
                        }
                    }
                }

                $this->collValidations = $collValidations;
                $this->collValidationsPartial = false;
            }
        }

        return $this->collValidations;
    }

    /**
     * Sets a collection of Validation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $validations A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Company The current object (for fluent API support)
     */
    public function setValidations(PropelCollection $validations, PropelPDO $con = null)
    {
        $validationsToDelete = $this->getValidations(new Criteria(), $con)->diff($validations);


        $this->validationsScheduledForDeletion = $validationsToDelete;

        foreach ($validationsToDelete as $validationRemoved) {
            $validationRemoved->setCompany(null);
        }

        $this->collValidations = null;
        foreach ($validations as $validation) {
            $this->addValidation($validation);
        }

        $this->collValidations = $validations;
        $this->collValidationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Validation objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Validation objects.
     * @throws PropelException
     */
    public function countValidations(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collValidationsPartial && !$this->isNew();
        if (null === $this->collValidations || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collValidations) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getValidations());
            }
            $query = ValidationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCompany($this)
                ->count($con);
        }

        return count($this->collValidations);
    }

    /**
     * Method called to associate a Validation object to this object
     * through the Validation foreign key attribute.
     *
     * @param    Validation $l Validation
     * @return Company The current object (for fluent API support)
     */
    public function addValidation(Validation $l)
    {
        if ($this->collValidations === null) {
            $this->initValidations();
            $this->collValidationsPartial = true;
        }

        if (!in_array($l, $this->collValidations->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddValidation($l);

            if ($this->validationsScheduledForDeletion and $this->validationsScheduledForDeletion->contains($l)) {
                $this->validationsScheduledForDeletion->remove($this->validationsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	Validation $validation The validation object to add.
     */
    protected function doAddValidation($validation)
    {
        $this->collValidations[]= $validation;
        $validation->setCompany($this);
    }

    /**
     * @param	Validation $validation The validation object to remove.
     * @return Company The current object (for fluent API support)
     */
    public function removeValidation($validation)
    {
        if ($this->getValidations()->contains($validation)) {
            $this->collValidations->remove($this->collValidations->search($validation));
            if (null === $this->validationsScheduledForDeletion) {
                $this->validationsScheduledForDeletion = clone $this->collValidations;
                $this->validationsScheduledForDeletion->clear();
            }
            $this->validationsScheduledForDeletion[]= $validation;
            $validation->setCompany(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Company is new, it will return
     * an empty collection; or if this Company has previously
     * been saved, it will retrieve related Validations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Company.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Validation[] List of Validation objects
     */
    public function getValidationsJoinUser($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = ValidationQuery::create(null, $criteria);
        $query->joinWith('User', $join_behavior);

        return $this->getValidations($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->address_id = null;
        $this->contact_id = null;
        $this->industry_id = null;
        $this->name = null;
        $this->siret = null;
        $this->password = null;
        $this->email = null;
        $this->telephone = null;
        $this->description = null;
        $this->workforce = null;
        $this->logo_file = null;
        $this->url_website = null;
        $this->first_connection = null;
        $this->validated = null;
        $this->newsletter = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collInternships) {
                foreach ($this->collInternships as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUserJobs) {
                foreach ($this->collUserJobs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collValidations) {
                foreach ($this->collValidations as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aAddress instanceof Persistent) {
              $this->aAddress->clearAllReferences($deep);
            }
            if ($this->aContact instanceof Persistent) {
              $this->aContact->clearAllReferences($deep);
            }
            if ($this->aIndustry instanceof Persistent) {
              $this->aIndustry->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collInternships instanceof PropelCollection) {
            $this->collInternships->clearIterator();
        }
        $this->collInternships = null;
        if ($this->collUserJobs instanceof PropelCollection) {
            $this->collUserJobs->clearIterator();
        }
        $this->collUserJobs = null;
        if ($this->collValidations instanceof PropelCollection) {
            $this->collValidations->clearIterator();
        }
        $this->collValidations = null;
        $this->aAddress = null;
        $this->aContact = null;
        $this->aIndustry = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CompanyPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
