<?php

namespace MMIBundle\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use MMIBundle\Model\Internship;
use MMIBundle\Model\Offerinternship;
use MMIBundle\Model\OfferinternshipPeer;
use MMIBundle\Model\OfferinternshipQuery;

/**
 * @method OfferinternshipQuery orderById($order = Criteria::ASC) Order by the id column
 * @method OfferinternshipQuery orderByInternshipId($order = Criteria::ASC) Order by the internship_id column
 * @method OfferinternshipQuery orderByType($order = Criteria::ASC) Order by the type column
 * @method OfferinternshipQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method OfferinternshipQuery orderByProfil($order = Criteria::ASC) Order by the profil column
 *
 * @method OfferinternshipQuery groupById() Group by the id column
 * @method OfferinternshipQuery groupByInternshipId() Group by the internship_id column
 * @method OfferinternshipQuery groupByType() Group by the type column
 * @method OfferinternshipQuery groupByName() Group by the name column
 * @method OfferinternshipQuery groupByProfil() Group by the profil column
 *
 * @method OfferinternshipQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method OfferinternshipQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method OfferinternshipQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method OfferinternshipQuery leftJoinInternshipRelatedByInternshipId($relationAlias = null) Adds a LEFT JOIN clause to the query using the InternshipRelatedByInternshipId relation
 * @method OfferinternshipQuery rightJoinInternshipRelatedByInternshipId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the InternshipRelatedByInternshipId relation
 * @method OfferinternshipQuery innerJoinInternshipRelatedByInternshipId($relationAlias = null) Adds a INNER JOIN clause to the query using the InternshipRelatedByInternshipId relation
 *
 * @method OfferinternshipQuery leftJoinInternshipRelatedByOfferId($relationAlias = null) Adds a LEFT JOIN clause to the query using the InternshipRelatedByOfferId relation
 * @method OfferinternshipQuery rightJoinInternshipRelatedByOfferId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the InternshipRelatedByOfferId relation
 * @method OfferinternshipQuery innerJoinInternshipRelatedByOfferId($relationAlias = null) Adds a INNER JOIN clause to the query using the InternshipRelatedByOfferId relation
 *
 * @method Offerinternship findOne(PropelPDO $con = null) Return the first Offerinternship matching the query
 * @method Offerinternship findOneOrCreate(PropelPDO $con = null) Return the first Offerinternship matching the query, or a new Offerinternship object populated from the query conditions when no match is found
 *
 * @method Offerinternship findOneByInternshipId(int $internship_id) Return the first Offerinternship filtered by the internship_id column
 * @method Offerinternship findOneByType(int $type) Return the first Offerinternship filtered by the type column
 * @method Offerinternship findOneByName(string $name) Return the first Offerinternship filtered by the name column
 * @method Offerinternship findOneByProfil(string $profil) Return the first Offerinternship filtered by the profil column
 *
 * @method array findById(int $id) Return Offerinternship objects filtered by the id column
 * @method array findByInternshipId(int $internship_id) Return Offerinternship objects filtered by the internship_id column
 * @method array findByType(int $type) Return Offerinternship objects filtered by the type column
 * @method array findByName(string $name) Return Offerinternship objects filtered by the name column
 * @method array findByProfil(string $profil) Return Offerinternship objects filtered by the profil column
 */
abstract class BaseOfferinternshipQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseOfferinternshipQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'default';
        }
        if (null === $modelName) {
            $modelName = 'MMIBundle\\Model\\Offerinternship';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new OfferinternshipQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   OfferinternshipQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return OfferinternshipQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof OfferinternshipQuery) {
            return $criteria;
        }
        $query = new OfferinternshipQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Offerinternship|Offerinternship[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = OfferinternshipPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(OfferinternshipPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Offerinternship A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Offerinternship A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `internship_id`, `type`, `name`, `profil` FROM `offerInternship` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Offerinternship();
            $obj->hydrate($row);
            OfferinternshipPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Offerinternship|Offerinternship[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Offerinternship[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return OfferinternshipQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(OfferinternshipPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return OfferinternshipQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(OfferinternshipPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return OfferinternshipQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(OfferinternshipPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(OfferinternshipPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OfferinternshipPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the internship_id column
     *
     * Example usage:
     * <code>
     * $query->filterByInternshipId(1234); // WHERE internship_id = 1234
     * $query->filterByInternshipId(array(12, 34)); // WHERE internship_id IN (12, 34)
     * $query->filterByInternshipId(array('min' => 12)); // WHERE internship_id >= 12
     * $query->filterByInternshipId(array('max' => 12)); // WHERE internship_id <= 12
     * </code>
     *
     * @see       filterByInternshipRelatedByInternshipId()
     *
     * @param     mixed $internshipId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return OfferinternshipQuery The current query, for fluid interface
     */
    public function filterByInternshipId($internshipId = null, $comparison = null)
    {
        if (is_array($internshipId)) {
            $useMinMax = false;
            if (isset($internshipId['min'])) {
                $this->addUsingAlias(OfferinternshipPeer::INTERNSHIP_ID, $internshipId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($internshipId['max'])) {
                $this->addUsingAlias(OfferinternshipPeer::INTERNSHIP_ID, $internshipId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OfferinternshipPeer::INTERNSHIP_ID, $internshipId, $comparison);
    }

    /**
     * Filter the query on the type column
     *
     * Example usage:
     * <code>
     * $query->filterByType(1234); // WHERE type = 1234
     * $query->filterByType(array(12, 34)); // WHERE type IN (12, 34)
     * $query->filterByType(array('min' => 12)); // WHERE type >= 12
     * $query->filterByType(array('max' => 12)); // WHERE type <= 12
     * </code>
     *
     * @param     mixed $type The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return OfferinternshipQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        if (is_array($type)) {
            $useMinMax = false;
            if (isset($type['min'])) {
                $this->addUsingAlias(OfferinternshipPeer::TYPE, $type['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($type['max'])) {
                $this->addUsingAlias(OfferinternshipPeer::TYPE, $type['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OfferinternshipPeer::TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return OfferinternshipQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(OfferinternshipPeer::NAME, $name, $comparison);
    }

    /**
     * Filter the query on the profil column
     *
     * Example usage:
     * <code>
     * $query->filterByProfil('fooValue');   // WHERE profil = 'fooValue'
     * $query->filterByProfil('%fooValue%'); // WHERE profil LIKE '%fooValue%'
     * </code>
     *
     * @param     string $profil The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return OfferinternshipQuery The current query, for fluid interface
     */
    public function filterByProfil($profil = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($profil)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $profil)) {
                $profil = str_replace('*', '%', $profil);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(OfferinternshipPeer::PROFIL, $profil, $comparison);
    }

    /**
     * Filter the query by a related Internship object
     *
     * @param   Internship|PropelObjectCollection $internship The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 OfferinternshipQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByInternshipRelatedByInternshipId($internship, $comparison = null)
    {
        if ($internship instanceof Internship) {
            return $this
                ->addUsingAlias(OfferinternshipPeer::INTERNSHIP_ID, $internship->getId(), $comparison);
        } elseif ($internship instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(OfferinternshipPeer::INTERNSHIP_ID, $internship->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByInternshipRelatedByInternshipId() only accepts arguments of type Internship or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the InternshipRelatedByInternshipId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return OfferinternshipQuery The current query, for fluid interface
     */
    public function joinInternshipRelatedByInternshipId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('InternshipRelatedByInternshipId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'InternshipRelatedByInternshipId');
        }

        return $this;
    }

    /**
     * Use the InternshipRelatedByInternshipId relation Internship object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MMIBundle\Model\InternshipQuery A secondary query class using the current class as primary query
     */
    public function useInternshipRelatedByInternshipIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinInternshipRelatedByInternshipId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'InternshipRelatedByInternshipId', '\MMIBundle\Model\InternshipQuery');
    }

    /**
     * Filter the query by a related Internship object
     *
     * @param   Internship|PropelObjectCollection $internship  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 OfferinternshipQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByInternshipRelatedByOfferId($internship, $comparison = null)
    {
        if ($internship instanceof Internship) {
            return $this
                ->addUsingAlias(OfferinternshipPeer::ID, $internship->getOfferId(), $comparison);
        } elseif ($internship instanceof PropelObjectCollection) {
            return $this
                ->useInternshipRelatedByOfferIdQuery()
                ->filterByPrimaryKeys($internship->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByInternshipRelatedByOfferId() only accepts arguments of type Internship or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the InternshipRelatedByOfferId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return OfferinternshipQuery The current query, for fluid interface
     */
    public function joinInternshipRelatedByOfferId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('InternshipRelatedByOfferId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'InternshipRelatedByOfferId');
        }

        return $this;
    }

    /**
     * Use the InternshipRelatedByOfferId relation Internship object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MMIBundle\Model\InternshipQuery A secondary query class using the current class as primary query
     */
    public function useInternshipRelatedByOfferIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinInternshipRelatedByOfferId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'InternshipRelatedByOfferId', '\MMIBundle\Model\InternshipQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Offerinternship $offerinternship Object to remove from the list of results
     *
     * @return OfferinternshipQuery The current query, for fluid interface
     */
    public function prune($offerinternship = null)
    {
        if ($offerinternship) {
            $this->addUsingAlias(OfferinternshipPeer::ID, $offerinternship->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
