<?php

namespace MMIBundle\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use MMIBundle\Model\Company;
use MMIBundle\Model\Diaryweek;
use MMIBundle\Model\Internship;
use MMIBundle\Model\InternshipPeer;
use MMIBundle\Model\InternshipQuery;
use MMIBundle\Model\Offerinternship;
use MMIBundle\Model\User;

/**
 * @method InternshipQuery orderById($order = Criteria::ASC) Order by the id column
 * @method InternshipQuery orderByOfferId($order = Criteria::ASC) Order by the offer_id column
 * @method InternshipQuery orderByUserId($order = Criteria::ASC) Order by the user_id column
 * @method InternshipQuery orderByCompanyId($order = Criteria::ASC) Order by the company_id column
 * @method InternshipQuery orderByTutorId($order = Criteria::ASC) Order by the tutor_id column
 * @method InternshipQuery orderByWorkFunction($order = Criteria::ASC) Order by the work_function column
 * @method InternshipQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method InternshipQuery orderByInformation($order = Criteria::ASC) Order by the information column
 * @method InternshipQuery orderByUrlWebsiteDiffusion($order = Criteria::ASC) Order by the url_website_diffusion column
 * @method InternshipQuery orderBySignatoryLastname($order = Criteria::ASC) Order by the signatory_lastname column
 * @method InternshipQuery orderBySignatoryFirstname($order = Criteria::ASC) Order by the signatory_firstname column
 * @method InternshipQuery orderBySignatoryFunction($order = Criteria::ASC) Order by the signatory_function column
 * @method InternshipQuery orderByMasterLastname($order = Criteria::ASC) Order by the master_lastname column
 * @method InternshipQuery orderByMasterFirstname($order = Criteria::ASC) Order by the master_firstname column
 * @method InternshipQuery orderByMasterEmail($order = Criteria::ASC) Order by the master_email column
 * @method InternshipQuery orderByMasterTelephone($order = Criteria::ASC) Order by the master_telephone column
 * @method InternshipQuery orderByValide($order = Criteria::ASC) Order by the valide column
 * @method InternshipQuery orderByEtabli($order = Criteria::ASC) Order by the etabli column
 * @method InternshipQuery orderByDateVisite($order = Criteria::ASC) Order by the date_visite column
 * @method InternshipQuery orderByDateSoutenance($order = Criteria::ASC) Order by the date_soutenance column
 * @method InternshipQuery orderByInternshipBegining($order = Criteria::ASC) Order by the internship_begining column
 * @method InternshipQuery orderByInternshipEnding($order = Criteria::ASC) Order by the internship_ending column
 *
 * @method InternshipQuery groupById() Group by the id column
 * @method InternshipQuery groupByOfferId() Group by the offer_id column
 * @method InternshipQuery groupByUserId() Group by the user_id column
 * @method InternshipQuery groupByCompanyId() Group by the company_id column
 * @method InternshipQuery groupByTutorId() Group by the tutor_id column
 * @method InternshipQuery groupByWorkFunction() Group by the work_function column
 * @method InternshipQuery groupByDescription() Group by the description column
 * @method InternshipQuery groupByInformation() Group by the information column
 * @method InternshipQuery groupByUrlWebsiteDiffusion() Group by the url_website_diffusion column
 * @method InternshipQuery groupBySignatoryLastname() Group by the signatory_lastname column
 * @method InternshipQuery groupBySignatoryFirstname() Group by the signatory_firstname column
 * @method InternshipQuery groupBySignatoryFunction() Group by the signatory_function column
 * @method InternshipQuery groupByMasterLastname() Group by the master_lastname column
 * @method InternshipQuery groupByMasterFirstname() Group by the master_firstname column
 * @method InternshipQuery groupByMasterEmail() Group by the master_email column
 * @method InternshipQuery groupByMasterTelephone() Group by the master_telephone column
 * @method InternshipQuery groupByValide() Group by the valide column
 * @method InternshipQuery groupByEtabli() Group by the etabli column
 * @method InternshipQuery groupByDateVisite() Group by the date_visite column
 * @method InternshipQuery groupByDateSoutenance() Group by the date_soutenance column
 * @method InternshipQuery groupByInternshipBegining() Group by the internship_begining column
 * @method InternshipQuery groupByInternshipEnding() Group by the internship_ending column
 *
 * @method InternshipQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method InternshipQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method InternshipQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method InternshipQuery leftJoinOfferinternshipRelatedByOfferId($relationAlias = null) Adds a LEFT JOIN clause to the query using the OfferinternshipRelatedByOfferId relation
 * @method InternshipQuery rightJoinOfferinternshipRelatedByOfferId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OfferinternshipRelatedByOfferId relation
 * @method InternshipQuery innerJoinOfferinternshipRelatedByOfferId($relationAlias = null) Adds a INNER JOIN clause to the query using the OfferinternshipRelatedByOfferId relation
 *
 * @method InternshipQuery leftJoinUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the User relation
 * @method InternshipQuery rightJoinUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the User relation
 * @method InternshipQuery innerJoinUser($relationAlias = null) Adds a INNER JOIN clause to the query using the User relation
 *
 * @method InternshipQuery leftJoinCompany($relationAlias = null) Adds a LEFT JOIN clause to the query using the Company relation
 * @method InternshipQuery rightJoinCompany($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Company relation
 * @method InternshipQuery innerJoinCompany($relationAlias = null) Adds a INNER JOIN clause to the query using the Company relation
 *
 * @method InternshipQuery leftJoinOfferinternshipRelatedByInternshipId($relationAlias = null) Adds a LEFT JOIN clause to the query using the OfferinternshipRelatedByInternshipId relation
 * @method InternshipQuery rightJoinOfferinternshipRelatedByInternshipId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OfferinternshipRelatedByInternshipId relation
 * @method InternshipQuery innerJoinOfferinternshipRelatedByInternshipId($relationAlias = null) Adds a INNER JOIN clause to the query using the OfferinternshipRelatedByInternshipId relation
 *
 * @method InternshipQuery leftJoinDiaryweek($relationAlias = null) Adds a LEFT JOIN clause to the query using the Diaryweek relation
 * @method InternshipQuery rightJoinDiaryweek($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Diaryweek relation
 * @method InternshipQuery innerJoinDiaryweek($relationAlias = null) Adds a INNER JOIN clause to the query using the Diaryweek relation
 *
 * @method Internship findOne(PropelPDO $con = null) Return the first Internship matching the query
 * @method Internship findOneOrCreate(PropelPDO $con = null) Return the first Internship matching the query, or a new Internship object populated from the query conditions when no match is found
 *
 * @method Internship findOneByOfferId(int $offer_id) Return the first Internship filtered by the offer_id column
 * @method Internship findOneByUserId(int $user_id) Return the first Internship filtered by the user_id column
 * @method Internship findOneByCompanyId(int $company_id) Return the first Internship filtered by the company_id column
 * @method Internship findOneByTutorId(int $tutor_id) Return the first Internship filtered by the tutor_id column
 * @method Internship findOneByWorkFunction(string $work_function) Return the first Internship filtered by the work_function column
 * @method Internship findOneByDescription(string $description) Return the first Internship filtered by the description column
 * @method Internship findOneByInformation(string $information) Return the first Internship filtered by the information column
 * @method Internship findOneByUrlWebsiteDiffusion(string $url_website_diffusion) Return the first Internship filtered by the url_website_diffusion column
 * @method Internship findOneBySignatoryLastname(string $signatory_lastname) Return the first Internship filtered by the signatory_lastname column
 * @method Internship findOneBySignatoryFirstname(string $signatory_firstname) Return the first Internship filtered by the signatory_firstname column
 * @method Internship findOneBySignatoryFunction(string $signatory_function) Return the first Internship filtered by the signatory_function column
 * @method Internship findOneByMasterLastname(string $master_lastname) Return the first Internship filtered by the master_lastname column
 * @method Internship findOneByMasterFirstname(string $master_firstname) Return the first Internship filtered by the master_firstname column
 * @method Internship findOneByMasterEmail(string $master_email) Return the first Internship filtered by the master_email column
 * @method Internship findOneByMasterTelephone(string $master_telephone) Return the first Internship filtered by the master_telephone column
 * @method Internship findOneByValide(int $valide) Return the first Internship filtered by the valide column
 * @method Internship findOneByEtabli(int $etabli) Return the first Internship filtered by the etabli column
 * @method Internship findOneByDateVisite(string $date_visite) Return the first Internship filtered by the date_visite column
 * @method Internship findOneByDateSoutenance(string $date_soutenance) Return the first Internship filtered by the date_soutenance column
 * @method Internship findOneByInternshipBegining(string $internship_begining) Return the first Internship filtered by the internship_begining column
 * @method Internship findOneByInternshipEnding(string $internship_ending) Return the first Internship filtered by the internship_ending column
 *
 * @method array findById(int $id) Return Internship objects filtered by the id column
 * @method array findByOfferId(int $offer_id) Return Internship objects filtered by the offer_id column
 * @method array findByUserId(int $user_id) Return Internship objects filtered by the user_id column
 * @method array findByCompanyId(int $company_id) Return Internship objects filtered by the company_id column
 * @method array findByTutorId(int $tutor_id) Return Internship objects filtered by the tutor_id column
 * @method array findByWorkFunction(string $work_function) Return Internship objects filtered by the work_function column
 * @method array findByDescription(string $description) Return Internship objects filtered by the description column
 * @method array findByInformation(string $information) Return Internship objects filtered by the information column
 * @method array findByUrlWebsiteDiffusion(string $url_website_diffusion) Return Internship objects filtered by the url_website_diffusion column
 * @method array findBySignatoryLastname(string $signatory_lastname) Return Internship objects filtered by the signatory_lastname column
 * @method array findBySignatoryFirstname(string $signatory_firstname) Return Internship objects filtered by the signatory_firstname column
 * @method array findBySignatoryFunction(string $signatory_function) Return Internship objects filtered by the signatory_function column
 * @method array findByMasterLastname(string $master_lastname) Return Internship objects filtered by the master_lastname column
 * @method array findByMasterFirstname(string $master_firstname) Return Internship objects filtered by the master_firstname column
 * @method array findByMasterEmail(string $master_email) Return Internship objects filtered by the master_email column
 * @method array findByMasterTelephone(string $master_telephone) Return Internship objects filtered by the master_telephone column
 * @method array findByValide(int $valide) Return Internship objects filtered by the valide column
 * @method array findByEtabli(int $etabli) Return Internship objects filtered by the etabli column
 * @method array findByDateVisite(string $date_visite) Return Internship objects filtered by the date_visite column
 * @method array findByDateSoutenance(string $date_soutenance) Return Internship objects filtered by the date_soutenance column
 * @method array findByInternshipBegining(string $internship_begining) Return Internship objects filtered by the internship_begining column
 * @method array findByInternshipEnding(string $internship_ending) Return Internship objects filtered by the internship_ending column
 */
abstract class BaseInternshipQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseInternshipQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'default';
        }
        if (null === $modelName) {
            $modelName = 'MMIBundle\\Model\\Internship';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new InternshipQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   InternshipQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return InternshipQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof InternshipQuery) {
            return $criteria;
        }
        $query = new InternshipQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Internship|Internship[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = InternshipPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(InternshipPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Internship A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Internship A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `offer_id`, `user_id`, `company_id`, `tutor_id`, `work_function`, `description`, `information`, `url_website_diffusion`, `signatory_lastname`, `signatory_firstname`, `signatory_function`, `master_lastname`, `master_firstname`, `master_email`, `master_telephone`, `valide`, `etabli`, `date_visite`, `date_soutenance`, `internship_begining`, `internship_ending` FROM `internship` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Internship();
            $obj->hydrate($row);
            InternshipPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Internship|Internship[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Internship[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return InternshipQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(InternshipPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return InternshipQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(InternshipPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InternshipQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(InternshipPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(InternshipPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InternshipPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the offer_id column
     *
     * Example usage:
     * <code>
     * $query->filterByOfferId(1234); // WHERE offer_id = 1234
     * $query->filterByOfferId(array(12, 34)); // WHERE offer_id IN (12, 34)
     * $query->filterByOfferId(array('min' => 12)); // WHERE offer_id >= 12
     * $query->filterByOfferId(array('max' => 12)); // WHERE offer_id <= 12
     * </code>
     *
     * @see       filterByOfferinternshipRelatedByOfferId()
     *
     * @param     mixed $offerId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InternshipQuery The current query, for fluid interface
     */
    public function filterByOfferId($offerId = null, $comparison = null)
    {
        if (is_array($offerId)) {
            $useMinMax = false;
            if (isset($offerId['min'])) {
                $this->addUsingAlias(InternshipPeer::OFFER_ID, $offerId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($offerId['max'])) {
                $this->addUsingAlias(InternshipPeer::OFFER_ID, $offerId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InternshipPeer::OFFER_ID, $offerId, $comparison);
    }

    /**
     * Filter the query on the user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUserId(1234); // WHERE user_id = 1234
     * $query->filterByUserId(array(12, 34)); // WHERE user_id IN (12, 34)
     * $query->filterByUserId(array('min' => 12)); // WHERE user_id >= 12
     * $query->filterByUserId(array('max' => 12)); // WHERE user_id <= 12
     * </code>
     *
     * @see       filterByUser()
     *
     * @param     mixed $userId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InternshipQuery The current query, for fluid interface
     */
    public function filterByUserId($userId = null, $comparison = null)
    {
        if (is_array($userId)) {
            $useMinMax = false;
            if (isset($userId['min'])) {
                $this->addUsingAlias(InternshipPeer::USER_ID, $userId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userId['max'])) {
                $this->addUsingAlias(InternshipPeer::USER_ID, $userId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InternshipPeer::USER_ID, $userId, $comparison);
    }

    /**
     * Filter the query on the company_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCompanyId(1234); // WHERE company_id = 1234
     * $query->filterByCompanyId(array(12, 34)); // WHERE company_id IN (12, 34)
     * $query->filterByCompanyId(array('min' => 12)); // WHERE company_id >= 12
     * $query->filterByCompanyId(array('max' => 12)); // WHERE company_id <= 12
     * </code>
     *
     * @see       filterByCompany()
     *
     * @param     mixed $companyId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InternshipQuery The current query, for fluid interface
     */
    public function filterByCompanyId($companyId = null, $comparison = null)
    {
        if (is_array($companyId)) {
            $useMinMax = false;
            if (isset($companyId['min'])) {
                $this->addUsingAlias(InternshipPeer::COMPANY_ID, $companyId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($companyId['max'])) {
                $this->addUsingAlias(InternshipPeer::COMPANY_ID, $companyId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InternshipPeer::COMPANY_ID, $companyId, $comparison);
    }

    /**
     * Filter the query on the tutor_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTutorId(1234); // WHERE tutor_id = 1234
     * $query->filterByTutorId(array(12, 34)); // WHERE tutor_id IN (12, 34)
     * $query->filterByTutorId(array('min' => 12)); // WHERE tutor_id >= 12
     * $query->filterByTutorId(array('max' => 12)); // WHERE tutor_id <= 12
     * </code>
     *
     * @param     mixed $tutorId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InternshipQuery The current query, for fluid interface
     */
    public function filterByTutorId($tutorId = null, $comparison = null)
    {
        if (is_array($tutorId)) {
            $useMinMax = false;
            if (isset($tutorId['min'])) {
                $this->addUsingAlias(InternshipPeer::TUTOR_ID, $tutorId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tutorId['max'])) {
                $this->addUsingAlias(InternshipPeer::TUTOR_ID, $tutorId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InternshipPeer::TUTOR_ID, $tutorId, $comparison);
    }

    /**
     * Filter the query on the work_function column
     *
     * Example usage:
     * <code>
     * $query->filterByWorkFunction('fooValue');   // WHERE work_function = 'fooValue'
     * $query->filterByWorkFunction('%fooValue%'); // WHERE work_function LIKE '%fooValue%'
     * </code>
     *
     * @param     string $workFunction The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InternshipQuery The current query, for fluid interface
     */
    public function filterByWorkFunction($workFunction = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($workFunction)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $workFunction)) {
                $workFunction = str_replace('*', '%', $workFunction);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(InternshipPeer::WORK_FUNCTION, $workFunction, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InternshipQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $description)) {
                $description = str_replace('*', '%', $description);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(InternshipPeer::DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the information column
     *
     * Example usage:
     * <code>
     * $query->filterByInformation('fooValue');   // WHERE information = 'fooValue'
     * $query->filterByInformation('%fooValue%'); // WHERE information LIKE '%fooValue%'
     * </code>
     *
     * @param     string $information The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InternshipQuery The current query, for fluid interface
     */
    public function filterByInformation($information = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($information)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $information)) {
                $information = str_replace('*', '%', $information);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(InternshipPeer::INFORMATION, $information, $comparison);
    }

    /**
     * Filter the query on the url_website_diffusion column
     *
     * Example usage:
     * <code>
     * $query->filterByUrlWebsiteDiffusion('fooValue');   // WHERE url_website_diffusion = 'fooValue'
     * $query->filterByUrlWebsiteDiffusion('%fooValue%'); // WHERE url_website_diffusion LIKE '%fooValue%'
     * </code>
     *
     * @param     string $urlWebsiteDiffusion The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InternshipQuery The current query, for fluid interface
     */
    public function filterByUrlWebsiteDiffusion($urlWebsiteDiffusion = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($urlWebsiteDiffusion)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $urlWebsiteDiffusion)) {
                $urlWebsiteDiffusion = str_replace('*', '%', $urlWebsiteDiffusion);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(InternshipPeer::URL_WEBSITE_DIFFUSION, $urlWebsiteDiffusion, $comparison);
    }

    /**
     * Filter the query on the signatory_lastname column
     *
     * Example usage:
     * <code>
     * $query->filterBySignatoryLastname('fooValue');   // WHERE signatory_lastname = 'fooValue'
     * $query->filterBySignatoryLastname('%fooValue%'); // WHERE signatory_lastname LIKE '%fooValue%'
     * </code>
     *
     * @param     string $signatoryLastname The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InternshipQuery The current query, for fluid interface
     */
    public function filterBySignatoryLastname($signatoryLastname = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($signatoryLastname)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $signatoryLastname)) {
                $signatoryLastname = str_replace('*', '%', $signatoryLastname);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(InternshipPeer::SIGNATORY_LASTNAME, $signatoryLastname, $comparison);
    }

    /**
     * Filter the query on the signatory_firstname column
     *
     * Example usage:
     * <code>
     * $query->filterBySignatoryFirstname('fooValue');   // WHERE signatory_firstname = 'fooValue'
     * $query->filterBySignatoryFirstname('%fooValue%'); // WHERE signatory_firstname LIKE '%fooValue%'
     * </code>
     *
     * @param     string $signatoryFirstname The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InternshipQuery The current query, for fluid interface
     */
    public function filterBySignatoryFirstname($signatoryFirstname = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($signatoryFirstname)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $signatoryFirstname)) {
                $signatoryFirstname = str_replace('*', '%', $signatoryFirstname);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(InternshipPeer::SIGNATORY_FIRSTNAME, $signatoryFirstname, $comparison);
    }

    /**
     * Filter the query on the signatory_function column
     *
     * Example usage:
     * <code>
     * $query->filterBySignatoryFunction('fooValue');   // WHERE signatory_function = 'fooValue'
     * $query->filterBySignatoryFunction('%fooValue%'); // WHERE signatory_function LIKE '%fooValue%'
     * </code>
     *
     * @param     string $signatoryFunction The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InternshipQuery The current query, for fluid interface
     */
    public function filterBySignatoryFunction($signatoryFunction = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($signatoryFunction)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $signatoryFunction)) {
                $signatoryFunction = str_replace('*', '%', $signatoryFunction);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(InternshipPeer::SIGNATORY_FUNCTION, $signatoryFunction, $comparison);
    }

    /**
     * Filter the query on the master_lastname column
     *
     * Example usage:
     * <code>
     * $query->filterByMasterLastname('fooValue');   // WHERE master_lastname = 'fooValue'
     * $query->filterByMasterLastname('%fooValue%'); // WHERE master_lastname LIKE '%fooValue%'
     * </code>
     *
     * @param     string $masterLastname The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InternshipQuery The current query, for fluid interface
     */
    public function filterByMasterLastname($masterLastname = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($masterLastname)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $masterLastname)) {
                $masterLastname = str_replace('*', '%', $masterLastname);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(InternshipPeer::MASTER_LASTNAME, $masterLastname, $comparison);
    }

    /**
     * Filter the query on the master_firstname column
     *
     * Example usage:
     * <code>
     * $query->filterByMasterFirstname('fooValue');   // WHERE master_firstname = 'fooValue'
     * $query->filterByMasterFirstname('%fooValue%'); // WHERE master_firstname LIKE '%fooValue%'
     * </code>
     *
     * @param     string $masterFirstname The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InternshipQuery The current query, for fluid interface
     */
    public function filterByMasterFirstname($masterFirstname = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($masterFirstname)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $masterFirstname)) {
                $masterFirstname = str_replace('*', '%', $masterFirstname);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(InternshipPeer::MASTER_FIRSTNAME, $masterFirstname, $comparison);
    }

    /**
     * Filter the query on the master_email column
     *
     * Example usage:
     * <code>
     * $query->filterByMasterEmail('fooValue');   // WHERE master_email = 'fooValue'
     * $query->filterByMasterEmail('%fooValue%'); // WHERE master_email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $masterEmail The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InternshipQuery The current query, for fluid interface
     */
    public function filterByMasterEmail($masterEmail = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($masterEmail)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $masterEmail)) {
                $masterEmail = str_replace('*', '%', $masterEmail);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(InternshipPeer::MASTER_EMAIL, $masterEmail, $comparison);
    }

    /**
     * Filter the query on the master_telephone column
     *
     * Example usage:
     * <code>
     * $query->filterByMasterTelephone('fooValue');   // WHERE master_telephone = 'fooValue'
     * $query->filterByMasterTelephone('%fooValue%'); // WHERE master_telephone LIKE '%fooValue%'
     * </code>
     *
     * @param     string $masterTelephone The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InternshipQuery The current query, for fluid interface
     */
    public function filterByMasterTelephone($masterTelephone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($masterTelephone)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $masterTelephone)) {
                $masterTelephone = str_replace('*', '%', $masterTelephone);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(InternshipPeer::MASTER_TELEPHONE, $masterTelephone, $comparison);
    }

    /**
     * Filter the query on the valide column
     *
     * Example usage:
     * <code>
     * $query->filterByValide(1234); // WHERE valide = 1234
     * $query->filterByValide(array(12, 34)); // WHERE valide IN (12, 34)
     * $query->filterByValide(array('min' => 12)); // WHERE valide >= 12
     * $query->filterByValide(array('max' => 12)); // WHERE valide <= 12
     * </code>
     *
     * @param     mixed $valide The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InternshipQuery The current query, for fluid interface
     */
    public function filterByValide($valide = null, $comparison = null)
    {
        if (is_array($valide)) {
            $useMinMax = false;
            if (isset($valide['min'])) {
                $this->addUsingAlias(InternshipPeer::VALIDE, $valide['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valide['max'])) {
                $this->addUsingAlias(InternshipPeer::VALIDE, $valide['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InternshipPeer::VALIDE, $valide, $comparison);
    }

    /**
     * Filter the query on the etabli column
     *
     * Example usage:
     * <code>
     * $query->filterByEtabli(1234); // WHERE etabli = 1234
     * $query->filterByEtabli(array(12, 34)); // WHERE etabli IN (12, 34)
     * $query->filterByEtabli(array('min' => 12)); // WHERE etabli >= 12
     * $query->filterByEtabli(array('max' => 12)); // WHERE etabli <= 12
     * </code>
     *
     * @param     mixed $etabli The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InternshipQuery The current query, for fluid interface
     */
    public function filterByEtabli($etabli = null, $comparison = null)
    {
        if (is_array($etabli)) {
            $useMinMax = false;
            if (isset($etabli['min'])) {
                $this->addUsingAlias(InternshipPeer::ETABLI, $etabli['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($etabli['max'])) {
                $this->addUsingAlias(InternshipPeer::ETABLI, $etabli['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InternshipPeer::ETABLI, $etabli, $comparison);
    }

    /**
     * Filter the query on the date_visite column
     *
     * Example usage:
     * <code>
     * $query->filterByDateVisite('2011-03-14'); // WHERE date_visite = '2011-03-14'
     * $query->filterByDateVisite('now'); // WHERE date_visite = '2011-03-14'
     * $query->filterByDateVisite(array('max' => 'yesterday')); // WHERE date_visite < '2011-03-13'
     * </code>
     *
     * @param     mixed $dateVisite The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InternshipQuery The current query, for fluid interface
     */
    public function filterByDateVisite($dateVisite = null, $comparison = null)
    {
        if (is_array($dateVisite)) {
            $useMinMax = false;
            if (isset($dateVisite['min'])) {
                $this->addUsingAlias(InternshipPeer::DATE_VISITE, $dateVisite['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateVisite['max'])) {
                $this->addUsingAlias(InternshipPeer::DATE_VISITE, $dateVisite['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InternshipPeer::DATE_VISITE, $dateVisite, $comparison);
    }

    /**
     * Filter the query on the date_soutenance column
     *
     * Example usage:
     * <code>
     * $query->filterByDateSoutenance('2011-03-14'); // WHERE date_soutenance = '2011-03-14'
     * $query->filterByDateSoutenance('now'); // WHERE date_soutenance = '2011-03-14'
     * $query->filterByDateSoutenance(array('max' => 'yesterday')); // WHERE date_soutenance < '2011-03-13'
     * </code>
     *
     * @param     mixed $dateSoutenance The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InternshipQuery The current query, for fluid interface
     */
    public function filterByDateSoutenance($dateSoutenance = null, $comparison = null)
    {
        if (is_array($dateSoutenance)) {
            $useMinMax = false;
            if (isset($dateSoutenance['min'])) {
                $this->addUsingAlias(InternshipPeer::DATE_SOUTENANCE, $dateSoutenance['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateSoutenance['max'])) {
                $this->addUsingAlias(InternshipPeer::DATE_SOUTENANCE, $dateSoutenance['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InternshipPeer::DATE_SOUTENANCE, $dateSoutenance, $comparison);
    }

    /**
     * Filter the query on the internship_begining column
     *
     * Example usage:
     * <code>
     * $query->filterByInternshipBegining('2011-03-14'); // WHERE internship_begining = '2011-03-14'
     * $query->filterByInternshipBegining('now'); // WHERE internship_begining = '2011-03-14'
     * $query->filterByInternshipBegining(array('max' => 'yesterday')); // WHERE internship_begining < '2011-03-13'
     * </code>
     *
     * @param     mixed $internshipBegining The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InternshipQuery The current query, for fluid interface
     */
    public function filterByInternshipBegining($internshipBegining = null, $comparison = null)
    {
        if (is_array($internshipBegining)) {
            $useMinMax = false;
            if (isset($internshipBegining['min'])) {
                $this->addUsingAlias(InternshipPeer::INTERNSHIP_BEGINING, $internshipBegining['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($internshipBegining['max'])) {
                $this->addUsingAlias(InternshipPeer::INTERNSHIP_BEGINING, $internshipBegining['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InternshipPeer::INTERNSHIP_BEGINING, $internshipBegining, $comparison);
    }

    /**
     * Filter the query on the internship_ending column
     *
     * Example usage:
     * <code>
     * $query->filterByInternshipEnding('2011-03-14'); // WHERE internship_ending = '2011-03-14'
     * $query->filterByInternshipEnding('now'); // WHERE internship_ending = '2011-03-14'
     * $query->filterByInternshipEnding(array('max' => 'yesterday')); // WHERE internship_ending < '2011-03-13'
     * </code>
     *
     * @param     mixed $internshipEnding The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return InternshipQuery The current query, for fluid interface
     */
    public function filterByInternshipEnding($internshipEnding = null, $comparison = null)
    {
        if (is_array($internshipEnding)) {
            $useMinMax = false;
            if (isset($internshipEnding['min'])) {
                $this->addUsingAlias(InternshipPeer::INTERNSHIP_ENDING, $internshipEnding['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($internshipEnding['max'])) {
                $this->addUsingAlias(InternshipPeer::INTERNSHIP_ENDING, $internshipEnding['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(InternshipPeer::INTERNSHIP_ENDING, $internshipEnding, $comparison);
    }

    /**
     * Filter the query by a related Offerinternship object
     *
     * @param   Offerinternship|PropelObjectCollection $offerinternship The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 InternshipQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByOfferinternshipRelatedByOfferId($offerinternship, $comparison = null)
    {
        if ($offerinternship instanceof Offerinternship) {
            return $this
                ->addUsingAlias(InternshipPeer::OFFER_ID, $offerinternship->getId(), $comparison);
        } elseif ($offerinternship instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(InternshipPeer::OFFER_ID, $offerinternship->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByOfferinternshipRelatedByOfferId() only accepts arguments of type Offerinternship or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OfferinternshipRelatedByOfferId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return InternshipQuery The current query, for fluid interface
     */
    public function joinOfferinternshipRelatedByOfferId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OfferinternshipRelatedByOfferId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OfferinternshipRelatedByOfferId');
        }

        return $this;
    }

    /**
     * Use the OfferinternshipRelatedByOfferId relation Offerinternship object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MMIBundle\Model\OfferinternshipQuery A secondary query class using the current class as primary query
     */
    public function useOfferinternshipRelatedByOfferIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinOfferinternshipRelatedByOfferId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OfferinternshipRelatedByOfferId', '\MMIBundle\Model\OfferinternshipQuery');
    }

    /**
     * Filter the query by a related User object
     *
     * @param   User|PropelObjectCollection $user The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 InternshipQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByUser($user, $comparison = null)
    {
        if ($user instanceof User) {
            return $this
                ->addUsingAlias(InternshipPeer::USER_ID, $user->getId(), $comparison);
        } elseif ($user instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(InternshipPeer::USER_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUser() only accepts arguments of type User or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the User relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return InternshipQuery The current query, for fluid interface
     */
    public function joinUser($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('User');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'User');
        }

        return $this;
    }

    /**
     * Use the User relation User object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MMIBundle\Model\UserQuery A secondary query class using the current class as primary query
     */
    public function useUserQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'User', '\MMIBundle\Model\UserQuery');
    }

    /**
     * Filter the query by a related Company object
     *
     * @param   Company|PropelObjectCollection $company The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 InternshipQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCompany($company, $comparison = null)
    {
        if ($company instanceof Company) {
            return $this
                ->addUsingAlias(InternshipPeer::COMPANY_ID, $company->getId(), $comparison);
        } elseif ($company instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(InternshipPeer::COMPANY_ID, $company->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCompany() only accepts arguments of type Company or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Company relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return InternshipQuery The current query, for fluid interface
     */
    public function joinCompany($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Company');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Company');
        }

        return $this;
    }

    /**
     * Use the Company relation Company object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MMIBundle\Model\CompanyQuery A secondary query class using the current class as primary query
     */
    public function useCompanyQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCompany($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Company', '\MMIBundle\Model\CompanyQuery');
    }

    /**
     * Filter the query by a related Offerinternship object
     *
     * @param   Offerinternship|PropelObjectCollection $offerinternship  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 InternshipQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByOfferinternshipRelatedByInternshipId($offerinternship, $comparison = null)
    {
        if ($offerinternship instanceof Offerinternship) {
            return $this
                ->addUsingAlias(InternshipPeer::ID, $offerinternship->getInternshipId(), $comparison);
        } elseif ($offerinternship instanceof PropelObjectCollection) {
            return $this
                ->useOfferinternshipRelatedByInternshipIdQuery()
                ->filterByPrimaryKeys($offerinternship->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByOfferinternshipRelatedByInternshipId() only accepts arguments of type Offerinternship or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OfferinternshipRelatedByInternshipId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return InternshipQuery The current query, for fluid interface
     */
    public function joinOfferinternshipRelatedByInternshipId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OfferinternshipRelatedByInternshipId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OfferinternshipRelatedByInternshipId');
        }

        return $this;
    }

    /**
     * Use the OfferinternshipRelatedByInternshipId relation Offerinternship object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MMIBundle\Model\OfferinternshipQuery A secondary query class using the current class as primary query
     */
    public function useOfferinternshipRelatedByInternshipIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinOfferinternshipRelatedByInternshipId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OfferinternshipRelatedByInternshipId', '\MMIBundle\Model\OfferinternshipQuery');
    }

    /**
     * Filter the query by a related Diaryweek object
     *
     * @param   Diaryweek|PropelObjectCollection $diaryweek  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 InternshipQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByDiaryweek($diaryweek, $comparison = null)
    {
        if ($diaryweek instanceof Diaryweek) {
            return $this
                ->addUsingAlias(InternshipPeer::ID, $diaryweek->getInternshipId(), $comparison);
        } elseif ($diaryweek instanceof PropelObjectCollection) {
            return $this
                ->useDiaryweekQuery()
                ->filterByPrimaryKeys($diaryweek->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByDiaryweek() only accepts arguments of type Diaryweek or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Diaryweek relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return InternshipQuery The current query, for fluid interface
     */
    public function joinDiaryweek($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Diaryweek');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Diaryweek');
        }

        return $this;
    }

    /**
     * Use the Diaryweek relation Diaryweek object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MMIBundle\Model\DiaryweekQuery A secondary query class using the current class as primary query
     */
    public function useDiaryweekQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinDiaryweek($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Diaryweek', '\MMIBundle\Model\DiaryweekQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Internship $internship Object to remove from the list of results
     *
     * @return InternshipQuery The current query, for fluid interface
     */
    public function prune($internship = null)
    {
        if ($internship) {
            $this->addUsingAlias(InternshipPeer::ID, $internship->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
