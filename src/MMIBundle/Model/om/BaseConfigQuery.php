<?php

namespace MMIBundle\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \PDO;
use \Propel;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use MMIBundle\Model\Config;
use MMIBundle\Model\ConfigPeer;
use MMIBundle\Model\ConfigQuery;

/**
 * @method ConfigQuery orderById($order = Criteria::ASC) Order by the id column
 * @method ConfigQuery orderByInternshipBegining($order = Criteria::ASC) Order by the internship_begining column
 * @method ConfigQuery orderByInternshipEnding($order = Criteria::ASC) Order by the internship_ending column
 *
 * @method ConfigQuery groupById() Group by the id column
 * @method ConfigQuery groupByInternshipBegining() Group by the internship_begining column
 * @method ConfigQuery groupByInternshipEnding() Group by the internship_ending column
 *
 * @method ConfigQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method ConfigQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method ConfigQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method Config findOne(PropelPDO $con = null) Return the first Config matching the query
 * @method Config findOneOrCreate(PropelPDO $con = null) Return the first Config matching the query, or a new Config object populated from the query conditions when no match is found
 *
 * @method Config findOneByInternshipBegining(string $internship_begining) Return the first Config filtered by the internship_begining column
 * @method Config findOneByInternshipEnding(string $internship_ending) Return the first Config filtered by the internship_ending column
 *
 * @method array findById(int $id) Return Config objects filtered by the id column
 * @method array findByInternshipBegining(string $internship_begining) Return Config objects filtered by the internship_begining column
 * @method array findByInternshipEnding(string $internship_ending) Return Config objects filtered by the internship_ending column
 */
abstract class BaseConfigQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseConfigQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'default';
        }
        if (null === $modelName) {
            $modelName = 'MMIBundle\\Model\\Config';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ConfigQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   ConfigQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ConfigQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ConfigQuery) {
            return $criteria;
        }
        $query = new ConfigQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Config|Config[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ConfigPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ConfigPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Config A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Config A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `internship_begining`, `internship_ending` FROM `config` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Config();
            $obj->hydrate($row);
            ConfigPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Config|Config[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Config[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ConfigQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ConfigPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ConfigQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ConfigPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ConfigQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ConfigPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ConfigPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ConfigPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the internship_begining column
     *
     * Example usage:
     * <code>
     * $query->filterByInternshipBegining('2011-03-14'); // WHERE internship_begining = '2011-03-14'
     * $query->filterByInternshipBegining('now'); // WHERE internship_begining = '2011-03-14'
     * $query->filterByInternshipBegining(array('max' => 'yesterday')); // WHERE internship_begining < '2011-03-13'
     * </code>
     *
     * @param     mixed $internshipBegining The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ConfigQuery The current query, for fluid interface
     */
    public function filterByInternshipBegining($internshipBegining = null, $comparison = null)
    {
        if (is_array($internshipBegining)) {
            $useMinMax = false;
            if (isset($internshipBegining['min'])) {
                $this->addUsingAlias(ConfigPeer::INTERNSHIP_BEGINING, $internshipBegining['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($internshipBegining['max'])) {
                $this->addUsingAlias(ConfigPeer::INTERNSHIP_BEGINING, $internshipBegining['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ConfigPeer::INTERNSHIP_BEGINING, $internshipBegining, $comparison);
    }

    /**
     * Filter the query on the internship_ending column
     *
     * Example usage:
     * <code>
     * $query->filterByInternshipEnding('2011-03-14'); // WHERE internship_ending = '2011-03-14'
     * $query->filterByInternshipEnding('now'); // WHERE internship_ending = '2011-03-14'
     * $query->filterByInternshipEnding(array('max' => 'yesterday')); // WHERE internship_ending < '2011-03-13'
     * </code>
     *
     * @param     mixed $internshipEnding The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ConfigQuery The current query, for fluid interface
     */
    public function filterByInternshipEnding($internshipEnding = null, $comparison = null)
    {
        if (is_array($internshipEnding)) {
            $useMinMax = false;
            if (isset($internshipEnding['min'])) {
                $this->addUsingAlias(ConfigPeer::INTERNSHIP_ENDING, $internshipEnding['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($internshipEnding['max'])) {
                $this->addUsingAlias(ConfigPeer::INTERNSHIP_ENDING, $internshipEnding['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ConfigPeer::INTERNSHIP_ENDING, $internshipEnding, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Config $config Object to remove from the list of results
     *
     * @return ConfigQuery The current query, for fluid interface
     */
    public function prune($config = null)
    {
        if ($config) {
            $this->addUsingAlias(ConfigPeer::ID, $config->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
