<?php

namespace MMIBundle\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use MMIBundle\Model\Company;
use MMIBundle\Model\CompanyQuery;
use MMIBundle\Model\Diaryweek;
use MMIBundle\Model\DiaryweekQuery;
use MMIBundle\Model\Internship;
use MMIBundle\Model\InternshipPeer;
use MMIBundle\Model\InternshipQuery;
use MMIBundle\Model\Offerinternship;
use MMIBundle\Model\OfferinternshipQuery;
use MMIBundle\Model\User;
use MMIBundle\Model\UserQuery;

abstract class BaseInternship extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'MMIBundle\\Model\\InternshipPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        InternshipPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the offer_id field.
     * @var        int
     */
    protected $offer_id;

    /**
     * The value for the user_id field.
     * @var        int
     */
    protected $user_id;

    /**
     * The value for the company_id field.
     * @var        int
     */
    protected $company_id;

    /**
     * The value for the tutor_id field.
     * @var        int
     */
    protected $tutor_id;

    /**
     * The value for the work_function field.
     * @var        string
     */
    protected $work_function;

    /**
     * The value for the description field.
     * @var        string
     */
    protected $description;

    /**
     * The value for the information field.
     * @var        string
     */
    protected $information;

    /**
     * The value for the url_website_diffusion field.
     * @var        string
     */
    protected $url_website_diffusion;

    /**
     * The value for the signatory_lastname field.
     * @var        string
     */
    protected $signatory_lastname;

    /**
     * The value for the signatory_firstname field.
     * @var        string
     */
    protected $signatory_firstname;

    /**
     * The value for the signatory_function field.
     * @var        string
     */
    protected $signatory_function;

    /**
     * The value for the master_lastname field.
     * @var        string
     */
    protected $master_lastname;

    /**
     * The value for the master_firstname field.
     * @var        string
     */
    protected $master_firstname;

    /**
     * The value for the master_email field.
     * @var        string
     */
    protected $master_email;

    /**
     * The value for the master_telephone field.
     * @var        string
     */
    protected $master_telephone;

    /**
     * The value for the valide field.
     * @var        int
     */
    protected $valide;

    /**
     * The value for the etabli field.
     * @var        int
     */
    protected $etabli;

    /**
     * The value for the date_visite field.
     * @var        string
     */
    protected $date_visite;

    /**
     * The value for the date_soutenance field.
     * @var        string
     */
    protected $date_soutenance;

    /**
     * The value for the internship_begining field.
     * @var        string
     */
    protected $internship_begining;

    /**
     * The value for the internship_ending field.
     * @var        string
     */
    protected $internship_ending;

    /**
     * @var        Offerinternship
     */
    protected $aOfferinternshipRelatedByOfferId;

    /**
     * @var        User
     */
    protected $aUser;

    /**
     * @var        Company
     */
    protected $aCompany;

    /**
     * @var        PropelObjectCollection|Offerinternship[] Collection to store aggregation of Offerinternship objects.
     */
    protected $collOfferinternshipsRelatedByInternshipId;
    protected $collOfferinternshipsRelatedByInternshipIdPartial;

    /**
     * @var        PropelObjectCollection|Diaryweek[] Collection to store aggregation of Diaryweek objects.
     */
    protected $collDiaryweeks;
    protected $collDiaryweeksPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $offerinternshipsRelatedByInternshipIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $diaryweeksScheduledForDeletion = null;

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [offer_id] column value.
     *
     * @return int
     */
    public function getOfferId()
    {

        return $this->offer_id;
    }

    /**
     * Get the [user_id] column value.
     *
     * @return int
     */
    public function getUserId()
    {

        return $this->user_id;
    }

    /**
     * Get the [company_id] column value.
     *
     * @return int
     */
    public function getCompanyId()
    {

        return $this->company_id;
    }

    /**
     * Get the [tutor_id] column value.
     *
     * @return int
     */
    public function getTutorId()
    {

        return $this->tutor_id;
    }

    /**
     * Get the [work_function] column value.
     *
     * @return string
     */
    public function getWorkFunction()
    {

        return $this->work_function;
    }

    /**
     * Get the [description] column value.
     *
     * @return string
     */
    public function getDescription()
    {

        return $this->description;
    }

    /**
     * Get the [information] column value.
     *
     * @return string
     */
    public function getInformation()
    {

        return $this->information;
    }

    /**
     * Get the [url_website_diffusion] column value.
     *
     * @return string
     */
    public function getUrlWebsiteDiffusion()
    {

        return $this->url_website_diffusion;
    }

    /**
     * Get the [signatory_lastname] column value.
     *
     * @return string
     */
    public function getSignatoryLastname()
    {

        return $this->signatory_lastname;
    }

    /**
     * Get the [signatory_firstname] column value.
     *
     * @return string
     */
    public function getSignatoryFirstname()
    {

        return $this->signatory_firstname;
    }

    /**
     * Get the [signatory_function] column value.
     *
     * @return string
     */
    public function getSignatoryFunction()
    {

        return $this->signatory_function;
    }

    /**
     * Get the [master_lastname] column value.
     *
     * @return string
     */
    public function getMasterLastname()
    {

        return $this->master_lastname;
    }

    /**
     * Get the [master_firstname] column value.
     *
     * @return string
     */
    public function getMasterFirstname()
    {

        return $this->master_firstname;
    }

    /**
     * Get the [master_email] column value.
     *
     * @return string
     */
    public function getMasterEmail()
    {

        return $this->master_email;
    }

    /**
     * Get the [master_telephone] column value.
     *
     * @return string
     */
    public function getMasterTelephone()
    {

        return $this->master_telephone;
    }

    /**
     * Get the [valide] column value.
     *
     * @return int
     */
    public function getValide()
    {

        return $this->valide;
    }

    /**
     * Get the [etabli] column value.
     *
     * @return int
     */
    public function getEtabli()
    {

        return $this->etabli;
    }

    /**
     * Get the [optionally formatted] temporal [date_visite] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateVisite($format = null)
    {
        if ($this->date_visite === null) {
            return null;
        }

        if ($this->date_visite === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_visite);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_visite, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [date_soutenance] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateSoutenance($format = null)
    {
        if ($this->date_soutenance === null) {
            return null;
        }

        if ($this->date_soutenance === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_soutenance);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_soutenance, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [internship_begining] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getInternshipBegining($format = null)
    {
        if ($this->internship_begining === null) {
            return null;
        }

        if ($this->internship_begining === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->internship_begining);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->internship_begining, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [internship_ending] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getInternshipEnding($format = null)
    {
        if ($this->internship_ending === null) {
            return null;
        }

        if ($this->internship_ending === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->internship_ending);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->internship_ending, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Set the value of [id] column.
     *
     * @param  int $v new value
     * @return Internship The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = InternshipPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [offer_id] column.
     *
     * @param  int $v new value
     * @return Internship The current object (for fluent API support)
     */
    public function setOfferId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->offer_id !== $v) {
            $this->offer_id = $v;
            $this->modifiedColumns[] = InternshipPeer::OFFER_ID;
        }

        if ($this->aOfferinternshipRelatedByOfferId !== null && $this->aOfferinternshipRelatedByOfferId->getId() !== $v) {
            $this->aOfferinternshipRelatedByOfferId = null;
        }


        return $this;
    } // setOfferId()

    /**
     * Set the value of [user_id] column.
     *
     * @param  int $v new value
     * @return Internship The current object (for fluent API support)
     */
    public function setUserId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->user_id !== $v) {
            $this->user_id = $v;
            $this->modifiedColumns[] = InternshipPeer::USER_ID;
        }

        if ($this->aUser !== null && $this->aUser->getId() !== $v) {
            $this->aUser = null;
        }


        return $this;
    } // setUserId()

    /**
     * Set the value of [company_id] column.
     *
     * @param  int $v new value
     * @return Internship The current object (for fluent API support)
     */
    public function setCompanyId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->company_id !== $v) {
            $this->company_id = $v;
            $this->modifiedColumns[] = InternshipPeer::COMPANY_ID;
        }

        if ($this->aCompany !== null && $this->aCompany->getId() !== $v) {
            $this->aCompany = null;
        }


        return $this;
    } // setCompanyId()

    /**
     * Set the value of [tutor_id] column.
     *
     * @param  int $v new value
     * @return Internship The current object (for fluent API support)
     */
    public function setTutorId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->tutor_id !== $v) {
            $this->tutor_id = $v;
            $this->modifiedColumns[] = InternshipPeer::TUTOR_ID;
        }


        return $this;
    } // setTutorId()

    /**
     * Set the value of [work_function] column.
     *
     * @param  string $v new value
     * @return Internship The current object (for fluent API support)
     */
    public function setWorkFunction($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->work_function !== $v) {
            $this->work_function = $v;
            $this->modifiedColumns[] = InternshipPeer::WORK_FUNCTION;
        }


        return $this;
    } // setWorkFunction()

    /**
     * Set the value of [description] column.
     *
     * @param  string $v new value
     * @return Internship The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->description !== $v) {
            $this->description = $v;
            $this->modifiedColumns[] = InternshipPeer::DESCRIPTION;
        }


        return $this;
    } // setDescription()

    /**
     * Set the value of [information] column.
     *
     * @param  string $v new value
     * @return Internship The current object (for fluent API support)
     */
    public function setInformation($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->information !== $v) {
            $this->information = $v;
            $this->modifiedColumns[] = InternshipPeer::INFORMATION;
        }


        return $this;
    } // setInformation()

    /**
     * Set the value of [url_website_diffusion] column.
     *
     * @param  string $v new value
     * @return Internship The current object (for fluent API support)
     */
    public function setUrlWebsiteDiffusion($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->url_website_diffusion !== $v) {
            $this->url_website_diffusion = $v;
            $this->modifiedColumns[] = InternshipPeer::URL_WEBSITE_DIFFUSION;
        }


        return $this;
    } // setUrlWebsiteDiffusion()

    /**
     * Set the value of [signatory_lastname] column.
     *
     * @param  string $v new value
     * @return Internship The current object (for fluent API support)
     */
    public function setSignatoryLastname($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->signatory_lastname !== $v) {
            $this->signatory_lastname = $v;
            $this->modifiedColumns[] = InternshipPeer::SIGNATORY_LASTNAME;
        }


        return $this;
    } // setSignatoryLastname()

    /**
     * Set the value of [signatory_firstname] column.
     *
     * @param  string $v new value
     * @return Internship The current object (for fluent API support)
     */
    public function setSignatoryFirstname($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->signatory_firstname !== $v) {
            $this->signatory_firstname = $v;
            $this->modifiedColumns[] = InternshipPeer::SIGNATORY_FIRSTNAME;
        }


        return $this;
    } // setSignatoryFirstname()

    /**
     * Set the value of [signatory_function] column.
     *
     * @param  string $v new value
     * @return Internship The current object (for fluent API support)
     */
    public function setSignatoryFunction($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->signatory_function !== $v) {
            $this->signatory_function = $v;
            $this->modifiedColumns[] = InternshipPeer::SIGNATORY_FUNCTION;
        }


        return $this;
    } // setSignatoryFunction()

    /**
     * Set the value of [master_lastname] column.
     *
     * @param  string $v new value
     * @return Internship The current object (for fluent API support)
     */
    public function setMasterLastname($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->master_lastname !== $v) {
            $this->master_lastname = $v;
            $this->modifiedColumns[] = InternshipPeer::MASTER_LASTNAME;
        }


        return $this;
    } // setMasterLastname()

    /**
     * Set the value of [master_firstname] column.
     *
     * @param  string $v new value
     * @return Internship The current object (for fluent API support)
     */
    public function setMasterFirstname($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->master_firstname !== $v) {
            $this->master_firstname = $v;
            $this->modifiedColumns[] = InternshipPeer::MASTER_FIRSTNAME;
        }


        return $this;
    } // setMasterFirstname()

    /**
     * Set the value of [master_email] column.
     *
     * @param  string $v new value
     * @return Internship The current object (for fluent API support)
     */
    public function setMasterEmail($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->master_email !== $v) {
            $this->master_email = $v;
            $this->modifiedColumns[] = InternshipPeer::MASTER_EMAIL;
        }


        return $this;
    } // setMasterEmail()

    /**
     * Set the value of [master_telephone] column.
     *
     * @param  string $v new value
     * @return Internship The current object (for fluent API support)
     */
    public function setMasterTelephone($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->master_telephone !== $v) {
            $this->master_telephone = $v;
            $this->modifiedColumns[] = InternshipPeer::MASTER_TELEPHONE;
        }


        return $this;
    } // setMasterTelephone()

    /**
     * Set the value of [valide] column.
     *
     * @param  int $v new value
     * @return Internship The current object (for fluent API support)
     */
    public function setValide($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->valide !== $v) {
            $this->valide = $v;
            $this->modifiedColumns[] = InternshipPeer::VALIDE;
        }


        return $this;
    } // setValide()

    /**
     * Set the value of [etabli] column.
     *
     * @param  int $v new value
     * @return Internship The current object (for fluent API support)
     */
    public function setEtabli($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->etabli !== $v) {
            $this->etabli = $v;
            $this->modifiedColumns[] = InternshipPeer::ETABLI;
        }


        return $this;
    } // setEtabli()

    /**
     * Sets the value of [date_visite] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Internship The current object (for fluent API support)
     */
    public function setDateVisite($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_visite !== null || $dt !== null) {
            $currentDateAsString = ($this->date_visite !== null && $tmpDt = new DateTime($this->date_visite)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_visite = $newDateAsString;
                $this->modifiedColumns[] = InternshipPeer::DATE_VISITE;
            }
        } // if either are not null


        return $this;
    } // setDateVisite()

    /**
     * Sets the value of [date_soutenance] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Internship The current object (for fluent API support)
     */
    public function setDateSoutenance($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_soutenance !== null || $dt !== null) {
            $currentDateAsString = ($this->date_soutenance !== null && $tmpDt = new DateTime($this->date_soutenance)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_soutenance = $newDateAsString;
                $this->modifiedColumns[] = InternshipPeer::DATE_SOUTENANCE;
            }
        } // if either are not null


        return $this;
    } // setDateSoutenance()

    /**
     * Sets the value of [internship_begining] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Internship The current object (for fluent API support)
     */
    public function setInternshipBegining($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->internship_begining !== null || $dt !== null) {
            $currentDateAsString = ($this->internship_begining !== null && $tmpDt = new DateTime($this->internship_begining)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->internship_begining = $newDateAsString;
                $this->modifiedColumns[] = InternshipPeer::INTERNSHIP_BEGINING;
            }
        } // if either are not null


        return $this;
    } // setInternshipBegining()

    /**
     * Sets the value of [internship_ending] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return Internship The current object (for fluent API support)
     */
    public function setInternshipEnding($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->internship_ending !== null || $dt !== null) {
            $currentDateAsString = ($this->internship_ending !== null && $tmpDt = new DateTime($this->internship_ending)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->internship_ending = $newDateAsString;
                $this->modifiedColumns[] = InternshipPeer::INTERNSHIP_ENDING;
            }
        } // if either are not null


        return $this;
    } // setInternshipEnding()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->offer_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->user_id = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->company_id = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
            $this->tutor_id = ($row[$startcol + 4] !== null) ? (int) $row[$startcol + 4] : null;
            $this->work_function = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->description = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->information = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->url_website_diffusion = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->signatory_lastname = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->signatory_firstname = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->signatory_function = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->master_lastname = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->master_firstname = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->master_email = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
            $this->master_telephone = ($row[$startcol + 15] !== null) ? (string) $row[$startcol + 15] : null;
            $this->valide = ($row[$startcol + 16] !== null) ? (int) $row[$startcol + 16] : null;
            $this->etabli = ($row[$startcol + 17] !== null) ? (int) $row[$startcol + 17] : null;
            $this->date_visite = ($row[$startcol + 18] !== null) ? (string) $row[$startcol + 18] : null;
            $this->date_soutenance = ($row[$startcol + 19] !== null) ? (string) $row[$startcol + 19] : null;
            $this->internship_begining = ($row[$startcol + 20] !== null) ? (string) $row[$startcol + 20] : null;
            $this->internship_ending = ($row[$startcol + 21] !== null) ? (string) $row[$startcol + 21] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 22; // 22 = InternshipPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Internship object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aOfferinternshipRelatedByOfferId !== null && $this->offer_id !== $this->aOfferinternshipRelatedByOfferId->getId()) {
            $this->aOfferinternshipRelatedByOfferId = null;
        }
        if ($this->aUser !== null && $this->user_id !== $this->aUser->getId()) {
            $this->aUser = null;
        }
        if ($this->aCompany !== null && $this->company_id !== $this->aCompany->getId()) {
            $this->aCompany = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(InternshipPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = InternshipPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aOfferinternshipRelatedByOfferId = null;
            $this->aUser = null;
            $this->aCompany = null;
            $this->collOfferinternshipsRelatedByInternshipId = null;

            $this->collDiaryweeks = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(InternshipPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = InternshipQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(InternshipPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                InternshipPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aOfferinternshipRelatedByOfferId !== null) {
                if ($this->aOfferinternshipRelatedByOfferId->isModified() || $this->aOfferinternshipRelatedByOfferId->isNew()) {
                    $affectedRows += $this->aOfferinternshipRelatedByOfferId->save($con);
                }
                $this->setOfferinternshipRelatedByOfferId($this->aOfferinternshipRelatedByOfferId);
            }

            if ($this->aUser !== null) {
                if ($this->aUser->isModified() || $this->aUser->isNew()) {
                    $affectedRows += $this->aUser->save($con);
                }
                $this->setUser($this->aUser);
            }

            if ($this->aCompany !== null) {
                if ($this->aCompany->isModified() || $this->aCompany->isNew()) {
                    $affectedRows += $this->aCompany->save($con);
                }
                $this->setCompany($this->aCompany);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->offerinternshipsRelatedByInternshipIdScheduledForDeletion !== null) {
                if (!$this->offerinternshipsRelatedByInternshipIdScheduledForDeletion->isEmpty()) {
                    OfferinternshipQuery::create()
                        ->filterByPrimaryKeys($this->offerinternshipsRelatedByInternshipIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->offerinternshipsRelatedByInternshipIdScheduledForDeletion = null;
                }
            }

            if ($this->collOfferinternshipsRelatedByInternshipId !== null) {
                foreach ($this->collOfferinternshipsRelatedByInternshipId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->diaryweeksScheduledForDeletion !== null) {
                if (!$this->diaryweeksScheduledForDeletion->isEmpty()) {
                    DiaryweekQuery::create()
                        ->filterByPrimaryKeys($this->diaryweeksScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->diaryweeksScheduledForDeletion = null;
                }
            }

            if ($this->collDiaryweeks !== null) {
                foreach ($this->collDiaryweeks as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = InternshipPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . InternshipPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(InternshipPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(InternshipPeer::OFFER_ID)) {
            $modifiedColumns[':p' . $index++]  = '`offer_id`';
        }
        if ($this->isColumnModified(InternshipPeer::USER_ID)) {
            $modifiedColumns[':p' . $index++]  = '`user_id`';
        }
        if ($this->isColumnModified(InternshipPeer::COMPANY_ID)) {
            $modifiedColumns[':p' . $index++]  = '`company_id`';
        }
        if ($this->isColumnModified(InternshipPeer::TUTOR_ID)) {
            $modifiedColumns[':p' . $index++]  = '`tutor_id`';
        }
        if ($this->isColumnModified(InternshipPeer::WORK_FUNCTION)) {
            $modifiedColumns[':p' . $index++]  = '`work_function`';
        }
        if ($this->isColumnModified(InternshipPeer::DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = '`description`';
        }
        if ($this->isColumnModified(InternshipPeer::INFORMATION)) {
            $modifiedColumns[':p' . $index++]  = '`information`';
        }
        if ($this->isColumnModified(InternshipPeer::URL_WEBSITE_DIFFUSION)) {
            $modifiedColumns[':p' . $index++]  = '`url_website_diffusion`';
        }
        if ($this->isColumnModified(InternshipPeer::SIGNATORY_LASTNAME)) {
            $modifiedColumns[':p' . $index++]  = '`signatory_lastname`';
        }
        if ($this->isColumnModified(InternshipPeer::SIGNATORY_FIRSTNAME)) {
            $modifiedColumns[':p' . $index++]  = '`signatory_firstname`';
        }
        if ($this->isColumnModified(InternshipPeer::SIGNATORY_FUNCTION)) {
            $modifiedColumns[':p' . $index++]  = '`signatory_function`';
        }
        if ($this->isColumnModified(InternshipPeer::MASTER_LASTNAME)) {
            $modifiedColumns[':p' . $index++]  = '`master_lastname`';
        }
        if ($this->isColumnModified(InternshipPeer::MASTER_FIRSTNAME)) {
            $modifiedColumns[':p' . $index++]  = '`master_firstname`';
        }
        if ($this->isColumnModified(InternshipPeer::MASTER_EMAIL)) {
            $modifiedColumns[':p' . $index++]  = '`master_email`';
        }
        if ($this->isColumnModified(InternshipPeer::MASTER_TELEPHONE)) {
            $modifiedColumns[':p' . $index++]  = '`master_telephone`';
        }
        if ($this->isColumnModified(InternshipPeer::VALIDE)) {
            $modifiedColumns[':p' . $index++]  = '`valide`';
        }
        if ($this->isColumnModified(InternshipPeer::ETABLI)) {
            $modifiedColumns[':p' . $index++]  = '`etabli`';
        }
        if ($this->isColumnModified(InternshipPeer::DATE_VISITE)) {
            $modifiedColumns[':p' . $index++]  = '`date_visite`';
        }
        if ($this->isColumnModified(InternshipPeer::DATE_SOUTENANCE)) {
            $modifiedColumns[':p' . $index++]  = '`date_soutenance`';
        }
        if ($this->isColumnModified(InternshipPeer::INTERNSHIP_BEGINING)) {
            $modifiedColumns[':p' . $index++]  = '`internship_begining`';
        }
        if ($this->isColumnModified(InternshipPeer::INTERNSHIP_ENDING)) {
            $modifiedColumns[':p' . $index++]  = '`internship_ending`';
        }

        $sql = sprintf(
            'INSERT INTO `internship` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`offer_id`':
                        $stmt->bindValue($identifier, $this->offer_id, PDO::PARAM_INT);
                        break;
                    case '`user_id`':
                        $stmt->bindValue($identifier, $this->user_id, PDO::PARAM_INT);
                        break;
                    case '`company_id`':
                        $stmt->bindValue($identifier, $this->company_id, PDO::PARAM_INT);
                        break;
                    case '`tutor_id`':
                        $stmt->bindValue($identifier, $this->tutor_id, PDO::PARAM_INT);
                        break;
                    case '`work_function`':
                        $stmt->bindValue($identifier, $this->work_function, PDO::PARAM_STR);
                        break;
                    case '`description`':
                        $stmt->bindValue($identifier, $this->description, PDO::PARAM_STR);
                        break;
                    case '`information`':
                        $stmt->bindValue($identifier, $this->information, PDO::PARAM_STR);
                        break;
                    case '`url_website_diffusion`':
                        $stmt->bindValue($identifier, $this->url_website_diffusion, PDO::PARAM_STR);
                        break;
                    case '`signatory_lastname`':
                        $stmt->bindValue($identifier, $this->signatory_lastname, PDO::PARAM_STR);
                        break;
                    case '`signatory_firstname`':
                        $stmt->bindValue($identifier, $this->signatory_firstname, PDO::PARAM_STR);
                        break;
                    case '`signatory_function`':
                        $stmt->bindValue($identifier, $this->signatory_function, PDO::PARAM_STR);
                        break;
                    case '`master_lastname`':
                        $stmt->bindValue($identifier, $this->master_lastname, PDO::PARAM_STR);
                        break;
                    case '`master_firstname`':
                        $stmt->bindValue($identifier, $this->master_firstname, PDO::PARAM_STR);
                        break;
                    case '`master_email`':
                        $stmt->bindValue($identifier, $this->master_email, PDO::PARAM_STR);
                        break;
                    case '`master_telephone`':
                        $stmt->bindValue($identifier, $this->master_telephone, PDO::PARAM_STR);
                        break;
                    case '`valide`':
                        $stmt->bindValue($identifier, $this->valide, PDO::PARAM_INT);
                        break;
                    case '`etabli`':
                        $stmt->bindValue($identifier, $this->etabli, PDO::PARAM_INT);
                        break;
                    case '`date_visite`':
                        $stmt->bindValue($identifier, $this->date_visite, PDO::PARAM_STR);
                        break;
                    case '`date_soutenance`':
                        $stmt->bindValue($identifier, $this->date_soutenance, PDO::PARAM_STR);
                        break;
                    case '`internship_begining`':
                        $stmt->bindValue($identifier, $this->internship_begining, PDO::PARAM_STR);
                        break;
                    case '`internship_ending`':
                        $stmt->bindValue($identifier, $this->internship_ending, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aOfferinternshipRelatedByOfferId !== null) {
                if (!$this->aOfferinternshipRelatedByOfferId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aOfferinternshipRelatedByOfferId->getValidationFailures());
                }
            }

            if ($this->aUser !== null) {
                if (!$this->aUser->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aUser->getValidationFailures());
                }
            }

            if ($this->aCompany !== null) {
                if (!$this->aCompany->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCompany->getValidationFailures());
                }
            }


            if (($retval = InternshipPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collOfferinternshipsRelatedByInternshipId !== null) {
                    foreach ($this->collOfferinternshipsRelatedByInternshipId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collDiaryweeks !== null) {
                    foreach ($this->collDiaryweeks as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = InternshipPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getOfferId();
                break;
            case 2:
                return $this->getUserId();
                break;
            case 3:
                return $this->getCompanyId();
                break;
            case 4:
                return $this->getTutorId();
                break;
            case 5:
                return $this->getWorkFunction();
                break;
            case 6:
                return $this->getDescription();
                break;
            case 7:
                return $this->getInformation();
                break;
            case 8:
                return $this->getUrlWebsiteDiffusion();
                break;
            case 9:
                return $this->getSignatoryLastname();
                break;
            case 10:
                return $this->getSignatoryFirstname();
                break;
            case 11:
                return $this->getSignatoryFunction();
                break;
            case 12:
                return $this->getMasterLastname();
                break;
            case 13:
                return $this->getMasterFirstname();
                break;
            case 14:
                return $this->getMasterEmail();
                break;
            case 15:
                return $this->getMasterTelephone();
                break;
            case 16:
                return $this->getValide();
                break;
            case 17:
                return $this->getEtabli();
                break;
            case 18:
                return $this->getDateVisite();
                break;
            case 19:
                return $this->getDateSoutenance();
                break;
            case 20:
                return $this->getInternshipBegining();
                break;
            case 21:
                return $this->getInternshipEnding();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Internship'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Internship'][$this->getPrimaryKey()] = true;
        $keys = InternshipPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getOfferId(),
            $keys[2] => $this->getUserId(),
            $keys[3] => $this->getCompanyId(),
            $keys[4] => $this->getTutorId(),
            $keys[5] => $this->getWorkFunction(),
            $keys[6] => $this->getDescription(),
            $keys[7] => $this->getInformation(),
            $keys[8] => $this->getUrlWebsiteDiffusion(),
            $keys[9] => $this->getSignatoryLastname(),
            $keys[10] => $this->getSignatoryFirstname(),
            $keys[11] => $this->getSignatoryFunction(),
            $keys[12] => $this->getMasterLastname(),
            $keys[13] => $this->getMasterFirstname(),
            $keys[14] => $this->getMasterEmail(),
            $keys[15] => $this->getMasterTelephone(),
            $keys[16] => $this->getValide(),
            $keys[17] => $this->getEtabli(),
            $keys[18] => $this->getDateVisite(),
            $keys[19] => $this->getDateSoutenance(),
            $keys[20] => $this->getInternshipBegining(),
            $keys[21] => $this->getInternshipEnding(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aOfferinternshipRelatedByOfferId) {
                $result['OfferinternshipRelatedByOfferId'] = $this->aOfferinternshipRelatedByOfferId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aUser) {
                $result['User'] = $this->aUser->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCompany) {
                $result['Company'] = $this->aCompany->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collOfferinternshipsRelatedByInternshipId) {
                $result['OfferinternshipsRelatedByInternshipId'] = $this->collOfferinternshipsRelatedByInternshipId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collDiaryweeks) {
                $result['Diaryweeks'] = $this->collDiaryweeks->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = InternshipPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setOfferId($value);
                break;
            case 2:
                $this->setUserId($value);
                break;
            case 3:
                $this->setCompanyId($value);
                break;
            case 4:
                $this->setTutorId($value);
                break;
            case 5:
                $this->setWorkFunction($value);
                break;
            case 6:
                $this->setDescription($value);
                break;
            case 7:
                $this->setInformation($value);
                break;
            case 8:
                $this->setUrlWebsiteDiffusion($value);
                break;
            case 9:
                $this->setSignatoryLastname($value);
                break;
            case 10:
                $this->setSignatoryFirstname($value);
                break;
            case 11:
                $this->setSignatoryFunction($value);
                break;
            case 12:
                $this->setMasterLastname($value);
                break;
            case 13:
                $this->setMasterFirstname($value);
                break;
            case 14:
                $this->setMasterEmail($value);
                break;
            case 15:
                $this->setMasterTelephone($value);
                break;
            case 16:
                $this->setValide($value);
                break;
            case 17:
                $this->setEtabli($value);
                break;
            case 18:
                $this->setDateVisite($value);
                break;
            case 19:
                $this->setDateSoutenance($value);
                break;
            case 20:
                $this->setInternshipBegining($value);
                break;
            case 21:
                $this->setInternshipEnding($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = InternshipPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setOfferId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setUserId($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setCompanyId($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setTutorId($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setWorkFunction($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setDescription($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setInformation($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setUrlWebsiteDiffusion($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setSignatoryLastname($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setSignatoryFirstname($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setSignatoryFunction($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setMasterLastname($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setMasterFirstname($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setMasterEmail($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setMasterTelephone($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setValide($arr[$keys[16]]);
        if (array_key_exists($keys[17], $arr)) $this->setEtabli($arr[$keys[17]]);
        if (array_key_exists($keys[18], $arr)) $this->setDateVisite($arr[$keys[18]]);
        if (array_key_exists($keys[19], $arr)) $this->setDateSoutenance($arr[$keys[19]]);
        if (array_key_exists($keys[20], $arr)) $this->setInternshipBegining($arr[$keys[20]]);
        if (array_key_exists($keys[21], $arr)) $this->setInternshipEnding($arr[$keys[21]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(InternshipPeer::DATABASE_NAME);

        if ($this->isColumnModified(InternshipPeer::ID)) $criteria->add(InternshipPeer::ID, $this->id);
        if ($this->isColumnModified(InternshipPeer::OFFER_ID)) $criteria->add(InternshipPeer::OFFER_ID, $this->offer_id);
        if ($this->isColumnModified(InternshipPeer::USER_ID)) $criteria->add(InternshipPeer::USER_ID, $this->user_id);
        if ($this->isColumnModified(InternshipPeer::COMPANY_ID)) $criteria->add(InternshipPeer::COMPANY_ID, $this->company_id);
        if ($this->isColumnModified(InternshipPeer::TUTOR_ID)) $criteria->add(InternshipPeer::TUTOR_ID, $this->tutor_id);
        if ($this->isColumnModified(InternshipPeer::WORK_FUNCTION)) $criteria->add(InternshipPeer::WORK_FUNCTION, $this->work_function);
        if ($this->isColumnModified(InternshipPeer::DESCRIPTION)) $criteria->add(InternshipPeer::DESCRIPTION, $this->description);
        if ($this->isColumnModified(InternshipPeer::INFORMATION)) $criteria->add(InternshipPeer::INFORMATION, $this->information);
        if ($this->isColumnModified(InternshipPeer::URL_WEBSITE_DIFFUSION)) $criteria->add(InternshipPeer::URL_WEBSITE_DIFFUSION, $this->url_website_diffusion);
        if ($this->isColumnModified(InternshipPeer::SIGNATORY_LASTNAME)) $criteria->add(InternshipPeer::SIGNATORY_LASTNAME, $this->signatory_lastname);
        if ($this->isColumnModified(InternshipPeer::SIGNATORY_FIRSTNAME)) $criteria->add(InternshipPeer::SIGNATORY_FIRSTNAME, $this->signatory_firstname);
        if ($this->isColumnModified(InternshipPeer::SIGNATORY_FUNCTION)) $criteria->add(InternshipPeer::SIGNATORY_FUNCTION, $this->signatory_function);
        if ($this->isColumnModified(InternshipPeer::MASTER_LASTNAME)) $criteria->add(InternshipPeer::MASTER_LASTNAME, $this->master_lastname);
        if ($this->isColumnModified(InternshipPeer::MASTER_FIRSTNAME)) $criteria->add(InternshipPeer::MASTER_FIRSTNAME, $this->master_firstname);
        if ($this->isColumnModified(InternshipPeer::MASTER_EMAIL)) $criteria->add(InternshipPeer::MASTER_EMAIL, $this->master_email);
        if ($this->isColumnModified(InternshipPeer::MASTER_TELEPHONE)) $criteria->add(InternshipPeer::MASTER_TELEPHONE, $this->master_telephone);
        if ($this->isColumnModified(InternshipPeer::VALIDE)) $criteria->add(InternshipPeer::VALIDE, $this->valide);
        if ($this->isColumnModified(InternshipPeer::ETABLI)) $criteria->add(InternshipPeer::ETABLI, $this->etabli);
        if ($this->isColumnModified(InternshipPeer::DATE_VISITE)) $criteria->add(InternshipPeer::DATE_VISITE, $this->date_visite);
        if ($this->isColumnModified(InternshipPeer::DATE_SOUTENANCE)) $criteria->add(InternshipPeer::DATE_SOUTENANCE, $this->date_soutenance);
        if ($this->isColumnModified(InternshipPeer::INTERNSHIP_BEGINING)) $criteria->add(InternshipPeer::INTERNSHIP_BEGINING, $this->internship_begining);
        if ($this->isColumnModified(InternshipPeer::INTERNSHIP_ENDING)) $criteria->add(InternshipPeer::INTERNSHIP_ENDING, $this->internship_ending);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(InternshipPeer::DATABASE_NAME);
        $criteria->add(InternshipPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Internship (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setOfferId($this->getOfferId());
        $copyObj->setUserId($this->getUserId());
        $copyObj->setCompanyId($this->getCompanyId());
        $copyObj->setTutorId($this->getTutorId());
        $copyObj->setWorkFunction($this->getWorkFunction());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setInformation($this->getInformation());
        $copyObj->setUrlWebsiteDiffusion($this->getUrlWebsiteDiffusion());
        $copyObj->setSignatoryLastname($this->getSignatoryLastname());
        $copyObj->setSignatoryFirstname($this->getSignatoryFirstname());
        $copyObj->setSignatoryFunction($this->getSignatoryFunction());
        $copyObj->setMasterLastname($this->getMasterLastname());
        $copyObj->setMasterFirstname($this->getMasterFirstname());
        $copyObj->setMasterEmail($this->getMasterEmail());
        $copyObj->setMasterTelephone($this->getMasterTelephone());
        $copyObj->setValide($this->getValide());
        $copyObj->setEtabli($this->getEtabli());
        $copyObj->setDateVisite($this->getDateVisite());
        $copyObj->setDateSoutenance($this->getDateSoutenance());
        $copyObj->setInternshipBegining($this->getInternshipBegining());
        $copyObj->setInternshipEnding($this->getInternshipEnding());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getOfferinternshipsRelatedByInternshipId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addOfferinternshipRelatedByInternshipId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getDiaryweeks() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addDiaryweek($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Internship Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return InternshipPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new InternshipPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a Offerinternship object.
     *
     * @param                  Offerinternship $v
     * @return Internship The current object (for fluent API support)
     * @throws PropelException
     */
    public function setOfferinternshipRelatedByOfferId(Offerinternship $v = null)
    {
        if ($v === null) {
            $this->setOfferId(NULL);
        } else {
            $this->setOfferId($v->getId());
        }

        $this->aOfferinternshipRelatedByOfferId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Offerinternship object, it will not be re-added.
        if ($v !== null) {
            $v->addInternshipRelatedByOfferId($this);
        }


        return $this;
    }


    /**
     * Get the associated Offerinternship object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Offerinternship The associated Offerinternship object.
     * @throws PropelException
     */
    public function getOfferinternshipRelatedByOfferId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aOfferinternshipRelatedByOfferId === null && ($this->offer_id !== null) && $doQuery) {
            $this->aOfferinternshipRelatedByOfferId = OfferinternshipQuery::create()->findPk($this->offer_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aOfferinternshipRelatedByOfferId->addInternshipsRelatedByOfferId($this);
             */
        }

        return $this->aOfferinternshipRelatedByOfferId;
    }

    /**
     * Declares an association between this object and a User object.
     *
     * @param                  User $v
     * @return Internship The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUser(User $v = null)
    {
        if ($v === null) {
            $this->setUserId(NULL);
        } else {
            $this->setUserId($v->getId());
        }

        $this->aUser = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the User object, it will not be re-added.
        if ($v !== null) {
            $v->addInternship($this);
        }


        return $this;
    }


    /**
     * Get the associated User object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return User The associated User object.
     * @throws PropelException
     */
    public function getUser(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aUser === null && ($this->user_id !== null) && $doQuery) {
            $this->aUser = UserQuery::create()->findPk($this->user_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUser->addInternships($this);
             */
        }

        return $this->aUser;
    }

    /**
     * Declares an association between this object and a Company object.
     *
     * @param                  Company $v
     * @return Internship The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCompany(Company $v = null)
    {
        if ($v === null) {
            $this->setCompanyId(NULL);
        } else {
            $this->setCompanyId($v->getId());
        }

        $this->aCompany = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Company object, it will not be re-added.
        if ($v !== null) {
            $v->addInternship($this);
        }


        return $this;
    }


    /**
     * Get the associated Company object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Company The associated Company object.
     * @throws PropelException
     */
    public function getCompany(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCompany === null && ($this->company_id !== null) && $doQuery) {
            $this->aCompany = CompanyQuery::create()->findPk($this->company_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCompany->addInternships($this);
             */
        }

        return $this->aCompany;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('OfferinternshipRelatedByInternshipId' == $relationName) {
            $this->initOfferinternshipsRelatedByInternshipId();
        }
        if ('Diaryweek' == $relationName) {
            $this->initDiaryweeks();
        }
    }

    /**
     * Clears out the collOfferinternshipsRelatedByInternshipId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Internship The current object (for fluent API support)
     * @see        addOfferinternshipsRelatedByInternshipId()
     */
    public function clearOfferinternshipsRelatedByInternshipId()
    {
        $this->collOfferinternshipsRelatedByInternshipId = null; // important to set this to null since that means it is uninitialized
        $this->collOfferinternshipsRelatedByInternshipIdPartial = null;

        return $this;
    }

    /**
     * reset is the collOfferinternshipsRelatedByInternshipId collection loaded partially
     *
     * @return void
     */
    public function resetPartialOfferinternshipsRelatedByInternshipId($v = true)
    {
        $this->collOfferinternshipsRelatedByInternshipIdPartial = $v;
    }

    /**
     * Initializes the collOfferinternshipsRelatedByInternshipId collection.
     *
     * By default this just sets the collOfferinternshipsRelatedByInternshipId collection to an empty array (like clearcollOfferinternshipsRelatedByInternshipId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initOfferinternshipsRelatedByInternshipId($overrideExisting = true)
    {
        if (null !== $this->collOfferinternshipsRelatedByInternshipId && !$overrideExisting) {
            return;
        }
        $this->collOfferinternshipsRelatedByInternshipId = new PropelObjectCollection();
        $this->collOfferinternshipsRelatedByInternshipId->setModel('Offerinternship');
    }

    /**
     * Gets an array of Offerinternship objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Internship is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Offerinternship[] List of Offerinternship objects
     * @throws PropelException
     */
    public function getOfferinternshipsRelatedByInternshipId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collOfferinternshipsRelatedByInternshipIdPartial && !$this->isNew();
        if (null === $this->collOfferinternshipsRelatedByInternshipId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collOfferinternshipsRelatedByInternshipId) {
                // return empty collection
                $this->initOfferinternshipsRelatedByInternshipId();
            } else {
                $collOfferinternshipsRelatedByInternshipId = OfferinternshipQuery::create(null, $criteria)
                    ->filterByInternshipRelatedByInternshipId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collOfferinternshipsRelatedByInternshipIdPartial && count($collOfferinternshipsRelatedByInternshipId)) {
                      $this->initOfferinternshipsRelatedByInternshipId(false);

                      foreach ($collOfferinternshipsRelatedByInternshipId as $obj) {
                        if (false == $this->collOfferinternshipsRelatedByInternshipId->contains($obj)) {
                          $this->collOfferinternshipsRelatedByInternshipId->append($obj);
                        }
                      }

                      $this->collOfferinternshipsRelatedByInternshipIdPartial = true;
                    }

                    $collOfferinternshipsRelatedByInternshipId->getInternalIterator()->rewind();

                    return $collOfferinternshipsRelatedByInternshipId;
                }

                if ($partial && $this->collOfferinternshipsRelatedByInternshipId) {
                    foreach ($this->collOfferinternshipsRelatedByInternshipId as $obj) {
                        if ($obj->isNew()) {
                            $collOfferinternshipsRelatedByInternshipId[] = $obj;
                        }
                    }
                }

                $this->collOfferinternshipsRelatedByInternshipId = $collOfferinternshipsRelatedByInternshipId;
                $this->collOfferinternshipsRelatedByInternshipIdPartial = false;
            }
        }

        return $this->collOfferinternshipsRelatedByInternshipId;
    }

    /**
     * Sets a collection of OfferinternshipRelatedByInternshipId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $offerinternshipsRelatedByInternshipId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Internship The current object (for fluent API support)
     */
    public function setOfferinternshipsRelatedByInternshipId(PropelCollection $offerinternshipsRelatedByInternshipId, PropelPDO $con = null)
    {
        $offerinternshipsRelatedByInternshipIdToDelete = $this->getOfferinternshipsRelatedByInternshipId(new Criteria(), $con)->diff($offerinternshipsRelatedByInternshipId);


        $this->offerinternshipsRelatedByInternshipIdScheduledForDeletion = $offerinternshipsRelatedByInternshipIdToDelete;

        foreach ($offerinternshipsRelatedByInternshipIdToDelete as $offerinternshipRelatedByInternshipIdRemoved) {
            $offerinternshipRelatedByInternshipIdRemoved->setInternshipRelatedByInternshipId(null);
        }

        $this->collOfferinternshipsRelatedByInternshipId = null;
        foreach ($offerinternshipsRelatedByInternshipId as $offerinternshipRelatedByInternshipId) {
            $this->addOfferinternshipRelatedByInternshipId($offerinternshipRelatedByInternshipId);
        }

        $this->collOfferinternshipsRelatedByInternshipId = $offerinternshipsRelatedByInternshipId;
        $this->collOfferinternshipsRelatedByInternshipIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Offerinternship objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Offerinternship objects.
     * @throws PropelException
     */
    public function countOfferinternshipsRelatedByInternshipId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collOfferinternshipsRelatedByInternshipIdPartial && !$this->isNew();
        if (null === $this->collOfferinternshipsRelatedByInternshipId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collOfferinternshipsRelatedByInternshipId) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getOfferinternshipsRelatedByInternshipId());
            }
            $query = OfferinternshipQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByInternshipRelatedByInternshipId($this)
                ->count($con);
        }

        return count($this->collOfferinternshipsRelatedByInternshipId);
    }

    /**
     * Method called to associate a Offerinternship object to this object
     * through the Offerinternship foreign key attribute.
     *
     * @param    Offerinternship $l Offerinternship
     * @return Internship The current object (for fluent API support)
     */
    public function addOfferinternshipRelatedByInternshipId(Offerinternship $l)
    {
        if ($this->collOfferinternshipsRelatedByInternshipId === null) {
            $this->initOfferinternshipsRelatedByInternshipId();
            $this->collOfferinternshipsRelatedByInternshipIdPartial = true;
        }

        if (!in_array($l, $this->collOfferinternshipsRelatedByInternshipId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddOfferinternshipRelatedByInternshipId($l);

            if ($this->offerinternshipsRelatedByInternshipIdScheduledForDeletion and $this->offerinternshipsRelatedByInternshipIdScheduledForDeletion->contains($l)) {
                $this->offerinternshipsRelatedByInternshipIdScheduledForDeletion->remove($this->offerinternshipsRelatedByInternshipIdScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	OfferinternshipRelatedByInternshipId $offerinternshipRelatedByInternshipId The offerinternshipRelatedByInternshipId object to add.
     */
    protected function doAddOfferinternshipRelatedByInternshipId($offerinternshipRelatedByInternshipId)
    {
        $this->collOfferinternshipsRelatedByInternshipId[]= $offerinternshipRelatedByInternshipId;
        $offerinternshipRelatedByInternshipId->setInternshipRelatedByInternshipId($this);
    }

    /**
     * @param	OfferinternshipRelatedByInternshipId $offerinternshipRelatedByInternshipId The offerinternshipRelatedByInternshipId object to remove.
     * @return Internship The current object (for fluent API support)
     */
    public function removeOfferinternshipRelatedByInternshipId($offerinternshipRelatedByInternshipId)
    {
        if ($this->getOfferinternshipsRelatedByInternshipId()->contains($offerinternshipRelatedByInternshipId)) {
            $this->collOfferinternshipsRelatedByInternshipId->remove($this->collOfferinternshipsRelatedByInternshipId->search($offerinternshipRelatedByInternshipId));
            if (null === $this->offerinternshipsRelatedByInternshipIdScheduledForDeletion) {
                $this->offerinternshipsRelatedByInternshipIdScheduledForDeletion = clone $this->collOfferinternshipsRelatedByInternshipId;
                $this->offerinternshipsRelatedByInternshipIdScheduledForDeletion->clear();
            }
            $this->offerinternshipsRelatedByInternshipIdScheduledForDeletion[]= clone $offerinternshipRelatedByInternshipId;
            $offerinternshipRelatedByInternshipId->setInternshipRelatedByInternshipId(null);
        }

        return $this;
    }

    /**
     * Clears out the collDiaryweeks collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Internship The current object (for fluent API support)
     * @see        addDiaryweeks()
     */
    public function clearDiaryweeks()
    {
        $this->collDiaryweeks = null; // important to set this to null since that means it is uninitialized
        $this->collDiaryweeksPartial = null;

        return $this;
    }

    /**
     * reset is the collDiaryweeks collection loaded partially
     *
     * @return void
     */
    public function resetPartialDiaryweeks($v = true)
    {
        $this->collDiaryweeksPartial = $v;
    }

    /**
     * Initializes the collDiaryweeks collection.
     *
     * By default this just sets the collDiaryweeks collection to an empty array (like clearcollDiaryweeks());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initDiaryweeks($overrideExisting = true)
    {
        if (null !== $this->collDiaryweeks && !$overrideExisting) {
            return;
        }
        $this->collDiaryweeks = new PropelObjectCollection();
        $this->collDiaryweeks->setModel('Diaryweek');
    }

    /**
     * Gets an array of Diaryweek objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Internship is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Diaryweek[] List of Diaryweek objects
     * @throws PropelException
     */
    public function getDiaryweeks($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collDiaryweeksPartial && !$this->isNew();
        if (null === $this->collDiaryweeks || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collDiaryweeks) {
                // return empty collection
                $this->initDiaryweeks();
            } else {
                $collDiaryweeks = DiaryweekQuery::create(null, $criteria)
                    ->filterByInternship($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collDiaryweeksPartial && count($collDiaryweeks)) {
                      $this->initDiaryweeks(false);

                      foreach ($collDiaryweeks as $obj) {
                        if (false == $this->collDiaryweeks->contains($obj)) {
                          $this->collDiaryweeks->append($obj);
                        }
                      }

                      $this->collDiaryweeksPartial = true;
                    }

                    $collDiaryweeks->getInternalIterator()->rewind();

                    return $collDiaryweeks;
                }

                if ($partial && $this->collDiaryweeks) {
                    foreach ($this->collDiaryweeks as $obj) {
                        if ($obj->isNew()) {
                            $collDiaryweeks[] = $obj;
                        }
                    }
                }

                $this->collDiaryweeks = $collDiaryweeks;
                $this->collDiaryweeksPartial = false;
            }
        }

        return $this->collDiaryweeks;
    }

    /**
     * Sets a collection of Diaryweek objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $diaryweeks A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Internship The current object (for fluent API support)
     */
    public function setDiaryweeks(PropelCollection $diaryweeks, PropelPDO $con = null)
    {
        $diaryweeksToDelete = $this->getDiaryweeks(new Criteria(), $con)->diff($diaryweeks);


        $this->diaryweeksScheduledForDeletion = $diaryweeksToDelete;

        foreach ($diaryweeksToDelete as $diaryweekRemoved) {
            $diaryweekRemoved->setInternship(null);
        }

        $this->collDiaryweeks = null;
        foreach ($diaryweeks as $diaryweek) {
            $this->addDiaryweek($diaryweek);
        }

        $this->collDiaryweeks = $diaryweeks;
        $this->collDiaryweeksPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Diaryweek objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Diaryweek objects.
     * @throws PropelException
     */
    public function countDiaryweeks(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collDiaryweeksPartial && !$this->isNew();
        if (null === $this->collDiaryweeks || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collDiaryweeks) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getDiaryweeks());
            }
            $query = DiaryweekQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByInternship($this)
                ->count($con);
        }

        return count($this->collDiaryweeks);
    }

    /**
     * Method called to associate a Diaryweek object to this object
     * through the Diaryweek foreign key attribute.
     *
     * @param    Diaryweek $l Diaryweek
     * @return Internship The current object (for fluent API support)
     */
    public function addDiaryweek(Diaryweek $l)
    {
        if ($this->collDiaryweeks === null) {
            $this->initDiaryweeks();
            $this->collDiaryweeksPartial = true;
        }

        if (!in_array($l, $this->collDiaryweeks->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddDiaryweek($l);

            if ($this->diaryweeksScheduledForDeletion and $this->diaryweeksScheduledForDeletion->contains($l)) {
                $this->diaryweeksScheduledForDeletion->remove($this->diaryweeksScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	Diaryweek $diaryweek The diaryweek object to add.
     */
    protected function doAddDiaryweek($diaryweek)
    {
        $this->collDiaryweeks[]= $diaryweek;
        $diaryweek->setInternship($this);
    }

    /**
     * @param	Diaryweek $diaryweek The diaryweek object to remove.
     * @return Internship The current object (for fluent API support)
     */
    public function removeDiaryweek($diaryweek)
    {
        if ($this->getDiaryweeks()->contains($diaryweek)) {
            $this->collDiaryweeks->remove($this->collDiaryweeks->search($diaryweek));
            if (null === $this->diaryweeksScheduledForDeletion) {
                $this->diaryweeksScheduledForDeletion = clone $this->collDiaryweeks;
                $this->diaryweeksScheduledForDeletion->clear();
            }
            $this->diaryweeksScheduledForDeletion[]= clone $diaryweek;
            $diaryweek->setInternship(null);
        }

        return $this;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->offer_id = null;
        $this->user_id = null;
        $this->company_id = null;
        $this->tutor_id = null;
        $this->work_function = null;
        $this->description = null;
        $this->information = null;
        $this->url_website_diffusion = null;
        $this->signatory_lastname = null;
        $this->signatory_firstname = null;
        $this->signatory_function = null;
        $this->master_lastname = null;
        $this->master_firstname = null;
        $this->master_email = null;
        $this->master_telephone = null;
        $this->valide = null;
        $this->etabli = null;
        $this->date_visite = null;
        $this->date_soutenance = null;
        $this->internship_begining = null;
        $this->internship_ending = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collOfferinternshipsRelatedByInternshipId) {
                foreach ($this->collOfferinternshipsRelatedByInternshipId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collDiaryweeks) {
                foreach ($this->collDiaryweeks as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aOfferinternshipRelatedByOfferId instanceof Persistent) {
              $this->aOfferinternshipRelatedByOfferId->clearAllReferences($deep);
            }
            if ($this->aUser instanceof Persistent) {
              $this->aUser->clearAllReferences($deep);
            }
            if ($this->aCompany instanceof Persistent) {
              $this->aCompany->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collOfferinternshipsRelatedByInternshipId instanceof PropelCollection) {
            $this->collOfferinternshipsRelatedByInternshipId->clearIterator();
        }
        $this->collOfferinternshipsRelatedByInternshipId = null;
        if ($this->collDiaryweeks instanceof PropelCollection) {
            $this->collDiaryweeks->clearIterator();
        }
        $this->collDiaryweeks = null;
        $this->aOfferinternshipRelatedByOfferId = null;
        $this->aUser = null;
        $this->aCompany = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(InternshipPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
