<?php

namespace MMIBundle\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use MMIBundle\Model\Degree;
use MMIBundle\Model\Promo;
use MMIBundle\Model\PromoPeer;
use MMIBundle\Model\PromoQuery;
use MMIBundle\Model\User;
use MMIBundle\Model\UserObtainedDegree;

/**
 * @method PromoQuery orderById($order = Criteria::ASC) Order by the id column
 * @method PromoQuery orderByDegreeId($order = Criteria::ASC) Order by the degree_id column
 * @method PromoQuery orderByYear($order = Criteria::ASC) Order by the year column
 *
 * @method PromoQuery groupById() Group by the id column
 * @method PromoQuery groupByDegreeId() Group by the degree_id column
 * @method PromoQuery groupByYear() Group by the year column
 *
 * @method PromoQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method PromoQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method PromoQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method PromoQuery leftJoinDegree($relationAlias = null) Adds a LEFT JOIN clause to the query using the Degree relation
 * @method PromoQuery rightJoinDegree($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Degree relation
 * @method PromoQuery innerJoinDegree($relationAlias = null) Adds a INNER JOIN clause to the query using the Degree relation
 *
 * @method PromoQuery leftJoinUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the User relation
 * @method PromoQuery rightJoinUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the User relation
 * @method PromoQuery innerJoinUser($relationAlias = null) Adds a INNER JOIN clause to the query using the User relation
 *
 * @method PromoQuery leftJoinUserObtainedDegree($relationAlias = null) Adds a LEFT JOIN clause to the query using the UserObtainedDegree relation
 * @method PromoQuery rightJoinUserObtainedDegree($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UserObtainedDegree relation
 * @method PromoQuery innerJoinUserObtainedDegree($relationAlias = null) Adds a INNER JOIN clause to the query using the UserObtainedDegree relation
 *
 * @method Promo findOne(PropelPDO $con = null) Return the first Promo matching the query
 * @method Promo findOneOrCreate(PropelPDO $con = null) Return the first Promo matching the query, or a new Promo object populated from the query conditions when no match is found
 *
 * @method Promo findOneByDegreeId(int $degree_id) Return the first Promo filtered by the degree_id column
 * @method Promo findOneByYear(int $year) Return the first Promo filtered by the year column
 *
 * @method array findById(int $id) Return Promo objects filtered by the id column
 * @method array findByDegreeId(int $degree_id) Return Promo objects filtered by the degree_id column
 * @method array findByYear(int $year) Return Promo objects filtered by the year column
 */
abstract class BasePromoQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BasePromoQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'default';
        }
        if (null === $modelName) {
            $modelName = 'MMIBundle\\Model\\Promo';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new PromoQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   PromoQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return PromoQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof PromoQuery) {
            return $criteria;
        }
        $query = new PromoQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Promo|Promo[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = PromoPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(PromoPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Promo A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Promo A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `degree_id`, `year` FROM `promo` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Promo();
            $obj->hydrate($row);
            PromoPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Promo|Promo[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Promo[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return PromoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PromoPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return PromoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PromoPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PromoQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(PromoPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(PromoPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PromoPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the degree_id column
     *
     * Example usage:
     * <code>
     * $query->filterByDegreeId(1234); // WHERE degree_id = 1234
     * $query->filterByDegreeId(array(12, 34)); // WHERE degree_id IN (12, 34)
     * $query->filterByDegreeId(array('min' => 12)); // WHERE degree_id >= 12
     * $query->filterByDegreeId(array('max' => 12)); // WHERE degree_id <= 12
     * </code>
     *
     * @see       filterByDegree()
     *
     * @param     mixed $degreeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PromoQuery The current query, for fluid interface
     */
    public function filterByDegreeId($degreeId = null, $comparison = null)
    {
        if (is_array($degreeId)) {
            $useMinMax = false;
            if (isset($degreeId['min'])) {
                $this->addUsingAlias(PromoPeer::DEGREE_ID, $degreeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($degreeId['max'])) {
                $this->addUsingAlias(PromoPeer::DEGREE_ID, $degreeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PromoPeer::DEGREE_ID, $degreeId, $comparison);
    }

    /**
     * Filter the query on the year column
     *
     * Example usage:
     * <code>
     * $query->filterByYear(1234); // WHERE year = 1234
     * $query->filterByYear(array(12, 34)); // WHERE year IN (12, 34)
     * $query->filterByYear(array('min' => 12)); // WHERE year >= 12
     * $query->filterByYear(array('max' => 12)); // WHERE year <= 12
     * </code>
     *
     * @param     mixed $year The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PromoQuery The current query, for fluid interface
     */
    public function filterByYear($year = null, $comparison = null)
    {
        if (is_array($year)) {
            $useMinMax = false;
            if (isset($year['min'])) {
                $this->addUsingAlias(PromoPeer::YEAR, $year['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($year['max'])) {
                $this->addUsingAlias(PromoPeer::YEAR, $year['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PromoPeer::YEAR, $year, $comparison);
    }

    /**
     * Filter the query by a related Degree object
     *
     * @param   Degree|PropelObjectCollection $degree The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PromoQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByDegree($degree, $comparison = null)
    {
        if ($degree instanceof Degree) {
            return $this
                ->addUsingAlias(PromoPeer::DEGREE_ID, $degree->getId(), $comparison);
        } elseif ($degree instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PromoPeer::DEGREE_ID, $degree->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByDegree() only accepts arguments of type Degree or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Degree relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PromoQuery The current query, for fluid interface
     */
    public function joinDegree($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Degree');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Degree');
        }

        return $this;
    }

    /**
     * Use the Degree relation Degree object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MMIBundle\Model\DegreeQuery A secondary query class using the current class as primary query
     */
    public function useDegreeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinDegree($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Degree', '\MMIBundle\Model\DegreeQuery');
    }

    /**
     * Filter the query by a related User object
     *
     * @param   User|PropelObjectCollection $user  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PromoQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByUser($user, $comparison = null)
    {
        if ($user instanceof User) {
            return $this
                ->addUsingAlias(PromoPeer::ID, $user->getPromoId(), $comparison);
        } elseif ($user instanceof PropelObjectCollection) {
            return $this
                ->useUserQuery()
                ->filterByPrimaryKeys($user->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUser() only accepts arguments of type User or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the User relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PromoQuery The current query, for fluid interface
     */
    public function joinUser($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('User');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'User');
        }

        return $this;
    }

    /**
     * Use the User relation User object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MMIBundle\Model\UserQuery A secondary query class using the current class as primary query
     */
    public function useUserQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'User', '\MMIBundle\Model\UserQuery');
    }

    /**
     * Filter the query by a related UserObtainedDegree object
     *
     * @param   UserObtainedDegree|PropelObjectCollection $userObtainedDegree  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PromoQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByUserObtainedDegree($userObtainedDegree, $comparison = null)
    {
        if ($userObtainedDegree instanceof UserObtainedDegree) {
            return $this
                ->addUsingAlias(PromoPeer::ID, $userObtainedDegree->getDegreeId(), $comparison);
        } elseif ($userObtainedDegree instanceof PropelObjectCollection) {
            return $this
                ->useUserObtainedDegreeQuery()
                ->filterByPrimaryKeys($userObtainedDegree->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUserObtainedDegree() only accepts arguments of type UserObtainedDegree or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UserObtainedDegree relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PromoQuery The current query, for fluid interface
     */
    public function joinUserObtainedDegree($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UserObtainedDegree');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UserObtainedDegree');
        }

        return $this;
    }

    /**
     * Use the UserObtainedDegree relation UserObtainedDegree object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MMIBundle\Model\UserObtainedDegreeQuery A secondary query class using the current class as primary query
     */
    public function useUserObtainedDegreeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUserObtainedDegree($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UserObtainedDegree', '\MMIBundle\Model\UserObtainedDegreeQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Promo $promo Object to remove from the list of results
     *
     * @return PromoQuery The current query, for fluid interface
     */
    public function prune($promo = null)
    {
        if ($promo) {
            $this->addUsingAlias(PromoPeer::ID, $promo->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
