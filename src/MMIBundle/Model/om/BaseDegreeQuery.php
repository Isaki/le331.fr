<?php

namespace MMIBundle\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use MMIBundle\Model\Degree;
use MMIBundle\Model\DegreePeer;
use MMIBundle\Model\DegreeQuery;
use MMIBundle\Model\Promo;

/**
 * @method DegreeQuery orderById($order = Criteria::ASC) Order by the id column
 * @method DegreeQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method DegreeQuery orderByOption($order = Criteria::ASC) Order by the option column
 * @method DegreeQuery orderBySummary($order = Criteria::ASC) Order by the summary column
 *
 * @method DegreeQuery groupById() Group by the id column
 * @method DegreeQuery groupByName() Group by the name column
 * @method DegreeQuery groupByOption() Group by the option column
 * @method DegreeQuery groupBySummary() Group by the summary column
 *
 * @method DegreeQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method DegreeQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method DegreeQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method DegreeQuery leftJoinPromo($relationAlias = null) Adds a LEFT JOIN clause to the query using the Promo relation
 * @method DegreeQuery rightJoinPromo($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Promo relation
 * @method DegreeQuery innerJoinPromo($relationAlias = null) Adds a INNER JOIN clause to the query using the Promo relation
 *
 * @method Degree findOne(PropelPDO $con = null) Return the first Degree matching the query
 * @method Degree findOneOrCreate(PropelPDO $con = null) Return the first Degree matching the query, or a new Degree object populated from the query conditions when no match is found
 *
 * @method Degree findOneByName(string $name) Return the first Degree filtered by the name column
 * @method Degree findOneByOption(string $option) Return the first Degree filtered by the option column
 * @method Degree findOneBySummary(string $summary) Return the first Degree filtered by the summary column
 *
 * @method array findById(int $id) Return Degree objects filtered by the id column
 * @method array findByName(string $name) Return Degree objects filtered by the name column
 * @method array findByOption(string $option) Return Degree objects filtered by the option column
 * @method array findBySummary(string $summary) Return Degree objects filtered by the summary column
 */
abstract class BaseDegreeQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseDegreeQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'default';
        }
        if (null === $modelName) {
            $modelName = 'MMIBundle\\Model\\Degree';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new DegreeQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   DegreeQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return DegreeQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof DegreeQuery) {
            return $criteria;
        }
        $query = new DegreeQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Degree|Degree[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = DegreePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(DegreePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Degree A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Degree A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `name`, `option`, `summary` FROM `degree` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Degree();
            $obj->hydrate($row);
            DegreePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Degree|Degree[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Degree[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return DegreeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(DegreePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return DegreeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(DegreePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DegreeQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(DegreePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(DegreePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DegreePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DegreeQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(DegreePeer::NAME, $name, $comparison);
    }

    /**
     * Filter the query on the option column
     *
     * Example usage:
     * <code>
     * $query->filterByOption('fooValue');   // WHERE option = 'fooValue'
     * $query->filterByOption('%fooValue%'); // WHERE option LIKE '%fooValue%'
     * </code>
     *
     * @param     string $option The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DegreeQuery The current query, for fluid interface
     */
    public function filterByOption($option = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($option)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $option)) {
                $option = str_replace('*', '%', $option);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(DegreePeer::OPTION, $option, $comparison);
    }

    /**
     * Filter the query on the summary column
     *
     * Example usage:
     * <code>
     * $query->filterBySummary('fooValue');   // WHERE summary = 'fooValue'
     * $query->filterBySummary('%fooValue%'); // WHERE summary LIKE '%fooValue%'
     * </code>
     *
     * @param     string $summary The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DegreeQuery The current query, for fluid interface
     */
    public function filterBySummary($summary = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($summary)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $summary)) {
                $summary = str_replace('*', '%', $summary);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(DegreePeer::SUMMARY, $summary, $comparison);
    }

    /**
     * Filter the query by a related Promo object
     *
     * @param   Promo|PropelObjectCollection $promo  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 DegreeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPromo($promo, $comparison = null)
    {
        if ($promo instanceof Promo) {
            return $this
                ->addUsingAlias(DegreePeer::ID, $promo->getDegreeId(), $comparison);
        } elseif ($promo instanceof PropelObjectCollection) {
            return $this
                ->usePromoQuery()
                ->filterByPrimaryKeys($promo->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPromo() only accepts arguments of type Promo or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Promo relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return DegreeQuery The current query, for fluid interface
     */
    public function joinPromo($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Promo');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Promo');
        }

        return $this;
    }

    /**
     * Use the Promo relation Promo object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MMIBundle\Model\PromoQuery A secondary query class using the current class as primary query
     */
    public function usePromoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPromo($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Promo', '\MMIBundle\Model\PromoQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Degree $degree Object to remove from the list of results
     *
     * @return DegreeQuery The current query, for fluid interface
     */
    public function prune($degree = null)
    {
        if ($degree) {
            $this->addUsingAlias(DegreePeer::ID, $degree->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
