<?php

namespace MMIBundle\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use MMIBundle\Model\Address;
use MMIBundle\Model\Company;
use MMIBundle\Model\CompanyPeer;
use MMIBundle\Model\CompanyQuery;
use MMIBundle\Model\Contact;
use MMIBundle\Model\Industry;
use MMIBundle\Model\Internship;
use MMIBundle\Model\UserJob;
use MMIBundle\Model\Validation;

/**
 * @method CompanyQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CompanyQuery orderByAddressId($order = Criteria::ASC) Order by the address_id column
 * @method CompanyQuery orderByContactId($order = Criteria::ASC) Order by the contact_id column
 * @method CompanyQuery orderByIndustryId($order = Criteria::ASC) Order by the industry_id column
 * @method CompanyQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method CompanyQuery orderBySiret($order = Criteria::ASC) Order by the siret column
 * @method CompanyQuery orderByPassword($order = Criteria::ASC) Order by the password column
 * @method CompanyQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method CompanyQuery orderByTelephone($order = Criteria::ASC) Order by the telephone column
 * @method CompanyQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method CompanyQuery orderByWorkforce($order = Criteria::ASC) Order by the workforce column
 * @method CompanyQuery orderByLogoFile($order = Criteria::ASC) Order by the logo_file column
 * @method CompanyQuery orderByUrlWebsite($order = Criteria::ASC) Order by the url_website column
 * @method CompanyQuery orderByFirstConnection($order = Criteria::ASC) Order by the first_connection column
 * @method CompanyQuery orderByValidated($order = Criteria::ASC) Order by the validated column
 * @method CompanyQuery orderByNewsletter($order = Criteria::ASC) Order by the newsletter column
 *
 * @method CompanyQuery groupById() Group by the id column
 * @method CompanyQuery groupByAddressId() Group by the address_id column
 * @method CompanyQuery groupByContactId() Group by the contact_id column
 * @method CompanyQuery groupByIndustryId() Group by the industry_id column
 * @method CompanyQuery groupByName() Group by the name column
 * @method CompanyQuery groupBySiret() Group by the siret column
 * @method CompanyQuery groupByPassword() Group by the password column
 * @method CompanyQuery groupByEmail() Group by the email column
 * @method CompanyQuery groupByTelephone() Group by the telephone column
 * @method CompanyQuery groupByDescription() Group by the description column
 * @method CompanyQuery groupByWorkforce() Group by the workforce column
 * @method CompanyQuery groupByLogoFile() Group by the logo_file column
 * @method CompanyQuery groupByUrlWebsite() Group by the url_website column
 * @method CompanyQuery groupByFirstConnection() Group by the first_connection column
 * @method CompanyQuery groupByValidated() Group by the validated column
 * @method CompanyQuery groupByNewsletter() Group by the newsletter column
 *
 * @method CompanyQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CompanyQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CompanyQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CompanyQuery leftJoinAddress($relationAlias = null) Adds a LEFT JOIN clause to the query using the Address relation
 * @method CompanyQuery rightJoinAddress($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Address relation
 * @method CompanyQuery innerJoinAddress($relationAlias = null) Adds a INNER JOIN clause to the query using the Address relation
 *
 * @method CompanyQuery leftJoinContact($relationAlias = null) Adds a LEFT JOIN clause to the query using the Contact relation
 * @method CompanyQuery rightJoinContact($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Contact relation
 * @method CompanyQuery innerJoinContact($relationAlias = null) Adds a INNER JOIN clause to the query using the Contact relation
 *
 * @method CompanyQuery leftJoinIndustry($relationAlias = null) Adds a LEFT JOIN clause to the query using the Industry relation
 * @method CompanyQuery rightJoinIndustry($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Industry relation
 * @method CompanyQuery innerJoinIndustry($relationAlias = null) Adds a INNER JOIN clause to the query using the Industry relation
 *
 * @method CompanyQuery leftJoinInternship($relationAlias = null) Adds a LEFT JOIN clause to the query using the Internship relation
 * @method CompanyQuery rightJoinInternship($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Internship relation
 * @method CompanyQuery innerJoinInternship($relationAlias = null) Adds a INNER JOIN clause to the query using the Internship relation
 *
 * @method CompanyQuery leftJoinUserJob($relationAlias = null) Adds a LEFT JOIN clause to the query using the UserJob relation
 * @method CompanyQuery rightJoinUserJob($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UserJob relation
 * @method CompanyQuery innerJoinUserJob($relationAlias = null) Adds a INNER JOIN clause to the query using the UserJob relation
 *
 * @method CompanyQuery leftJoinValidation($relationAlias = null) Adds a LEFT JOIN clause to the query using the Validation relation
 * @method CompanyQuery rightJoinValidation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Validation relation
 * @method CompanyQuery innerJoinValidation($relationAlias = null) Adds a INNER JOIN clause to the query using the Validation relation
 *
 * @method Company findOne(PropelPDO $con = null) Return the first Company matching the query
 * @method Company findOneOrCreate(PropelPDO $con = null) Return the first Company matching the query, or a new Company object populated from the query conditions when no match is found
 *
 * @method Company findOneByAddressId(int $address_id) Return the first Company filtered by the address_id column
 * @method Company findOneByContactId(int $contact_id) Return the first Company filtered by the contact_id column
 * @method Company findOneByIndustryId(int $industry_id) Return the first Company filtered by the industry_id column
 * @method Company findOneByName(string $name) Return the first Company filtered by the name column
 * @method Company findOneBySiret(int $siret) Return the first Company filtered by the siret column
 * @method Company findOneByPassword(string $password) Return the first Company filtered by the password column
 * @method Company findOneByEmail(string $email) Return the first Company filtered by the email column
 * @method Company findOneByTelephone(string $telephone) Return the first Company filtered by the telephone column
 * @method Company findOneByDescription(string $description) Return the first Company filtered by the description column
 * @method Company findOneByWorkforce(string $workforce) Return the first Company filtered by the workforce column
 * @method Company findOneByLogoFile(string $logo_file) Return the first Company filtered by the logo_file column
 * @method Company findOneByUrlWebsite(string $url_website) Return the first Company filtered by the url_website column
 * @method Company findOneByFirstConnection(boolean $first_connection) Return the first Company filtered by the first_connection column
 * @method Company findOneByValidated(boolean $validated) Return the first Company filtered by the validated column
 * @method Company findOneByNewsletter(boolean $newsletter) Return the first Company filtered by the newsletter column
 *
 * @method array findById(int $id) Return Company objects filtered by the id column
 * @method array findByAddressId(int $address_id) Return Company objects filtered by the address_id column
 * @method array findByContactId(int $contact_id) Return Company objects filtered by the contact_id column
 * @method array findByIndustryId(int $industry_id) Return Company objects filtered by the industry_id column
 * @method array findByName(string $name) Return Company objects filtered by the name column
 * @method array findBySiret(int $siret) Return Company objects filtered by the siret column
 * @method array findByPassword(string $password) Return Company objects filtered by the password column
 * @method array findByEmail(string $email) Return Company objects filtered by the email column
 * @method array findByTelephone(string $telephone) Return Company objects filtered by the telephone column
 * @method array findByDescription(string $description) Return Company objects filtered by the description column
 * @method array findByWorkforce(string $workforce) Return Company objects filtered by the workforce column
 * @method array findByLogoFile(string $logo_file) Return Company objects filtered by the logo_file column
 * @method array findByUrlWebsite(string $url_website) Return Company objects filtered by the url_website column
 * @method array findByFirstConnection(boolean $first_connection) Return Company objects filtered by the first_connection column
 * @method array findByValidated(boolean $validated) Return Company objects filtered by the validated column
 * @method array findByNewsletter(boolean $newsletter) Return Company objects filtered by the newsletter column
 */
abstract class BaseCompanyQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCompanyQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'default';
        }
        if (null === $modelName) {
            $modelName = 'MMIBundle\\Model\\Company';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CompanyQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CompanyQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CompanyQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CompanyQuery) {
            return $criteria;
        }
        $query = new CompanyQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Company|Company[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CompanyPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CompanyPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Company A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Company A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `address_id`, `contact_id`, `industry_id`, `name`, `siret`, `password`, `email`, `telephone`, `description`, `workforce`, `logo_file`, `url_website`, `first_connection`, `validated`, `newsletter` FROM `company` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Company();
            $obj->hydrate($row);
            CompanyPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Company|Company[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Company[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CompanyQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CompanyPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CompanyQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CompanyPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CompanyQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CompanyPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CompanyPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the address_id column
     *
     * Example usage:
     * <code>
     * $query->filterByAddressId(1234); // WHERE address_id = 1234
     * $query->filterByAddressId(array(12, 34)); // WHERE address_id IN (12, 34)
     * $query->filterByAddressId(array('min' => 12)); // WHERE address_id >= 12
     * $query->filterByAddressId(array('max' => 12)); // WHERE address_id <= 12
     * </code>
     *
     * @see       filterByAddress()
     *
     * @param     mixed $addressId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CompanyQuery The current query, for fluid interface
     */
    public function filterByAddressId($addressId = null, $comparison = null)
    {
        if (is_array($addressId)) {
            $useMinMax = false;
            if (isset($addressId['min'])) {
                $this->addUsingAlias(CompanyPeer::ADDRESS_ID, $addressId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($addressId['max'])) {
                $this->addUsingAlias(CompanyPeer::ADDRESS_ID, $addressId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyPeer::ADDRESS_ID, $addressId, $comparison);
    }

    /**
     * Filter the query on the contact_id column
     *
     * Example usage:
     * <code>
     * $query->filterByContactId(1234); // WHERE contact_id = 1234
     * $query->filterByContactId(array(12, 34)); // WHERE contact_id IN (12, 34)
     * $query->filterByContactId(array('min' => 12)); // WHERE contact_id >= 12
     * $query->filterByContactId(array('max' => 12)); // WHERE contact_id <= 12
     * </code>
     *
     * @see       filterByContact()
     *
     * @param     mixed $contactId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CompanyQuery The current query, for fluid interface
     */
    public function filterByContactId($contactId = null, $comparison = null)
    {
        if (is_array($contactId)) {
            $useMinMax = false;
            if (isset($contactId['min'])) {
                $this->addUsingAlias(CompanyPeer::CONTACT_ID, $contactId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($contactId['max'])) {
                $this->addUsingAlias(CompanyPeer::CONTACT_ID, $contactId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyPeer::CONTACT_ID, $contactId, $comparison);
    }

    /**
     * Filter the query on the industry_id column
     *
     * Example usage:
     * <code>
     * $query->filterByIndustryId(1234); // WHERE industry_id = 1234
     * $query->filterByIndustryId(array(12, 34)); // WHERE industry_id IN (12, 34)
     * $query->filterByIndustryId(array('min' => 12)); // WHERE industry_id >= 12
     * $query->filterByIndustryId(array('max' => 12)); // WHERE industry_id <= 12
     * </code>
     *
     * @see       filterByIndustry()
     *
     * @param     mixed $industryId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CompanyQuery The current query, for fluid interface
     */
    public function filterByIndustryId($industryId = null, $comparison = null)
    {
        if (is_array($industryId)) {
            $useMinMax = false;
            if (isset($industryId['min'])) {
                $this->addUsingAlias(CompanyPeer::INDUSTRY_ID, $industryId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($industryId['max'])) {
                $this->addUsingAlias(CompanyPeer::INDUSTRY_ID, $industryId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyPeer::INDUSTRY_ID, $industryId, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CompanyQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CompanyPeer::NAME, $name, $comparison);
    }

    /**
     * Filter the query on the siret column
     *
     * Example usage:
     * <code>
     * $query->filterBySiret(1234); // WHERE siret = 1234
     * $query->filterBySiret(array(12, 34)); // WHERE siret IN (12, 34)
     * $query->filterBySiret(array('min' => 12)); // WHERE siret >= 12
     * $query->filterBySiret(array('max' => 12)); // WHERE siret <= 12
     * </code>
     *
     * @param     mixed $siret The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CompanyQuery The current query, for fluid interface
     */
    public function filterBySiret($siret = null, $comparison = null)
    {
        if (is_array($siret)) {
            $useMinMax = false;
            if (isset($siret['min'])) {
                $this->addUsingAlias(CompanyPeer::SIRET, $siret['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($siret['max'])) {
                $this->addUsingAlias(CompanyPeer::SIRET, $siret['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyPeer::SIRET, $siret, $comparison);
    }

    /**
     * Filter the query on the password column
     *
     * Example usage:
     * <code>
     * $query->filterByPassword('fooValue');   // WHERE password = 'fooValue'
     * $query->filterByPassword('%fooValue%'); // WHERE password LIKE '%fooValue%'
     * </code>
     *
     * @param     string $password The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CompanyQuery The current query, for fluid interface
     */
    public function filterByPassword($password = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($password)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $password)) {
                $password = str_replace('*', '%', $password);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CompanyPeer::PASSWORD, $password, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%'); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CompanyQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $email)) {
                $email = str_replace('*', '%', $email);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CompanyPeer::EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the telephone column
     *
     * Example usage:
     * <code>
     * $query->filterByTelephone('fooValue');   // WHERE telephone = 'fooValue'
     * $query->filterByTelephone('%fooValue%'); // WHERE telephone LIKE '%fooValue%'
     * </code>
     *
     * @param     string $telephone The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CompanyQuery The current query, for fluid interface
     */
    public function filterByTelephone($telephone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($telephone)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $telephone)) {
                $telephone = str_replace('*', '%', $telephone);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CompanyPeer::TELEPHONE, $telephone, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CompanyQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $description)) {
                $description = str_replace('*', '%', $description);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CompanyPeer::DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the workforce column
     *
     * Example usage:
     * <code>
     * $query->filterByWorkforce('fooValue');   // WHERE workforce = 'fooValue'
     * $query->filterByWorkforce('%fooValue%'); // WHERE workforce LIKE '%fooValue%'
     * </code>
     *
     * @param     string $workforce The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CompanyQuery The current query, for fluid interface
     */
    public function filterByWorkforce($workforce = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($workforce)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $workforce)) {
                $workforce = str_replace('*', '%', $workforce);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CompanyPeer::WORKFORCE, $workforce, $comparison);
    }

    /**
     * Filter the query on the logo_file column
     *
     * Example usage:
     * <code>
     * $query->filterByLogoFile('fooValue');   // WHERE logo_file = 'fooValue'
     * $query->filterByLogoFile('%fooValue%'); // WHERE logo_file LIKE '%fooValue%'
     * </code>
     *
     * @param     string $logoFile The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CompanyQuery The current query, for fluid interface
     */
    public function filterByLogoFile($logoFile = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($logoFile)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $logoFile)) {
                $logoFile = str_replace('*', '%', $logoFile);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CompanyPeer::LOGO_FILE, $logoFile, $comparison);
    }

    /**
     * Filter the query on the url_website column
     *
     * Example usage:
     * <code>
     * $query->filterByUrlWebsite('fooValue');   // WHERE url_website = 'fooValue'
     * $query->filterByUrlWebsite('%fooValue%'); // WHERE url_website LIKE '%fooValue%'
     * </code>
     *
     * @param     string $urlWebsite The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CompanyQuery The current query, for fluid interface
     */
    public function filterByUrlWebsite($urlWebsite = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($urlWebsite)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $urlWebsite)) {
                $urlWebsite = str_replace('*', '%', $urlWebsite);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CompanyPeer::URL_WEBSITE, $urlWebsite, $comparison);
    }

    /**
     * Filter the query on the first_connection column
     *
     * Example usage:
     * <code>
     * $query->filterByFirstConnection(true); // WHERE first_connection = true
     * $query->filterByFirstConnection('yes'); // WHERE first_connection = true
     * </code>
     *
     * @param     boolean|string $firstConnection The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CompanyQuery The current query, for fluid interface
     */
    public function filterByFirstConnection($firstConnection = null, $comparison = null)
    {
        if (is_string($firstConnection)) {
            $firstConnection = in_array(strtolower($firstConnection), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CompanyPeer::FIRST_CONNECTION, $firstConnection, $comparison);
    }

    /**
     * Filter the query on the validated column
     *
     * Example usage:
     * <code>
     * $query->filterByValidated(true); // WHERE validated = true
     * $query->filterByValidated('yes'); // WHERE validated = true
     * </code>
     *
     * @param     boolean|string $validated The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CompanyQuery The current query, for fluid interface
     */
    public function filterByValidated($validated = null, $comparison = null)
    {
        if (is_string($validated)) {
            $validated = in_array(strtolower($validated), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CompanyPeer::VALIDATED, $validated, $comparison);
    }

    /**
     * Filter the query on the newsletter column
     *
     * Example usage:
     * <code>
     * $query->filterByNewsletter(true); // WHERE newsletter = true
     * $query->filterByNewsletter('yes'); // WHERE newsletter = true
     * </code>
     *
     * @param     boolean|string $newsletter The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CompanyQuery The current query, for fluid interface
     */
    public function filterByNewsletter($newsletter = null, $comparison = null)
    {
        if (is_string($newsletter)) {
            $newsletter = in_array(strtolower($newsletter), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CompanyPeer::NEWSLETTER, $newsletter, $comparison);
    }

    /**
     * Filter the query by a related Address object
     *
     * @param   Address|PropelObjectCollection $address The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CompanyQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByAddress($address, $comparison = null)
    {
        if ($address instanceof Address) {
            return $this
                ->addUsingAlias(CompanyPeer::ADDRESS_ID, $address->getId(), $comparison);
        } elseif ($address instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CompanyPeer::ADDRESS_ID, $address->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByAddress() only accepts arguments of type Address or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Address relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CompanyQuery The current query, for fluid interface
     */
    public function joinAddress($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Address');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Address');
        }

        return $this;
    }

    /**
     * Use the Address relation Address object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MMIBundle\Model\AddressQuery A secondary query class using the current class as primary query
     */
    public function useAddressQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinAddress($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Address', '\MMIBundle\Model\AddressQuery');
    }

    /**
     * Filter the query by a related Contact object
     *
     * @param   Contact|PropelObjectCollection $contact The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CompanyQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByContact($contact, $comparison = null)
    {
        if ($contact instanceof Contact) {
            return $this
                ->addUsingAlias(CompanyPeer::CONTACT_ID, $contact->getId(), $comparison);
        } elseif ($contact instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CompanyPeer::CONTACT_ID, $contact->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByContact() only accepts arguments of type Contact or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Contact relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CompanyQuery The current query, for fluid interface
     */
    public function joinContact($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Contact');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Contact');
        }

        return $this;
    }

    /**
     * Use the Contact relation Contact object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MMIBundle\Model\ContactQuery A secondary query class using the current class as primary query
     */
    public function useContactQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinContact($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Contact', '\MMIBundle\Model\ContactQuery');
    }

    /**
     * Filter the query by a related Industry object
     *
     * @param   Industry|PropelObjectCollection $industry The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CompanyQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByIndustry($industry, $comparison = null)
    {
        if ($industry instanceof Industry) {
            return $this
                ->addUsingAlias(CompanyPeer::INDUSTRY_ID, $industry->getId(), $comparison);
        } elseif ($industry instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CompanyPeer::INDUSTRY_ID, $industry->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByIndustry() only accepts arguments of type Industry or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Industry relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CompanyQuery The current query, for fluid interface
     */
    public function joinIndustry($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Industry');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Industry');
        }

        return $this;
    }

    /**
     * Use the Industry relation Industry object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MMIBundle\Model\IndustryQuery A secondary query class using the current class as primary query
     */
    public function useIndustryQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinIndustry($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Industry', '\MMIBundle\Model\IndustryQuery');
    }

    /**
     * Filter the query by a related Internship object
     *
     * @param   Internship|PropelObjectCollection $internship  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CompanyQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByInternship($internship, $comparison = null)
    {
        if ($internship instanceof Internship) {
            return $this
                ->addUsingAlias(CompanyPeer::ID, $internship->getCompanyId(), $comparison);
        } elseif ($internship instanceof PropelObjectCollection) {
            return $this
                ->useInternshipQuery()
                ->filterByPrimaryKeys($internship->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByInternship() only accepts arguments of type Internship or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Internship relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CompanyQuery The current query, for fluid interface
     */
    public function joinInternship($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Internship');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Internship');
        }

        return $this;
    }

    /**
     * Use the Internship relation Internship object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MMIBundle\Model\InternshipQuery A secondary query class using the current class as primary query
     */
    public function useInternshipQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinInternship($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Internship', '\MMIBundle\Model\InternshipQuery');
    }

    /**
     * Filter the query by a related UserJob object
     *
     * @param   UserJob|PropelObjectCollection $userJob  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CompanyQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByUserJob($userJob, $comparison = null)
    {
        if ($userJob instanceof UserJob) {
            return $this
                ->addUsingAlias(CompanyPeer::ID, $userJob->getCompanyId(), $comparison);
        } elseif ($userJob instanceof PropelObjectCollection) {
            return $this
                ->useUserJobQuery()
                ->filterByPrimaryKeys($userJob->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUserJob() only accepts arguments of type UserJob or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UserJob relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CompanyQuery The current query, for fluid interface
     */
    public function joinUserJob($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UserJob');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UserJob');
        }

        return $this;
    }

    /**
     * Use the UserJob relation UserJob object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MMIBundle\Model\UserJobQuery A secondary query class using the current class as primary query
     */
    public function useUserJobQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUserJob($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UserJob', '\MMIBundle\Model\UserJobQuery');
    }

    /**
     * Filter the query by a related Validation object
     *
     * @param   Validation|PropelObjectCollection $validation  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CompanyQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByValidation($validation, $comparison = null)
    {
        if ($validation instanceof Validation) {
            return $this
                ->addUsingAlias(CompanyPeer::ID, $validation->getCompanyId(), $comparison);
        } elseif ($validation instanceof PropelObjectCollection) {
            return $this
                ->useValidationQuery()
                ->filterByPrimaryKeys($validation->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByValidation() only accepts arguments of type Validation or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Validation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CompanyQuery The current query, for fluid interface
     */
    public function joinValidation($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Validation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Validation');
        }

        return $this;
    }

    /**
     * Use the Validation relation Validation object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MMIBundle\Model\ValidationQuery A secondary query class using the current class as primary query
     */
    public function useValidationQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinValidation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Validation', '\MMIBundle\Model\ValidationQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Company $company Object to remove from the list of results
     *
     * @return CompanyQuery The current query, for fluid interface
     */
    public function prune($company = null)
    {
        if ($company) {
            $this->addUsingAlias(CompanyPeer::ID, $company->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
