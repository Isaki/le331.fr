<?php

namespace MMIBundle\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \DateTime;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelCollection;
use \PropelDateTime;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use MMIBundle\Model\City;
use MMIBundle\Model\CityQuery;
use MMIBundle\Model\Internship;
use MMIBundle\Model\InternshipQuery;
use MMIBundle\Model\Profession;
use MMIBundle\Model\ProfessionQuery;
use MMIBundle\Model\Promo;
use MMIBundle\Model\PromoQuery;
use MMIBundle\Model\Role;
use MMIBundle\Model\RoleQuery;
use MMIBundle\Model\UnivProject;
use MMIBundle\Model\UnivProjectQuery;
use MMIBundle\Model\User;
use MMIBundle\Model\UserJob;
use MMIBundle\Model\UserJobQuery;
use MMIBundle\Model\UserMemberUnipro;
use MMIBundle\Model\UserMemberUniproQuery;
use MMIBundle\Model\UserObtainedDegree;
use MMIBundle\Model\UserObtainedDegreeQuery;
use MMIBundle\Model\UserPeer;
use MMIBundle\Model\UserQuery;
use MMIBundle\Model\UserRole;
use MMIBundle\Model\UserRoleQuery;
use MMIBundle\Model\Validation;
use MMIBundle\Model\ValidationQuery;

abstract class BaseUser extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'MMIBundle\\Model\\UserPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        UserPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the city_id field.
     * @var        int
     */
    protected $city_id;

    /**
     * The value for the profile_id field.
     * @var        int
     */
    protected $profile_id;

    /**
     * The value for the promo_id field.
     * @var        int
     */
    protected $promo_id;

    /**
     * The value for the email field.
     * @var        string
     */
    protected $email;

    /**
     * The value for the password field.
     * @var        string
     */
    protected $password;

    /**
     * The value for the lastname field.
     * @var        string
     */
    protected $lastname;

    /**
     * The value for the firstname field.
     * @var        string
     */
    protected $firstname;

    /**
     * The value for the description field.
     * @var        string
     */
    protected $description;

    /**
     * The value for the newsletter field.
     * Note: this column has a database default value of: true
     * @var        boolean
     */
    protected $newsletter;

    /**
     * The value for the birthday field.
     * @var        string
     */
    protected $birthday;

    /**
     * The value for the url_portfolio field.
     * @var        string
     */
    protected $url_portfolio;

    /**
     * The value for the url_linkedin field.
     * @var        string
     */
    protected $url_linkedin;

    /**
     * The value for the url_facebook field.
     * @var        string
     */
    protected $url_facebook;

    /**
     * The value for the url_google field.
     * @var        string
     */
    protected $url_google;

    /**
     * The value for the url_twitter field.
     * @var        string
     */
    protected $url_twitter;

    /**
     * The value for the url_tumblr field.
     * @var        string
     */
    protected $url_tumblr;

    /**
     * The value for the url_instagram field.
     * @var        string
     */
    protected $url_instagram;

    /**
     * The value for the url_blog field.
     * @var        string
     */
    protected $url_blog;

    /**
     * The value for the cv_file field.
     * @var        string
     */
    protected $cv_file;

    /**
     * The value for the image_profil_file field.
     * Note: this column has a database default value of: 'default.svg'
     * @var        string
     */
    protected $image_profil_file;

    /**
     * The value for the image_couv_file field.
     * Note: this column has a database default value of: 'default.jpg'
     * @var        string
     */
    protected $image_couv_file;

    /**
     * The value for the telephone field.
     * @var        string
     */
    protected $telephone;

    /**
     * The value for the first_connection field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $first_connection;

    /**
     * The value for the validated field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $validated;

    /**
     * @var        City
     */
    protected $aCity;

    /**
     * @var        Profession
     */
    protected $aProfession;

    /**
     * @var        Promo
     */
    protected $aPromo;

    /**
     * @var        PropelObjectCollection|UserRole[] Collection to store aggregation of UserRole objects.
     */
    protected $collUserRoles;
    protected $collUserRolesPartial;

    /**
     * @var        PropelObjectCollection|Internship[] Collection to store aggregation of Internship objects.
     */
    protected $collInternships;
    protected $collInternshipsPartial;

    /**
     * @var        PropelObjectCollection|UserJob[] Collection to store aggregation of UserJob objects.
     */
    protected $collUserJobs;
    protected $collUserJobsPartial;

    /**
     * @var        PropelObjectCollection|UserObtainedDegree[] Collection to store aggregation of UserObtainedDegree objects.
     */
    protected $collUserObtainedDegrees;
    protected $collUserObtainedDegreesPartial;

    /**
     * @var        PropelObjectCollection|UnivProject[] Collection to store aggregation of UnivProject objects.
     */
    protected $collUnivProjects;
    protected $collUnivProjectsPartial;

    /**
     * @var        PropelObjectCollection|UserMemberUnipro[] Collection to store aggregation of UserMemberUnipro objects.
     */
    protected $collUserMemberUnipros;
    protected $collUserMemberUniprosPartial;

    /**
     * @var        PropelObjectCollection|Validation[] Collection to store aggregation of Validation objects.
     */
    protected $collValidations;
    protected $collValidationsPartial;

    /**
     * @var        PropelObjectCollection|Role[] Collection to store aggregation of Role objects.
     */
    protected $collRoles;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rolesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $userRolesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $internshipsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $userJobsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $userObtainedDegreesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $univProjectsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $userMemberUniprosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $validationsScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->newsletter = true;
        $this->image_profil_file = 'default.svg';
        $this->image_couv_file = 'default.jpg';
        $this->first_connection = false;
        $this->validated = false;
    }

    /**
     * Initializes internal state of BaseUser object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [city_id] column value.
     *
     * @return int
     */
    public function getCityId()
    {

        return $this->city_id;
    }

    /**
     * Get the [profile_id] column value.
     *
     * @return int
     */
    public function getProfileId()
    {

        return $this->profile_id;
    }

    /**
     * Get the [promo_id] column value.
     *
     * @return int
     */
    public function getPromoId()
    {

        return $this->promo_id;
    }

    /**
     * Get the [email] column value.
     *
     * @return string
     */
    public function getEmail()
    {

        return $this->email;
    }

    /**
     * Get the [password] column value.
     *
     * @return string
     */
    public function getPassword()
    {

        return $this->password;
    }

    /**
     * Get the [lastname] column value.
     *
     * @return string
     */
    public function getLastname()
    {

        return $this->lastname;
    }

    /**
     * Get the [firstname] column value.
     *
     * @return string
     */
    public function getFirstname()
    {

        return $this->firstname;
    }

    /**
     * Get the [description] column value.
     *
     * @return string
     */
    public function getDescription()
    {

        return $this->description;
    }

    /**
     * Get the [newsletter] column value.
     *
     * @return boolean
     */
    public function getNewsletter()
    {

        return $this->newsletter;
    }

    /**
     * Get the [optionally formatted] temporal [birthday] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getBirthday($format = null)
    {
        if ($this->birthday === null) {
            return null;
        }

        if ($this->birthday === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->birthday);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->birthday, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [url_portfolio] column value.
     *
     * @return string
     */
    public function getUrlPortfolio()
    {

        return $this->url_portfolio;
    }

    /**
     * Get the [url_linkedin] column value.
     *
     * @return string
     */
    public function getUrlLinkedin()
    {

        return $this->url_linkedin;
    }

    /**
     * Get the [url_facebook] column value.
     *
     * @return string
     */
    public function getUrlFacebook()
    {

        return $this->url_facebook;
    }

    /**
     * Get the [url_google] column value.
     *
     * @return string
     */
    public function getUrlGoogle()
    {

        return $this->url_google;
    }

    /**
     * Get the [url_twitter] column value.
     *
     * @return string
     */
    public function getUrlTwitter()
    {

        return $this->url_twitter;
    }

    /**
     * Get the [url_tumblr] column value.
     *
     * @return string
     */
    public function getUrlTumblr()
    {

        return $this->url_tumblr;
    }

    /**
     * Get the [url_instagram] column value.
     *
     * @return string
     */
    public function getUrlInstagram()
    {

        return $this->url_instagram;
    }

    /**
     * Get the [url_blog] column value.
     *
     * @return string
     */
    public function getUrlBlog()
    {

        return $this->url_blog;
    }

    /**
     * Get the [cv_file] column value.
     *
     * @return string
     */
    public function getCvFile()
    {

        return $this->cv_file;
    }

    /**
     * Get the [image_profil_file] column value.
     *
     * @return string
     */
    public function getImageProfilFile()
    {

        return $this->image_profil_file;
    }

    /**
     * Get the [image_couv_file] column value.
     *
     * @return string
     */
    public function getImageCouvFile()
    {

        return $this->image_couv_file;
    }

    /**
     * Get the [telephone] column value.
     *
     * @return string
     */
    public function getTelephone()
    {

        return $this->telephone;
    }

    /**
     * Get the [first_connection] column value.
     *
     * @return boolean
     */
    public function getFirstConnection()
    {

        return $this->first_connection;
    }

    /**
     * Get the [validated] column value.
     *
     * @return boolean
     */
    public function getValidated()
    {

        return $this->validated;
    }

    /**
     * Set the value of [id] column.
     *
     * @param  int $v new value
     * @return User The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = UserPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [city_id] column.
     *
     * @param  int $v new value
     * @return User The current object (for fluent API support)
     */
    public function setCityId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->city_id !== $v) {
            $this->city_id = $v;
            $this->modifiedColumns[] = UserPeer::CITY_ID;
        }

        if ($this->aCity !== null && $this->aCity->getId() !== $v) {
            $this->aCity = null;
        }


        return $this;
    } // setCityId()

    /**
     * Set the value of [profile_id] column.
     *
     * @param  int $v new value
     * @return User The current object (for fluent API support)
     */
    public function setProfileId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->profile_id !== $v) {
            $this->profile_id = $v;
            $this->modifiedColumns[] = UserPeer::PROFILE_ID;
        }

        if ($this->aProfession !== null && $this->aProfession->getId() !== $v) {
            $this->aProfession = null;
        }


        return $this;
    } // setProfileId()

    /**
     * Set the value of [promo_id] column.
     *
     * @param  int $v new value
     * @return User The current object (for fluent API support)
     */
    public function setPromoId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->promo_id !== $v) {
            $this->promo_id = $v;
            $this->modifiedColumns[] = UserPeer::PROMO_ID;
        }

        if ($this->aPromo !== null && $this->aPromo->getId() !== $v) {
            $this->aPromo = null;
        }


        return $this;
    } // setPromoId()

    /**
     * Set the value of [email] column.
     *
     * @param  string $v new value
     * @return User The current object (for fluent API support)
     */
    public function setEmail($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->email !== $v) {
            $this->email = $v;
            $this->modifiedColumns[] = UserPeer::EMAIL;
        }


        return $this;
    } // setEmail()

    /**
     * Set the value of [password] column.
     *
     * @param  string $v new value
     * @return User The current object (for fluent API support)
     */
    public function setPassword($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->password !== $v) {
            $this->password = $v;
            $this->modifiedColumns[] = UserPeer::PASSWORD;
        }


        return $this;
    } // setPassword()

    /**
     * Set the value of [lastname] column.
     *
     * @param  string $v new value
     * @return User The current object (for fluent API support)
     */
    public function setLastname($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->lastname !== $v) {
            $this->lastname = $v;
            $this->modifiedColumns[] = UserPeer::LASTNAME;
        }


        return $this;
    } // setLastname()

    /**
     * Set the value of [firstname] column.
     *
     * @param  string $v new value
     * @return User The current object (for fluent API support)
     */
    public function setFirstname($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->firstname !== $v) {
            $this->firstname = $v;
            $this->modifiedColumns[] = UserPeer::FIRSTNAME;
        }


        return $this;
    } // setFirstname()

    /**
     * Set the value of [description] column.
     *
     * @param  string $v new value
     * @return User The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->description !== $v) {
            $this->description = $v;
            $this->modifiedColumns[] = UserPeer::DESCRIPTION;
        }


        return $this;
    } // setDescription()

    /**
     * Sets the value of the [newsletter] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return User The current object (for fluent API support)
     */
    public function setNewsletter($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->newsletter !== $v) {
            $this->newsletter = $v;
            $this->modifiedColumns[] = UserPeer::NEWSLETTER;
        }


        return $this;
    } // setNewsletter()

    /**
     * Sets the value of [birthday] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return User The current object (for fluent API support)
     */
    public function setBirthday($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->birthday !== null || $dt !== null) {
            $currentDateAsString = ($this->birthday !== null && $tmpDt = new DateTime($this->birthday)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->birthday = $newDateAsString;
                $this->modifiedColumns[] = UserPeer::BIRTHDAY;
            }
        } // if either are not null


        return $this;
    } // setBirthday()

    /**
     * Set the value of [url_portfolio] column.
     *
     * @param  string $v new value
     * @return User The current object (for fluent API support)
     */
    public function setUrlPortfolio($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->url_portfolio !== $v) {
            $this->url_portfolio = $v;
            $this->modifiedColumns[] = UserPeer::URL_PORTFOLIO;
        }


        return $this;
    } // setUrlPortfolio()

    /**
     * Set the value of [url_linkedin] column.
     *
     * @param  string $v new value
     * @return User The current object (for fluent API support)
     */
    public function setUrlLinkedin($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->url_linkedin !== $v) {
            $this->url_linkedin = $v;
            $this->modifiedColumns[] = UserPeer::URL_LINKEDIN;
        }


        return $this;
    } // setUrlLinkedin()

    /**
     * Set the value of [url_facebook] column.
     *
     * @param  string $v new value
     * @return User The current object (for fluent API support)
     */
    public function setUrlFacebook($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->url_facebook !== $v) {
            $this->url_facebook = $v;
            $this->modifiedColumns[] = UserPeer::URL_FACEBOOK;
        }


        return $this;
    } // setUrlFacebook()

    /**
     * Set the value of [url_google] column.
     *
     * @param  string $v new value
     * @return User The current object (for fluent API support)
     */
    public function setUrlGoogle($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->url_google !== $v) {
            $this->url_google = $v;
            $this->modifiedColumns[] = UserPeer::URL_GOOGLE;
        }


        return $this;
    } // setUrlGoogle()

    /**
     * Set the value of [url_twitter] column.
     *
     * @param  string $v new value
     * @return User The current object (for fluent API support)
     */
    public function setUrlTwitter($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->url_twitter !== $v) {
            $this->url_twitter = $v;
            $this->modifiedColumns[] = UserPeer::URL_TWITTER;
        }


        return $this;
    } // setUrlTwitter()

    /**
     * Set the value of [url_tumblr] column.
     *
     * @param  string $v new value
     * @return User The current object (for fluent API support)
     */
    public function setUrlTumblr($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->url_tumblr !== $v) {
            $this->url_tumblr = $v;
            $this->modifiedColumns[] = UserPeer::URL_TUMBLR;
        }


        return $this;
    } // setUrlTumblr()

    /**
     * Set the value of [url_instagram] column.
     *
     * @param  string $v new value
     * @return User The current object (for fluent API support)
     */
    public function setUrlInstagram($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->url_instagram !== $v) {
            $this->url_instagram = $v;
            $this->modifiedColumns[] = UserPeer::URL_INSTAGRAM;
        }


        return $this;
    } // setUrlInstagram()

    /**
     * Set the value of [url_blog] column.
     *
     * @param  string $v new value
     * @return User The current object (for fluent API support)
     */
    public function setUrlBlog($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->url_blog !== $v) {
            $this->url_blog = $v;
            $this->modifiedColumns[] = UserPeer::URL_BLOG;
        }


        return $this;
    } // setUrlBlog()

    /**
     * Set the value of [cv_file] column.
     *
     * @param  string $v new value
     * @return User The current object (for fluent API support)
     */
    public function setCvFile($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cv_file !== $v) {
            $this->cv_file = $v;
            $this->modifiedColumns[] = UserPeer::CV_FILE;
        }


        return $this;
    } // setCvFile()

    /**
     * Set the value of [image_profil_file] column.
     *
     * @param  string $v new value
     * @return User The current object (for fluent API support)
     */
    public function setImageProfilFile($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->image_profil_file !== $v) {
            $this->image_profil_file = $v;
            $this->modifiedColumns[] = UserPeer::IMAGE_PROFIL_FILE;
        }


        return $this;
    } // setImageProfilFile()

    /**
     * Set the value of [image_couv_file] column.
     *
     * @param  string $v new value
     * @return User The current object (for fluent API support)
     */
    public function setImageCouvFile($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->image_couv_file !== $v) {
            $this->image_couv_file = $v;
            $this->modifiedColumns[] = UserPeer::IMAGE_COUV_FILE;
        }


        return $this;
    } // setImageCouvFile()

    /**
     * Set the value of [telephone] column.
     *
     * @param  string $v new value
     * @return User The current object (for fluent API support)
     */
    public function setTelephone($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->telephone !== $v) {
            $this->telephone = $v;
            $this->modifiedColumns[] = UserPeer::TELEPHONE;
        }


        return $this;
    } // setTelephone()

    /**
     * Sets the value of the [first_connection] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return User The current object (for fluent API support)
     */
    public function setFirstConnection($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->first_connection !== $v) {
            $this->first_connection = $v;
            $this->modifiedColumns[] = UserPeer::FIRST_CONNECTION;
        }


        return $this;
    } // setFirstConnection()

    /**
     * Sets the value of the [validated] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return User The current object (for fluent API support)
     */
    public function setValidated($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->validated !== $v) {
            $this->validated = $v;
            $this->modifiedColumns[] = UserPeer::VALIDATED;
        }


        return $this;
    } // setValidated()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->newsletter !== true) {
                return false;
            }

            if ($this->image_profil_file !== 'default.svg') {
                return false;
            }

            if ($this->image_couv_file !== 'default.jpg') {
                return false;
            }

            if ($this->first_connection !== false) {
                return false;
            }

            if ($this->validated !== false) {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->city_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->profile_id = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->promo_id = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
            $this->email = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->password = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->lastname = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->firstname = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->description = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->newsletter = ($row[$startcol + 9] !== null) ? (boolean) $row[$startcol + 9] : null;
            $this->birthday = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->url_portfolio = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->url_linkedin = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->url_facebook = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->url_google = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
            $this->url_twitter = ($row[$startcol + 15] !== null) ? (string) $row[$startcol + 15] : null;
            $this->url_tumblr = ($row[$startcol + 16] !== null) ? (string) $row[$startcol + 16] : null;
            $this->url_instagram = ($row[$startcol + 17] !== null) ? (string) $row[$startcol + 17] : null;
            $this->url_blog = ($row[$startcol + 18] !== null) ? (string) $row[$startcol + 18] : null;
            $this->cv_file = ($row[$startcol + 19] !== null) ? (string) $row[$startcol + 19] : null;
            $this->image_profil_file = ($row[$startcol + 20] !== null) ? (string) $row[$startcol + 20] : null;
            $this->image_couv_file = ($row[$startcol + 21] !== null) ? (string) $row[$startcol + 21] : null;
            $this->telephone = ($row[$startcol + 22] !== null) ? (string) $row[$startcol + 22] : null;
            $this->first_connection = ($row[$startcol + 23] !== null) ? (boolean) $row[$startcol + 23] : null;
            $this->validated = ($row[$startcol + 24] !== null) ? (boolean) $row[$startcol + 24] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 25; // 25 = UserPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating User object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aCity !== null && $this->city_id !== $this->aCity->getId()) {
            $this->aCity = null;
        }
        if ($this->aProfession !== null && $this->profile_id !== $this->aProfession->getId()) {
            $this->aProfession = null;
        }
        if ($this->aPromo !== null && $this->promo_id !== $this->aPromo->getId()) {
            $this->aPromo = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(UserPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = UserPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCity = null;
            $this->aProfession = null;
            $this->aPromo = null;
            $this->collUserRoles = null;

            $this->collInternships = null;

            $this->collUserJobs = null;

            $this->collUserObtainedDegrees = null;

            $this->collUnivProjects = null;

            $this->collUserMemberUnipros = null;

            $this->collValidations = null;

            $this->collRoles = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(UserPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = UserQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(UserPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                UserPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCity !== null) {
                if ($this->aCity->isModified() || $this->aCity->isNew()) {
                    $affectedRows += $this->aCity->save($con);
                }
                $this->setCity($this->aCity);
            }

            if ($this->aProfession !== null) {
                if ($this->aProfession->isModified() || $this->aProfession->isNew()) {
                    $affectedRows += $this->aProfession->save($con);
                }
                $this->setProfession($this->aProfession);
            }

            if ($this->aPromo !== null) {
                if ($this->aPromo->isModified() || $this->aPromo->isNew()) {
                    $affectedRows += $this->aPromo->save($con);
                }
                $this->setPromo($this->aPromo);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->rolesScheduledForDeletion !== null) {
                if (!$this->rolesScheduledForDeletion->isEmpty()) {
                    $pks = array();
                    $pk = $this->getPrimaryKey();
                    foreach ($this->rolesScheduledForDeletion->getPrimaryKeys(false) as $remotePk) {
                        $pks[] = array($pk, $remotePk);
                    }
                    UserRoleQuery::create()
                        ->filterByPrimaryKeys($pks)
                        ->delete($con);
                    $this->rolesScheduledForDeletion = null;
                }

                foreach ($this->getRoles() as $role) {
                    if ($role->isModified()) {
                        $role->save($con);
                    }
                }
            } elseif ($this->collRoles) {
                foreach ($this->collRoles as $role) {
                    if ($role->isModified()) {
                        $role->save($con);
                    }
                }
            }

            if ($this->userRolesScheduledForDeletion !== null) {
                if (!$this->userRolesScheduledForDeletion->isEmpty()) {
                    UserRoleQuery::create()
                        ->filterByPrimaryKeys($this->userRolesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->userRolesScheduledForDeletion = null;
                }
            }

            if ($this->collUserRoles !== null) {
                foreach ($this->collUserRoles as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->internshipsScheduledForDeletion !== null) {
                if (!$this->internshipsScheduledForDeletion->isEmpty()) {
                    InternshipQuery::create()
                        ->filterByPrimaryKeys($this->internshipsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->internshipsScheduledForDeletion = null;
                }
            }

            if ($this->collInternships !== null) {
                foreach ($this->collInternships as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->userJobsScheduledForDeletion !== null) {
                if (!$this->userJobsScheduledForDeletion->isEmpty()) {
                    UserJobQuery::create()
                        ->filterByPrimaryKeys($this->userJobsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->userJobsScheduledForDeletion = null;
                }
            }

            if ($this->collUserJobs !== null) {
                foreach ($this->collUserJobs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->userObtainedDegreesScheduledForDeletion !== null) {
                if (!$this->userObtainedDegreesScheduledForDeletion->isEmpty()) {
                    UserObtainedDegreeQuery::create()
                        ->filterByPrimaryKeys($this->userObtainedDegreesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->userObtainedDegreesScheduledForDeletion = null;
                }
            }

            if ($this->collUserObtainedDegrees !== null) {
                foreach ($this->collUserObtainedDegrees as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->univProjectsScheduledForDeletion !== null) {
                if (!$this->univProjectsScheduledForDeletion->isEmpty()) {
                    foreach ($this->univProjectsScheduledForDeletion as $univProject) {
                        // need to save related object because we set the relation to null
                        $univProject->save($con);
                    }
                    $this->univProjectsScheduledForDeletion = null;
                }
            }

            if ($this->collUnivProjects !== null) {
                foreach ($this->collUnivProjects as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->userMemberUniprosScheduledForDeletion !== null) {
                if (!$this->userMemberUniprosScheduledForDeletion->isEmpty()) {
                    UserMemberUniproQuery::create()
                        ->filterByPrimaryKeys($this->userMemberUniprosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->userMemberUniprosScheduledForDeletion = null;
                }
            }

            if ($this->collUserMemberUnipros !== null) {
                foreach ($this->collUserMemberUnipros as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->validationsScheduledForDeletion !== null) {
                if (!$this->validationsScheduledForDeletion->isEmpty()) {
                    foreach ($this->validationsScheduledForDeletion as $validation) {
                        // need to save related object because we set the relation to null
                        $validation->save($con);
                    }
                    $this->validationsScheduledForDeletion = null;
                }
            }

            if ($this->collValidations !== null) {
                foreach ($this->collValidations as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = UserPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . UserPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(UserPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(UserPeer::CITY_ID)) {
            $modifiedColumns[':p' . $index++]  = '`city_id`';
        }
        if ($this->isColumnModified(UserPeer::PROFILE_ID)) {
            $modifiedColumns[':p' . $index++]  = '`profile_id`';
        }
        if ($this->isColumnModified(UserPeer::PROMO_ID)) {
            $modifiedColumns[':p' . $index++]  = '`promo_id`';
        }
        if ($this->isColumnModified(UserPeer::EMAIL)) {
            $modifiedColumns[':p' . $index++]  = '`email`';
        }
        if ($this->isColumnModified(UserPeer::PASSWORD)) {
            $modifiedColumns[':p' . $index++]  = '`password`';
        }
        if ($this->isColumnModified(UserPeer::LASTNAME)) {
            $modifiedColumns[':p' . $index++]  = '`lastname`';
        }
        if ($this->isColumnModified(UserPeer::FIRSTNAME)) {
            $modifiedColumns[':p' . $index++]  = '`firstname`';
        }
        if ($this->isColumnModified(UserPeer::DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = '`description`';
        }
        if ($this->isColumnModified(UserPeer::NEWSLETTER)) {
            $modifiedColumns[':p' . $index++]  = '`newsletter`';
        }
        if ($this->isColumnModified(UserPeer::BIRTHDAY)) {
            $modifiedColumns[':p' . $index++]  = '`birthday`';
        }
        if ($this->isColumnModified(UserPeer::URL_PORTFOLIO)) {
            $modifiedColumns[':p' . $index++]  = '`url_portfolio`';
        }
        if ($this->isColumnModified(UserPeer::URL_LINKEDIN)) {
            $modifiedColumns[':p' . $index++]  = '`url_linkedin`';
        }
        if ($this->isColumnModified(UserPeer::URL_FACEBOOK)) {
            $modifiedColumns[':p' . $index++]  = '`url_facebook`';
        }
        if ($this->isColumnModified(UserPeer::URL_GOOGLE)) {
            $modifiedColumns[':p' . $index++]  = '`url_google`';
        }
        if ($this->isColumnModified(UserPeer::URL_TWITTER)) {
            $modifiedColumns[':p' . $index++]  = '`url_twitter`';
        }
        if ($this->isColumnModified(UserPeer::URL_TUMBLR)) {
            $modifiedColumns[':p' . $index++]  = '`url_tumblr`';
        }
        if ($this->isColumnModified(UserPeer::URL_INSTAGRAM)) {
            $modifiedColumns[':p' . $index++]  = '`url_instagram`';
        }
        if ($this->isColumnModified(UserPeer::URL_BLOG)) {
            $modifiedColumns[':p' . $index++]  = '`url_blog`';
        }
        if ($this->isColumnModified(UserPeer::CV_FILE)) {
            $modifiedColumns[':p' . $index++]  = '`cv_file`';
        }
        if ($this->isColumnModified(UserPeer::IMAGE_PROFIL_FILE)) {
            $modifiedColumns[':p' . $index++]  = '`image_profil_file`';
        }
        if ($this->isColumnModified(UserPeer::IMAGE_COUV_FILE)) {
            $modifiedColumns[':p' . $index++]  = '`image_couv_file`';
        }
        if ($this->isColumnModified(UserPeer::TELEPHONE)) {
            $modifiedColumns[':p' . $index++]  = '`telephone`';
        }
        if ($this->isColumnModified(UserPeer::FIRST_CONNECTION)) {
            $modifiedColumns[':p' . $index++]  = '`first_connection`';
        }
        if ($this->isColumnModified(UserPeer::VALIDATED)) {
            $modifiedColumns[':p' . $index++]  = '`validated`';
        }

        $sql = sprintf(
            'INSERT INTO `user` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`city_id`':
                        $stmt->bindValue($identifier, $this->city_id, PDO::PARAM_INT);
                        break;
                    case '`profile_id`':
                        $stmt->bindValue($identifier, $this->profile_id, PDO::PARAM_INT);
                        break;
                    case '`promo_id`':
                        $stmt->bindValue($identifier, $this->promo_id, PDO::PARAM_INT);
                        break;
                    case '`email`':
                        $stmt->bindValue($identifier, $this->email, PDO::PARAM_STR);
                        break;
                    case '`password`':
                        $stmt->bindValue($identifier, $this->password, PDO::PARAM_STR);
                        break;
                    case '`lastname`':
                        $stmt->bindValue($identifier, $this->lastname, PDO::PARAM_STR);
                        break;
                    case '`firstname`':
                        $stmt->bindValue($identifier, $this->firstname, PDO::PARAM_STR);
                        break;
                    case '`description`':
                        $stmt->bindValue($identifier, $this->description, PDO::PARAM_STR);
                        break;
                    case '`newsletter`':
                        $stmt->bindValue($identifier, (int) $this->newsletter, PDO::PARAM_INT);
                        break;
                    case '`birthday`':
                        $stmt->bindValue($identifier, $this->birthday, PDO::PARAM_STR);
                        break;
                    case '`url_portfolio`':
                        $stmt->bindValue($identifier, $this->url_portfolio, PDO::PARAM_STR);
                        break;
                    case '`url_linkedin`':
                        $stmt->bindValue($identifier, $this->url_linkedin, PDO::PARAM_STR);
                        break;
                    case '`url_facebook`':
                        $stmt->bindValue($identifier, $this->url_facebook, PDO::PARAM_STR);
                        break;
                    case '`url_google`':
                        $stmt->bindValue($identifier, $this->url_google, PDO::PARAM_STR);
                        break;
                    case '`url_twitter`':
                        $stmt->bindValue($identifier, $this->url_twitter, PDO::PARAM_STR);
                        break;
                    case '`url_tumblr`':
                        $stmt->bindValue($identifier, $this->url_tumblr, PDO::PARAM_STR);
                        break;
                    case '`url_instagram`':
                        $stmt->bindValue($identifier, $this->url_instagram, PDO::PARAM_STR);
                        break;
                    case '`url_blog`':
                        $stmt->bindValue($identifier, $this->url_blog, PDO::PARAM_STR);
                        break;
                    case '`cv_file`':
                        $stmt->bindValue($identifier, $this->cv_file, PDO::PARAM_STR);
                        break;
                    case '`image_profil_file`':
                        $stmt->bindValue($identifier, $this->image_profil_file, PDO::PARAM_STR);
                        break;
                    case '`image_couv_file`':
                        $stmt->bindValue($identifier, $this->image_couv_file, PDO::PARAM_STR);
                        break;
                    case '`telephone`':
                        $stmt->bindValue($identifier, $this->telephone, PDO::PARAM_STR);
                        break;
                    case '`first_connection`':
                        $stmt->bindValue($identifier, (int) $this->first_connection, PDO::PARAM_INT);
                        break;
                    case '`validated`':
                        $stmt->bindValue($identifier, (int) $this->validated, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCity !== null) {
                if (!$this->aCity->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCity->getValidationFailures());
                }
            }

            if ($this->aProfession !== null) {
                if (!$this->aProfession->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aProfession->getValidationFailures());
                }
            }

            if ($this->aPromo !== null) {
                if (!$this->aPromo->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aPromo->getValidationFailures());
                }
            }


            if (($retval = UserPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collUserRoles !== null) {
                    foreach ($this->collUserRoles as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collInternships !== null) {
                    foreach ($this->collInternships as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collUserJobs !== null) {
                    foreach ($this->collUserJobs as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collUserObtainedDegrees !== null) {
                    foreach ($this->collUserObtainedDegrees as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collUnivProjects !== null) {
                    foreach ($this->collUnivProjects as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collUserMemberUnipros !== null) {
                    foreach ($this->collUserMemberUnipros as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collValidations !== null) {
                    foreach ($this->collValidations as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = UserPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getCityId();
                break;
            case 2:
                return $this->getProfileId();
                break;
            case 3:
                return $this->getPromoId();
                break;
            case 4:
                return $this->getEmail();
                break;
            case 5:
                return $this->getPassword();
                break;
            case 6:
                return $this->getLastname();
                break;
            case 7:
                return $this->getFirstname();
                break;
            case 8:
                return $this->getDescription();
                break;
            case 9:
                return $this->getNewsletter();
                break;
            case 10:
                return $this->getBirthday();
                break;
            case 11:
                return $this->getUrlPortfolio();
                break;
            case 12:
                return $this->getUrlLinkedin();
                break;
            case 13:
                return $this->getUrlFacebook();
                break;
            case 14:
                return $this->getUrlGoogle();
                break;
            case 15:
                return $this->getUrlTwitter();
                break;
            case 16:
                return $this->getUrlTumblr();
                break;
            case 17:
                return $this->getUrlInstagram();
                break;
            case 18:
                return $this->getUrlBlog();
                break;
            case 19:
                return $this->getCvFile();
                break;
            case 20:
                return $this->getImageProfilFile();
                break;
            case 21:
                return $this->getImageCouvFile();
                break;
            case 22:
                return $this->getTelephone();
                break;
            case 23:
                return $this->getFirstConnection();
                break;
            case 24:
                return $this->getValidated();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['User'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['User'][$this->getPrimaryKey()] = true;
        $keys = UserPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getCityId(),
            $keys[2] => $this->getProfileId(),
            $keys[3] => $this->getPromoId(),
            $keys[4] => $this->getEmail(),
            $keys[5] => $this->getPassword(),
            $keys[6] => $this->getLastname(),
            $keys[7] => $this->getFirstname(),
            $keys[8] => $this->getDescription(),
            $keys[9] => $this->getNewsletter(),
            $keys[10] => $this->getBirthday(),
            $keys[11] => $this->getUrlPortfolio(),
            $keys[12] => $this->getUrlLinkedin(),
            $keys[13] => $this->getUrlFacebook(),
            $keys[14] => $this->getUrlGoogle(),
            $keys[15] => $this->getUrlTwitter(),
            $keys[16] => $this->getUrlTumblr(),
            $keys[17] => $this->getUrlInstagram(),
            $keys[18] => $this->getUrlBlog(),
            $keys[19] => $this->getCvFile(),
            $keys[20] => $this->getImageProfilFile(),
            $keys[21] => $this->getImageCouvFile(),
            $keys[22] => $this->getTelephone(),
            $keys[23] => $this->getFirstConnection(),
            $keys[24] => $this->getValidated(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aCity) {
                $result['City'] = $this->aCity->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aProfession) {
                $result['Profession'] = $this->aProfession->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPromo) {
                $result['Promo'] = $this->aPromo->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collUserRoles) {
                $result['UserRoles'] = $this->collUserRoles->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collInternships) {
                $result['Internships'] = $this->collInternships->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUserJobs) {
                $result['UserJobs'] = $this->collUserJobs->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUserObtainedDegrees) {
                $result['UserObtainedDegrees'] = $this->collUserObtainedDegrees->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUnivProjects) {
                $result['UnivProjects'] = $this->collUnivProjects->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUserMemberUnipros) {
                $result['UserMemberUnipros'] = $this->collUserMemberUnipros->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collValidations) {
                $result['Validations'] = $this->collValidations->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = UserPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setCityId($value);
                break;
            case 2:
                $this->setProfileId($value);
                break;
            case 3:
                $this->setPromoId($value);
                break;
            case 4:
                $this->setEmail($value);
                break;
            case 5:
                $this->setPassword($value);
                break;
            case 6:
                $this->setLastname($value);
                break;
            case 7:
                $this->setFirstname($value);
                break;
            case 8:
                $this->setDescription($value);
                break;
            case 9:
                $this->setNewsletter($value);
                break;
            case 10:
                $this->setBirthday($value);
                break;
            case 11:
                $this->setUrlPortfolio($value);
                break;
            case 12:
                $this->setUrlLinkedin($value);
                break;
            case 13:
                $this->setUrlFacebook($value);
                break;
            case 14:
                $this->setUrlGoogle($value);
                break;
            case 15:
                $this->setUrlTwitter($value);
                break;
            case 16:
                $this->setUrlTumblr($value);
                break;
            case 17:
                $this->setUrlInstagram($value);
                break;
            case 18:
                $this->setUrlBlog($value);
                break;
            case 19:
                $this->setCvFile($value);
                break;
            case 20:
                $this->setImageProfilFile($value);
                break;
            case 21:
                $this->setImageCouvFile($value);
                break;
            case 22:
                $this->setTelephone($value);
                break;
            case 23:
                $this->setFirstConnection($value);
                break;
            case 24:
                $this->setValidated($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = UserPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setCityId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setProfileId($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setPromoId($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setEmail($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setPassword($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setLastname($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setFirstname($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setDescription($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setNewsletter($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setBirthday($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setUrlPortfolio($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setUrlLinkedin($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setUrlFacebook($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setUrlGoogle($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setUrlTwitter($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setUrlTumblr($arr[$keys[16]]);
        if (array_key_exists($keys[17], $arr)) $this->setUrlInstagram($arr[$keys[17]]);
        if (array_key_exists($keys[18], $arr)) $this->setUrlBlog($arr[$keys[18]]);
        if (array_key_exists($keys[19], $arr)) $this->setCvFile($arr[$keys[19]]);
        if (array_key_exists($keys[20], $arr)) $this->setImageProfilFile($arr[$keys[20]]);
        if (array_key_exists($keys[21], $arr)) $this->setImageCouvFile($arr[$keys[21]]);
        if (array_key_exists($keys[22], $arr)) $this->setTelephone($arr[$keys[22]]);
        if (array_key_exists($keys[23], $arr)) $this->setFirstConnection($arr[$keys[23]]);
        if (array_key_exists($keys[24], $arr)) $this->setValidated($arr[$keys[24]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(UserPeer::DATABASE_NAME);

        if ($this->isColumnModified(UserPeer::ID)) $criteria->add(UserPeer::ID, $this->id);
        if ($this->isColumnModified(UserPeer::CITY_ID)) $criteria->add(UserPeer::CITY_ID, $this->city_id);
        if ($this->isColumnModified(UserPeer::PROFILE_ID)) $criteria->add(UserPeer::PROFILE_ID, $this->profile_id);
        if ($this->isColumnModified(UserPeer::PROMO_ID)) $criteria->add(UserPeer::PROMO_ID, $this->promo_id);
        if ($this->isColumnModified(UserPeer::EMAIL)) $criteria->add(UserPeer::EMAIL, $this->email);
        if ($this->isColumnModified(UserPeer::PASSWORD)) $criteria->add(UserPeer::PASSWORD, $this->password);
        if ($this->isColumnModified(UserPeer::LASTNAME)) $criteria->add(UserPeer::LASTNAME, $this->lastname);
        if ($this->isColumnModified(UserPeer::FIRSTNAME)) $criteria->add(UserPeer::FIRSTNAME, $this->firstname);
        if ($this->isColumnModified(UserPeer::DESCRIPTION)) $criteria->add(UserPeer::DESCRIPTION, $this->description);
        if ($this->isColumnModified(UserPeer::NEWSLETTER)) $criteria->add(UserPeer::NEWSLETTER, $this->newsletter);
        if ($this->isColumnModified(UserPeer::BIRTHDAY)) $criteria->add(UserPeer::BIRTHDAY, $this->birthday);
        if ($this->isColumnModified(UserPeer::URL_PORTFOLIO)) $criteria->add(UserPeer::URL_PORTFOLIO, $this->url_portfolio);
        if ($this->isColumnModified(UserPeer::URL_LINKEDIN)) $criteria->add(UserPeer::URL_LINKEDIN, $this->url_linkedin);
        if ($this->isColumnModified(UserPeer::URL_FACEBOOK)) $criteria->add(UserPeer::URL_FACEBOOK, $this->url_facebook);
        if ($this->isColumnModified(UserPeer::URL_GOOGLE)) $criteria->add(UserPeer::URL_GOOGLE, $this->url_google);
        if ($this->isColumnModified(UserPeer::URL_TWITTER)) $criteria->add(UserPeer::URL_TWITTER, $this->url_twitter);
        if ($this->isColumnModified(UserPeer::URL_TUMBLR)) $criteria->add(UserPeer::URL_TUMBLR, $this->url_tumblr);
        if ($this->isColumnModified(UserPeer::URL_INSTAGRAM)) $criteria->add(UserPeer::URL_INSTAGRAM, $this->url_instagram);
        if ($this->isColumnModified(UserPeer::URL_BLOG)) $criteria->add(UserPeer::URL_BLOG, $this->url_blog);
        if ($this->isColumnModified(UserPeer::CV_FILE)) $criteria->add(UserPeer::CV_FILE, $this->cv_file);
        if ($this->isColumnModified(UserPeer::IMAGE_PROFIL_FILE)) $criteria->add(UserPeer::IMAGE_PROFIL_FILE, $this->image_profil_file);
        if ($this->isColumnModified(UserPeer::IMAGE_COUV_FILE)) $criteria->add(UserPeer::IMAGE_COUV_FILE, $this->image_couv_file);
        if ($this->isColumnModified(UserPeer::TELEPHONE)) $criteria->add(UserPeer::TELEPHONE, $this->telephone);
        if ($this->isColumnModified(UserPeer::FIRST_CONNECTION)) $criteria->add(UserPeer::FIRST_CONNECTION, $this->first_connection);
        if ($this->isColumnModified(UserPeer::VALIDATED)) $criteria->add(UserPeer::VALIDATED, $this->validated);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(UserPeer::DATABASE_NAME);
        $criteria->add(UserPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of User (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setCityId($this->getCityId());
        $copyObj->setProfileId($this->getProfileId());
        $copyObj->setPromoId($this->getPromoId());
        $copyObj->setEmail($this->getEmail());
        $copyObj->setPassword($this->getPassword());
        $copyObj->setLastname($this->getLastname());
        $copyObj->setFirstname($this->getFirstname());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setNewsletter($this->getNewsletter());
        $copyObj->setBirthday($this->getBirthday());
        $copyObj->setUrlPortfolio($this->getUrlPortfolio());
        $copyObj->setUrlLinkedin($this->getUrlLinkedin());
        $copyObj->setUrlFacebook($this->getUrlFacebook());
        $copyObj->setUrlGoogle($this->getUrlGoogle());
        $copyObj->setUrlTwitter($this->getUrlTwitter());
        $copyObj->setUrlTumblr($this->getUrlTumblr());
        $copyObj->setUrlInstagram($this->getUrlInstagram());
        $copyObj->setUrlBlog($this->getUrlBlog());
        $copyObj->setCvFile($this->getCvFile());
        $copyObj->setImageProfilFile($this->getImageProfilFile());
        $copyObj->setImageCouvFile($this->getImageCouvFile());
        $copyObj->setTelephone($this->getTelephone());
        $copyObj->setFirstConnection($this->getFirstConnection());
        $copyObj->setValidated($this->getValidated());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getUserRoles() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUserRole($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getInternships() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addInternship($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUserJobs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUserJob($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUserObtainedDegrees() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUserObtainedDegree($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUnivProjects() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUnivProject($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUserMemberUnipros() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUserMemberUnipro($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getValidations() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addValidation($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return User Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return UserPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new UserPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a City object.
     *
     * @param                  City $v
     * @return User The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCity(City $v = null)
    {
        if ($v === null) {
            $this->setCityId(NULL);
        } else {
            $this->setCityId($v->getId());
        }

        $this->aCity = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the City object, it will not be re-added.
        if ($v !== null) {
            $v->addUser($this);
        }


        return $this;
    }


    /**
     * Get the associated City object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return City The associated City object.
     * @throws PropelException
     */
    public function getCity(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCity === null && ($this->city_id !== null) && $doQuery) {
            $this->aCity = CityQuery::create()->findPk($this->city_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCity->addUsers($this);
             */
        }

        return $this->aCity;
    }

    /**
     * Declares an association between this object and a Profession object.
     *
     * @param                  Profession $v
     * @return User The current object (for fluent API support)
     * @throws PropelException
     */
    public function setProfession(Profession $v = null)
    {
        if ($v === null) {
            $this->setProfileId(NULL);
        } else {
            $this->setProfileId($v->getId());
        }

        $this->aProfession = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Profession object, it will not be re-added.
        if ($v !== null) {
            $v->addUser($this);
        }


        return $this;
    }


    /**
     * Get the associated Profession object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Profession The associated Profession object.
     * @throws PropelException
     */
    public function getProfession(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aProfession === null && ($this->profile_id !== null) && $doQuery) {
            $this->aProfession = ProfessionQuery::create()->findPk($this->profile_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aProfession->addUsers($this);
             */
        }

        return $this->aProfession;
    }

    /**
     * Declares an association between this object and a Promo object.
     *
     * @param                  Promo $v
     * @return User The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPromo(Promo $v = null)
    {
        if ($v === null) {
            $this->setPromoId(NULL);
        } else {
            $this->setPromoId($v->getId());
        }

        $this->aPromo = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Promo object, it will not be re-added.
        if ($v !== null) {
            $v->addUser($this);
        }


        return $this;
    }


    /**
     * Get the associated Promo object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Promo The associated Promo object.
     * @throws PropelException
     */
    public function getPromo(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aPromo === null && ($this->promo_id !== null) && $doQuery) {
            $this->aPromo = PromoQuery::create()->findPk($this->promo_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPromo->addUsers($this);
             */
        }

        return $this->aPromo;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('UserRole' == $relationName) {
            $this->initUserRoles();
        }
        if ('Internship' == $relationName) {
            $this->initInternships();
        }
        if ('UserJob' == $relationName) {
            $this->initUserJobs();
        }
        if ('UserObtainedDegree' == $relationName) {
            $this->initUserObtainedDegrees();
        }
        if ('UnivProject' == $relationName) {
            $this->initUnivProjects();
        }
        if ('UserMemberUnipro' == $relationName) {
            $this->initUserMemberUnipros();
        }
        if ('Validation' == $relationName) {
            $this->initValidations();
        }
    }

    /**
     * Clears out the collUserRoles collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return User The current object (for fluent API support)
     * @see        addUserRoles()
     */
    public function clearUserRoles()
    {
        $this->collUserRoles = null; // important to set this to null since that means it is uninitialized
        $this->collUserRolesPartial = null;

        return $this;
    }

    /**
     * reset is the collUserRoles collection loaded partially
     *
     * @return void
     */
    public function resetPartialUserRoles($v = true)
    {
        $this->collUserRolesPartial = $v;
    }

    /**
     * Initializes the collUserRoles collection.
     *
     * By default this just sets the collUserRoles collection to an empty array (like clearcollUserRoles());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUserRoles($overrideExisting = true)
    {
        if (null !== $this->collUserRoles && !$overrideExisting) {
            return;
        }
        $this->collUserRoles = new PropelObjectCollection();
        $this->collUserRoles->setModel('UserRole');
    }

    /**
     * Gets an array of UserRole objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this User is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|UserRole[] List of UserRole objects
     * @throws PropelException
     */
    public function getUserRoles($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collUserRolesPartial && !$this->isNew();
        if (null === $this->collUserRoles || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collUserRoles) {
                // return empty collection
                $this->initUserRoles();
            } else {
                $collUserRoles = UserRoleQuery::create(null, $criteria)
                    ->filterByUser($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collUserRolesPartial && count($collUserRoles)) {
                      $this->initUserRoles(false);

                      foreach ($collUserRoles as $obj) {
                        if (false == $this->collUserRoles->contains($obj)) {
                          $this->collUserRoles->append($obj);
                        }
                      }

                      $this->collUserRolesPartial = true;
                    }

                    $collUserRoles->getInternalIterator()->rewind();

                    return $collUserRoles;
                }

                if ($partial && $this->collUserRoles) {
                    foreach ($this->collUserRoles as $obj) {
                        if ($obj->isNew()) {
                            $collUserRoles[] = $obj;
                        }
                    }
                }

                $this->collUserRoles = $collUserRoles;
                $this->collUserRolesPartial = false;
            }
        }

        return $this->collUserRoles;
    }

    /**
     * Sets a collection of UserRole objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $userRoles A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return User The current object (for fluent API support)
     */
    public function setUserRoles(PropelCollection $userRoles, PropelPDO $con = null)
    {
        $userRolesToDelete = $this->getUserRoles(new Criteria(), $con)->diff($userRoles);


        $this->userRolesScheduledForDeletion = $userRolesToDelete;

        foreach ($userRolesToDelete as $userRoleRemoved) {
            $userRoleRemoved->setUser(null);
        }

        $this->collUserRoles = null;
        foreach ($userRoles as $userRole) {
            $this->addUserRole($userRole);
        }

        $this->collUserRoles = $userRoles;
        $this->collUserRolesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related UserRole objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related UserRole objects.
     * @throws PropelException
     */
    public function countUserRoles(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collUserRolesPartial && !$this->isNew();
        if (null === $this->collUserRoles || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUserRoles) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUserRoles());
            }
            $query = UserRoleQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUser($this)
                ->count($con);
        }

        return count($this->collUserRoles);
    }

    /**
     * Method called to associate a UserRole object to this object
     * through the UserRole foreign key attribute.
     *
     * @param    UserRole $l UserRole
     * @return User The current object (for fluent API support)
     */
    public function addUserRole(UserRole $l)
    {
        if ($this->collUserRoles === null) {
            $this->initUserRoles();
            $this->collUserRolesPartial = true;
        }

        if (!in_array($l, $this->collUserRoles->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddUserRole($l);

            if ($this->userRolesScheduledForDeletion and $this->userRolesScheduledForDeletion->contains($l)) {
                $this->userRolesScheduledForDeletion->remove($this->userRolesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	UserRole $userRole The userRole object to add.
     */
    protected function doAddUserRole($userRole)
    {
        $this->collUserRoles[]= $userRole;
        $userRole->setUser($this);
    }

    /**
     * @param	UserRole $userRole The userRole object to remove.
     * @return User The current object (for fluent API support)
     */
    public function removeUserRole($userRole)
    {
        if ($this->getUserRoles()->contains($userRole)) {
            $this->collUserRoles->remove($this->collUserRoles->search($userRole));
            if (null === $this->userRolesScheduledForDeletion) {
                $this->userRolesScheduledForDeletion = clone $this->collUserRoles;
                $this->userRolesScheduledForDeletion->clear();
            }
            $this->userRolesScheduledForDeletion[]= clone $userRole;
            $userRole->setUser(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related UserRoles from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|UserRole[] List of UserRole objects
     */
    public function getUserRolesJoinRole($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = UserRoleQuery::create(null, $criteria);
        $query->joinWith('Role', $join_behavior);

        return $this->getUserRoles($query, $con);
    }

    /**
     * Clears out the collInternships collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return User The current object (for fluent API support)
     * @see        addInternships()
     */
    public function clearInternships()
    {
        $this->collInternships = null; // important to set this to null since that means it is uninitialized
        $this->collInternshipsPartial = null;

        return $this;
    }

    /**
     * reset is the collInternships collection loaded partially
     *
     * @return void
     */
    public function resetPartialInternships($v = true)
    {
        $this->collInternshipsPartial = $v;
    }

    /**
     * Initializes the collInternships collection.
     *
     * By default this just sets the collInternships collection to an empty array (like clearcollInternships());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initInternships($overrideExisting = true)
    {
        if (null !== $this->collInternships && !$overrideExisting) {
            return;
        }
        $this->collInternships = new PropelObjectCollection();
        $this->collInternships->setModel('Internship');
    }

    /**
     * Gets an array of Internship objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this User is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Internship[] List of Internship objects
     * @throws PropelException
     */
    public function getInternships($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collInternshipsPartial && !$this->isNew();
        if (null === $this->collInternships || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collInternships) {
                // return empty collection
                $this->initInternships();
            } else {
                $collInternships = InternshipQuery::create(null, $criteria)
                    ->filterByUser($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collInternshipsPartial && count($collInternships)) {
                      $this->initInternships(false);

                      foreach ($collInternships as $obj) {
                        if (false == $this->collInternships->contains($obj)) {
                          $this->collInternships->append($obj);
                        }
                      }

                      $this->collInternshipsPartial = true;
                    }

                    $collInternships->getInternalIterator()->rewind();

                    return $collInternships;
                }

                if ($partial && $this->collInternships) {
                    foreach ($this->collInternships as $obj) {
                        if ($obj->isNew()) {
                            $collInternships[] = $obj;
                        }
                    }
                }

                $this->collInternships = $collInternships;
                $this->collInternshipsPartial = false;
            }
        }

        return $this->collInternships;
    }

    /**
     * Sets a collection of Internship objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $internships A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return User The current object (for fluent API support)
     */
    public function setInternships(PropelCollection $internships, PropelPDO $con = null)
    {
        $internshipsToDelete = $this->getInternships(new Criteria(), $con)->diff($internships);


        $this->internshipsScheduledForDeletion = $internshipsToDelete;

        foreach ($internshipsToDelete as $internshipRemoved) {
            $internshipRemoved->setUser(null);
        }

        $this->collInternships = null;
        foreach ($internships as $internship) {
            $this->addInternship($internship);
        }

        $this->collInternships = $internships;
        $this->collInternshipsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Internship objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Internship objects.
     * @throws PropelException
     */
    public function countInternships(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collInternshipsPartial && !$this->isNew();
        if (null === $this->collInternships || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collInternships) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getInternships());
            }
            $query = InternshipQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUser($this)
                ->count($con);
        }

        return count($this->collInternships);
    }

    /**
     * Method called to associate a Internship object to this object
     * through the Internship foreign key attribute.
     *
     * @param    Internship $l Internship
     * @return User The current object (for fluent API support)
     */
    public function addInternship(Internship $l)
    {
        if ($this->collInternships === null) {
            $this->initInternships();
            $this->collInternshipsPartial = true;
        }

        if (!in_array($l, $this->collInternships->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddInternship($l);

            if ($this->internshipsScheduledForDeletion and $this->internshipsScheduledForDeletion->contains($l)) {
                $this->internshipsScheduledForDeletion->remove($this->internshipsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	Internship $internship The internship object to add.
     */
    protected function doAddInternship($internship)
    {
        $this->collInternships[]= $internship;
        $internship->setUser($this);
    }

    /**
     * @param	Internship $internship The internship object to remove.
     * @return User The current object (for fluent API support)
     */
    public function removeInternship($internship)
    {
        if ($this->getInternships()->contains($internship)) {
            $this->collInternships->remove($this->collInternships->search($internship));
            if (null === $this->internshipsScheduledForDeletion) {
                $this->internshipsScheduledForDeletion = clone $this->collInternships;
                $this->internshipsScheduledForDeletion->clear();
            }
            $this->internshipsScheduledForDeletion[]= clone $internship;
            $internship->setUser(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related Internships from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Internship[] List of Internship objects
     */
    public function getInternshipsJoinOfferinternshipRelatedByOfferId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = InternshipQuery::create(null, $criteria);
        $query->joinWith('OfferinternshipRelatedByOfferId', $join_behavior);

        return $this->getInternships($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related Internships from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Internship[] List of Internship objects
     */
    public function getInternshipsJoinCompany($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = InternshipQuery::create(null, $criteria);
        $query->joinWith('Company', $join_behavior);

        return $this->getInternships($query, $con);
    }

    /**
     * Clears out the collUserJobs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return User The current object (for fluent API support)
     * @see        addUserJobs()
     */
    public function clearUserJobs()
    {
        $this->collUserJobs = null; // important to set this to null since that means it is uninitialized
        $this->collUserJobsPartial = null;

        return $this;
    }

    /**
     * reset is the collUserJobs collection loaded partially
     *
     * @return void
     */
    public function resetPartialUserJobs($v = true)
    {
        $this->collUserJobsPartial = $v;
    }

    /**
     * Initializes the collUserJobs collection.
     *
     * By default this just sets the collUserJobs collection to an empty array (like clearcollUserJobs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUserJobs($overrideExisting = true)
    {
        if (null !== $this->collUserJobs && !$overrideExisting) {
            return;
        }
        $this->collUserJobs = new PropelObjectCollection();
        $this->collUserJobs->setModel('UserJob');
    }

    /**
     * Gets an array of UserJob objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this User is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|UserJob[] List of UserJob objects
     * @throws PropelException
     */
    public function getUserJobs($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collUserJobsPartial && !$this->isNew();
        if (null === $this->collUserJobs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collUserJobs) {
                // return empty collection
                $this->initUserJobs();
            } else {
                $collUserJobs = UserJobQuery::create(null, $criteria)
                    ->filterByUser($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collUserJobsPartial && count($collUserJobs)) {
                      $this->initUserJobs(false);

                      foreach ($collUserJobs as $obj) {
                        if (false == $this->collUserJobs->contains($obj)) {
                          $this->collUserJobs->append($obj);
                        }
                      }

                      $this->collUserJobsPartial = true;
                    }

                    $collUserJobs->getInternalIterator()->rewind();

                    return $collUserJobs;
                }

                if ($partial && $this->collUserJobs) {
                    foreach ($this->collUserJobs as $obj) {
                        if ($obj->isNew()) {
                            $collUserJobs[] = $obj;
                        }
                    }
                }

                $this->collUserJobs = $collUserJobs;
                $this->collUserJobsPartial = false;
            }
        }

        return $this->collUserJobs;
    }

    /**
     * Sets a collection of UserJob objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $userJobs A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return User The current object (for fluent API support)
     */
    public function setUserJobs(PropelCollection $userJobs, PropelPDO $con = null)
    {
        $userJobsToDelete = $this->getUserJobs(new Criteria(), $con)->diff($userJobs);


        $this->userJobsScheduledForDeletion = $userJobsToDelete;

        foreach ($userJobsToDelete as $userJobRemoved) {
            $userJobRemoved->setUser(null);
        }

        $this->collUserJobs = null;
        foreach ($userJobs as $userJob) {
            $this->addUserJob($userJob);
        }

        $this->collUserJobs = $userJobs;
        $this->collUserJobsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related UserJob objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related UserJob objects.
     * @throws PropelException
     */
    public function countUserJobs(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collUserJobsPartial && !$this->isNew();
        if (null === $this->collUserJobs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUserJobs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUserJobs());
            }
            $query = UserJobQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUser($this)
                ->count($con);
        }

        return count($this->collUserJobs);
    }

    /**
     * Method called to associate a UserJob object to this object
     * through the UserJob foreign key attribute.
     *
     * @param    UserJob $l UserJob
     * @return User The current object (for fluent API support)
     */
    public function addUserJob(UserJob $l)
    {
        if ($this->collUserJobs === null) {
            $this->initUserJobs();
            $this->collUserJobsPartial = true;
        }

        if (!in_array($l, $this->collUserJobs->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddUserJob($l);

            if ($this->userJobsScheduledForDeletion and $this->userJobsScheduledForDeletion->contains($l)) {
                $this->userJobsScheduledForDeletion->remove($this->userJobsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	UserJob $userJob The userJob object to add.
     */
    protected function doAddUserJob($userJob)
    {
        $this->collUserJobs[]= $userJob;
        $userJob->setUser($this);
    }

    /**
     * @param	UserJob $userJob The userJob object to remove.
     * @return User The current object (for fluent API support)
     */
    public function removeUserJob($userJob)
    {
        if ($this->getUserJobs()->contains($userJob)) {
            $this->collUserJobs->remove($this->collUserJobs->search($userJob));
            if (null === $this->userJobsScheduledForDeletion) {
                $this->userJobsScheduledForDeletion = clone $this->collUserJobs;
                $this->userJobsScheduledForDeletion->clear();
            }
            $this->userJobsScheduledForDeletion[]= clone $userJob;
            $userJob->setUser(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related UserJobs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|UserJob[] List of UserJob objects
     */
    public function getUserJobsJoinProfession($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = UserJobQuery::create(null, $criteria);
        $query->joinWith('Profession', $join_behavior);

        return $this->getUserJobs($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related UserJobs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|UserJob[] List of UserJob objects
     */
    public function getUserJobsJoinCompany($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = UserJobQuery::create(null, $criteria);
        $query->joinWith('Company', $join_behavior);

        return $this->getUserJobs($query, $con);
    }

    /**
     * Clears out the collUserObtainedDegrees collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return User The current object (for fluent API support)
     * @see        addUserObtainedDegrees()
     */
    public function clearUserObtainedDegrees()
    {
        $this->collUserObtainedDegrees = null; // important to set this to null since that means it is uninitialized
        $this->collUserObtainedDegreesPartial = null;

        return $this;
    }

    /**
     * reset is the collUserObtainedDegrees collection loaded partially
     *
     * @return void
     */
    public function resetPartialUserObtainedDegrees($v = true)
    {
        $this->collUserObtainedDegreesPartial = $v;
    }

    /**
     * Initializes the collUserObtainedDegrees collection.
     *
     * By default this just sets the collUserObtainedDegrees collection to an empty array (like clearcollUserObtainedDegrees());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUserObtainedDegrees($overrideExisting = true)
    {
        if (null !== $this->collUserObtainedDegrees && !$overrideExisting) {
            return;
        }
        $this->collUserObtainedDegrees = new PropelObjectCollection();
        $this->collUserObtainedDegrees->setModel('UserObtainedDegree');
    }

    /**
     * Gets an array of UserObtainedDegree objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this User is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|UserObtainedDegree[] List of UserObtainedDegree objects
     * @throws PropelException
     */
    public function getUserObtainedDegrees($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collUserObtainedDegreesPartial && !$this->isNew();
        if (null === $this->collUserObtainedDegrees || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collUserObtainedDegrees) {
                // return empty collection
                $this->initUserObtainedDegrees();
            } else {
                $collUserObtainedDegrees = UserObtainedDegreeQuery::create(null, $criteria)
                    ->filterByUser($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collUserObtainedDegreesPartial && count($collUserObtainedDegrees)) {
                      $this->initUserObtainedDegrees(false);

                      foreach ($collUserObtainedDegrees as $obj) {
                        if (false == $this->collUserObtainedDegrees->contains($obj)) {
                          $this->collUserObtainedDegrees->append($obj);
                        }
                      }

                      $this->collUserObtainedDegreesPartial = true;
                    }

                    $collUserObtainedDegrees->getInternalIterator()->rewind();

                    return $collUserObtainedDegrees;
                }

                if ($partial && $this->collUserObtainedDegrees) {
                    foreach ($this->collUserObtainedDegrees as $obj) {
                        if ($obj->isNew()) {
                            $collUserObtainedDegrees[] = $obj;
                        }
                    }
                }

                $this->collUserObtainedDegrees = $collUserObtainedDegrees;
                $this->collUserObtainedDegreesPartial = false;
            }
        }

        return $this->collUserObtainedDegrees;
    }

    /**
     * Sets a collection of UserObtainedDegree objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $userObtainedDegrees A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return User The current object (for fluent API support)
     */
    public function setUserObtainedDegrees(PropelCollection $userObtainedDegrees, PropelPDO $con = null)
    {
        $userObtainedDegreesToDelete = $this->getUserObtainedDegrees(new Criteria(), $con)->diff($userObtainedDegrees);


        $this->userObtainedDegreesScheduledForDeletion = $userObtainedDegreesToDelete;

        foreach ($userObtainedDegreesToDelete as $userObtainedDegreeRemoved) {
            $userObtainedDegreeRemoved->setUser(null);
        }

        $this->collUserObtainedDegrees = null;
        foreach ($userObtainedDegrees as $userObtainedDegree) {
            $this->addUserObtainedDegree($userObtainedDegree);
        }

        $this->collUserObtainedDegrees = $userObtainedDegrees;
        $this->collUserObtainedDegreesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related UserObtainedDegree objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related UserObtainedDegree objects.
     * @throws PropelException
     */
    public function countUserObtainedDegrees(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collUserObtainedDegreesPartial && !$this->isNew();
        if (null === $this->collUserObtainedDegrees || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUserObtainedDegrees) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUserObtainedDegrees());
            }
            $query = UserObtainedDegreeQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUser($this)
                ->count($con);
        }

        return count($this->collUserObtainedDegrees);
    }

    /**
     * Method called to associate a UserObtainedDegree object to this object
     * through the UserObtainedDegree foreign key attribute.
     *
     * @param    UserObtainedDegree $l UserObtainedDegree
     * @return User The current object (for fluent API support)
     */
    public function addUserObtainedDegree(UserObtainedDegree $l)
    {
        if ($this->collUserObtainedDegrees === null) {
            $this->initUserObtainedDegrees();
            $this->collUserObtainedDegreesPartial = true;
        }

        if (!in_array($l, $this->collUserObtainedDegrees->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddUserObtainedDegree($l);

            if ($this->userObtainedDegreesScheduledForDeletion and $this->userObtainedDegreesScheduledForDeletion->contains($l)) {
                $this->userObtainedDegreesScheduledForDeletion->remove($this->userObtainedDegreesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	UserObtainedDegree $userObtainedDegree The userObtainedDegree object to add.
     */
    protected function doAddUserObtainedDegree($userObtainedDegree)
    {
        $this->collUserObtainedDegrees[]= $userObtainedDegree;
        $userObtainedDegree->setUser($this);
    }

    /**
     * @param	UserObtainedDegree $userObtainedDegree The userObtainedDegree object to remove.
     * @return User The current object (for fluent API support)
     */
    public function removeUserObtainedDegree($userObtainedDegree)
    {
        if ($this->getUserObtainedDegrees()->contains($userObtainedDegree)) {
            $this->collUserObtainedDegrees->remove($this->collUserObtainedDegrees->search($userObtainedDegree));
            if (null === $this->userObtainedDegreesScheduledForDeletion) {
                $this->userObtainedDegreesScheduledForDeletion = clone $this->collUserObtainedDegrees;
                $this->userObtainedDegreesScheduledForDeletion->clear();
            }
            $this->userObtainedDegreesScheduledForDeletion[]= clone $userObtainedDegree;
            $userObtainedDegree->setUser(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related UserObtainedDegrees from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|UserObtainedDegree[] List of UserObtainedDegree objects
     */
    public function getUserObtainedDegreesJoinPromo($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = UserObtainedDegreeQuery::create(null, $criteria);
        $query->joinWith('Promo', $join_behavior);

        return $this->getUserObtainedDegrees($query, $con);
    }

    /**
     * Clears out the collUnivProjects collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return User The current object (for fluent API support)
     * @see        addUnivProjects()
     */
    public function clearUnivProjects()
    {
        $this->collUnivProjects = null; // important to set this to null since that means it is uninitialized
        $this->collUnivProjectsPartial = null;

        return $this;
    }

    /**
     * reset is the collUnivProjects collection loaded partially
     *
     * @return void
     */
    public function resetPartialUnivProjects($v = true)
    {
        $this->collUnivProjectsPartial = $v;
    }

    /**
     * Initializes the collUnivProjects collection.
     *
     * By default this just sets the collUnivProjects collection to an empty array (like clearcollUnivProjects());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUnivProjects($overrideExisting = true)
    {
        if (null !== $this->collUnivProjects && !$overrideExisting) {
            return;
        }
        $this->collUnivProjects = new PropelObjectCollection();
        $this->collUnivProjects->setModel('UnivProject');
    }

    /**
     * Gets an array of UnivProject objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this User is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|UnivProject[] List of UnivProject objects
     * @throws PropelException
     */
    public function getUnivProjects($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collUnivProjectsPartial && !$this->isNew();
        if (null === $this->collUnivProjects || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collUnivProjects) {
                // return empty collection
                $this->initUnivProjects();
            } else {
                $collUnivProjects = UnivProjectQuery::create(null, $criteria)
                    ->filterByUser($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collUnivProjectsPartial && count($collUnivProjects)) {
                      $this->initUnivProjects(false);

                      foreach ($collUnivProjects as $obj) {
                        if (false == $this->collUnivProjects->contains($obj)) {
                          $this->collUnivProjects->append($obj);
                        }
                      }

                      $this->collUnivProjectsPartial = true;
                    }

                    $collUnivProjects->getInternalIterator()->rewind();

                    return $collUnivProjects;
                }

                if ($partial && $this->collUnivProjects) {
                    foreach ($this->collUnivProjects as $obj) {
                        if ($obj->isNew()) {
                            $collUnivProjects[] = $obj;
                        }
                    }
                }

                $this->collUnivProjects = $collUnivProjects;
                $this->collUnivProjectsPartial = false;
            }
        }

        return $this->collUnivProjects;
    }

    /**
     * Sets a collection of UnivProject objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $univProjects A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return User The current object (for fluent API support)
     */
    public function setUnivProjects(PropelCollection $univProjects, PropelPDO $con = null)
    {
        $univProjectsToDelete = $this->getUnivProjects(new Criteria(), $con)->diff($univProjects);


        $this->univProjectsScheduledForDeletion = $univProjectsToDelete;

        foreach ($univProjectsToDelete as $univProjectRemoved) {
            $univProjectRemoved->setUser(null);
        }

        $this->collUnivProjects = null;
        foreach ($univProjects as $univProject) {
            $this->addUnivProject($univProject);
        }

        $this->collUnivProjects = $univProjects;
        $this->collUnivProjectsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related UnivProject objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related UnivProject objects.
     * @throws PropelException
     */
    public function countUnivProjects(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collUnivProjectsPartial && !$this->isNew();
        if (null === $this->collUnivProjects || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUnivProjects) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUnivProjects());
            }
            $query = UnivProjectQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUser($this)
                ->count($con);
        }

        return count($this->collUnivProjects);
    }

    /**
     * Method called to associate a UnivProject object to this object
     * through the UnivProject foreign key attribute.
     *
     * @param    UnivProject $l UnivProject
     * @return User The current object (for fluent API support)
     */
    public function addUnivProject(UnivProject $l)
    {
        if ($this->collUnivProjects === null) {
            $this->initUnivProjects();
            $this->collUnivProjectsPartial = true;
        }

        if (!in_array($l, $this->collUnivProjects->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddUnivProject($l);

            if ($this->univProjectsScheduledForDeletion and $this->univProjectsScheduledForDeletion->contains($l)) {
                $this->univProjectsScheduledForDeletion->remove($this->univProjectsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	UnivProject $univProject The univProject object to add.
     */
    protected function doAddUnivProject($univProject)
    {
        $this->collUnivProjects[]= $univProject;
        $univProject->setUser($this);
    }

    /**
     * @param	UnivProject $univProject The univProject object to remove.
     * @return User The current object (for fluent API support)
     */
    public function removeUnivProject($univProject)
    {
        if ($this->getUnivProjects()->contains($univProject)) {
            $this->collUnivProjects->remove($this->collUnivProjects->search($univProject));
            if (null === $this->univProjectsScheduledForDeletion) {
                $this->univProjectsScheduledForDeletion = clone $this->collUnivProjects;
                $this->univProjectsScheduledForDeletion->clear();
            }
            $this->univProjectsScheduledForDeletion[]= $univProject;
            $univProject->setUser(null);
        }

        return $this;
    }

    /**
     * Clears out the collUserMemberUnipros collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return User The current object (for fluent API support)
     * @see        addUserMemberUnipros()
     */
    public function clearUserMemberUnipros()
    {
        $this->collUserMemberUnipros = null; // important to set this to null since that means it is uninitialized
        $this->collUserMemberUniprosPartial = null;

        return $this;
    }

    /**
     * reset is the collUserMemberUnipros collection loaded partially
     *
     * @return void
     */
    public function resetPartialUserMemberUnipros($v = true)
    {
        $this->collUserMemberUniprosPartial = $v;
    }

    /**
     * Initializes the collUserMemberUnipros collection.
     *
     * By default this just sets the collUserMemberUnipros collection to an empty array (like clearcollUserMemberUnipros());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUserMemberUnipros($overrideExisting = true)
    {
        if (null !== $this->collUserMemberUnipros && !$overrideExisting) {
            return;
        }
        $this->collUserMemberUnipros = new PropelObjectCollection();
        $this->collUserMemberUnipros->setModel('UserMemberUnipro');
    }

    /**
     * Gets an array of UserMemberUnipro objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this User is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|UserMemberUnipro[] List of UserMemberUnipro objects
     * @throws PropelException
     */
    public function getUserMemberUnipros($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collUserMemberUniprosPartial && !$this->isNew();
        if (null === $this->collUserMemberUnipros || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collUserMemberUnipros) {
                // return empty collection
                $this->initUserMemberUnipros();
            } else {
                $collUserMemberUnipros = UserMemberUniproQuery::create(null, $criteria)
                    ->filterByUser($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collUserMemberUniprosPartial && count($collUserMemberUnipros)) {
                      $this->initUserMemberUnipros(false);

                      foreach ($collUserMemberUnipros as $obj) {
                        if (false == $this->collUserMemberUnipros->contains($obj)) {
                          $this->collUserMemberUnipros->append($obj);
                        }
                      }

                      $this->collUserMemberUniprosPartial = true;
                    }

                    $collUserMemberUnipros->getInternalIterator()->rewind();

                    return $collUserMemberUnipros;
                }

                if ($partial && $this->collUserMemberUnipros) {
                    foreach ($this->collUserMemberUnipros as $obj) {
                        if ($obj->isNew()) {
                            $collUserMemberUnipros[] = $obj;
                        }
                    }
                }

                $this->collUserMemberUnipros = $collUserMemberUnipros;
                $this->collUserMemberUniprosPartial = false;
            }
        }

        return $this->collUserMemberUnipros;
    }

    /**
     * Sets a collection of UserMemberUnipro objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $userMemberUnipros A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return User The current object (for fluent API support)
     */
    public function setUserMemberUnipros(PropelCollection $userMemberUnipros, PropelPDO $con = null)
    {
        $userMemberUniprosToDelete = $this->getUserMemberUnipros(new Criteria(), $con)->diff($userMemberUnipros);


        $this->userMemberUniprosScheduledForDeletion = $userMemberUniprosToDelete;

        foreach ($userMemberUniprosToDelete as $userMemberUniproRemoved) {
            $userMemberUniproRemoved->setUser(null);
        }

        $this->collUserMemberUnipros = null;
        foreach ($userMemberUnipros as $userMemberUnipro) {
            $this->addUserMemberUnipro($userMemberUnipro);
        }

        $this->collUserMemberUnipros = $userMemberUnipros;
        $this->collUserMemberUniprosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related UserMemberUnipro objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related UserMemberUnipro objects.
     * @throws PropelException
     */
    public function countUserMemberUnipros(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collUserMemberUniprosPartial && !$this->isNew();
        if (null === $this->collUserMemberUnipros || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUserMemberUnipros) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUserMemberUnipros());
            }
            $query = UserMemberUniproQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUser($this)
                ->count($con);
        }

        return count($this->collUserMemberUnipros);
    }

    /**
     * Method called to associate a UserMemberUnipro object to this object
     * through the UserMemberUnipro foreign key attribute.
     *
     * @param    UserMemberUnipro $l UserMemberUnipro
     * @return User The current object (for fluent API support)
     */
    public function addUserMemberUnipro(UserMemberUnipro $l)
    {
        if ($this->collUserMemberUnipros === null) {
            $this->initUserMemberUnipros();
            $this->collUserMemberUniprosPartial = true;
        }

        if (!in_array($l, $this->collUserMemberUnipros->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddUserMemberUnipro($l);

            if ($this->userMemberUniprosScheduledForDeletion and $this->userMemberUniprosScheduledForDeletion->contains($l)) {
                $this->userMemberUniprosScheduledForDeletion->remove($this->userMemberUniprosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	UserMemberUnipro $userMemberUnipro The userMemberUnipro object to add.
     */
    protected function doAddUserMemberUnipro($userMemberUnipro)
    {
        $this->collUserMemberUnipros[]= $userMemberUnipro;
        $userMemberUnipro->setUser($this);
    }

    /**
     * @param	UserMemberUnipro $userMemberUnipro The userMemberUnipro object to remove.
     * @return User The current object (for fluent API support)
     */
    public function removeUserMemberUnipro($userMemberUnipro)
    {
        if ($this->getUserMemberUnipros()->contains($userMemberUnipro)) {
            $this->collUserMemberUnipros->remove($this->collUserMemberUnipros->search($userMemberUnipro));
            if (null === $this->userMemberUniprosScheduledForDeletion) {
                $this->userMemberUniprosScheduledForDeletion = clone $this->collUserMemberUnipros;
                $this->userMemberUniprosScheduledForDeletion->clear();
            }
            $this->userMemberUniprosScheduledForDeletion[]= clone $userMemberUnipro;
            $userMemberUnipro->setUser(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related UserMemberUnipros from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|UserMemberUnipro[] List of UserMemberUnipro objects
     */
    public function getUserMemberUniprosJoinUnivProject($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = UserMemberUniproQuery::create(null, $criteria);
        $query->joinWith('UnivProject', $join_behavior);

        return $this->getUserMemberUnipros($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related UserMemberUnipros from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|UserMemberUnipro[] List of UserMemberUnipro objects
     */
    public function getUserMemberUniprosJoinProfession($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = UserMemberUniproQuery::create(null, $criteria);
        $query->joinWith('Profession', $join_behavior);

        return $this->getUserMemberUnipros($query, $con);
    }

    /**
     * Clears out the collValidations collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return User The current object (for fluent API support)
     * @see        addValidations()
     */
    public function clearValidations()
    {
        $this->collValidations = null; // important to set this to null since that means it is uninitialized
        $this->collValidationsPartial = null;

        return $this;
    }

    /**
     * reset is the collValidations collection loaded partially
     *
     * @return void
     */
    public function resetPartialValidations($v = true)
    {
        $this->collValidationsPartial = $v;
    }

    /**
     * Initializes the collValidations collection.
     *
     * By default this just sets the collValidations collection to an empty array (like clearcollValidations());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initValidations($overrideExisting = true)
    {
        if (null !== $this->collValidations && !$overrideExisting) {
            return;
        }
        $this->collValidations = new PropelObjectCollection();
        $this->collValidations->setModel('Validation');
    }

    /**
     * Gets an array of Validation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this User is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|Validation[] List of Validation objects
     * @throws PropelException
     */
    public function getValidations($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collValidationsPartial && !$this->isNew();
        if (null === $this->collValidations || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collValidations) {
                // return empty collection
                $this->initValidations();
            } else {
                $collValidations = ValidationQuery::create(null, $criteria)
                    ->filterByUser($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collValidationsPartial && count($collValidations)) {
                      $this->initValidations(false);

                      foreach ($collValidations as $obj) {
                        if (false == $this->collValidations->contains($obj)) {
                          $this->collValidations->append($obj);
                        }
                      }

                      $this->collValidationsPartial = true;
                    }

                    $collValidations->getInternalIterator()->rewind();

                    return $collValidations;
                }

                if ($partial && $this->collValidations) {
                    foreach ($this->collValidations as $obj) {
                        if ($obj->isNew()) {
                            $collValidations[] = $obj;
                        }
                    }
                }

                $this->collValidations = $collValidations;
                $this->collValidationsPartial = false;
            }
        }

        return $this->collValidations;
    }

    /**
     * Sets a collection of Validation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $validations A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return User The current object (for fluent API support)
     */
    public function setValidations(PropelCollection $validations, PropelPDO $con = null)
    {
        $validationsToDelete = $this->getValidations(new Criteria(), $con)->diff($validations);


        $this->validationsScheduledForDeletion = $validationsToDelete;

        foreach ($validationsToDelete as $validationRemoved) {
            $validationRemoved->setUser(null);
        }

        $this->collValidations = null;
        foreach ($validations as $validation) {
            $this->addValidation($validation);
        }

        $this->collValidations = $validations;
        $this->collValidationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Validation objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related Validation objects.
     * @throws PropelException
     */
    public function countValidations(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collValidationsPartial && !$this->isNew();
        if (null === $this->collValidations || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collValidations) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getValidations());
            }
            $query = ValidationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUser($this)
                ->count($con);
        }

        return count($this->collValidations);
    }

    /**
     * Method called to associate a Validation object to this object
     * through the Validation foreign key attribute.
     *
     * @param    Validation $l Validation
     * @return User The current object (for fluent API support)
     */
    public function addValidation(Validation $l)
    {
        if ($this->collValidations === null) {
            $this->initValidations();
            $this->collValidationsPartial = true;
        }

        if (!in_array($l, $this->collValidations->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddValidation($l);

            if ($this->validationsScheduledForDeletion and $this->validationsScheduledForDeletion->contains($l)) {
                $this->validationsScheduledForDeletion->remove($this->validationsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	Validation $validation The validation object to add.
     */
    protected function doAddValidation($validation)
    {
        $this->collValidations[]= $validation;
        $validation->setUser($this);
    }

    /**
     * @param	Validation $validation The validation object to remove.
     * @return User The current object (for fluent API support)
     */
    public function removeValidation($validation)
    {
        if ($this->getValidations()->contains($validation)) {
            $this->collValidations->remove($this->collValidations->search($validation));
            if (null === $this->validationsScheduledForDeletion) {
                $this->validationsScheduledForDeletion = clone $this->collValidations;
                $this->validationsScheduledForDeletion->clear();
            }
            $this->validationsScheduledForDeletion[]= $validation;
            $validation->setUser(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related Validations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|Validation[] List of Validation objects
     */
    public function getValidationsJoinCompany($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = ValidationQuery::create(null, $criteria);
        $query->joinWith('Company', $join_behavior);

        return $this->getValidations($query, $con);
    }

    /**
     * Clears out the collRoles collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return User The current object (for fluent API support)
     * @see        addRoles()
     */
    public function clearRoles()
    {
        $this->collRoles = null; // important to set this to null since that means it is uninitialized
        $this->collRolesPartial = null;

        return $this;
    }

    /**
     * Initializes the collRoles collection.
     *
     * By default this just sets the collRoles collection to an empty collection (like clearRoles());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @return void
     */
    public function initRoles()
    {
        $this->collRoles = new PropelObjectCollection();
        $this->collRoles->setModel('Role');
    }

    /**
     * Gets a collection of Role objects related by a many-to-many relationship
     * to the current object by way of the user_role cross-reference table.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this User is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria Optional query object to filter the query
     * @param PropelPDO $con Optional connection object
     *
     * @return PropelObjectCollection|Role[] List of Role objects
     */
    public function getRoles($criteria = null, PropelPDO $con = null)
    {
        if (null === $this->collRoles || null !== $criteria) {
            if ($this->isNew() && null === $this->collRoles) {
                // return empty collection
                $this->initRoles();
            } else {
                $collRoles = RoleQuery::create(null, $criteria)
                    ->filterByUser($this)
                    ->find($con);
                if (null !== $criteria) {
                    return $collRoles;
                }
                $this->collRoles = $collRoles;
            }
        }

        return $this->collRoles;
    }

    /**
     * Sets a collection of Role objects related by a many-to-many relationship
     * to the current object by way of the user_role cross-reference table.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $roles A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return User The current object (for fluent API support)
     */
    public function setRoles(PropelCollection $roles, PropelPDO $con = null)
    {
        $this->clearRoles();
        $currentRoles = $this->getRoles(null, $con);

        $this->rolesScheduledForDeletion = $currentRoles->diff($roles);

        foreach ($roles as $role) {
            if (!$currentRoles->contains($role)) {
                $this->doAddRole($role);
            }
        }

        $this->collRoles = $roles;

        return $this;
    }

    /**
     * Gets the number of Role objects related by a many-to-many relationship
     * to the current object by way of the user_role cross-reference table.
     *
     * @param Criteria $criteria Optional query object to filter the query
     * @param boolean $distinct Set to true to force count distinct
     * @param PropelPDO $con Optional connection object
     *
     * @return int the number of related Role objects
     */
    public function countRoles($criteria = null, $distinct = false, PropelPDO $con = null)
    {
        if (null === $this->collRoles || null !== $criteria) {
            if ($this->isNew() && null === $this->collRoles) {
                return 0;
            } else {
                $query = RoleQuery::create(null, $criteria);
                if ($distinct) {
                    $query->distinct();
                }

                return $query
                    ->filterByUser($this)
                    ->count($con);
            }
        } else {
            return count($this->collRoles);
        }
    }

    /**
     * Associate a Role object to this object
     * through the user_role cross reference table.
     *
     * @param  Role $role The UserRole object to relate
     * @return User The current object (for fluent API support)
     */
    public function addRole(Role $role)
    {
        if ($this->collRoles === null) {
            $this->initRoles();
        }

        if (!$this->collRoles->contains($role)) { // only add it if the **same** object is not already associated
            $this->doAddRole($role);
            $this->collRoles[] = $role;

            if ($this->rolesScheduledForDeletion and $this->rolesScheduledForDeletion->contains($role)) {
                $this->rolesScheduledForDeletion->remove($this->rolesScheduledForDeletion->search($role));
            }
        }

        return $this;
    }

    /**
     * @param	Role $role The role object to add.
     */
    protected function doAddRole(Role $role)
    {
        // set the back reference to this object directly as using provided method either results
        // in endless loop or in multiple relations
        if (!$role->getUsers()->contains($this)) { $userRole = new UserRole();
            $userRole->setRole($role);
            $this->addUserRole($userRole);

            $foreignCollection = $role->getUsers();
            $foreignCollection[] = $this;
        }
    }

    /**
     * Remove a Role object to this object
     * through the user_role cross reference table.
     *
     * @param Role $role The UserRole object to relate
     * @return User The current object (for fluent API support)
     */
    public function removeRole(Role $role)
    {
        if ($this->getRoles()->contains($role)) {
            $this->collRoles->remove($this->collRoles->search($role));
            if (null === $this->rolesScheduledForDeletion) {
                $this->rolesScheduledForDeletion = clone $this->collRoles;
                $this->rolesScheduledForDeletion->clear();
            }
            $this->rolesScheduledForDeletion[]= $role;
        }

        return $this;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->city_id = null;
        $this->profile_id = null;
        $this->promo_id = null;
        $this->email = null;
        $this->password = null;
        $this->lastname = null;
        $this->firstname = null;
        $this->description = null;
        $this->newsletter = null;
        $this->birthday = null;
        $this->url_portfolio = null;
        $this->url_linkedin = null;
        $this->url_facebook = null;
        $this->url_google = null;
        $this->url_twitter = null;
        $this->url_tumblr = null;
        $this->url_instagram = null;
        $this->url_blog = null;
        $this->cv_file = null;
        $this->image_profil_file = null;
        $this->image_couv_file = null;
        $this->telephone = null;
        $this->first_connection = null;
        $this->validated = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collUserRoles) {
                foreach ($this->collUserRoles as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collInternships) {
                foreach ($this->collInternships as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUserJobs) {
                foreach ($this->collUserJobs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUserObtainedDegrees) {
                foreach ($this->collUserObtainedDegrees as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUnivProjects) {
                foreach ($this->collUnivProjects as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUserMemberUnipros) {
                foreach ($this->collUserMemberUnipros as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collValidations) {
                foreach ($this->collValidations as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRoles) {
                foreach ($this->collRoles as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aCity instanceof Persistent) {
              $this->aCity->clearAllReferences($deep);
            }
            if ($this->aProfession instanceof Persistent) {
              $this->aProfession->clearAllReferences($deep);
            }
            if ($this->aPromo instanceof Persistent) {
              $this->aPromo->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collUserRoles instanceof PropelCollection) {
            $this->collUserRoles->clearIterator();
        }
        $this->collUserRoles = null;
        if ($this->collInternships instanceof PropelCollection) {
            $this->collInternships->clearIterator();
        }
        $this->collInternships = null;
        if ($this->collUserJobs instanceof PropelCollection) {
            $this->collUserJobs->clearIterator();
        }
        $this->collUserJobs = null;
        if ($this->collUserObtainedDegrees instanceof PropelCollection) {
            $this->collUserObtainedDegrees->clearIterator();
        }
        $this->collUserObtainedDegrees = null;
        if ($this->collUnivProjects instanceof PropelCollection) {
            $this->collUnivProjects->clearIterator();
        }
        $this->collUnivProjects = null;
        if ($this->collUserMemberUnipros instanceof PropelCollection) {
            $this->collUserMemberUnipros->clearIterator();
        }
        $this->collUserMemberUnipros = null;
        if ($this->collValidations instanceof PropelCollection) {
            $this->collValidations->clearIterator();
        }
        $this->collValidations = null;
        if ($this->collRoles instanceof PropelCollection) {
            $this->collRoles->clearIterator();
        }
        $this->collRoles = null;
        $this->aCity = null;
        $this->aProfession = null;
        $this->aPromo = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(UserPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
