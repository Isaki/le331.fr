<?php

namespace MMIBundle\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use MMIBundle\Model\UnivProject;
use MMIBundle\Model\UnivProjectPeer;
use MMIBundle\Model\UnivProjectQuery;
use MMIBundle\Model\User;
use MMIBundle\Model\UserMemberUnipro;

/**
 * @method UnivProjectQuery orderById($order = Criteria::ASC) Order by the id column
 * @method UnivProjectQuery orderByUserId($order = Criteria::ASC) Order by the user_id column
 * @method UnivProjectQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method UnivProjectQuery orderByNeed($order = Criteria::ASC) Order by the need column
 * @method UnivProjectQuery orderByBrief($order = Criteria::ASC) Order by the brief column
 * @method UnivProjectQuery orderByWebsite($order = Criteria::ASC) Order by the website column
 *
 * @method UnivProjectQuery groupById() Group by the id column
 * @method UnivProjectQuery groupByUserId() Group by the user_id column
 * @method UnivProjectQuery groupByName() Group by the name column
 * @method UnivProjectQuery groupByNeed() Group by the need column
 * @method UnivProjectQuery groupByBrief() Group by the brief column
 * @method UnivProjectQuery groupByWebsite() Group by the website column
 *
 * @method UnivProjectQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method UnivProjectQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method UnivProjectQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method UnivProjectQuery leftJoinUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the User relation
 * @method UnivProjectQuery rightJoinUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the User relation
 * @method UnivProjectQuery innerJoinUser($relationAlias = null) Adds a INNER JOIN clause to the query using the User relation
 *
 * @method UnivProjectQuery leftJoinUserMemberUnipro($relationAlias = null) Adds a LEFT JOIN clause to the query using the UserMemberUnipro relation
 * @method UnivProjectQuery rightJoinUserMemberUnipro($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UserMemberUnipro relation
 * @method UnivProjectQuery innerJoinUserMemberUnipro($relationAlias = null) Adds a INNER JOIN clause to the query using the UserMemberUnipro relation
 *
 * @method UnivProject findOne(PropelPDO $con = null) Return the first UnivProject matching the query
 * @method UnivProject findOneOrCreate(PropelPDO $con = null) Return the first UnivProject matching the query, or a new UnivProject object populated from the query conditions when no match is found
 *
 * @method UnivProject findOneByUserId(string $user_id) Return the first UnivProject filtered by the user_id column
 * @method UnivProject findOneByName(string $name) Return the first UnivProject filtered by the name column
 * @method UnivProject findOneByNeed(string $need) Return the first UnivProject filtered by the need column
 * @method UnivProject findOneByBrief(string $brief) Return the first UnivProject filtered by the brief column
 * @method UnivProject findOneByWebsite(string $website) Return the first UnivProject filtered by the website column
 *
 * @method array findById(int $id) Return UnivProject objects filtered by the id column
 * @method array findByUserId(string $user_id) Return UnivProject objects filtered by the user_id column
 * @method array findByName(string $name) Return UnivProject objects filtered by the name column
 * @method array findByNeed(string $need) Return UnivProject objects filtered by the need column
 * @method array findByBrief(string $brief) Return UnivProject objects filtered by the brief column
 * @method array findByWebsite(string $website) Return UnivProject objects filtered by the website column
 */
abstract class BaseUnivProjectQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseUnivProjectQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'default';
        }
        if (null === $modelName) {
            $modelName = 'MMIBundle\\Model\\UnivProject';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new UnivProjectQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   UnivProjectQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return UnivProjectQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof UnivProjectQuery) {
            return $criteria;
        }
        $query = new UnivProjectQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   UnivProject|UnivProject[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = UnivProjectPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(UnivProjectPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 UnivProject A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 UnivProject A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `user_id`, `name`, `need`, `brief`, `website` FROM `univ_project` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new UnivProject();
            $obj->hydrate($row);
            UnivProjectPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return UnivProject|UnivProject[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|UnivProject[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return UnivProjectQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(UnivProjectPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return UnivProjectQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(UnivProjectPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UnivProjectQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(UnivProjectPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(UnivProjectPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UnivProjectPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUserId('fooValue');   // WHERE user_id = 'fooValue'
     * $query->filterByUserId('%fooValue%'); // WHERE user_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $userId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UnivProjectQuery The current query, for fluid interface
     */
    public function filterByUserId($userId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($userId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $userId)) {
                $userId = str_replace('*', '%', $userId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UnivProjectPeer::USER_ID, $userId, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UnivProjectQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UnivProjectPeer::NAME, $name, $comparison);
    }

    /**
     * Filter the query on the need column
     *
     * Example usage:
     * <code>
     * $query->filterByNeed('fooValue');   // WHERE need = 'fooValue'
     * $query->filterByNeed('%fooValue%'); // WHERE need LIKE '%fooValue%'
     * </code>
     *
     * @param     string $need The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UnivProjectQuery The current query, for fluid interface
     */
    public function filterByNeed($need = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($need)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $need)) {
                $need = str_replace('*', '%', $need);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UnivProjectPeer::NEED, $need, $comparison);
    }

    /**
     * Filter the query on the brief column
     *
     * Example usage:
     * <code>
     * $query->filterByBrief('fooValue');   // WHERE brief = 'fooValue'
     * $query->filterByBrief('%fooValue%'); // WHERE brief LIKE '%fooValue%'
     * </code>
     *
     * @param     string $brief The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UnivProjectQuery The current query, for fluid interface
     */
    public function filterByBrief($brief = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($brief)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $brief)) {
                $brief = str_replace('*', '%', $brief);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UnivProjectPeer::BRIEF, $brief, $comparison);
    }

    /**
     * Filter the query on the website column
     *
     * Example usage:
     * <code>
     * $query->filterByWebsite('fooValue');   // WHERE website = 'fooValue'
     * $query->filterByWebsite('%fooValue%'); // WHERE website LIKE '%fooValue%'
     * </code>
     *
     * @param     string $website The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UnivProjectQuery The current query, for fluid interface
     */
    public function filterByWebsite($website = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($website)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $website)) {
                $website = str_replace('*', '%', $website);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UnivProjectPeer::WEBSITE, $website, $comparison);
    }

    /**
     * Filter the query by a related User object
     *
     * @param   User|PropelObjectCollection $user The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 UnivProjectQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByUser($user, $comparison = null)
    {
        if ($user instanceof User) {
            return $this
                ->addUsingAlias(UnivProjectPeer::USER_ID, $user->getId(), $comparison);
        } elseif ($user instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UnivProjectPeer::USER_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUser() only accepts arguments of type User or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the User relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return UnivProjectQuery The current query, for fluid interface
     */
    public function joinUser($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('User');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'User');
        }

        return $this;
    }

    /**
     * Use the User relation User object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MMIBundle\Model\UserQuery A secondary query class using the current class as primary query
     */
    public function useUserQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'User', '\MMIBundle\Model\UserQuery');
    }

    /**
     * Filter the query by a related UserMemberUnipro object
     *
     * @param   UserMemberUnipro|PropelObjectCollection $userMemberUnipro  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 UnivProjectQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByUserMemberUnipro($userMemberUnipro, $comparison = null)
    {
        if ($userMemberUnipro instanceof UserMemberUnipro) {
            return $this
                ->addUsingAlias(UnivProjectPeer::ID, $userMemberUnipro->getUniproId(), $comparison);
        } elseif ($userMemberUnipro instanceof PropelObjectCollection) {
            return $this
                ->useUserMemberUniproQuery()
                ->filterByPrimaryKeys($userMemberUnipro->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUserMemberUnipro() only accepts arguments of type UserMemberUnipro or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UserMemberUnipro relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return UnivProjectQuery The current query, for fluid interface
     */
    public function joinUserMemberUnipro($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UserMemberUnipro');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UserMemberUnipro');
        }

        return $this;
    }

    /**
     * Use the UserMemberUnipro relation UserMemberUnipro object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MMIBundle\Model\UserMemberUniproQuery A secondary query class using the current class as primary query
     */
    public function useUserMemberUniproQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUserMemberUnipro($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UserMemberUnipro', '\MMIBundle\Model\UserMemberUniproQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   UnivProject $univProject Object to remove from the list of results
     *
     * @return UnivProjectQuery The current query, for fluid interface
     */
    public function prune($univProject = null)
    {
        if ($univProject) {
            $this->addUsingAlias(UnivProjectPeer::ID, $univProject->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
