<?php

namespace MMIBundle\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use MMIBundle\Model\Profession;
use MMIBundle\Model\UnivProject;
use MMIBundle\Model\User;
use MMIBundle\Model\UserMemberUnipro;
use MMIBundle\Model\UserMemberUniproPeer;
use MMIBundle\Model\UserMemberUniproQuery;

/**
 * @method UserMemberUniproQuery orderById($order = Criteria::ASC) Order by the id column
 * @method UserMemberUniproQuery orderByUserId($order = Criteria::ASC) Order by the user_id column
 * @method UserMemberUniproQuery orderByUniproId($order = Criteria::ASC) Order by the unipro_id column
 * @method UserMemberUniproQuery orderByProfessionId($order = Criteria::ASC) Order by the profession_id column
 * @method UserMemberUniproQuery orderByYearFrom($order = Criteria::ASC) Order by the year_from column
 * @method UserMemberUniproQuery orderByYearTo($order = Criteria::ASC) Order by the year_to column
 *
 * @method UserMemberUniproQuery groupById() Group by the id column
 * @method UserMemberUniproQuery groupByUserId() Group by the user_id column
 * @method UserMemberUniproQuery groupByUniproId() Group by the unipro_id column
 * @method UserMemberUniproQuery groupByProfessionId() Group by the profession_id column
 * @method UserMemberUniproQuery groupByYearFrom() Group by the year_from column
 * @method UserMemberUniproQuery groupByYearTo() Group by the year_to column
 *
 * @method UserMemberUniproQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method UserMemberUniproQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method UserMemberUniproQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method UserMemberUniproQuery leftJoinUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the User relation
 * @method UserMemberUniproQuery rightJoinUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the User relation
 * @method UserMemberUniproQuery innerJoinUser($relationAlias = null) Adds a INNER JOIN clause to the query using the User relation
 *
 * @method UserMemberUniproQuery leftJoinUnivProject($relationAlias = null) Adds a LEFT JOIN clause to the query using the UnivProject relation
 * @method UserMemberUniproQuery rightJoinUnivProject($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UnivProject relation
 * @method UserMemberUniproQuery innerJoinUnivProject($relationAlias = null) Adds a INNER JOIN clause to the query using the UnivProject relation
 *
 * @method UserMemberUniproQuery leftJoinProfession($relationAlias = null) Adds a LEFT JOIN clause to the query using the Profession relation
 * @method UserMemberUniproQuery rightJoinProfession($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Profession relation
 * @method UserMemberUniproQuery innerJoinProfession($relationAlias = null) Adds a INNER JOIN clause to the query using the Profession relation
 *
 * @method UserMemberUnipro findOne(PropelPDO $con = null) Return the first UserMemberUnipro matching the query
 * @method UserMemberUnipro findOneOrCreate(PropelPDO $con = null) Return the first UserMemberUnipro matching the query, or a new UserMemberUnipro object populated from the query conditions when no match is found
 *
 * @method UserMemberUnipro findOneByUserId(int $user_id) Return the first UserMemberUnipro filtered by the user_id column
 * @method UserMemberUnipro findOneByUniproId(int $unipro_id) Return the first UserMemberUnipro filtered by the unipro_id column
 * @method UserMemberUnipro findOneByProfessionId(int $profession_id) Return the first UserMemberUnipro filtered by the profession_id column
 * @method UserMemberUnipro findOneByYearFrom(int $year_from) Return the first UserMemberUnipro filtered by the year_from column
 * @method UserMemberUnipro findOneByYearTo(int $year_to) Return the first UserMemberUnipro filtered by the year_to column
 *
 * @method array findById(int $id) Return UserMemberUnipro objects filtered by the id column
 * @method array findByUserId(int $user_id) Return UserMemberUnipro objects filtered by the user_id column
 * @method array findByUniproId(int $unipro_id) Return UserMemberUnipro objects filtered by the unipro_id column
 * @method array findByProfessionId(int $profession_id) Return UserMemberUnipro objects filtered by the profession_id column
 * @method array findByYearFrom(int $year_from) Return UserMemberUnipro objects filtered by the year_from column
 * @method array findByYearTo(int $year_to) Return UserMemberUnipro objects filtered by the year_to column
 */
abstract class BaseUserMemberUniproQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseUserMemberUniproQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'default';
        }
        if (null === $modelName) {
            $modelName = 'MMIBundle\\Model\\UserMemberUnipro';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new UserMemberUniproQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   UserMemberUniproQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return UserMemberUniproQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof UserMemberUniproQuery) {
            return $criteria;
        }
        $query = new UserMemberUniproQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   UserMemberUnipro|UserMemberUnipro[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = UserMemberUniproPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(UserMemberUniproPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 UserMemberUnipro A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 UserMemberUnipro A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `user_id`, `unipro_id`, `profession_id`, `year_from`, `year_to` FROM `user_member_unipro` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new UserMemberUnipro();
            $obj->hydrate($row);
            UserMemberUniproPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return UserMemberUnipro|UserMemberUnipro[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|UserMemberUnipro[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return UserMemberUniproQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(UserMemberUniproPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return UserMemberUniproQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(UserMemberUniproPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserMemberUniproQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(UserMemberUniproPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(UserMemberUniproPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserMemberUniproPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUserId(1234); // WHERE user_id = 1234
     * $query->filterByUserId(array(12, 34)); // WHERE user_id IN (12, 34)
     * $query->filterByUserId(array('min' => 12)); // WHERE user_id >= 12
     * $query->filterByUserId(array('max' => 12)); // WHERE user_id <= 12
     * </code>
     *
     * @see       filterByUser()
     *
     * @param     mixed $userId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserMemberUniproQuery The current query, for fluid interface
     */
    public function filterByUserId($userId = null, $comparison = null)
    {
        if (is_array($userId)) {
            $useMinMax = false;
            if (isset($userId['min'])) {
                $this->addUsingAlias(UserMemberUniproPeer::USER_ID, $userId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userId['max'])) {
                $this->addUsingAlias(UserMemberUniproPeer::USER_ID, $userId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserMemberUniproPeer::USER_ID, $userId, $comparison);
    }

    /**
     * Filter the query on the unipro_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUniproId(1234); // WHERE unipro_id = 1234
     * $query->filterByUniproId(array(12, 34)); // WHERE unipro_id IN (12, 34)
     * $query->filterByUniproId(array('min' => 12)); // WHERE unipro_id >= 12
     * $query->filterByUniproId(array('max' => 12)); // WHERE unipro_id <= 12
     * </code>
     *
     * @see       filterByUnivProject()
     *
     * @param     mixed $uniproId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserMemberUniproQuery The current query, for fluid interface
     */
    public function filterByUniproId($uniproId = null, $comparison = null)
    {
        if (is_array($uniproId)) {
            $useMinMax = false;
            if (isset($uniproId['min'])) {
                $this->addUsingAlias(UserMemberUniproPeer::UNIPRO_ID, $uniproId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($uniproId['max'])) {
                $this->addUsingAlias(UserMemberUniproPeer::UNIPRO_ID, $uniproId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserMemberUniproPeer::UNIPRO_ID, $uniproId, $comparison);
    }

    /**
     * Filter the query on the profession_id column
     *
     * Example usage:
     * <code>
     * $query->filterByProfessionId(1234); // WHERE profession_id = 1234
     * $query->filterByProfessionId(array(12, 34)); // WHERE profession_id IN (12, 34)
     * $query->filterByProfessionId(array('min' => 12)); // WHERE profession_id >= 12
     * $query->filterByProfessionId(array('max' => 12)); // WHERE profession_id <= 12
     * </code>
     *
     * @see       filterByProfession()
     *
     * @param     mixed $professionId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserMemberUniproQuery The current query, for fluid interface
     */
    public function filterByProfessionId($professionId = null, $comparison = null)
    {
        if (is_array($professionId)) {
            $useMinMax = false;
            if (isset($professionId['min'])) {
                $this->addUsingAlias(UserMemberUniproPeer::PROFESSION_ID, $professionId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($professionId['max'])) {
                $this->addUsingAlias(UserMemberUniproPeer::PROFESSION_ID, $professionId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserMemberUniproPeer::PROFESSION_ID, $professionId, $comparison);
    }

    /**
     * Filter the query on the year_from column
     *
     * Example usage:
     * <code>
     * $query->filterByYearFrom(1234); // WHERE year_from = 1234
     * $query->filterByYearFrom(array(12, 34)); // WHERE year_from IN (12, 34)
     * $query->filterByYearFrom(array('min' => 12)); // WHERE year_from >= 12
     * $query->filterByYearFrom(array('max' => 12)); // WHERE year_from <= 12
     * </code>
     *
     * @param     mixed $yearFrom The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserMemberUniproQuery The current query, for fluid interface
     */
    public function filterByYearFrom($yearFrom = null, $comparison = null)
    {
        if (is_array($yearFrom)) {
            $useMinMax = false;
            if (isset($yearFrom['min'])) {
                $this->addUsingAlias(UserMemberUniproPeer::YEAR_FROM, $yearFrom['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($yearFrom['max'])) {
                $this->addUsingAlias(UserMemberUniproPeer::YEAR_FROM, $yearFrom['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserMemberUniproPeer::YEAR_FROM, $yearFrom, $comparison);
    }

    /**
     * Filter the query on the year_to column
     *
     * Example usage:
     * <code>
     * $query->filterByYearTo(1234); // WHERE year_to = 1234
     * $query->filterByYearTo(array(12, 34)); // WHERE year_to IN (12, 34)
     * $query->filterByYearTo(array('min' => 12)); // WHERE year_to >= 12
     * $query->filterByYearTo(array('max' => 12)); // WHERE year_to <= 12
     * </code>
     *
     * @param     mixed $yearTo The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserMemberUniproQuery The current query, for fluid interface
     */
    public function filterByYearTo($yearTo = null, $comparison = null)
    {
        if (is_array($yearTo)) {
            $useMinMax = false;
            if (isset($yearTo['min'])) {
                $this->addUsingAlias(UserMemberUniproPeer::YEAR_TO, $yearTo['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($yearTo['max'])) {
                $this->addUsingAlias(UserMemberUniproPeer::YEAR_TO, $yearTo['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserMemberUniproPeer::YEAR_TO, $yearTo, $comparison);
    }

    /**
     * Filter the query by a related User object
     *
     * @param   User|PropelObjectCollection $user The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 UserMemberUniproQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByUser($user, $comparison = null)
    {
        if ($user instanceof User) {
            return $this
                ->addUsingAlias(UserMemberUniproPeer::USER_ID, $user->getId(), $comparison);
        } elseif ($user instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UserMemberUniproPeer::USER_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUser() only accepts arguments of type User or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the User relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return UserMemberUniproQuery The current query, for fluid interface
     */
    public function joinUser($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('User');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'User');
        }

        return $this;
    }

    /**
     * Use the User relation User object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MMIBundle\Model\UserQuery A secondary query class using the current class as primary query
     */
    public function useUserQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'User', '\MMIBundle\Model\UserQuery');
    }

    /**
     * Filter the query by a related UnivProject object
     *
     * @param   UnivProject|PropelObjectCollection $univProject The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 UserMemberUniproQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByUnivProject($univProject, $comparison = null)
    {
        if ($univProject instanceof UnivProject) {
            return $this
                ->addUsingAlias(UserMemberUniproPeer::UNIPRO_ID, $univProject->getId(), $comparison);
        } elseif ($univProject instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UserMemberUniproPeer::UNIPRO_ID, $univProject->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUnivProject() only accepts arguments of type UnivProject or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UnivProject relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return UserMemberUniproQuery The current query, for fluid interface
     */
    public function joinUnivProject($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UnivProject');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UnivProject');
        }

        return $this;
    }

    /**
     * Use the UnivProject relation UnivProject object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MMIBundle\Model\UnivProjectQuery A secondary query class using the current class as primary query
     */
    public function useUnivProjectQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUnivProject($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UnivProject', '\MMIBundle\Model\UnivProjectQuery');
    }

    /**
     * Filter the query by a related Profession object
     *
     * @param   Profession|PropelObjectCollection $profession The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 UserMemberUniproQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByProfession($profession, $comparison = null)
    {
        if ($profession instanceof Profession) {
            return $this
                ->addUsingAlias(UserMemberUniproPeer::PROFESSION_ID, $profession->getId(), $comparison);
        } elseif ($profession instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UserMemberUniproPeer::PROFESSION_ID, $profession->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByProfession() only accepts arguments of type Profession or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Profession relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return UserMemberUniproQuery The current query, for fluid interface
     */
    public function joinProfession($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Profession');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Profession');
        }

        return $this;
    }

    /**
     * Use the Profession relation Profession object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MMIBundle\Model\ProfessionQuery A secondary query class using the current class as primary query
     */
    public function useProfessionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProfession($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Profession', '\MMIBundle\Model\ProfessionQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   UserMemberUnipro $userMemberUnipro Object to remove from the list of results
     *
     * @return UserMemberUniproQuery The current query, for fluid interface
     */
    public function prune($userMemberUnipro = null)
    {
        if ($userMemberUnipro) {
            $this->addUsingAlias(UserMemberUniproPeer::ID, $userMemberUnipro->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
