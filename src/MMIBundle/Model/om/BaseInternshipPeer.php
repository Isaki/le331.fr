<?php

namespace MMIBundle\Model\om;

use \BasePeer;
use \Criteria;
use \PDO;
use \PDOStatement;
use \Propel;
use \PropelException;
use \PropelPDO;
use MMIBundle\Model\CompanyPeer;
use MMIBundle\Model\Internship;
use MMIBundle\Model\InternshipPeer;
use MMIBundle\Model\OfferinternshipPeer;
use MMIBundle\Model\UserPeer;
use MMIBundle\Model\map\InternshipTableMap;

abstract class BaseInternshipPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'default';

    /** the table name for this class */
    const TABLE_NAME = 'internship';

    /** the related Propel class for this table */
    const OM_CLASS = 'MMIBundle\\Model\\Internship';

    /** the related TableMap class for this table */
    const TM_CLASS = 'MMIBundle\\Model\\map\\InternshipTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 22;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 22;

    /** the column name for the id field */
    const ID = 'internship.id';

    /** the column name for the offer_id field */
    const OFFER_ID = 'internship.offer_id';

    /** the column name for the user_id field */
    const USER_ID = 'internship.user_id';

    /** the column name for the company_id field */
    const COMPANY_ID = 'internship.company_id';

    /** the column name for the tutor_id field */
    const TUTOR_ID = 'internship.tutor_id';

    /** the column name for the work_function field */
    const WORK_FUNCTION = 'internship.work_function';

    /** the column name for the description field */
    const DESCRIPTION = 'internship.description';

    /** the column name for the information field */
    const INFORMATION = 'internship.information';

    /** the column name for the url_website_diffusion field */
    const URL_WEBSITE_DIFFUSION = 'internship.url_website_diffusion';

    /** the column name for the signatory_lastname field */
    const SIGNATORY_LASTNAME = 'internship.signatory_lastname';

    /** the column name for the signatory_firstname field */
    const SIGNATORY_FIRSTNAME = 'internship.signatory_firstname';

    /** the column name for the signatory_function field */
    const SIGNATORY_FUNCTION = 'internship.signatory_function';

    /** the column name for the master_lastname field */
    const MASTER_LASTNAME = 'internship.master_lastname';

    /** the column name for the master_firstname field */
    const MASTER_FIRSTNAME = 'internship.master_firstname';

    /** the column name for the master_email field */
    const MASTER_EMAIL = 'internship.master_email';

    /** the column name for the master_telephone field */
    const MASTER_TELEPHONE = 'internship.master_telephone';

    /** the column name for the valide field */
    const VALIDE = 'internship.valide';

    /** the column name for the etabli field */
    const ETABLI = 'internship.etabli';

    /** the column name for the date_visite field */
    const DATE_VISITE = 'internship.date_visite';

    /** the column name for the date_soutenance field */
    const DATE_SOUTENANCE = 'internship.date_soutenance';

    /** the column name for the internship_begining field */
    const INTERNSHIP_BEGINING = 'internship.internship_begining';

    /** the column name for the internship_ending field */
    const INTERNSHIP_ENDING = 'internship.internship_ending';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of Internship objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array Internship[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. InternshipPeer::$fieldNames[InternshipPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('Id', 'OfferId', 'UserId', 'CompanyId', 'TutorId', 'WorkFunction', 'Description', 'Information', 'UrlWebsiteDiffusion', 'SignatoryLastname', 'SignatoryFirstname', 'SignatoryFunction', 'MasterLastname', 'MasterFirstname', 'MasterEmail', 'MasterTelephone', 'Valide', 'Etabli', 'DateVisite', 'DateSoutenance', 'InternshipBegining', 'InternshipEnding', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id', 'offerId', 'userId', 'companyId', 'tutorId', 'workFunction', 'description', 'information', 'urlWebsiteDiffusion', 'signatoryLastname', 'signatoryFirstname', 'signatoryFunction', 'masterLastname', 'masterFirstname', 'masterEmail', 'masterTelephone', 'valide', 'etabli', 'dateVisite', 'dateSoutenance', 'internshipBegining', 'internshipEnding', ),
        BasePeer::TYPE_COLNAME => array (InternshipPeer::ID, InternshipPeer::OFFER_ID, InternshipPeer::USER_ID, InternshipPeer::COMPANY_ID, InternshipPeer::TUTOR_ID, InternshipPeer::WORK_FUNCTION, InternshipPeer::DESCRIPTION, InternshipPeer::INFORMATION, InternshipPeer::URL_WEBSITE_DIFFUSION, InternshipPeer::SIGNATORY_LASTNAME, InternshipPeer::SIGNATORY_FIRSTNAME, InternshipPeer::SIGNATORY_FUNCTION, InternshipPeer::MASTER_LASTNAME, InternshipPeer::MASTER_FIRSTNAME, InternshipPeer::MASTER_EMAIL, InternshipPeer::MASTER_TELEPHONE, InternshipPeer::VALIDE, InternshipPeer::ETABLI, InternshipPeer::DATE_VISITE, InternshipPeer::DATE_SOUTENANCE, InternshipPeer::INTERNSHIP_BEGINING, InternshipPeer::INTERNSHIP_ENDING, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID', 'OFFER_ID', 'USER_ID', 'COMPANY_ID', 'TUTOR_ID', 'WORK_FUNCTION', 'DESCRIPTION', 'INFORMATION', 'URL_WEBSITE_DIFFUSION', 'SIGNATORY_LASTNAME', 'SIGNATORY_FIRSTNAME', 'SIGNATORY_FUNCTION', 'MASTER_LASTNAME', 'MASTER_FIRSTNAME', 'MASTER_EMAIL', 'MASTER_TELEPHONE', 'VALIDE', 'ETABLI', 'DATE_VISITE', 'DATE_SOUTENANCE', 'INTERNSHIP_BEGINING', 'INTERNSHIP_ENDING', ),
        BasePeer::TYPE_FIELDNAME => array ('id', 'offer_id', 'user_id', 'company_id', 'tutor_id', 'work_function', 'description', 'information', 'url_website_diffusion', 'signatory_lastname', 'signatory_firstname', 'signatory_function', 'master_lastname', 'master_firstname', 'master_email', 'master_telephone', 'valide', 'etabli', 'date_visite', 'date_soutenance', 'internship_begining', 'internship_ending', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. InternshipPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'OfferId' => 1, 'UserId' => 2, 'CompanyId' => 3, 'TutorId' => 4, 'WorkFunction' => 5, 'Description' => 6, 'Information' => 7, 'UrlWebsiteDiffusion' => 8, 'SignatoryLastname' => 9, 'SignatoryFirstname' => 10, 'SignatoryFunction' => 11, 'MasterLastname' => 12, 'MasterFirstname' => 13, 'MasterEmail' => 14, 'MasterTelephone' => 15, 'Valide' => 16, 'Etabli' => 17, 'DateVisite' => 18, 'DateSoutenance' => 19, 'InternshipBegining' => 20, 'InternshipEnding' => 21, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id' => 0, 'offerId' => 1, 'userId' => 2, 'companyId' => 3, 'tutorId' => 4, 'workFunction' => 5, 'description' => 6, 'information' => 7, 'urlWebsiteDiffusion' => 8, 'signatoryLastname' => 9, 'signatoryFirstname' => 10, 'signatoryFunction' => 11, 'masterLastname' => 12, 'masterFirstname' => 13, 'masterEmail' => 14, 'masterTelephone' => 15, 'valide' => 16, 'etabli' => 17, 'dateVisite' => 18, 'dateSoutenance' => 19, 'internshipBegining' => 20, 'internshipEnding' => 21, ),
        BasePeer::TYPE_COLNAME => array (InternshipPeer::ID => 0, InternshipPeer::OFFER_ID => 1, InternshipPeer::USER_ID => 2, InternshipPeer::COMPANY_ID => 3, InternshipPeer::TUTOR_ID => 4, InternshipPeer::WORK_FUNCTION => 5, InternshipPeer::DESCRIPTION => 6, InternshipPeer::INFORMATION => 7, InternshipPeer::URL_WEBSITE_DIFFUSION => 8, InternshipPeer::SIGNATORY_LASTNAME => 9, InternshipPeer::SIGNATORY_FIRSTNAME => 10, InternshipPeer::SIGNATORY_FUNCTION => 11, InternshipPeer::MASTER_LASTNAME => 12, InternshipPeer::MASTER_FIRSTNAME => 13, InternshipPeer::MASTER_EMAIL => 14, InternshipPeer::MASTER_TELEPHONE => 15, InternshipPeer::VALIDE => 16, InternshipPeer::ETABLI => 17, InternshipPeer::DATE_VISITE => 18, InternshipPeer::DATE_SOUTENANCE => 19, InternshipPeer::INTERNSHIP_BEGINING => 20, InternshipPeer::INTERNSHIP_ENDING => 21, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID' => 0, 'OFFER_ID' => 1, 'USER_ID' => 2, 'COMPANY_ID' => 3, 'TUTOR_ID' => 4, 'WORK_FUNCTION' => 5, 'DESCRIPTION' => 6, 'INFORMATION' => 7, 'URL_WEBSITE_DIFFUSION' => 8, 'SIGNATORY_LASTNAME' => 9, 'SIGNATORY_FIRSTNAME' => 10, 'SIGNATORY_FUNCTION' => 11, 'MASTER_LASTNAME' => 12, 'MASTER_FIRSTNAME' => 13, 'MASTER_EMAIL' => 14, 'MASTER_TELEPHONE' => 15, 'VALIDE' => 16, 'ETABLI' => 17, 'DATE_VISITE' => 18, 'DATE_SOUTENANCE' => 19, 'INTERNSHIP_BEGINING' => 20, 'INTERNSHIP_ENDING' => 21, ),
        BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'offer_id' => 1, 'user_id' => 2, 'company_id' => 3, 'tutor_id' => 4, 'work_function' => 5, 'description' => 6, 'information' => 7, 'url_website_diffusion' => 8, 'signatory_lastname' => 9, 'signatory_firstname' => 10, 'signatory_function' => 11, 'master_lastname' => 12, 'master_firstname' => 13, 'master_email' => 14, 'master_telephone' => 15, 'valide' => 16, 'etabli' => 17, 'date_visite' => 18, 'date_soutenance' => 19, 'internship_begining' => 20, 'internship_ending' => 21, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = InternshipPeer::getFieldNames($toType);
        $key = isset(InternshipPeer::$fieldKeys[$fromType][$name]) ? InternshipPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(InternshipPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, InternshipPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return InternshipPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. InternshipPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(InternshipPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(InternshipPeer::ID);
            $criteria->addSelectColumn(InternshipPeer::OFFER_ID);
            $criteria->addSelectColumn(InternshipPeer::USER_ID);
            $criteria->addSelectColumn(InternshipPeer::COMPANY_ID);
            $criteria->addSelectColumn(InternshipPeer::TUTOR_ID);
            $criteria->addSelectColumn(InternshipPeer::WORK_FUNCTION);
            $criteria->addSelectColumn(InternshipPeer::DESCRIPTION);
            $criteria->addSelectColumn(InternshipPeer::INFORMATION);
            $criteria->addSelectColumn(InternshipPeer::URL_WEBSITE_DIFFUSION);
            $criteria->addSelectColumn(InternshipPeer::SIGNATORY_LASTNAME);
            $criteria->addSelectColumn(InternshipPeer::SIGNATORY_FIRSTNAME);
            $criteria->addSelectColumn(InternshipPeer::SIGNATORY_FUNCTION);
            $criteria->addSelectColumn(InternshipPeer::MASTER_LASTNAME);
            $criteria->addSelectColumn(InternshipPeer::MASTER_FIRSTNAME);
            $criteria->addSelectColumn(InternshipPeer::MASTER_EMAIL);
            $criteria->addSelectColumn(InternshipPeer::MASTER_TELEPHONE);
            $criteria->addSelectColumn(InternshipPeer::VALIDE);
            $criteria->addSelectColumn(InternshipPeer::ETABLI);
            $criteria->addSelectColumn(InternshipPeer::DATE_VISITE);
            $criteria->addSelectColumn(InternshipPeer::DATE_SOUTENANCE);
            $criteria->addSelectColumn(InternshipPeer::INTERNSHIP_BEGINING);
            $criteria->addSelectColumn(InternshipPeer::INTERNSHIP_ENDING);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.offer_id');
            $criteria->addSelectColumn($alias . '.user_id');
            $criteria->addSelectColumn($alias . '.company_id');
            $criteria->addSelectColumn($alias . '.tutor_id');
            $criteria->addSelectColumn($alias . '.work_function');
            $criteria->addSelectColumn($alias . '.description');
            $criteria->addSelectColumn($alias . '.information');
            $criteria->addSelectColumn($alias . '.url_website_diffusion');
            $criteria->addSelectColumn($alias . '.signatory_lastname');
            $criteria->addSelectColumn($alias . '.signatory_firstname');
            $criteria->addSelectColumn($alias . '.signatory_function');
            $criteria->addSelectColumn($alias . '.master_lastname');
            $criteria->addSelectColumn($alias . '.master_firstname');
            $criteria->addSelectColumn($alias . '.master_email');
            $criteria->addSelectColumn($alias . '.master_telephone');
            $criteria->addSelectColumn($alias . '.valide');
            $criteria->addSelectColumn($alias . '.etabli');
            $criteria->addSelectColumn($alias . '.date_visite');
            $criteria->addSelectColumn($alias . '.date_soutenance');
            $criteria->addSelectColumn($alias . '.internship_begining');
            $criteria->addSelectColumn($alias . '.internship_ending');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(InternshipPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            InternshipPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(InternshipPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(InternshipPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return Internship
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = InternshipPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return InternshipPeer::populateObjects(InternshipPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(InternshipPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            InternshipPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(InternshipPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param Internship $obj A Internship object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getId();
            } // if key === null
            InternshipPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A Internship object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof Internship) {
                $key = (string) $value->getId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or Internship object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(InternshipPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return Internship Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(InternshipPeer::$instances[$key])) {
                return InternshipPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (InternshipPeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        InternshipPeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to internship
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = InternshipPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = InternshipPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = InternshipPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                InternshipPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (Internship object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = InternshipPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = InternshipPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + InternshipPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = InternshipPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            InternshipPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related OfferinternshipRelatedByOfferId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinOfferinternshipRelatedByOfferId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(InternshipPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            InternshipPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(InternshipPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(InternshipPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(InternshipPeer::OFFER_ID, OfferinternshipPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related User table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinUser(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(InternshipPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            InternshipPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(InternshipPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(InternshipPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(InternshipPeer::USER_ID, UserPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related Company table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCompany(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(InternshipPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            InternshipPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(InternshipPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(InternshipPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(InternshipPeer::COMPANY_ID, CompanyPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of Internship objects pre-filled with their Offerinternship objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Internship objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinOfferinternshipRelatedByOfferId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(InternshipPeer::DATABASE_NAME);
        }

        InternshipPeer::addSelectColumns($criteria);
        $startcol = InternshipPeer::NUM_HYDRATE_COLUMNS;
        OfferinternshipPeer::addSelectColumns($criteria);

        $criteria->addJoin(InternshipPeer::OFFER_ID, OfferinternshipPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = InternshipPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = InternshipPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = InternshipPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                InternshipPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = OfferinternshipPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = OfferinternshipPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = OfferinternshipPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    OfferinternshipPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (Internship) to $obj2 (Offerinternship)
                $obj2->addInternshipRelatedByOfferId($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of Internship objects pre-filled with their User objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Internship objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinUser(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(InternshipPeer::DATABASE_NAME);
        }

        InternshipPeer::addSelectColumns($criteria);
        $startcol = InternshipPeer::NUM_HYDRATE_COLUMNS;
        UserPeer::addSelectColumns($criteria);

        $criteria->addJoin(InternshipPeer::USER_ID, UserPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = InternshipPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = InternshipPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = InternshipPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                InternshipPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = UserPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = UserPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = UserPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    UserPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (Internship) to $obj2 (User)
                $obj2->addInternship($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of Internship objects pre-filled with their Company objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Internship objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCompany(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(InternshipPeer::DATABASE_NAME);
        }

        InternshipPeer::addSelectColumns($criteria);
        $startcol = InternshipPeer::NUM_HYDRATE_COLUMNS;
        CompanyPeer::addSelectColumns($criteria);

        $criteria->addJoin(InternshipPeer::COMPANY_ID, CompanyPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = InternshipPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = InternshipPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = InternshipPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                InternshipPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CompanyPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CompanyPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CompanyPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CompanyPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (Internship) to $obj2 (Company)
                $obj2->addInternship($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(InternshipPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            InternshipPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(InternshipPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(InternshipPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(InternshipPeer::OFFER_ID, OfferinternshipPeer::ID, $join_behavior);

        $criteria->addJoin(InternshipPeer::USER_ID, UserPeer::ID, $join_behavior);

        $criteria->addJoin(InternshipPeer::COMPANY_ID, CompanyPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of Internship objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Internship objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(InternshipPeer::DATABASE_NAME);
        }

        InternshipPeer::addSelectColumns($criteria);
        $startcol2 = InternshipPeer::NUM_HYDRATE_COLUMNS;

        OfferinternshipPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + OfferinternshipPeer::NUM_HYDRATE_COLUMNS;

        UserPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + UserPeer::NUM_HYDRATE_COLUMNS;

        CompanyPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CompanyPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(InternshipPeer::OFFER_ID, OfferinternshipPeer::ID, $join_behavior);

        $criteria->addJoin(InternshipPeer::USER_ID, UserPeer::ID, $join_behavior);

        $criteria->addJoin(InternshipPeer::COMPANY_ID, CompanyPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = InternshipPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = InternshipPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = InternshipPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                InternshipPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined Offerinternship rows

            $key2 = OfferinternshipPeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = OfferinternshipPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = OfferinternshipPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    OfferinternshipPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (Internship) to the collection in $obj2 (Offerinternship)
                $obj2->addInternshipRelatedByOfferId($obj1);
            } // if joined row not null

            // Add objects for joined User rows

            $key3 = UserPeer::getPrimaryKeyHashFromRow($row, $startcol3);
            if ($key3 !== null) {
                $obj3 = UserPeer::getInstanceFromPool($key3);
                if (!$obj3) {

                    $cls = UserPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    UserPeer::addInstanceToPool($obj3, $key3);
                } // if obj3 loaded

                // Add the $obj1 (Internship) to the collection in $obj3 (User)
                $obj3->addInternship($obj1);
            } // if joined row not null

            // Add objects for joined Company rows

            $key4 = CompanyPeer::getPrimaryKeyHashFromRow($row, $startcol4);
            if ($key4 !== null) {
                $obj4 = CompanyPeer::getInstanceFromPool($key4);
                if (!$obj4) {

                    $cls = CompanyPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CompanyPeer::addInstanceToPool($obj4, $key4);
                } // if obj4 loaded

                // Add the $obj1 (Internship) to the collection in $obj4 (Company)
                $obj4->addInternship($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining the related OfferinternshipRelatedByOfferId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptOfferinternshipRelatedByOfferId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(InternshipPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            InternshipPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(InternshipPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(InternshipPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(InternshipPeer::USER_ID, UserPeer::ID, $join_behavior);

        $criteria->addJoin(InternshipPeer::COMPANY_ID, CompanyPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related User table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptUser(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(InternshipPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            InternshipPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(InternshipPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(InternshipPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(InternshipPeer::OFFER_ID, OfferinternshipPeer::ID, $join_behavior);

        $criteria->addJoin(InternshipPeer::COMPANY_ID, CompanyPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related Company table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCompany(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(InternshipPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            InternshipPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(InternshipPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(InternshipPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(InternshipPeer::OFFER_ID, OfferinternshipPeer::ID, $join_behavior);

        $criteria->addJoin(InternshipPeer::USER_ID, UserPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of Internship objects pre-filled with all related objects except OfferinternshipRelatedByOfferId.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Internship objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptOfferinternshipRelatedByOfferId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(InternshipPeer::DATABASE_NAME);
        }

        InternshipPeer::addSelectColumns($criteria);
        $startcol2 = InternshipPeer::NUM_HYDRATE_COLUMNS;

        UserPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + UserPeer::NUM_HYDRATE_COLUMNS;

        CompanyPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CompanyPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(InternshipPeer::USER_ID, UserPeer::ID, $join_behavior);

        $criteria->addJoin(InternshipPeer::COMPANY_ID, CompanyPeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = InternshipPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = InternshipPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = InternshipPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                InternshipPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined User rows

                $key2 = UserPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = UserPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = UserPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    UserPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (Internship) to the collection in $obj2 (User)
                $obj2->addInternship($obj1);

            } // if joined row is not null

                // Add objects for joined Company rows

                $key3 = CompanyPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CompanyPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CompanyPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CompanyPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (Internship) to the collection in $obj3 (Company)
                $obj3->addInternship($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of Internship objects pre-filled with all related objects except User.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Internship objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptUser(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(InternshipPeer::DATABASE_NAME);
        }

        InternshipPeer::addSelectColumns($criteria);
        $startcol2 = InternshipPeer::NUM_HYDRATE_COLUMNS;

        OfferinternshipPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + OfferinternshipPeer::NUM_HYDRATE_COLUMNS;

        CompanyPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CompanyPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(InternshipPeer::OFFER_ID, OfferinternshipPeer::ID, $join_behavior);

        $criteria->addJoin(InternshipPeer::COMPANY_ID, CompanyPeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = InternshipPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = InternshipPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = InternshipPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                InternshipPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Offerinternship rows

                $key2 = OfferinternshipPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = OfferinternshipPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = OfferinternshipPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    OfferinternshipPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (Internship) to the collection in $obj2 (Offerinternship)
                $obj2->addInternshipRelatedByOfferId($obj1);

            } // if joined row is not null

                // Add objects for joined Company rows

                $key3 = CompanyPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CompanyPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CompanyPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CompanyPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (Internship) to the collection in $obj3 (Company)
                $obj3->addInternship($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of Internship objects pre-filled with all related objects except Company.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of Internship objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCompany(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(InternshipPeer::DATABASE_NAME);
        }

        InternshipPeer::addSelectColumns($criteria);
        $startcol2 = InternshipPeer::NUM_HYDRATE_COLUMNS;

        OfferinternshipPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + OfferinternshipPeer::NUM_HYDRATE_COLUMNS;

        UserPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + UserPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(InternshipPeer::OFFER_ID, OfferinternshipPeer::ID, $join_behavior);

        $criteria->addJoin(InternshipPeer::USER_ID, UserPeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = InternshipPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = InternshipPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = InternshipPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                InternshipPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Offerinternship rows

                $key2 = OfferinternshipPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = OfferinternshipPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = OfferinternshipPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    OfferinternshipPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (Internship) to the collection in $obj2 (Offerinternship)
                $obj2->addInternshipRelatedByOfferId($obj1);

            } // if joined row is not null

                // Add objects for joined User rows

                $key3 = UserPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = UserPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = UserPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    UserPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (Internship) to the collection in $obj3 (User)
                $obj3->addInternship($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(InternshipPeer::DATABASE_NAME)->getTable(InternshipPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseInternshipPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseInternshipPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new \MMIBundle\Model\map\InternshipTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return InternshipPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a Internship or Criteria object.
     *
     * @param      mixed $values Criteria or Internship object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(InternshipPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from Internship object
        }

        if ($criteria->containsKey(InternshipPeer::ID) && $criteria->keyContainsValue(InternshipPeer::ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.InternshipPeer::ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(InternshipPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a Internship or Criteria object.
     *
     * @param      mixed $values Criteria or Internship object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(InternshipPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(InternshipPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(InternshipPeer::ID);
            $value = $criteria->remove(InternshipPeer::ID);
            if ($value) {
                $selectCriteria->add(InternshipPeer::ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(InternshipPeer::TABLE_NAME);
            }

        } else { // $values is Internship object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(InternshipPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the internship table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(InternshipPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(InternshipPeer::TABLE_NAME, $con, InternshipPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            InternshipPeer::clearInstancePool();
            InternshipPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a Internship or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or Internship object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(InternshipPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            InternshipPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof Internship) { // it's a model object
            // invalidate the cache for this single object
            InternshipPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(InternshipPeer::DATABASE_NAME);
            $criteria->add(InternshipPeer::ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                InternshipPeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(InternshipPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            InternshipPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given Internship object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param Internship $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(InternshipPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(InternshipPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(InternshipPeer::DATABASE_NAME, InternshipPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return Internship
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = InternshipPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(InternshipPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(InternshipPeer::DATABASE_NAME);
        $criteria->add(InternshipPeer::ID, $pk);

        $v = InternshipPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return Internship[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(InternshipPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(InternshipPeer::DATABASE_NAME);
            $criteria->add(InternshipPeer::ID, $pks, Criteria::IN);
            $objs = InternshipPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseInternshipPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseInternshipPeer::buildTableMap();

