<?php

namespace MMIBundle\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use MMIBundle\Model\City;
use MMIBundle\Model\Internship;
use MMIBundle\Model\Profession;
use MMIBundle\Model\Promo;
use MMIBundle\Model\Role;
use MMIBundle\Model\UnivProject;
use MMIBundle\Model\User;
use MMIBundle\Model\UserJob;
use MMIBundle\Model\UserMemberUnipro;
use MMIBundle\Model\UserObtainedDegree;
use MMIBundle\Model\UserPeer;
use MMIBundle\Model\UserQuery;
use MMIBundle\Model\UserRole;
use MMIBundle\Model\Validation;

/**
 * @method UserQuery orderById($order = Criteria::ASC) Order by the id column
 * @method UserQuery orderByCityId($order = Criteria::ASC) Order by the city_id column
 * @method UserQuery orderByProfileId($order = Criteria::ASC) Order by the profile_id column
 * @method UserQuery orderByPromoId($order = Criteria::ASC) Order by the promo_id column
 * @method UserQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method UserQuery orderByPassword($order = Criteria::ASC) Order by the password column
 * @method UserQuery orderByLastname($order = Criteria::ASC) Order by the lastname column
 * @method UserQuery orderByFirstname($order = Criteria::ASC) Order by the firstname column
 * @method UserQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method UserQuery orderByNewsletter($order = Criteria::ASC) Order by the newsletter column
 * @method UserQuery orderByBirthday($order = Criteria::ASC) Order by the birthday column
 * @method UserQuery orderByUrlPortfolio($order = Criteria::ASC) Order by the url_portfolio column
 * @method UserQuery orderByUrlLinkedin($order = Criteria::ASC) Order by the url_linkedin column
 * @method UserQuery orderByUrlFacebook($order = Criteria::ASC) Order by the url_facebook column
 * @method UserQuery orderByUrlGoogle($order = Criteria::ASC) Order by the url_google column
 * @method UserQuery orderByUrlTwitter($order = Criteria::ASC) Order by the url_twitter column
 * @method UserQuery orderByUrlTumblr($order = Criteria::ASC) Order by the url_tumblr column
 * @method UserQuery orderByUrlInstagram($order = Criteria::ASC) Order by the url_instagram column
 * @method UserQuery orderByUrlBlog($order = Criteria::ASC) Order by the url_blog column
 * @method UserQuery orderByCvFile($order = Criteria::ASC) Order by the cv_file column
 * @method UserQuery orderByImageProfilFile($order = Criteria::ASC) Order by the image_profil_file column
 * @method UserQuery orderByImageCouvFile($order = Criteria::ASC) Order by the image_couv_file column
 * @method UserQuery orderByTelephone($order = Criteria::ASC) Order by the telephone column
 * @method UserQuery orderByFirstConnection($order = Criteria::ASC) Order by the first_connection column
 * @method UserQuery orderByValidated($order = Criteria::ASC) Order by the validated column
 *
 * @method UserQuery groupById() Group by the id column
 * @method UserQuery groupByCityId() Group by the city_id column
 * @method UserQuery groupByProfileId() Group by the profile_id column
 * @method UserQuery groupByPromoId() Group by the promo_id column
 * @method UserQuery groupByEmail() Group by the email column
 * @method UserQuery groupByPassword() Group by the password column
 * @method UserQuery groupByLastname() Group by the lastname column
 * @method UserQuery groupByFirstname() Group by the firstname column
 * @method UserQuery groupByDescription() Group by the description column
 * @method UserQuery groupByNewsletter() Group by the newsletter column
 * @method UserQuery groupByBirthday() Group by the birthday column
 * @method UserQuery groupByUrlPortfolio() Group by the url_portfolio column
 * @method UserQuery groupByUrlLinkedin() Group by the url_linkedin column
 * @method UserQuery groupByUrlFacebook() Group by the url_facebook column
 * @method UserQuery groupByUrlGoogle() Group by the url_google column
 * @method UserQuery groupByUrlTwitter() Group by the url_twitter column
 * @method UserQuery groupByUrlTumblr() Group by the url_tumblr column
 * @method UserQuery groupByUrlInstagram() Group by the url_instagram column
 * @method UserQuery groupByUrlBlog() Group by the url_blog column
 * @method UserQuery groupByCvFile() Group by the cv_file column
 * @method UserQuery groupByImageProfilFile() Group by the image_profil_file column
 * @method UserQuery groupByImageCouvFile() Group by the image_couv_file column
 * @method UserQuery groupByTelephone() Group by the telephone column
 * @method UserQuery groupByFirstConnection() Group by the first_connection column
 * @method UserQuery groupByValidated() Group by the validated column
 *
 * @method UserQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method UserQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method UserQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method UserQuery leftJoinCity($relationAlias = null) Adds a LEFT JOIN clause to the query using the City relation
 * @method UserQuery rightJoinCity($relationAlias = null) Adds a RIGHT JOIN clause to the query using the City relation
 * @method UserQuery innerJoinCity($relationAlias = null) Adds a INNER JOIN clause to the query using the City relation
 *
 * @method UserQuery leftJoinProfession($relationAlias = null) Adds a LEFT JOIN clause to the query using the Profession relation
 * @method UserQuery rightJoinProfession($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Profession relation
 * @method UserQuery innerJoinProfession($relationAlias = null) Adds a INNER JOIN clause to the query using the Profession relation
 *
 * @method UserQuery leftJoinPromo($relationAlias = null) Adds a LEFT JOIN clause to the query using the Promo relation
 * @method UserQuery rightJoinPromo($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Promo relation
 * @method UserQuery innerJoinPromo($relationAlias = null) Adds a INNER JOIN clause to the query using the Promo relation
 *
 * @method UserQuery leftJoinUserRole($relationAlias = null) Adds a LEFT JOIN clause to the query using the UserRole relation
 * @method UserQuery rightJoinUserRole($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UserRole relation
 * @method UserQuery innerJoinUserRole($relationAlias = null) Adds a INNER JOIN clause to the query using the UserRole relation
 *
 * @method UserQuery leftJoinInternship($relationAlias = null) Adds a LEFT JOIN clause to the query using the Internship relation
 * @method UserQuery rightJoinInternship($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Internship relation
 * @method UserQuery innerJoinInternship($relationAlias = null) Adds a INNER JOIN clause to the query using the Internship relation
 *
 * @method UserQuery leftJoinUserJob($relationAlias = null) Adds a LEFT JOIN clause to the query using the UserJob relation
 * @method UserQuery rightJoinUserJob($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UserJob relation
 * @method UserQuery innerJoinUserJob($relationAlias = null) Adds a INNER JOIN clause to the query using the UserJob relation
 *
 * @method UserQuery leftJoinUserObtainedDegree($relationAlias = null) Adds a LEFT JOIN clause to the query using the UserObtainedDegree relation
 * @method UserQuery rightJoinUserObtainedDegree($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UserObtainedDegree relation
 * @method UserQuery innerJoinUserObtainedDegree($relationAlias = null) Adds a INNER JOIN clause to the query using the UserObtainedDegree relation
 *
 * @method UserQuery leftJoinUnivProject($relationAlias = null) Adds a LEFT JOIN clause to the query using the UnivProject relation
 * @method UserQuery rightJoinUnivProject($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UnivProject relation
 * @method UserQuery innerJoinUnivProject($relationAlias = null) Adds a INNER JOIN clause to the query using the UnivProject relation
 *
 * @method UserQuery leftJoinUserMemberUnipro($relationAlias = null) Adds a LEFT JOIN clause to the query using the UserMemberUnipro relation
 * @method UserQuery rightJoinUserMemberUnipro($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UserMemberUnipro relation
 * @method UserQuery innerJoinUserMemberUnipro($relationAlias = null) Adds a INNER JOIN clause to the query using the UserMemberUnipro relation
 *
 * @method UserQuery leftJoinValidation($relationAlias = null) Adds a LEFT JOIN clause to the query using the Validation relation
 * @method UserQuery rightJoinValidation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Validation relation
 * @method UserQuery innerJoinValidation($relationAlias = null) Adds a INNER JOIN clause to the query using the Validation relation
 *
 * @method User findOne(PropelPDO $con = null) Return the first User matching the query
 * @method User findOneOrCreate(PropelPDO $con = null) Return the first User matching the query, or a new User object populated from the query conditions when no match is found
 *
 * @method User findOneByCityId(int $city_id) Return the first User filtered by the city_id column
 * @method User findOneByProfileId(int $profile_id) Return the first User filtered by the profile_id column
 * @method User findOneByPromoId(int $promo_id) Return the first User filtered by the promo_id column
 * @method User findOneByEmail(string $email) Return the first User filtered by the email column
 * @method User findOneByPassword(string $password) Return the first User filtered by the password column
 * @method User findOneByLastname(string $lastname) Return the first User filtered by the lastname column
 * @method User findOneByFirstname(string $firstname) Return the first User filtered by the firstname column
 * @method User findOneByDescription(string $description) Return the first User filtered by the description column
 * @method User findOneByNewsletter(boolean $newsletter) Return the first User filtered by the newsletter column
 * @method User findOneByBirthday(string $birthday) Return the first User filtered by the birthday column
 * @method User findOneByUrlPortfolio(string $url_portfolio) Return the first User filtered by the url_portfolio column
 * @method User findOneByUrlLinkedin(string $url_linkedin) Return the first User filtered by the url_linkedin column
 * @method User findOneByUrlFacebook(string $url_facebook) Return the first User filtered by the url_facebook column
 * @method User findOneByUrlGoogle(string $url_google) Return the first User filtered by the url_google column
 * @method User findOneByUrlTwitter(string $url_twitter) Return the first User filtered by the url_twitter column
 * @method User findOneByUrlTumblr(string $url_tumblr) Return the first User filtered by the url_tumblr column
 * @method User findOneByUrlInstagram(string $url_instagram) Return the first User filtered by the url_instagram column
 * @method User findOneByUrlBlog(string $url_blog) Return the first User filtered by the url_blog column
 * @method User findOneByCvFile(string $cv_file) Return the first User filtered by the cv_file column
 * @method User findOneByImageProfilFile(string $image_profil_file) Return the first User filtered by the image_profil_file column
 * @method User findOneByImageCouvFile(string $image_couv_file) Return the first User filtered by the image_couv_file column
 * @method User findOneByTelephone(string $telephone) Return the first User filtered by the telephone column
 * @method User findOneByFirstConnection(boolean $first_connection) Return the first User filtered by the first_connection column
 * @method User findOneByValidated(boolean $validated) Return the first User filtered by the validated column
 *
 * @method array findById(int $id) Return User objects filtered by the id column
 * @method array findByCityId(int $city_id) Return User objects filtered by the city_id column
 * @method array findByProfileId(int $profile_id) Return User objects filtered by the profile_id column
 * @method array findByPromoId(int $promo_id) Return User objects filtered by the promo_id column
 * @method array findByEmail(string $email) Return User objects filtered by the email column
 * @method array findByPassword(string $password) Return User objects filtered by the password column
 * @method array findByLastname(string $lastname) Return User objects filtered by the lastname column
 * @method array findByFirstname(string $firstname) Return User objects filtered by the firstname column
 * @method array findByDescription(string $description) Return User objects filtered by the description column
 * @method array findByNewsletter(boolean $newsletter) Return User objects filtered by the newsletter column
 * @method array findByBirthday(string $birthday) Return User objects filtered by the birthday column
 * @method array findByUrlPortfolio(string $url_portfolio) Return User objects filtered by the url_portfolio column
 * @method array findByUrlLinkedin(string $url_linkedin) Return User objects filtered by the url_linkedin column
 * @method array findByUrlFacebook(string $url_facebook) Return User objects filtered by the url_facebook column
 * @method array findByUrlGoogle(string $url_google) Return User objects filtered by the url_google column
 * @method array findByUrlTwitter(string $url_twitter) Return User objects filtered by the url_twitter column
 * @method array findByUrlTumblr(string $url_tumblr) Return User objects filtered by the url_tumblr column
 * @method array findByUrlInstagram(string $url_instagram) Return User objects filtered by the url_instagram column
 * @method array findByUrlBlog(string $url_blog) Return User objects filtered by the url_blog column
 * @method array findByCvFile(string $cv_file) Return User objects filtered by the cv_file column
 * @method array findByImageProfilFile(string $image_profil_file) Return User objects filtered by the image_profil_file column
 * @method array findByImageCouvFile(string $image_couv_file) Return User objects filtered by the image_couv_file column
 * @method array findByTelephone(string $telephone) Return User objects filtered by the telephone column
 * @method array findByFirstConnection(boolean $first_connection) Return User objects filtered by the first_connection column
 * @method array findByValidated(boolean $validated) Return User objects filtered by the validated column
 */
abstract class BaseUserQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseUserQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'default';
        }
        if (null === $modelName) {
            $modelName = 'MMIBundle\\Model\\User';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new UserQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   UserQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return UserQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof UserQuery) {
            return $criteria;
        }
        $query = new UserQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   User|User[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = UserPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(UserPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 User A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 User A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `city_id`, `profile_id`, `promo_id`, `email`, `password`, `lastname`, `firstname`, `description`, `newsletter`, `birthday`, `url_portfolio`, `url_linkedin`, `url_facebook`, `url_google`, `url_twitter`, `url_tumblr`, `url_instagram`, `url_blog`, `cv_file`, `image_profil_file`, `image_couv_file`, `telephone`, `first_connection`, `validated` FROM `user` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new User();
            $obj->hydrate($row);
            UserPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return User|User[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|User[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(UserPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(UserPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(UserPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(UserPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the city_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCityId(1234); // WHERE city_id = 1234
     * $query->filterByCityId(array(12, 34)); // WHERE city_id IN (12, 34)
     * $query->filterByCityId(array('min' => 12)); // WHERE city_id >= 12
     * $query->filterByCityId(array('max' => 12)); // WHERE city_id <= 12
     * </code>
     *
     * @see       filterByCity()
     *
     * @param     mixed $cityId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function filterByCityId($cityId = null, $comparison = null)
    {
        if (is_array($cityId)) {
            $useMinMax = false;
            if (isset($cityId['min'])) {
                $this->addUsingAlias(UserPeer::CITY_ID, $cityId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($cityId['max'])) {
                $this->addUsingAlias(UserPeer::CITY_ID, $cityId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserPeer::CITY_ID, $cityId, $comparison);
    }

    /**
     * Filter the query on the profile_id column
     *
     * Example usage:
     * <code>
     * $query->filterByProfileId(1234); // WHERE profile_id = 1234
     * $query->filterByProfileId(array(12, 34)); // WHERE profile_id IN (12, 34)
     * $query->filterByProfileId(array('min' => 12)); // WHERE profile_id >= 12
     * $query->filterByProfileId(array('max' => 12)); // WHERE profile_id <= 12
     * </code>
     *
     * @see       filterByProfession()
     *
     * @param     mixed $profileId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function filterByProfileId($profileId = null, $comparison = null)
    {
        if (is_array($profileId)) {
            $useMinMax = false;
            if (isset($profileId['min'])) {
                $this->addUsingAlias(UserPeer::PROFILE_ID, $profileId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($profileId['max'])) {
                $this->addUsingAlias(UserPeer::PROFILE_ID, $profileId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserPeer::PROFILE_ID, $profileId, $comparison);
    }

    /**
     * Filter the query on the promo_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPromoId(1234); // WHERE promo_id = 1234
     * $query->filterByPromoId(array(12, 34)); // WHERE promo_id IN (12, 34)
     * $query->filterByPromoId(array('min' => 12)); // WHERE promo_id >= 12
     * $query->filterByPromoId(array('max' => 12)); // WHERE promo_id <= 12
     * </code>
     *
     * @see       filterByPromo()
     *
     * @param     mixed $promoId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function filterByPromoId($promoId = null, $comparison = null)
    {
        if (is_array($promoId)) {
            $useMinMax = false;
            if (isset($promoId['min'])) {
                $this->addUsingAlias(UserPeer::PROMO_ID, $promoId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($promoId['max'])) {
                $this->addUsingAlias(UserPeer::PROMO_ID, $promoId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserPeer::PROMO_ID, $promoId, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%'); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $email)) {
                $email = str_replace('*', '%', $email);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UserPeer::EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the password column
     *
     * Example usage:
     * <code>
     * $query->filterByPassword('fooValue');   // WHERE password = 'fooValue'
     * $query->filterByPassword('%fooValue%'); // WHERE password LIKE '%fooValue%'
     * </code>
     *
     * @param     string $password The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function filterByPassword($password = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($password)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $password)) {
                $password = str_replace('*', '%', $password);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UserPeer::PASSWORD, $password, $comparison);
    }

    /**
     * Filter the query on the lastname column
     *
     * Example usage:
     * <code>
     * $query->filterByLastname('fooValue');   // WHERE lastname = 'fooValue'
     * $query->filterByLastname('%fooValue%'); // WHERE lastname LIKE '%fooValue%'
     * </code>
     *
     * @param     string $lastname The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function filterByLastname($lastname = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($lastname)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $lastname)) {
                $lastname = str_replace('*', '%', $lastname);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UserPeer::LASTNAME, $lastname, $comparison);
    }

    /**
     * Filter the query on the firstname column
     *
     * Example usage:
     * <code>
     * $query->filterByFirstname('fooValue');   // WHERE firstname = 'fooValue'
     * $query->filterByFirstname('%fooValue%'); // WHERE firstname LIKE '%fooValue%'
     * </code>
     *
     * @param     string $firstname The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function filterByFirstname($firstname = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($firstname)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $firstname)) {
                $firstname = str_replace('*', '%', $firstname);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UserPeer::FIRSTNAME, $firstname, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $description)) {
                $description = str_replace('*', '%', $description);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UserPeer::DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the newsletter column
     *
     * Example usage:
     * <code>
     * $query->filterByNewsletter(true); // WHERE newsletter = true
     * $query->filterByNewsletter('yes'); // WHERE newsletter = true
     * </code>
     *
     * @param     boolean|string $newsletter The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function filterByNewsletter($newsletter = null, $comparison = null)
    {
        if (is_string($newsletter)) {
            $newsletter = in_array(strtolower($newsletter), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UserPeer::NEWSLETTER, $newsletter, $comparison);
    }

    /**
     * Filter the query on the birthday column
     *
     * Example usage:
     * <code>
     * $query->filterByBirthday('2011-03-14'); // WHERE birthday = '2011-03-14'
     * $query->filterByBirthday('now'); // WHERE birthday = '2011-03-14'
     * $query->filterByBirthday(array('max' => 'yesterday')); // WHERE birthday < '2011-03-13'
     * </code>
     *
     * @param     mixed $birthday The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function filterByBirthday($birthday = null, $comparison = null)
    {
        if (is_array($birthday)) {
            $useMinMax = false;
            if (isset($birthday['min'])) {
                $this->addUsingAlias(UserPeer::BIRTHDAY, $birthday['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($birthday['max'])) {
                $this->addUsingAlias(UserPeer::BIRTHDAY, $birthday['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserPeer::BIRTHDAY, $birthday, $comparison);
    }

    /**
     * Filter the query on the url_portfolio column
     *
     * Example usage:
     * <code>
     * $query->filterByUrlPortfolio('fooValue');   // WHERE url_portfolio = 'fooValue'
     * $query->filterByUrlPortfolio('%fooValue%'); // WHERE url_portfolio LIKE '%fooValue%'
     * </code>
     *
     * @param     string $urlPortfolio The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function filterByUrlPortfolio($urlPortfolio = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($urlPortfolio)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $urlPortfolio)) {
                $urlPortfolio = str_replace('*', '%', $urlPortfolio);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UserPeer::URL_PORTFOLIO, $urlPortfolio, $comparison);
    }

    /**
     * Filter the query on the url_linkedin column
     *
     * Example usage:
     * <code>
     * $query->filterByUrlLinkedin('fooValue');   // WHERE url_linkedin = 'fooValue'
     * $query->filterByUrlLinkedin('%fooValue%'); // WHERE url_linkedin LIKE '%fooValue%'
     * </code>
     *
     * @param     string $urlLinkedin The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function filterByUrlLinkedin($urlLinkedin = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($urlLinkedin)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $urlLinkedin)) {
                $urlLinkedin = str_replace('*', '%', $urlLinkedin);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UserPeer::URL_LINKEDIN, $urlLinkedin, $comparison);
    }

    /**
     * Filter the query on the url_facebook column
     *
     * Example usage:
     * <code>
     * $query->filterByUrlFacebook('fooValue');   // WHERE url_facebook = 'fooValue'
     * $query->filterByUrlFacebook('%fooValue%'); // WHERE url_facebook LIKE '%fooValue%'
     * </code>
     *
     * @param     string $urlFacebook The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function filterByUrlFacebook($urlFacebook = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($urlFacebook)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $urlFacebook)) {
                $urlFacebook = str_replace('*', '%', $urlFacebook);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UserPeer::URL_FACEBOOK, $urlFacebook, $comparison);
    }

    /**
     * Filter the query on the url_google column
     *
     * Example usage:
     * <code>
     * $query->filterByUrlGoogle('fooValue');   // WHERE url_google = 'fooValue'
     * $query->filterByUrlGoogle('%fooValue%'); // WHERE url_google LIKE '%fooValue%'
     * </code>
     *
     * @param     string $urlGoogle The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function filterByUrlGoogle($urlGoogle = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($urlGoogle)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $urlGoogle)) {
                $urlGoogle = str_replace('*', '%', $urlGoogle);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UserPeer::URL_GOOGLE, $urlGoogle, $comparison);
    }

    /**
     * Filter the query on the url_twitter column
     *
     * Example usage:
     * <code>
     * $query->filterByUrlTwitter('fooValue');   // WHERE url_twitter = 'fooValue'
     * $query->filterByUrlTwitter('%fooValue%'); // WHERE url_twitter LIKE '%fooValue%'
     * </code>
     *
     * @param     string $urlTwitter The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function filterByUrlTwitter($urlTwitter = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($urlTwitter)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $urlTwitter)) {
                $urlTwitter = str_replace('*', '%', $urlTwitter);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UserPeer::URL_TWITTER, $urlTwitter, $comparison);
    }

    /**
     * Filter the query on the url_tumblr column
     *
     * Example usage:
     * <code>
     * $query->filterByUrlTumblr('fooValue');   // WHERE url_tumblr = 'fooValue'
     * $query->filterByUrlTumblr('%fooValue%'); // WHERE url_tumblr LIKE '%fooValue%'
     * </code>
     *
     * @param     string $urlTumblr The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function filterByUrlTumblr($urlTumblr = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($urlTumblr)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $urlTumblr)) {
                $urlTumblr = str_replace('*', '%', $urlTumblr);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UserPeer::URL_TUMBLR, $urlTumblr, $comparison);
    }

    /**
     * Filter the query on the url_instagram column
     *
     * Example usage:
     * <code>
     * $query->filterByUrlInstagram('fooValue');   // WHERE url_instagram = 'fooValue'
     * $query->filterByUrlInstagram('%fooValue%'); // WHERE url_instagram LIKE '%fooValue%'
     * </code>
     *
     * @param     string $urlInstagram The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function filterByUrlInstagram($urlInstagram = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($urlInstagram)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $urlInstagram)) {
                $urlInstagram = str_replace('*', '%', $urlInstagram);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UserPeer::URL_INSTAGRAM, $urlInstagram, $comparison);
    }

    /**
     * Filter the query on the url_blog column
     *
     * Example usage:
     * <code>
     * $query->filterByUrlBlog('fooValue');   // WHERE url_blog = 'fooValue'
     * $query->filterByUrlBlog('%fooValue%'); // WHERE url_blog LIKE '%fooValue%'
     * </code>
     *
     * @param     string $urlBlog The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function filterByUrlBlog($urlBlog = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($urlBlog)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $urlBlog)) {
                $urlBlog = str_replace('*', '%', $urlBlog);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UserPeer::URL_BLOG, $urlBlog, $comparison);
    }

    /**
     * Filter the query on the cv_file column
     *
     * Example usage:
     * <code>
     * $query->filterByCvFile('fooValue');   // WHERE cv_file = 'fooValue'
     * $query->filterByCvFile('%fooValue%'); // WHERE cv_file LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cvFile The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function filterByCvFile($cvFile = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cvFile)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $cvFile)) {
                $cvFile = str_replace('*', '%', $cvFile);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UserPeer::CV_FILE, $cvFile, $comparison);
    }

    /**
     * Filter the query on the image_profil_file column
     *
     * Example usage:
     * <code>
     * $query->filterByImageProfilFile('fooValue');   // WHERE image_profil_file = 'fooValue'
     * $query->filterByImageProfilFile('%fooValue%'); // WHERE image_profil_file LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageProfilFile The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function filterByImageProfilFile($imageProfilFile = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageProfilFile)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $imageProfilFile)) {
                $imageProfilFile = str_replace('*', '%', $imageProfilFile);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UserPeer::IMAGE_PROFIL_FILE, $imageProfilFile, $comparison);
    }

    /**
     * Filter the query on the image_couv_file column
     *
     * Example usage:
     * <code>
     * $query->filterByImageCouvFile('fooValue');   // WHERE image_couv_file = 'fooValue'
     * $query->filterByImageCouvFile('%fooValue%'); // WHERE image_couv_file LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageCouvFile The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function filterByImageCouvFile($imageCouvFile = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageCouvFile)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $imageCouvFile)) {
                $imageCouvFile = str_replace('*', '%', $imageCouvFile);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UserPeer::IMAGE_COUV_FILE, $imageCouvFile, $comparison);
    }

    /**
     * Filter the query on the telephone column
     *
     * Example usage:
     * <code>
     * $query->filterByTelephone('fooValue');   // WHERE telephone = 'fooValue'
     * $query->filterByTelephone('%fooValue%'); // WHERE telephone LIKE '%fooValue%'
     * </code>
     *
     * @param     string $telephone The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function filterByTelephone($telephone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($telephone)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $telephone)) {
                $telephone = str_replace('*', '%', $telephone);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UserPeer::TELEPHONE, $telephone, $comparison);
    }

    /**
     * Filter the query on the first_connection column
     *
     * Example usage:
     * <code>
     * $query->filterByFirstConnection(true); // WHERE first_connection = true
     * $query->filterByFirstConnection('yes'); // WHERE first_connection = true
     * </code>
     *
     * @param     boolean|string $firstConnection The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function filterByFirstConnection($firstConnection = null, $comparison = null)
    {
        if (is_string($firstConnection)) {
            $firstConnection = in_array(strtolower($firstConnection), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UserPeer::FIRST_CONNECTION, $firstConnection, $comparison);
    }

    /**
     * Filter the query on the validated column
     *
     * Example usage:
     * <code>
     * $query->filterByValidated(true); // WHERE validated = true
     * $query->filterByValidated('yes'); // WHERE validated = true
     * </code>
     *
     * @param     boolean|string $validated The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function filterByValidated($validated = null, $comparison = null)
    {
        if (is_string($validated)) {
            $validated = in_array(strtolower($validated), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UserPeer::VALIDATED, $validated, $comparison);
    }

    /**
     * Filter the query by a related City object
     *
     * @param   City|PropelObjectCollection $city The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 UserQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCity($city, $comparison = null)
    {
        if ($city instanceof City) {
            return $this
                ->addUsingAlias(UserPeer::CITY_ID, $city->getId(), $comparison);
        } elseif ($city instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UserPeer::CITY_ID, $city->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCity() only accepts arguments of type City or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the City relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function joinCity($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('City');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'City');
        }

        return $this;
    }

    /**
     * Use the City relation City object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MMIBundle\Model\CityQuery A secondary query class using the current class as primary query
     */
    public function useCityQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCity($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'City', '\MMIBundle\Model\CityQuery');
    }

    /**
     * Filter the query by a related Profession object
     *
     * @param   Profession|PropelObjectCollection $profession The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 UserQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByProfession($profession, $comparison = null)
    {
        if ($profession instanceof Profession) {
            return $this
                ->addUsingAlias(UserPeer::PROFILE_ID, $profession->getId(), $comparison);
        } elseif ($profession instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UserPeer::PROFILE_ID, $profession->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByProfession() only accepts arguments of type Profession or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Profession relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function joinProfession($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Profession');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Profession');
        }

        return $this;
    }

    /**
     * Use the Profession relation Profession object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MMIBundle\Model\ProfessionQuery A secondary query class using the current class as primary query
     */
    public function useProfessionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProfession($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Profession', '\MMIBundle\Model\ProfessionQuery');
    }

    /**
     * Filter the query by a related Promo object
     *
     * @param   Promo|PropelObjectCollection $promo The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 UserQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPromo($promo, $comparison = null)
    {
        if ($promo instanceof Promo) {
            return $this
                ->addUsingAlias(UserPeer::PROMO_ID, $promo->getId(), $comparison);
        } elseif ($promo instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UserPeer::PROMO_ID, $promo->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByPromo() only accepts arguments of type Promo or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Promo relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function joinPromo($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Promo');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Promo');
        }

        return $this;
    }

    /**
     * Use the Promo relation Promo object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MMIBundle\Model\PromoQuery A secondary query class using the current class as primary query
     */
    public function usePromoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPromo($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Promo', '\MMIBundle\Model\PromoQuery');
    }

    /**
     * Filter the query by a related UserRole object
     *
     * @param   UserRole|PropelObjectCollection $userRole  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 UserQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByUserRole($userRole, $comparison = null)
    {
        if ($userRole instanceof UserRole) {
            return $this
                ->addUsingAlias(UserPeer::ID, $userRole->getUserId(), $comparison);
        } elseif ($userRole instanceof PropelObjectCollection) {
            return $this
                ->useUserRoleQuery()
                ->filterByPrimaryKeys($userRole->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUserRole() only accepts arguments of type UserRole or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UserRole relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function joinUserRole($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UserRole');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UserRole');
        }

        return $this;
    }

    /**
     * Use the UserRole relation UserRole object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MMIBundle\Model\UserRoleQuery A secondary query class using the current class as primary query
     */
    public function useUserRoleQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUserRole($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UserRole', '\MMIBundle\Model\UserRoleQuery');
    }

    /**
     * Filter the query by a related Internship object
     *
     * @param   Internship|PropelObjectCollection $internship  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 UserQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByInternship($internship, $comparison = null)
    {
        if ($internship instanceof Internship) {
            return $this
                ->addUsingAlias(UserPeer::ID, $internship->getUserId(), $comparison);
        } elseif ($internship instanceof PropelObjectCollection) {
            return $this
                ->useInternshipQuery()
                ->filterByPrimaryKeys($internship->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByInternship() only accepts arguments of type Internship or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Internship relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function joinInternship($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Internship');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Internship');
        }

        return $this;
    }

    /**
     * Use the Internship relation Internship object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MMIBundle\Model\InternshipQuery A secondary query class using the current class as primary query
     */
    public function useInternshipQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinInternship($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Internship', '\MMIBundle\Model\InternshipQuery');
    }

    /**
     * Filter the query by a related UserJob object
     *
     * @param   UserJob|PropelObjectCollection $userJob  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 UserQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByUserJob($userJob, $comparison = null)
    {
        if ($userJob instanceof UserJob) {
            return $this
                ->addUsingAlias(UserPeer::ID, $userJob->getUserId(), $comparison);
        } elseif ($userJob instanceof PropelObjectCollection) {
            return $this
                ->useUserJobQuery()
                ->filterByPrimaryKeys($userJob->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUserJob() only accepts arguments of type UserJob or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UserJob relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function joinUserJob($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UserJob');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UserJob');
        }

        return $this;
    }

    /**
     * Use the UserJob relation UserJob object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MMIBundle\Model\UserJobQuery A secondary query class using the current class as primary query
     */
    public function useUserJobQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUserJob($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UserJob', '\MMIBundle\Model\UserJobQuery');
    }

    /**
     * Filter the query by a related UserObtainedDegree object
     *
     * @param   UserObtainedDegree|PropelObjectCollection $userObtainedDegree  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 UserQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByUserObtainedDegree($userObtainedDegree, $comparison = null)
    {
        if ($userObtainedDegree instanceof UserObtainedDegree) {
            return $this
                ->addUsingAlias(UserPeer::ID, $userObtainedDegree->getUserId(), $comparison);
        } elseif ($userObtainedDegree instanceof PropelObjectCollection) {
            return $this
                ->useUserObtainedDegreeQuery()
                ->filterByPrimaryKeys($userObtainedDegree->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUserObtainedDegree() only accepts arguments of type UserObtainedDegree or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UserObtainedDegree relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function joinUserObtainedDegree($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UserObtainedDegree');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UserObtainedDegree');
        }

        return $this;
    }

    /**
     * Use the UserObtainedDegree relation UserObtainedDegree object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MMIBundle\Model\UserObtainedDegreeQuery A secondary query class using the current class as primary query
     */
    public function useUserObtainedDegreeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUserObtainedDegree($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UserObtainedDegree', '\MMIBundle\Model\UserObtainedDegreeQuery');
    }

    /**
     * Filter the query by a related UnivProject object
     *
     * @param   UnivProject|PropelObjectCollection $univProject  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 UserQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByUnivProject($univProject, $comparison = null)
    {
        if ($univProject instanceof UnivProject) {
            return $this
                ->addUsingAlias(UserPeer::ID, $univProject->getUserId(), $comparison);
        } elseif ($univProject instanceof PropelObjectCollection) {
            return $this
                ->useUnivProjectQuery()
                ->filterByPrimaryKeys($univProject->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUnivProject() only accepts arguments of type UnivProject or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UnivProject relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function joinUnivProject($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UnivProject');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UnivProject');
        }

        return $this;
    }

    /**
     * Use the UnivProject relation UnivProject object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MMIBundle\Model\UnivProjectQuery A secondary query class using the current class as primary query
     */
    public function useUnivProjectQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUnivProject($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UnivProject', '\MMIBundle\Model\UnivProjectQuery');
    }

    /**
     * Filter the query by a related UserMemberUnipro object
     *
     * @param   UserMemberUnipro|PropelObjectCollection $userMemberUnipro  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 UserQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByUserMemberUnipro($userMemberUnipro, $comparison = null)
    {
        if ($userMemberUnipro instanceof UserMemberUnipro) {
            return $this
                ->addUsingAlias(UserPeer::ID, $userMemberUnipro->getUserId(), $comparison);
        } elseif ($userMemberUnipro instanceof PropelObjectCollection) {
            return $this
                ->useUserMemberUniproQuery()
                ->filterByPrimaryKeys($userMemberUnipro->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUserMemberUnipro() only accepts arguments of type UserMemberUnipro or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UserMemberUnipro relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function joinUserMemberUnipro($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UserMemberUnipro');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UserMemberUnipro');
        }

        return $this;
    }

    /**
     * Use the UserMemberUnipro relation UserMemberUnipro object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MMIBundle\Model\UserMemberUniproQuery A secondary query class using the current class as primary query
     */
    public function useUserMemberUniproQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUserMemberUnipro($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UserMemberUnipro', '\MMIBundle\Model\UserMemberUniproQuery');
    }

    /**
     * Filter the query by a related Validation object
     *
     * @param   Validation|PropelObjectCollection $validation  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 UserQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByValidation($validation, $comparison = null)
    {
        if ($validation instanceof Validation) {
            return $this
                ->addUsingAlias(UserPeer::ID, $validation->getUserId(), $comparison);
        } elseif ($validation instanceof PropelObjectCollection) {
            return $this
                ->useValidationQuery()
                ->filterByPrimaryKeys($validation->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByValidation() only accepts arguments of type Validation or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Validation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function joinValidation($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Validation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Validation');
        }

        return $this;
    }

    /**
     * Use the Validation relation Validation object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MMIBundle\Model\ValidationQuery A secondary query class using the current class as primary query
     */
    public function useValidationQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinValidation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Validation', '\MMIBundle\Model\ValidationQuery');
    }

    /**
     * Filter the query by a related Role object
     * using the user_role table as cross reference
     *
     * @param   Role $role the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   UserQuery The current query, for fluid interface
     */
    public function filterByRole($role, $comparison = Criteria::EQUAL)
    {
        return $this
            ->useUserRoleQuery()
            ->filterByRole($role, $comparison)
            ->endUse();
    }

    /**
     * Exclude object from result
     *
     * @param   User $user Object to remove from the list of results
     *
     * @return UserQuery The current query, for fluid interface
     */
    public function prune($user = null)
    {
        if ($user) {
            $this->addUsingAlias(UserPeer::ID, $user->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
