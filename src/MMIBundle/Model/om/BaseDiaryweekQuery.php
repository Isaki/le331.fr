<?php

namespace MMIBundle\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use MMIBundle\Model\Diaryweek;
use MMIBundle\Model\DiaryweekPeer;
use MMIBundle\Model\DiaryweekQuery;
use MMIBundle\Model\Internship;

/**
 * @method DiaryweekQuery orderById($order = Criteria::ASC) Order by the id column
 * @method DiaryweekQuery orderByInternshipId($order = Criteria::ASC) Order by the internship_id column
 * @method DiaryweekQuery orderByContent($order = Criteria::ASC) Order by the content column
 * @method DiaryweekQuery orderByWeek($order = Criteria::ASC) Order by the week column
 *
 * @method DiaryweekQuery groupById() Group by the id column
 * @method DiaryweekQuery groupByInternshipId() Group by the internship_id column
 * @method DiaryweekQuery groupByContent() Group by the content column
 * @method DiaryweekQuery groupByWeek() Group by the week column
 *
 * @method DiaryweekQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method DiaryweekQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method DiaryweekQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method DiaryweekQuery leftJoinInternship($relationAlias = null) Adds a LEFT JOIN clause to the query using the Internship relation
 * @method DiaryweekQuery rightJoinInternship($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Internship relation
 * @method DiaryweekQuery innerJoinInternship($relationAlias = null) Adds a INNER JOIN clause to the query using the Internship relation
 *
 * @method Diaryweek findOne(PropelPDO $con = null) Return the first Diaryweek matching the query
 * @method Diaryweek findOneOrCreate(PropelPDO $con = null) Return the first Diaryweek matching the query, or a new Diaryweek object populated from the query conditions when no match is found
 *
 * @method Diaryweek findOneByInternshipId(int $internship_id) Return the first Diaryweek filtered by the internship_id column
 * @method Diaryweek findOneByContent(string $content) Return the first Diaryweek filtered by the content column
 * @method Diaryweek findOneByWeek(int $week) Return the first Diaryweek filtered by the week column
 *
 * @method array findById(int $id) Return Diaryweek objects filtered by the id column
 * @method array findByInternshipId(int $internship_id) Return Diaryweek objects filtered by the internship_id column
 * @method array findByContent(string $content) Return Diaryweek objects filtered by the content column
 * @method array findByWeek(int $week) Return Diaryweek objects filtered by the week column
 */
abstract class BaseDiaryweekQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseDiaryweekQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'default';
        }
        if (null === $modelName) {
            $modelName = 'MMIBundle\\Model\\Diaryweek';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new DiaryweekQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   DiaryweekQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return DiaryweekQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof DiaryweekQuery) {
            return $criteria;
        }
        $query = new DiaryweekQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Diaryweek|Diaryweek[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = DiaryweekPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(DiaryweekPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Diaryweek A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Diaryweek A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `internship_id`, `content`, `week` FROM `diaryWeek` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Diaryweek();
            $obj->hydrate($row);
            DiaryweekPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Diaryweek|Diaryweek[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Diaryweek[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return DiaryweekQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(DiaryweekPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return DiaryweekQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(DiaryweekPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DiaryweekQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(DiaryweekPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(DiaryweekPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DiaryweekPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the internship_id column
     *
     * Example usage:
     * <code>
     * $query->filterByInternshipId(1234); // WHERE internship_id = 1234
     * $query->filterByInternshipId(array(12, 34)); // WHERE internship_id IN (12, 34)
     * $query->filterByInternshipId(array('min' => 12)); // WHERE internship_id >= 12
     * $query->filterByInternshipId(array('max' => 12)); // WHERE internship_id <= 12
     * </code>
     *
     * @see       filterByInternship()
     *
     * @param     mixed $internshipId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DiaryweekQuery The current query, for fluid interface
     */
    public function filterByInternshipId($internshipId = null, $comparison = null)
    {
        if (is_array($internshipId)) {
            $useMinMax = false;
            if (isset($internshipId['min'])) {
                $this->addUsingAlias(DiaryweekPeer::INTERNSHIP_ID, $internshipId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($internshipId['max'])) {
                $this->addUsingAlias(DiaryweekPeer::INTERNSHIP_ID, $internshipId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DiaryweekPeer::INTERNSHIP_ID, $internshipId, $comparison);
    }

    /**
     * Filter the query on the content column
     *
     * Example usage:
     * <code>
     * $query->filterByContent('fooValue');   // WHERE content = 'fooValue'
     * $query->filterByContent('%fooValue%'); // WHERE content LIKE '%fooValue%'
     * </code>
     *
     * @param     string $content The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DiaryweekQuery The current query, for fluid interface
     */
    public function filterByContent($content = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($content)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $content)) {
                $content = str_replace('*', '%', $content);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(DiaryweekPeer::CONTENT, $content, $comparison);
    }

    /**
     * Filter the query on the week column
     *
     * Example usage:
     * <code>
     * $query->filterByWeek(1234); // WHERE week = 1234
     * $query->filterByWeek(array(12, 34)); // WHERE week IN (12, 34)
     * $query->filterByWeek(array('min' => 12)); // WHERE week >= 12
     * $query->filterByWeek(array('max' => 12)); // WHERE week <= 12
     * </code>
     *
     * @param     mixed $week The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return DiaryweekQuery The current query, for fluid interface
     */
    public function filterByWeek($week = null, $comparison = null)
    {
        if (is_array($week)) {
            $useMinMax = false;
            if (isset($week['min'])) {
                $this->addUsingAlias(DiaryweekPeer::WEEK, $week['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($week['max'])) {
                $this->addUsingAlias(DiaryweekPeer::WEEK, $week['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DiaryweekPeer::WEEK, $week, $comparison);
    }

    /**
     * Filter the query by a related Internship object
     *
     * @param   Internship|PropelObjectCollection $internship The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 DiaryweekQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByInternship($internship, $comparison = null)
    {
        if ($internship instanceof Internship) {
            return $this
                ->addUsingAlias(DiaryweekPeer::INTERNSHIP_ID, $internship->getId(), $comparison);
        } elseif ($internship instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(DiaryweekPeer::INTERNSHIP_ID, $internship->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByInternship() only accepts arguments of type Internship or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Internship relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return DiaryweekQuery The current query, for fluid interface
     */
    public function joinInternship($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Internship');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Internship');
        }

        return $this;
    }

    /**
     * Use the Internship relation Internship object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MMIBundle\Model\InternshipQuery A secondary query class using the current class as primary query
     */
    public function useInternshipQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinInternship($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Internship', '\MMIBundle\Model\InternshipQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Diaryweek $diaryweek Object to remove from the list of results
     *
     * @return DiaryweekQuery The current query, for fluid interface
     */
    public function prune($diaryweek = null)
    {
        if ($diaryweek) {
            $this->addUsingAlias(DiaryweekPeer::ID, $diaryweek->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
