<?php

namespace MMIBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserRegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("email", "text", array(
                "attr" => array("placeholder" => "Adresse email")))
            ->add("password", "password", array(
                "attr" => array("placeholder" => "Mot de passe")))
            ->add("lastname", "text", array(
                "attr" => array("placeholder" => "Nom")))
            ->add("firstname", "text", array(
                "attr" => array("placeholder" => "Prénom")));

        $builder->get("password")->addModelTransformer(new CallbackTransformer(
            function ($original) {
                return $original;
            },
            function ($submitted) {
                return sha1($submitted);
            }
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array("data_class" => 'MMIBundle\Model\User'));
    }

    public function getName()
    {
        return "UserRegisterType";
    }
}