<?php

namespace MMIBundle\Form\Type;

use MMIBundle\Model\Company;
use MMIBundle\Model\CompanyQuery;
use MMIBundle\Model\Profession;
use MMIBundle\Model\ProfessionQuery;
use MMIBundle\Model\UnivProject;
use MMIBundle\Model\UnivProjectQuery;
use MMIBundle\Model\UserMemberUnipro;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserAddProjectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $years = [];
        for ($year = date('Y', time()); $year >= 1990; $year--) $years[$year] = $year;

        $builder
            ->add('project_id', 'hidden', ['mapped' => false])
            ->add('unipro_id', "text", [])
            ->add('profession_id', "text", [])
            ->add('yearFrom', 'choice', ['choices' => $years])
            ->add('yearTo', 'choice', ['choices' => $years]);

        $builder->get("profession_id")->addModelTransformer(new CallbackTransformer(
            function ($original) {
                $profession = "";
                if ($original != null) $profession = $original->getName();
                return $profession;
            },
            function ($submitted) {
                $profession = ProfessionQuery::create()->filterByName($submitted)->findOne();
                if ($profession == null) {
                    $profession = new Profession();
                    $profession->setName($submitted)->save();
                }
                return $profession->getId();
            }
        ));

        $builder->get("unipro_id")->addModelTransformer(new CallbackTransformer(
            function ($original) {
                $project = "";
                if ($original != null) $project = $original->getName();
                return $project;
            },
            function ($submitted) {
                $project = UnivProjectQuery::create()->filterByName($submitted)->findOne();
                if ($project == null) {
                    $project = new UnivProject();
                    $project->setName($submitted)->save();
                }
                return $project->getId();
            }
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array("data_class" => 'MMIBundle\Model\UserMemberUnipro'));
    }

    public function getName()
    {
        return "UserAddProjectType";
    }
}