<?php

namespace MMIBundle\Form\Type;

use MMIBundle\Model\Company;
use MMIBundle\Model\CompanyPeer;
use Propel\PropelBundle\Form\BaseAbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BeforeInternshipType extends BaseAbstractType
{
    protected $options = array(
        'data_class' => 'MMIBundle\Model\Internship',
        'name' => 'beforeInternship',
    );

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /*$builder->add('dateBegining', 'text');
        $builder->add('dateEnding', 'text');*/

        $builder->add('companyId', 'text');
        $builder->add('workFunction');

        /*$builder->add('offerId');*/

        $builder->add('signatoryLastname');
        $builder->add('signatoryFirstname');
        $builder->add('signatoryFunction');

        $builder->add('masterLastname');
        $builder->add('masterFirstname');
        $builder->add('masterEmail');
        $builder->add('masterTelephone');

        $builder->add('description', 'textarea');
        $builder->add('valider', 'submit');





        /*$builder->get("dateBegining")->addModelTransformer(new CallbackTransformer(
            function ($original) {
                if ($original != null) return $original->format("d/m/Y");
                else return date("d/m/Y", mktime(0, 0, 0, 1, 1, date("Y") - 18));
            },
            function ($submitted) {
                $d = explode("/", $submitted);
                $submitted = $d[2] . "-" . $d[1] . "-" . $d[0];
                // $submitted = date_create($submitted);

                return $submitted;
            }
        ));

        $builder->get("dateEnding")->addModelTransformer(new CallbackTransformer(
            function ($original) {
                if ($original != null) return $original->format("d/m/Y");
                else return date("d/m/Y", mktime(0, 0, 0, 1, 1, date("Y") - 18));
            },
            function ($submitted) {
                $d = explode("/", $submitted);
                $submitted = $d[2] . "-" . $d[1] . "-" . $d[0];
                // $submitted = date_create($submitted);

                return $submitted;
            }
        ));*/

        $builder->get("companyId")->addModelTransformer(new CallbackTransformer(
            function ($original) {
                $return = "";
                if ($original != null) $return = CompanyPeer::getOneById($original);
                return $return;
            },
            function ($submitted) {
                $company = CompanyPeer::getOneByName($submitted);
                if ($company == null) {
                    $company = new Company();
                    $company->setName($submitted);
                }
                return $company->getId();
            }
        ));

    }
}
