<?php

namespace MMIBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserModifyPasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("pass1", "password", array(
                "attr" => array("placeholder" => "Mot de passe"),
                "mapped" => false))
            ->add("pass2", "password", array(
                "attr" => array("placeholder" => "Nouveau mot de passe"),
                "mapped" => false))
            ->add("pass3", "password", array(
                "attr" => array("placeholder" => "Confirmation du mot de passe"),
                "mapped" => false));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array("data_class" => 'MMIBundle\Model\User'));
    }

    public function getName()
    {
        return "UserModifyPasswordType";
    }
}