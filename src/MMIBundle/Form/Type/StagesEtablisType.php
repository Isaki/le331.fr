<?php

namespace MMIBundle\Form\Type;

use MMIBundle\Model\Company;
use MMIBundle\Model\CompanyPeer;
use MMIBundle\Model\UserPeer;
use MMIBundle\Model\UserQuery;
use Propel\PropelBundle\Form\BaseAbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StagesEtablisType extends BaseAbstractType
{
    protected $options = array(
        'data_class' => 'MMIBundle\Model\Internship',
        'name' => 'stageEtablis',
    );

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('id', 'hidden');

        $builder->add(
            'tutor_id',
            'choice',
            array(
                'choices' => $this->getUsers(),
            )
        );

        $builder->add('date_visite', 'text');
        $builder->add('date_soutenance', 'text');
        $builder->add('valider', 'submit');
        

        $builder->get("date_visite")->addModelTransformer(new CallbackTransformer(
            function ($original) {
                if ($original != null) return $original->format("d/m/Y");
                else return date("d/m/Y", mktime(0, 0, 0, 1, 1, date("Y") - 18));
            },
            function ($submitted) {
                $d = explode("/", $submitted);
                $submitted = $d[2] . "-" . $d[1] . "-" . $d[0];
                // $submitted = date_create($submitted);

                return $submitted;
            }
        ));

        $builder->get("date_soutenance")->addModelTransformer(new CallbackTransformer(
            function ($original) {
                if ($original != null) return $original->format("d/m/Y");
                else return date("d/m/Y", mktime(0, 0, 0, 1, 1, date("Y") - 18));
            },
            function ($submitted) {
                $d = explode("/", $submitted);
                $submitted = $d[2] . "-" . $d[1] . "-" . $d[0];
                // $submitted = date_create($submitted);

                return $submitted;
            }
        ));


    }

    protected function getUsers() {
        $data = [];

        $users = UserQuery::create()
            ->orderByLastname()
            ->useUserRoleQuery()
                ->useRoleQuery()
                    ->filterByName('teacher')
                ->endUse()
            ->endUse()
            ->find();

        foreach ($users as $user) {
            $data[$user->getId()] = $user->getLastname().' '.$user->getFirstname();
        }

        return $data;
    }
}
