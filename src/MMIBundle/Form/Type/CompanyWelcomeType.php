<?php

namespace MMIBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CompanyWelcomeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("description", "textarea", [
                "attr" => ["placeholder" => "Pouvez vous nous donner un petit résumé sur vous ?"],
                'required' => false])
            ->add("address", "text", [
                "attr" => ["placeholder" => "Quelle est votre adresse ?"]])
            ->add("city", "text", [
                "attr" => ["placeholder" => "Dans quelle ville ?"],
                'mapped' => false])
            ->add("cp", "text", [
                "attr" => ["placeholder" => "Quel code postal ?"],
                'mapped' => false,
                'required' => false])
            ->add("telephone", "text", [
                "attr" => ["placeholder" => "Quel est votre numéro de téléphone ?"],
                'required' => false])
            ->add("firstname", "text", [
                "attr" => ["placeholder" => "Quel est votre prénom ?"],
                'mapped' => false])
            ->add("lastname", "text", [
                "attr" => ["placeholder" => "Quel est votre nom ?"],
                'mapped' => false])
            ->add("email", "text", [
                "attr" => ["placeholder" => "Quel est votre email ?"],
                'mapped' => false])
            ->add("profession", "text", [
                "attr" => ["placeholder" => "Quel le poste que vous occupez ?"],
                'mapped' => false,
                'required' => false])
            ->add("url_website", "text", [
                "attr" => ["placeholder" => "Quel est le lien vers votre site ?"],
                'required' => false]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(["data_class" => 'MMIBundle\Model\Company']);
    }

    // méthode pour obtenir le nom du modèle de formulaire
    public function getName()
    {
        return "CompanyWelcomeType";
    }
}