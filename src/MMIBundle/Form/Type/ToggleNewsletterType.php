<?php

namespace MMIBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ToggleNewsletterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($options["user"]->getNewsletter() == 1) {
            $builder->add(
                "unsubscribeNewsletter",
                "submit",
                array(
                    "label" => "Me désabonner",
                    "attr" => array(
                        "class" => "big grey",
                    )
                )
            );
        }
        else {
            $builder->add(
                "subscribeNewsletter",
                "submit",
                array(
                    "label" => "M'abonner",
                    "attr" => array(
                        "class" => "big grey",
                    )
                )
            );
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            "data_class" => 'MMIBundle\Model\User',
            "user" => null
        ]);
    }

    public function getName()
    {
        return "ToggleNewsletterType";
    }
}