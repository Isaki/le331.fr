<?php

namespace MMIBundle\Form\Type;

use Propel\PropelBundle\Form\BaseAbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;

class UserLoginType extends BaseAbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', "text", ['attr' => ['placeholder' => 'Adresse email']]);
        $builder->add('password', "password", ['attr' => ['placeholder' => 'Mot de passe']]);

        $builder->get("password")->addModelTransformer(new CallbackTransformer(
            function ($original) {
                return $original;
            },
            function ($submitted) {
                return sha1($submitted);
            }
        ));
    }
    protected $options = array(
        'data_class' => 'MMIBundle\Model\User',
        'name'       => 'UserLoginType',
    );
}