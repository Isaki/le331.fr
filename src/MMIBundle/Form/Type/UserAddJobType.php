<?php

namespace MMIBundle\Form\Type;

use MMIBundle\Model\Company;
use MMIBundle\Model\CompanyQuery;
use MMIBundle\Model\Profession;
use MMIBundle\Model\ProfessionQuery;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserAddJobType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $years = [];
        for ($year = date('Y', time()); $year >= 1990; $year--) $years[$year] = $year;

        $builder
            ->add('job_id', 'hidden', ['mapped' => false])
            ->add('profession_id', 'text', [])
            ->add('company_id', 'text', [])
            ->add('yearFrom', 'choice', ['choices' => $years])
            ->add('yearTo', 'choice', ['choices' => $years])
            ->add('description', 'textarea', []);

        $builder->get("profession_id")->addModelTransformer(new CallbackTransformer(
            function ($original) {
                $profession = "";
                if ($original != null) $profession = $original->getName();
                return $profession;
            },
            function ($submitted) {
                $profession = ProfessionQuery::create()->filterByName($submitted)->findOne();
                if ($profession == null) {
                    $profession = new Profession();
                    $profession->setName($submitted)->save();
                }
                return $profession->getId();
            }
        ));

        $builder->get("company_id")->addModelTransformer(new CallbackTransformer(
            function ($original) {
                $company = "";
                if ($original != null) $company = $original->getName();
                return $company;
            },
            function ($submitted) {
                $company = CompanyQuery::create()->filterByName($submitted)->findOne();
                if ($company == null) {
                    $company = new Company();
                    $company->setName($submitted)->save();
                }
                return $company->getId();
            }
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array("data_class" => 'MMIBundle\Model\UserJob'));
    }

    public function getName()
    {
        return "UserAddJobType";
    }
}