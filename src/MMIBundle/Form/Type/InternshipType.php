<?php

namespace MMIBundle\Form\Type;

use Propel\PropelBundle\Form\BaseAbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class InternshipType extends BaseAbstractType
{
    protected $options = array(
        'data_class' => 'MMIBundle\Model\Internship',
        'name'       => 'internship',
    );

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('offerId');
        $builder->add('companyId');
        $builder->add('userId');
        $builder->add('workFunction');
        $builder->add('dateBegining');
        $builder->add('dateEnding');
        $builder->add('description');
        $builder->add('information');
        $builder->add('urlWebsiteDiffusion');
        $builder->add('signatoryLastname');
        $builder->add('signatoryFirstname');
        $builder->add('signatoryFunction');
        $builder->add('masterLastname');
        $builder->add('masterFirstname');
        $builder->add('masterEmail');
        $builder->add('masterTelephone');
    }
}
