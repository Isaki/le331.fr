<?php

namespace MMIBundle\Form\Type;

use MMIBundle\Model\Company;
use MMIBundle\Model\CompanyQuery;
use MMIBundle\Model\Degree;
use MMIBundle\Model\DegreePeer;
use MMIBundle\Model\DegreeQuery;
use MMIBundle\Model\Profession;
use MMIBundle\Model\ProfessionQuery;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserAddDegreeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $years = [];
        for ($year = date('Y', time()); $year >= 1990; $year--) $years[$year] = $year;

        $builder
            ->add('training_id', 'hidden', ['mapped' => false])
            ->add('degree_id', "text", [])
            ->add('yearFrom', 'choice', ['choices' => $years])
            ->add('yearTo', 'choice', ['choices' => $years]);

        $builder->get("degree_id")->addModelTransformer(new CallbackTransformer(
            function ($original) {
                $degree = "";
                if ($original != null) $degree = $original->getName();
                return $degree;
            },
            function ($submitted) {
                $degree = DegreePeer::getOneLike($submitted);
                if ($degree == null) {
                    $degree = new Degree();
                    $degree->setName($submitted)->save();
                }
                return $degree->getId();
            }
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array("data_class" => 'MMIBundle\Model\UserObtainedDegree'));
    }

    public function getName()
    {
        return "UserAddDegreeType";
    }
}