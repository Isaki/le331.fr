<?php

namespace MMIBundle\Form\Type;

use MMIBundle\Model\City;
use MMIBundle\Model\CityPeer;
use MMIBundle\Model\CityQuery;
use MMIBundle\Model\Promo;
use MMIBundle\Model\PromoQuery;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Serializer\Tests\Model;

class UserWelcomeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('city_id', 'text', [
                'attr' => ['placeholder' => 'Vous vivez à ?']])
            ->add('birthday', 'text', [
                'attr' => ['placeholder' => 'Quelle est votre date de naissance ?']])
            ->add('url_portfolio', 'text', [
                'attr' => ['placeholder' => 'Quel est le lien vers votre portfolio ?'], 'required' => false])
            ->add('url_blog', 'text', [
                'attr' => ['placeholder' => 'Quel est le lien vers votre blog ?'], 'required' => false])
            ->add('url_facebook', 'text', [
                'attr' => ['placeholder' => 'Quel est le lien vers votre Facebook ?'], 'required' => false])
            ->add('url_twitter', 'text', [
                'attr' => ['placeholder' => 'Quel est le lien vers votre Twitter ?'], 'required' => false])
            ->add('url_linkedin', 'text', [
                'attr' => ['placeholder' => 'Quel est le lien vers votre LinkedIn ?'], 'required' => false])
            ->add('url_tumblr', 'text', [
                'attr' => ['placeholder' => 'Quel est le lien vers votre Tumblr ?'], 'required' => false])
            ->add('url_instagram', 'text', [
                'attr' => ['placeholder' => 'Quel est le lien vers votre Instagram ?'], 'required' => false])
            ->add('cv_file', 'file', [
                'attr' => [
                    'placeholder' => 'Uploader votre CV',
                    'accept' => 'application/pdf'],
                'mapped' => false,
                'required' => false])
            ->add('telephone', 'text', ['attr' => ['placeholder' => 'Votre téléphone'], 'required' => false])
            ->add('promo_id', 'choice', [
                'choices' => $this->getPromos(),
                'attr' => ['placeholder' => 'Promotion']]);

        $builder->get('birthday')->addModelTransformer(new CallbackTransformer(
            function ($original) {
                if ($original != null) {
                    return $original->format('d/m/Y');
                }
                return date('d/m/Y', mktime(0, 0, 0, 1, 1, date('Y') - 18));
            },
            function ($submitted) {
                $d = explode('/', $submitted);
                return $d[2] . '-' . $d[1] . '-' . $d[0];
            }
        ));

        $builder->get('city_id')->addModelTransformer(new CallbackTransformer(
            function ($id) {
                return ($city = CityQuery::create()->findOneById($id)) != null ? $city->getName() : '';
            },
            function ($name) {
                $city = CityQuery::create()->findOneByName($name);
                if ($city == null) {
                    $city = new City();
                    $city->setName($name);
                    $city->save();
                }
                return $city->getId();
            }
        ));

        $builder->get('promo_id')->addModelTransformer(new CallbackTransformer(
            function ($value) {
                return ($promo = PromoQuery::create()->findOneById($value)) != null ? $promo->getYear() : '';
            },
            function ($value) {
                if ($value == 'prof') return null;
                else {
                    $promo = PromoQuery::create()->findOneByYear($value);
                    if ($promo == null) {
                        $promo = new Promo();
                        $promo->setYear($value)->save();
                    }
                    return $promo->getId();
                }
            }
        ));
    }

    private function getPromos()
    {
        $list['prof'] = 'Je suis prof';

        for ($i = date('Y', time()) + 2; $i > 1980; $i--)
            $list[$i] = $i;

        return $list;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => 'MMIBundle\Model\User']);
    }

    public function getName()
    {
        return 'UserWelcomeType';
    }
}