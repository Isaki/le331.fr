<?php

namespace MMIBundle\Form\Type;

use MMIBundle\Model\Company;
use MMIBundle\Model\CompanyQuery;
use MMIBundle\Model\Profession;
use MMIBundle\Model\ProfessionQuery;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DegreeAddType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', "text", [])
            ->add('option', "text", [])
            ->add('summary', 'textarea', []);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array("data_class" => 'MMIBundle\Model\Degree'));
    }

    public function getName()
    {
        return "DegreeAddType";
    }
}