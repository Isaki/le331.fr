<?php

namespace MMIBundle\Form\Type;

use MMIBundle\Model\Company;
use MMIBundle\Model\CompanyQuery;
use MMIBundle\Model\Profession;
use MMIBundle\Model\ProfessionQuery;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CompanyAddType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', "text", [])
            ->add("logo_file", "file", ["attr" => ["accept" => "image/*"], "mapped" => false]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array("data_class" => 'MMIBundle\Model\Company'));
    }

    public function getName()
    {
        return "CompanyAddType";
    }
}