<?php

namespace MMIBundle\Form\Type;

use MMIBundle\Model\City;
use MMIBundle\Model\CityQuery;
use MMIBundle\Model\ProfessionQuery;
use MMIBundle\Model\PromoQuery;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserEditType extends AbstractType
{
    protected $user;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->user = $options['data'];

        $city = CityQuery::create()
            ->filterById($this->user->getCityId())
            ->findOne();
        $cp = $city != null ? $city->getCp() : '';

        $builder
            ->add('firstname', 'text', ['attr' => ['placeholder' => 'Prénom']])
            ->add('lastname', 'text', ['attr' => ['placeholder' => 'Nom']])
            ->add('description', 'textarea', [
                'attr' => ['placeholder' => 'Description ( 140 caractères )'],
                'required' => false])
            ->add('profession', 'model', [
                'class' => 'MMIBundle\Model\Profession',
                'query' => ProfessionQuery::create()->orderByName('ASC'),
                'choice_label' => 'name',
                'attr' => ['placeholder' => 'Profil'],
                'required' => false])
            ->add('promo', 'model', [
                'class' => 'MMIBundle\Model\Promo',
                'query' => PromoQuery::create()->orderByYear('DESC'),
                'choice_label' => 'year',
                'attr' => ['placeholder' => 'Promotion']
            ])
            ->add('city_id', 'text', [
                'attr' => ['placeholder' => 'Ville']])
            ->add('cp', 'text', [
                'attr' => [
                    'placeholder' => 'Code postal',
                    'value' => $cp
                ],
                'mapped' => false,
                'required' => false])
            ->add('birthday', 'text', [
                'attr' => ['placeholder' => 'Date de naissance']])
            ->add('image_profil_file', 'file', [
                'attr' => [
                    'placeholder' => 'Image de profil',
                    'accept' => 'image/*'],
                'mapped' => false,
                'required' => false])
            ->add('image_couverture_file', 'file', [
                'attr' => [
                    'placeholder' => 'Image de couverture',
                    'accept' => 'image/*'],
                'mapped' => false,
                'required' => false])
            ->add('url_portfolio', 'text', [
                'attr' => ['placeholder' => 'Portfolio'],
                'required' => false])
            ->add('url_blog', 'text', [
                'attr' => ['placeholder' => 'Blog'],
                'required' => false])
            ->add('url_facebook', 'text', [
                'attr' => ['placeholder' => 'Facebook'],
                'required' => false])
            ->add('url_twitter', 'text', [
                'attr' => ['placeholder' => 'Twitter'],
                'required' => false])
            ->add('url_linkedin', 'text', [
                'attr' => ['placeholder' => 'LinkedIn'],
                'required' => false])
            ->add('url_tumblr', 'text', [
                'attr' => ['placeholder' => 'Tumblr'],
                'required' => false])
            ->add('url_instagram', 'text', [
                'attr' => ['placeholder' => 'Instagram'],
                'required' => false])
            ->add('cv_file', 'file', [
                'attr' => [
                    'placeholder' => 'Téléchargez votre CV',
                    'accept' => 'application/pdf'],
                'mapped' => false,
                'required' => false])
            ->add('telephone', 'text', [
                'attr' => ['placeholder' => 'Votre téléphone'],
                'required' => false]);

        $builder->get('birthday')->addModelTransformer(new CallbackTransformer(
            function ($original) {
                if ($original != null) {
                    return $original->format('d/m/Y');
                }
                return date('d/m/Y', mktime(0, 0, 0, 1, 1, date('Y') - 18));
            },
            function ($submitted) {
                $d = explode('/', $submitted);
                return $d[2] . '-' . $d[1] . '-' . $d[0];
            }
        ));

        $builder->get('city_id')->addModelTransformer(new CallbackTransformer(
            function ($id) {
                return ($city = CityQuery::create()->findOneById($id)) != null ? $city->getName() : '';
            },
            function ($name) {
                $city = CityQuery::create()->findOneByName($name);
                if ($city == null) {
                    $city = new City();
                    $city->setName($name);
                    $city->save();
                }
                return $city->getId();
            }
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'MMIBundle\Model\User'
        ]);
    }

    public function getName()
    {
        return 'UserEditType';
    }
}