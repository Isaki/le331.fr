<?php

namespace MMIBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class ProfessionController extends BaseController
{
    public function indexAction()
    {
        return $this->render("MMIBundle:Profession:profession.html.twig");
    }

    public function add_professionAction()
    {
        return $this->render("MMIBundle:Profession:add_profession.html.twig");
    }

    public function edit_professionAction()
    {
        return $this->render("MMIBundle:Profession:edit_profession.html.twig");
    }
}
