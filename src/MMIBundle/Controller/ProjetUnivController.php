<?php

namespace MMIBundle\Controller;

use MMIBundle\Model\UnivProjectQuery;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class ProjetUnivController extends BaseController
{
    public function indexAction($id)
    {
        // Gestionnaire d'accès
        $response = $this->checkAccess();
        if ($response != null) return $this->redirectToRoute($response);

        $project = UnivProjectQuery::create()
            ->findOneById($id);
        $this->params['project'] = $project;

        return $this->render('MMIBundle:ProjetUniv:univ_project.html.twig', $this->params);
    }

    public function add_univ_projectAction()
    {
        return $this->render("MMIBundle:ProjetUniv:add_univ_project.html.twig");
    }

    public function edit_univ_projectAction()
    {
        return $this->render("MMIBundle:ProjetUniv:edit_univ_project.html.twig");
    }
}
