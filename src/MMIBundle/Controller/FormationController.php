<?php

namespace MMIBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class FormationController extends BaseController
{
    public function indexAction()
    {
        return $this->render("MMIBundle:Formation:training.html.twig");
    }

    public function add_trainingAction()
    {
        return $this->render("MMIBundle:Formation:add_training.html.twig");
    }

    public function edit_trainingAction()
    {
        return $this->render("MMIBundle:Formation:edit_training.html.twig");
    }
}
