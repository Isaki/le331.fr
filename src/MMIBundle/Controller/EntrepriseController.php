<?php

namespace MMIBundle\Controller;

use MMIBundle\Form\Type\CompanyLoginType;
use MMIBundle\Form\Type\CompanyRegisterType;
use MMIBundle\Form\Type\CompanyWelcomeType;
use MMIBundle\Model\Address;
use MMIBundle\Model\AddressQuery;
use MMIBundle\Model\City;
use MMIBundle\Model\CityQuery;
use MMIBundle\Model\Company;
use MMIBundle\Model\CompanyPeer;
use MMIBundle\Model\CompanyQuery;
use MMIBundle\Model\Validation;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class EntrepriseController extends BaseController
{
    public function indexAction(Request $request)
    {
        // Accès à la page
        $response = $this->checkAccess();
        if ($response != null) return $this->redirectToRoute($response);

        // Traitement du formulaire d'inscription
        $company = new Company();
        $form = $this->createForm(new CompanyRegisterType(), $company);

        // Tentative d'inscription
        if ($request->isMethod("POST")) {
            if ($request->request->has($form->getName())) {
                $form->handleRequest($request);

                if ($form->isValid()) {
                    $company = $form->getData();

                    if (CompanyPeer::companyExists($company->getEmail())) {
                        $this->params["result"] = "Cet email est déjà utilisé";
                    } else {
                        $company->setFirstConnection(0)->save();

                        $chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
                        $code = '';
                        for ($i = 0; $i < 16; $i++)
                            $code .= $chars[rand(0, strlen($chars) - 1)];

                        $validation = new Validation();
                        $validation->setType('validate_company')
                            ->setCompany($company)
                            ->setCode($code)
                            ->save();

                        $this->validate_company($company->getEmail(), $code);

                        $this->params["result"] = "Vous avez reçu un email pour activer votre compte.";
                    }
                }
            }
        }
        $this->params["inscription"] = $form->createView();

        // Traitement du formulaire de connexion
        $company = new Company();
        $form = $this->createForm(new CompanyLoginType(), $company);

        // Tentative de connexion
        if ($request->isMethod("POST")) {
            if ($request->request->has($form->getName())) {
                $form->handleRequest($request);

                if ($form->isValid()) {
                    if (!CompanyPeer::companyExists($company->getEmail())) {
                        $this->params["result"] = "Mauvais mot de passe";
                    } else {
                        $companyDb = CompanyPeer::getByEmail($company->getEmail());

                        if (CompanyPeer::isValidEmailPassword($companyDb, $company->getPassword())) {
                            if ($companyDb->getValidated()) {
                                $this->get("session")->set("Account", $companyDb);

                                $this->params["result"] = "Vous êtes connecté";

                                if ($companyDb->getFirstConnection() == 0)
                                    return $this->redirectToRoute("mmi_welcome_company");
                                else
                                    return $this->redirectToRoute("mmi_search");
                            } else {
                                $this->params['result'] = "Vous devez d'abord activer votre compte";
                            }
                        } else {
                            $this->params["result"] = "Mauvais mot de passe";
                        }
                    }
                }
            }
        }
        $this->params["connexion"] = $form->createView();

        if ($this->get("session")->has("Account"))
            $this->params["account"] = $this->get("session")->get("Account");

        return $this->render("MMIBundle:Default:pros.html.twig", $this->params);
    }

    public function welcomeAction(Request $request)
    {
        // Accès à la page
        $response = $this->checkAccess();
        if ($response != null) return $this->redirectToRoute($response);

        // Traitement du formulaire de changement d'email
        $company = $this->get("session")->get("Account");
        $form = $this->createForm(new CompanyWelcomeType(), $company);

        if ($request->isMethod("POST")) {
            if ($request->request->has($form->getName())) {
                $form->handleRequest($request);

                if ($form->isValid()) {
                    //$company->setFirstConnection(true);
                    $company->save();

                    $this->get("session")->set("Account", $company);

                    //return $this->redirectToRoute("mmi_search");
                }
            }
        }
        $this->params["welcome"] = $form->createView();

        $this->params["account"] = $this->get("session")->get("Account");

        return $this->render("MMIBundle:Entreprise:welcome_company.html.twig", $this->params);
    }

    public function companyAction($id)
    {
        // Accès à la page
        $response = $this->checkAccess();
        if ($response != null) return $this->redirectToRoute($response);

        $this->params['company'] = CompanyQuery::create()->findOneById($id);

        if ($this->get("session")->has("Account"))
            $this->params["account"] = $this->get("session")->get("Account");

        return $this->render("MMIBundle:Entreprise:company.html.twig", $this->params);
    }

    public function edit_companyAction()
    {
        // Accès à la page
        $response = $this->checkAccess();
        if ($response != null) return $this->redirectToRoute($response);

        return $this->render("MMIBundle:Entreprise:edit_company.html.twig", $this->params);
    }

    private function validate_company($email, $code)
    {
        $headers = 'From: contact@le331.fr' . "\n";
        $headers .= 'Reply-To: contact@le331.fr' . "\n";
        $headers .= 'Content-Type: text/html; charset="utf8"' . "\n";
        $headers .= 'Content-Transfer-Encoding: 8bit';
        mail($email,
            'Activation de votre compte sur le331.fr',
            $this->renderView(
                'MMIBundle:Email:validate_account.html.twig',
                ['link' => 'http://le331.fr' . $this->generateUrl('mmi_validate', ['code' => $code])]),
            $headers);
    }
}
