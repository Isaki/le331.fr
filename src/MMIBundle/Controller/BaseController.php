<?php

namespace MMIBundle\Controller;

use MMIBundle\Model\CompanyPeer;
use MMIBundle\Model\ItemmenuQuery;
use MMIBundle\Model\PagePeer;
use MMIBundle\Model\UserPeer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BaseController extends Controller
{
    protected $menuItems = null;
    protected $params = [];

    public function checkAccess()
    {
        // la route de la page courante
        $request = $this->container->get('request');
        $route = $request->get('_route');
        $default = ['User' => 'mmi_homepage', 'Company' => 'mmi_pros'];
        $allowToAnyone = ['mmi_homepage', 'mmi_pros'];

        // récupération des items du menu
        $connected = $this->get('session')->has('Account');
        $account = null;
        $type = null;
        if ($connected) {
            $account = $this->get('session')->get('Account');
            $type = $this->get_class($account);
        }
        $items = UserPeer::getMenuItems(null);

        // is menu black ?
        $this->params['menu_color'] = PagePeer::isMenuBlack($route) ? 'dark' : '';

        if ($type == 'User') {
            $items = UserPeer::getMenuItems($account->getId());
        } elseif ($type == 'Company') {
            $items = CompanyPeer::getMenuItems();
        }

        foreach ($items as $item) {
            $data = [];
            $page = $item->getPage();

            if ($page->getRoute() == 'mmi_student')
                $data['path'] = $this->generateUrl($page->getRoute(), ['id' => $account->getId()]);
            else if ($page->getRoute() == 'mmi_company')
                $data['path'] = $this->generateUrl($page->getRoute(), ['id' => $account->getId()]);
            else
                $data['path'] = preg_match('/mmi_/', $page->getRoute()) != null ? $this->generateUrl($page->getRoute()) : $page->getRoute();

            $data['name'] = $page->getName();
            $data['active'] = $page->getRoute() == $route ? 'blue' : '';

            $this->menuItems[] = $data;
        }

        $this->params['menu_items'] = $this->menuItems;

        // gestionnaire d'accès
        $response = null;

        if (!$connected) {
            // si non connecté
            if (!in_array($route, $allowToAnyone)) $response = $default['User'];
        } else {
            switch ($type) {
                case 'User':
                    // si pas admin
                    if (!UserPeer::isAdmin($account->getId())) {
                        if (!in_array($route, UserPeer::getPages($account->getId())))
                            $response = $default[$type];
                    }
                    break;

                case 'Company':
                    if (!in_array($route, CompanyPeer::getPages()))
                        $response = $default[$type];
                    break;
            }

            // si on s'est déjà connecté une fois
            if (in_array($route, ['mmi_welcome_student', 'mmi_welcome_company'])) {
                if ($account->getFirstConnection() == 1) $response = 'mmi_search';
            }
        }
        return $response;
    }

    public function get_class($object)
    {
        $p = explode('\\', get_class($object));
        return $p[count($p) - 1];
    }
}
