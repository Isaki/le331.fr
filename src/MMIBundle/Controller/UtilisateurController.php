<?php

namespace MMIBundle\Controller;

use MMIBundle\Form\Type\CompanyAddType;
use MMIBundle\Form\Type\DegreeAddType;
use MMIBundle\Form\Type\ProfessionAddType;
use MMIBundle\Form\Type\ProjectAddType;
use MMIBundle\Form\Type\UserAddDegreeType;
use MMIBundle\Form\Type\UserAddJobType;
use MMIBundle\Form\Type\UserAddProjectType;
use MMIBundle\Form\Type\UserEditType;
use MMIBundle\Form\Type\UserWelcomeType;
use MMIBundle\Model\CityQuery;
use MMIBundle\Model\Company;
use MMIBundle\Model\Degree;
use MMIBundle\Model\Profession;
use MMIBundle\Model\ProfessionQuery;
use MMIBundle\Model\PromoQuery;
use MMIBundle\Model\RoleQuery;
use MMIBundle\Model\UnivProject;
use MMIBundle\Model\UserJob;
use MMIBundle\Model\UserJobQuery;
use MMIBundle\Model\UserMemberUnipro;
use MMIBundle\Model\UserMemberUniproQuery;
use MMIBundle\Model\UserObtainedDegree;
use MMIBundle\Model\UserObtainedDegreeQuery;
use MMIBundle\Model\UserPeer;
use MMIBundle\Model\UserQuery;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use MMIBundle\Model\RolePeer;

class UtilisateurController extends BaseController
{
    public function welcomeAction(Request $request)
    {
        // Accès à la page
        $response = $this->checkAccess();
        if ($response != null) return $this->redirectToRoute($response);

        // Traitement du formulaire de changement d'email
        $user = $this->get("session")->get("Account");
        $form = $this->createForm(new UserWelcomeType(), $user);

        if ($request->isMethod("POST")) {
            if ($request->request->has($form->getName())) {
                $form->handleRequest($request);

                if ($form->isValid()) {
                    $file = $form->get("cv_file")->getData();
                    $promo = $form->get("promo_id")->getData();

                    if ($file != null) {
                        if ($file->isValid() == true) {
                            $basePath = empty($request->getBasePath()) ? "/web" : trim($request->getBasePath(), '/');
                            $dossier = $request->server->get("DOCUMENT_ROOT")
                                . $basePath . DIRECTORY_SEPARATOR . "cv";

                            $nouveauNom = md5(uniqid()) . "." . $file->guessExtension();

                            $nomEntier = $dossier . DIRECTORY_SEPARATOR . $nouveauNom;

                            $file->move($dossier, $nomEntier);

                            $user->setCvFile($nouveauNom);
                        }
                    }

                    if ($promo == null) {
                        // si prof
                        $user->addRole(RoleQuery::create()->findOneById(6));
                    } else {
                        // étudiant
                        $user->addRole(RoleQuery::create()->findOneById(2));
                        $year = PromoQuery::create()->findOneById($promo);

                        // si étudiant actuel
                        if ($year->getYear() >= date('Y', time())) {
                            $user->addRole(RoleQuery::create()->findOneById(3));
                        } // si ancien étudiant
                        else {
                            $user->addRole(RoleQuery::create()->findOneById(4));
                        }
                    }

                    $user->setFirstConnection(1);
                    $user->save();

                    $this->get("session")->set("Account", $user);

                    return $this->redirectToRoute("mmi_search");
                }
            }
        }
        $this->params["welcome"] = $form->createView();
        $this->params["account"] = $this->get("session")->get("Account");

        return $this->render("MMIBundle:Utilisateur:welcome_student.html.twig", $this->params);
    }

    public function studentAction($id, Request $request)
    {
        // Accès à la page
        $response = $this->checkAccess();
        if ($response != null) return $this->redirectToRoute($response);

        // les formulaires
        $userJob = new UserJob();
        $formUserJob = $this->createForm(new UserAddJobType(), $userJob);

        $userDegree = new UserObtainedDegree();
        $formUserDegree = $this->createForm(new UserAddDegreeType(), $userDegree);

        $userProject = new UserMemberUnipro();
        $formUserProject = $this->createForm(new UserAddProjectType(), $userProject);

        // ajouts simples
        $profession = new Profession();
        $formProfession = $this->createForm(new ProfessionAddType(), $profession);

        $company = new Company();
        $formCompany = $this->createForm(new CompanyAddType(), $company);

        $degree = new Degree();
        $formDegree = $this->createForm(new DegreeAddType(), $degree);

        $project = new UnivProject();
        $formProject = $this->createForm(new ProjectAddType(), $project);

        // la session
        $account = $this->get("session")->get("Account");

        // traitement des formulaires
        if ($request->isMethod("POST")) {
            // UserJob
            if ($request->request->has($formUserJob->getName())) {
                $formUserJob->handleRequest($request);

                if ($formUserJob->isValid()) {
                    $id = $formUserJob->get("job_id")->getData();

                    if ($id != null) {
                        $userJob = UserJobQuery::create()->findOneById($id);
                        $userJob->setCompanyId($formUserJob->get("company_id")->getData())
                            ->setProfessionId($formUserJob->get("profession_id")->getData())
                            ->setYearFrom($formUserJob->get("yearFrom")->getData())
                            ->setYearTo($formUserJob->get("yearTo")->getData())
                            ->setDescription($formUserJob->get("description")->getData());
                    }
                    $userJob->setUser($account);
                    $userJob->save();

                    return $this->redirect($this->generateUrl("mmi_student", ['id' => $account->getId()]));
                }
            } // UserDegree
            else if ($request->request->has($formUserDegree->getName())) {
                $formUserDegree->handleRequest($request);

                if ($formUserDegree->isValid()) {
                    $id = $formUserDegree->get("training_id")->getData();

                    if ($id != null) {
                        $userDegree = UserObtainedDegreeQuery::create()->findOneById($id);
                        $userDegree->setDegreeId($formUserDegree->get("degree_id")->getData())
                            ->setYearFrom($formUserDegree->get("yearFrom")->getData())
                            ->setYearTo($formUserDegree->get("yearTo")->getData());
                    }
                    $userDegree->setUser($account);
                    $userDegree->save();

                    return $this->redirect($this->generateUrl("mmi_student", ['id' => $account->getId()]));
                }
            } // UserProject
            else if ($request->request->has($formUserProject->getName())) {
                $formUserProject->handleRequest($request);

                if ($formUserProject->isValid()) {
                    $id = $formUserProject->get("project_id")->getData();

                    if ($id != null) {
                        $userProject = UserMemberUniproQuery::create()->findOneById($id);
                        $userProject->setUniproId($formUserProject->get("unipro_id")->getData())
                            ->setProfessionId($formUserProject->get("profession_id")->getData())
                            ->setYearFrom($formUserProject->get("yearFrom")->getData())
                            ->setYearTo($formUserProject->get("yearTo")->getData());
                    }
                    $userProject->setUser($account);
                    $userProject->save();

                    return $this->redirect($this->generateUrl("mmi_student", ['id' => $account->getId()]));
                }
            } // Profession
            else if ($request->request->has($formProfession->getName())) {
                $formProfession->handleRequest($request);

                if ($formProfession->isValid()) {
                    $profession->save();

                    return $this->redirect($this->generateUrl("mmi_student", ['id' => $account->getId()]));
                }
            } // Company
            else if ($request->request->has($formCompany->getName())) {
                $formCompany->handleRequest($request);

                if ($formCompany->isValid()) {
                    $file = $formCompany->get("logo_file")->getData();

                    if ($file != null) {
                        if ($file->isValid() == true) {
                            $dossier = $request->server->get("DOCUMENT_ROOT")
                                . $request->getBasePath() . DIRECTORY_SEPARATOR . "images/company";

                            $nouveauNom = md5(uniqid()) . "." . $file->guessExtension();

                            $nomEntier = $dossier . DIRECTORY_SEPARATOR . $nouveauNom;

                            $file->move($dossier, $nomEntier);

                            $company->setLogoFile($nouveauNom);
                        }
                    }

                    $company->save();

                    return $this->redirect($this->generateUrl("mmi_student", ['id' => $account->getId()]));
                }
            } // Degree
            else if ($request->request->has($formDegree->getName())) {
                $formDegree->handleRequest($request);

                if ($formDegree->isValid()) {
                    $degree->save();

                    return $this->redirect($this->generateUrl("mmi_student", ['id' => $account->getId()]));
                }
            } // Project
            else if ($request->request->has($formProject->getName())) {
                $formProject->handleRequest($request);

                if ($formProject->isValid()) {
                    $project->setUser($account);
                    $project->save();

                    return $this->redirect($this->generateUrl("mmi_student", ['id' => $account->getId()]));
                }
            }
        }
        $this->params["addUserJob"] = $formUserJob->createView();
        $this->params["addUserDegree"] = $formUserDegree->createView();
        $this->params["addUserProject"] = $formUserProject->createView();
        $this->params["addProfession"] = $formProfession->createView();
        $this->params["addCompany"] = $formCompany->createView();
        $this->params["addDegree"] = $formDegree->createView();
        $this->params["addProject"] = $formProject->createView();

        // Informations à afficher
        $student = UserQuery::create()
            ->filterById($id)
            ->findOne();
        $student->getCity();
        $student->getPromo();

        $experiences = UserPeer::getExperiences($id);
        $before = [];
        $during = [];
        $after = [];
        foreach ($experiences as $exp) {
            // after
            if ($exp['from'] >= $student->getPromo()->getYear()) {
                $after[] = $exp;
            } // during
            elseif ($exp['from'] >= $student->getPromo()->getYear() - 2 &&
                $exp['to'] <= $student->getPromo()->getYear()
            ) {
                $during[] = $exp;
            } // before
            elseif ($exp['from'] < $student->getPromo()->getYear() - 2) {
                $before[] = $exp;
            }
        }

        $this->params['experiences'] = $experiences;
        $this->params['after'] = $after;
        $this->params['during'] = $during;
        $this->params["before"] = $before;
        $this->params['student'] = $student;
        $this->params["account"] = $account;

        return $this->render("MMIBundle:Utilisateur:student.html.twig", $this->params);
    }

    public function edit_studentAction(Request $request)
    {
        // Accès à la page
        $response = $this->checkAccess();
        if ($response != null) return $this->redirectToRoute($response);

        $this->params['listPromos'] = PromoQuery::create()->orderByYear('ASC')->find();
        $this->params['listProfession'] = ProfessionQuery::create()->orderByName('ASC')->find();

        // Traitement du formulaire d'édition du profil utilisateur
        $user = $this->get("session")->get("Account");
        $form = $this->createForm(new UserEditType(), $user);

        if ($request->isMethod("POST")) {
            if ($request->request->has($form->getName())) {
                $form->handleRequest($request);

                if ($form->isValid()) {
                    $cv_file = $form->get("cv_file")->getData();
                    $image_profil_file = $form->get("image_profil_file")->getData();
                    $image_couverture_file = $form->get("image_couverture_file")->getData();

                    if ($cv_file != null) {
                        if ($cv_file->isValid() == true) {
                            $basePath = empty($request->getBasePath()) ? "/web" : trim($request->getBasePath(), '/');
                            $dossier = $request->server->get("DOCUMENT_ROOT")
                                . $basePath . DIRECTORY_SEPARATOR . "cv";

                            $nouveauNom = md5(uniqid()) . "." . $cv_file->guessExtension();

                            $nomEntier = $dossier . DIRECTORY_SEPARATOR . $nouveauNom;

                            $cv_file->move($dossier, $nomEntier);

                            $user->setCvFile($nouveauNom);
                        }
                    }

                    if ($image_profil_file != null) {
                        if ($image_profil_file->isValid() == true) {
                            $basePath = empty($request->getBasePath()) ? "/web" : trim($request->getBasePath(), '/');
                            $dossier = $request->server->get("DOCUMENT_ROOT")
                                . $basePath . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . "profil_picture";

                            $nouveauNom = md5(uniqid()) . "." . $image_profil_file->guessExtension();

                            $nomEntier = $dossier . DIRECTORY_SEPARATOR . $nouveauNom;

                            $image_profil_file->move($dossier, $nomEntier);

                            $user->setImageProfilFile($nouveauNom);
                        }
                    }

                    if ($image_couverture_file != null) {
                        if ($image_couverture_file->isValid() == true) {
                            $basePath = empty($request->getBasePath()) ? "/web" : trim($request->getBasePath(), '/');
                            $dossier = $request->server->get("DOCUMENT_ROOT")
                                . $basePath . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . "profil_cover";

                            $nouveauNom = md5(uniqid()) . "." . $image_couverture_file->guessExtension();

                            $nomEntier = $dossier . DIRECTORY_SEPARATOR . $nouveauNom;

                            $image_couverture_file->move($dossier, $nomEntier);

                            $user->setImageCouvFile($nouveauNom);
                        }
                    }

                    $city_id = $form->get('city_id')->getData();
                    $cp = $form->get('cp')->getData();

                    $city = CityQuery::create()
                        ->filterById($city_id)
                        ->findOne();
                    $city->setCp($cp);
                    $city->save();

                    $user->save();
                    $this->get("session")->set("Account", $user);

                    //return $this->redirect($this->generateUrl('mmi_student', ['id' => $user->getId()]));
                }
            }
        }
        $this->params["edit"] = $form->createView();
        $this->params["account"] = $this->get('session')->get('Account');

        return $this->render("MMIBundle:Utilisateur:edit_student.html.twig", $this->params);
    }
}
