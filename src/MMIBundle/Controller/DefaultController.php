<?php

namespace MMIBundle\Controller;

use MMIBundle\Form\Type\ToggleNewsletterType;
use MMIBundle\Form\Type\UserDeleteType;
use MMIBundle\Form\Type\UserLoginType;
use MMIBundle\Form\Type\UserModifyEmailType;
use MMIBundle\Form\Type\UserModifyPasswordType;
use MMIBundle\Form\Type\UserRegisterType;
use MMIBundle\Model\CityQuery;
use MMIBundle\Model\CompanyQuery;
use MMIBundle\Model\DegreeQuery;
use MMIBundle\Model\IndustryQuery;
use MMIBundle\Model\InternshipQuery;
use MMIBundle\Model\ProfessionQuery;
use MMIBundle\Model\PromoQuery;
use MMIBundle\Model\RoleQuery;
use MMIBundle\Model\UnivProjectQuery;
use MMIBundle\Model\User;
use MMIBundle\Model\UserPeer;
use MMIBundle\Model\UserQuery;
use MMIBundle\Model\Validation;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class DefaultController extends BaseController
{
    public function indexAction(Request $request)
    {
        // afficher msg d'erreur si existe
        if ($request->query->get('msg') != null) {
            // msg d'erreur si utilisateur doit se connecter pour accéder à une page
            if ($request->query->get('msg') == 'pleaseconnect') ;
            $this->params['result'] = "Veuillez vous connecter.";
        }

        // Gestionnaire d'accès
        $response = $this->checkAccess();
        if ($response != null) return $this->redirectToRoute($response);

        // Traitement du formulaire d'inscription
        $user = new User();
        $form = $this->createForm(new UserRegisterType(), $user);

        // Tentative d'inscription
        if ($request->isMethod("POST")) {
            if ($request->request->has($form->getName())) {
                $form->handleRequest($request);

                if ($form->isValid()) {
                    $user = $form->getData();

                    if (preg_match('/univ-fcomte/', $user->getEmail()) != null) {
                        $this->params["result"] = "Vous ne pouvez pas utiliser votre adresse universitaire";
                    } else {
                        if (UserPeer::userExists($user->getEmail())) {
                            $this->params["result"] = "Cet email est déjà utilisé";
                        } else {
                            $user->setFirstConnection(0)
                                ->addRole(RoleQuery::create()->findOneById(1))
                                ->save();

                            $chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
                            $code = '';
                            for ($i = 0; $i < 16; $i++)
                                $code .= $chars[rand(0, strlen($chars) - 1)];

                            $validation = new Validation();
                            $validation->setType('validate_user')
                                ->setUser($user)
                                ->setCode($code)
                                ->save();

                            $this->validate_user($user->getEmail(), $code);

                            $this->params["result"] = "Vous avez reçu un email pour activer votre compte.";
                        }
                    }
                }
            }
        }
        $this->params["register"] = $form->createView();

        // Traitement du formulaire de connexion
        $user = new User();
        $form = $this->createForm(new UserLoginType(), $user);

        // Tentative de connexion
        if ($request->isMethod("POST")) {
            if ($request->request->has($form->getName())) {
                $form->handleRequest($request);

                if ($form->isValid()) {
                    if (!UserPeer::userExists($user->getEmail())) {
                        $this->params["result"] = "Mauvais mot de passe";
                    } else {
                        $userDb = UserPeer::getByEmail($user->getEmail());

                        if (UserPeer::isValidEmailPassword($userDb, $user->getPassword())) {
                            if ($userDb->getValidated()) {
                                $this->get('session')->set('Account', $userDb);

                                $this->params["result"] = "Vous êtes connecté";

                                if ($userDb->getFirstConnection() == 0)
                                    return $this->redirectToRoute("mmi_welcome_student");
                                else
                                    return $this->redirectToRoute("mmi_search");
                            } else {
                                $this->params['result'] = "Vous devez d'abord activer votre compte";
                            }
                        } else {
                            $this->params["result"] = "Mauvais mot de passe";
                        }
                    }
                }
            }
        }
        $this->params["connection"] = $form->createView();

        if ($this->get("session")->has("Account")) $this->params["account"] = $this->get("session")->get("Account");

        return $this->render("MMIBundle:Default:index.html.twig", $this->params);
    }

    public function deconnexionAction()
    {
        $route = "mmi_homepage";
        if ($this->get_class($this->get("session")->get("Account")) == "Company") $route = "mmi_pros";

        $session = new Session();
        $session->clear();

        return $this->redirectToRoute($route);
    }

    public function blogAction()
    {
        return $this->redirect('http://le331.fr/blog');
    }

    public function add_univ_projectAction()
    {
        // Gestionnaire d'accès
        $response = $this->checkAccess();
        if ($response != null) return $this->redirectToRoute($response);

        return $this->render("MMIBundle:Default:add_univ_project.html.twig");
    }

    public function accountAction(Request $request)
    {
        // Accès à la page
        $response = $this->checkAccess();
        if ($response != null) return $this->redirectToRoute($response);

        // Traitement du formulaire de changement d'email
        $form = $this->createForm(new UserModifyEmailType());

        if ($request->isMethod("POST")) {
            if ($request->request->has($form->getName())) {
                $form->handleRequest($request);

                if ($form->isValid()) {
                    $user = $this->get("session")->get("Account");
                    $email = $form->get("email")->getData();

                    $user->setEmail($email);
                    $user->save();

                    //var_dump(\Propel::getConnection()->getLastExecutedQuery());

                    $this->get("session")->set("Account", $user);
                    $this->params["result"] = "Vous avez reçu un email";
                }
            }
        }
        $this->params["modifyEmail"] = $form->createView();


        // Traitement du formulaire de changement de mot de passe
        $form = $this->createForm(new UserModifyPasswordType());

        if ($request->isMethod("POST")) {
            if ($request->request->has($form->getName())) {
                $form->handleRequest($request);

                if ($form->isValid()) {
                    $pass1 = sha1($form->get("pass1")->getData());
                    $pass2 = sha1($form->get("pass2")->getData());
                    $pass3 = sha1($form->get("pass3")->getData());

                    $user = $this->get("session")->get("Account");

                    if ($pass1 != $user->getPassword()) {
                        $this->params["result"] = "Ce n'est pas votre mot de passe";
                    } else if ($pass2 != $pass3) {
                        $this->params["result"] = "Les mots de passe ne sont pas les mêmes";
                    } else {
                        $user->setPassword($pass2);
                        $user->save();

                        $this->get("session")->set("Account", $user);
                        $this->params["result"] = "Vous avez reçu un email";
                    }
                }
            }
        }
        $this->params["modifyPassword"] = $form->createView();


        // Traitement du formulaire d'abonnement à la newsletter
        $user = $this->get("session")->get("Account");
        $form = $this->createForm(new ToggleNewsletterType(), null, array("user" => $user));

        if ($request->isMethod("POST")) {
            if ($request->request->has($form->getName())) {
                $form->handleRequest($request);

                if ($form->isValid()) {
                    if ($form->has("subscribeNewsletter")) $user->setNewsletter(1);
                    else $user->setNewsletter(0);
                    $user->save();

                    $this->params["result"] = $user->getNewsletter() ? 'Vous recevrez désormais notre newsletter' : 'Vous ne recevrez plus notre newsletter';
                    $this->get("session")->set("Utilisateur", $user);

                    return $this->redirectToRoute("mmi_account");
                }
            }
        }
        $this->params["toggleNewsletter"] = $form->createView();


        // Traitement du formulaire de suppression du compte
        $form = $this->createForm(new UserDeleteType());

        if ($request->isMethod("POST")) {
            if ($request->request->has($form->getName())) {
                $form->handleRequest($request);

                if ($form->isValid()) {
                    $user = $this->get("session")->get("Account");
                    $password = sha1($form->get("password")->getData());

                    if ($password != $user->getPassword()) {
                        $this->params["result"] = "Ce n'est pas votre mot de passe";
                    } else {
                        $user->delete();
                        return $this->redirectToRoute("mmi_deconnexion");
                    }
                }
            }
        }
        $this->params["deleteUser"] = $form->createView();

        return $this->render("MMIBundle:Default:account.html.twig", $this->params);
    }

    public function searchAction($type)
    {
        // Gestionnaire d'accès
        $response = $this->checkAccess();
        if ($response != null) return $this->redirectToRoute($response);

        $this->params['search_type'] = $type;
        $this->params['data_list'] = null;

        switch ($type) {
            case 'students':
                $students = UserQuery::create()
                    ->orderByLastname('ASC')
                    ->orderByFirstname('ASC')
                    ->find();
                foreach ($students as $s) {
                    //$s->getPromos();
                    $s->getProfession();
                }
                $this->params['data_list'] = $students;
                break;

            case 'companies':
                $this->params['data_list'] = CompanyQuery::create()
                    ->orderByName('ASC')
                    ->find();
                break;

            case 'projects':
                $this->params['data_list'] = UnivProjectQuery::create()
                    ->orderByName('ASC')
                    ->find();
                break;

            case 'internships':
                $this->params['data_list'] = InternshipQuery::create()
                    ->orderByInternshipBegining('ASC')
                    ->find();
                break;

            case 'degrees':
                $this->params['data_list'] = DegreeQuery::create()
                    ->orderByName()
                    ->find();
                break;

            case 'professions':
                $this->params['data_list'] = ProfessionQuery::create()
                    ->orderByName()
                    ->find();
                break;

            default:
                return $this->redirectToRoute('mmi_search');
                break;
        }

        $this->params['listCity'] = CityQuery::create()->orderByName('ASC')->find();
        $this->params['listPromos'] = PromoQuery::create()->orderByYear('DESC')->find();
        $this->params['listProfession'] = ProfessionQuery::create()->orderByName('ASC')->find();
        $this->params['listIndustry'] = IndustryQuery::create()->orderByName('ASC')->find();

        return $this->render("MMIBundle:Default:search.html.twig", $this->params);
    }

    private function validate_user($email, $code)
    {
        $headers = 'From: contact@le331.fr' . "\n";
        $headers .= 'Reply-To: contact@le331.fr' . "\n";
        $headers .= 'Content-Type: text/html; charset="utf8"' . "\n";
        $headers .= 'Content-Transfer-Encoding: 8bit';
        mail($email,
            'Activation de votre compte sur le331.fr',
            $this->renderView(
                'MMIBundle:Email:validate_account.html.twig',
                ['link' => 'http://le331.fr' . $this->generateUrl('mmi_validate', ['code' => $code])]),
            $headers);
    }
}
