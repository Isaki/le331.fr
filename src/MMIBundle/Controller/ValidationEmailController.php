<?php

namespace MMIBundle\Controller;

use MMIBundle\Model\CompanyQuery;
use MMIBundle\Model\UserQuery;
use MMIBundle\Model\ValidationQuery;

class ValidationEmailController extends BaseController
{
    public function validateAction($code)
    {
        $validation = ValidationQuery::create()->findOneByCode($code);

        if ($validation != null) {
            switch ($validation->getType()) {
                case 'validate_user':
                    $user = UserQuery::create()->findOneById($validation->getUserId());

                    if ($user != null) {
                        $user->setValidated(true)->save();
                        $validation->delete();
                    }
                    return $this->redirectToRoute('mmi_homepage');
                    break;

                case 'validate_company':
                    $company = CompanyQuery::create()->findOneById($validation->getCompanyId());

                    if ($company != null) {
                        $company->setValidated(true)->save();
                        $validation->delete();
                    }
                    return $this->redirectToRoute('mmi_pros');
                    break;
            }
        }
    }
}
