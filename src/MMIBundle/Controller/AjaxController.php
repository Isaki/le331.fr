<?php

namespace MMIBundle\Controller;

use MMIBundle\Model\CityPeer;
use MMIBundle\Model\CityQuery;
use MMIBundle\Model\Company;
use MMIBundle\Model\CompanyQuery;
use MMIBundle\Model\DegreeQuery;
use MMIBundle\Model\ProfessionQuery;
use MMIBundle\Model\RolePeer;
use MMIBundle\Model\RoleQuery;
use MMIBundle\Model\UnivProjectQuery;
use MMIBundle\Model\UserJobQuery;
use MMIBundle\Model\UserMemberUnipro;
use MMIBundle\Model\UserMemberUniproQuery;
use MMIBundle\Model\UserObtainedDegreeQuery;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;

class AjaxController extends BaseController
{
    public function getListByTypeAction($type)
    {
        $ar = [];

        switch ($type) {
            case 'City':
                $cities = CityQuery::create()->find();
                foreach ($cities as $city)
                    if ($city->getName() != null) $ar[] = $city->getName();
                break;

            case 'Company':
                $companies = CompanyQuery::create()->find();
                foreach ($companies as $company) $ar[] = $company->getName();
                break;

            case 'Profession':
                $professions = ProfessionQuery::create()->find();
                foreach ($professions as $profession) $ar[] = $profession->getName();
                break;

            case 'Degree':
                $degrees = DegreeQuery::create()->find();
                foreach ($degrees as $degree) {
                    $result = $degree->getName();
                    if ($degree->getOption() != null) $result .= " option " . $degree->getOption();
                    $ar[] = $result;
                }
                break;

            case 'Project':
                $projects = UnivProjectQuery::create()->find();
                foreach ($projects as $project) $ar[] = $project->getName();
                break;

            case 'Tutor':
                $tutors = RolePeer::getUsersById(3);
                foreach ($tutors as $tutor) $ar[] = $tutor->firstName();
                break;

            default:
                $ar[] = 'error';
        }

        $response = new JsonResponse();
        return $response->setData(["list" => $ar]);
    }

    public function getExperienceAction($type, $id)
    {
        $ar = [];

        switch ($type) {
            case 'Job':
                $job = UserJobQuery::create()->findOneById($id);
                $ar['id'] = $job->getId();
                $ar['profession'] = $job->getProfession()->getName();
                $ar['company'] = $job->getCompany()->getName();
                $ar['description'] = $job->getDescription();
                $ar['yearFrom'] = $job->getYearFrom();
                $ar['yearTo'] = $job->getYearTo();
                break;

            case 'Degree':
                $degree = UserObtainedDegreeQuery::create()->findOneById($id);
                $d = DegreeQuery::create()->findOneById($degree->getDegreeId());
                $ar['id'] = $degree->getId();
                $ar['degree'] = $d->getName() != null ? $d->getName() . ' option ' . $d->getOption() : $d->getName();
                $ar['yearFrom'] = $degree->getYearFrom();
                $ar['yearTo'] = $degree->getYearTo();
                break;

            case 'Project':
                $project = UserMemberUniproQuery::create()->findOneById($id);
                $ar['id'] = $project->getId();
                $ar['project'] = $project->getUnivProject()->getName();
                $ar['role'] = $project->getProfession()->getName();
                $ar['yearFrom'] = $project->getYearFrom();
                $ar['yearTo'] = $project->getYearTo();
                break;
        }

        $response = new JsonResponse();
        return $response->setData($ar);
    }

    public function deleteExperienceAction($type, $id)
    {
        $val = null;

        switch ($type) {
            case 'Job':
                $job = UserJobQuery::create()->findOneById($id);
                $job->delete();
                $val = 'ok';
                break;

            case 'Degree':
                $degree = UserObtainedDegreeQuery::create()->findOneById($id);
                $degree->delete();
                $val = 'ok';
                break;

            case 'Project':
                $project = UserMemberUniproQuery::create()->findOneById($id);
                $project->delete();
                $val = 'ok';
                break;

            default:
                $val = 'error';
        }

        $response = new JsonResponse();
        return $response->setData(['result' => $val]);
    }
}
