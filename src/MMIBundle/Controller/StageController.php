<?php

namespace MMIBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use MMIBundle\Model\InternshipQuery;
use MMIBundle\Model\Internship;
use MMIBundle\Model\ConfigQuery;
use MMIBundle\Form\Type\BeforeInternshipType;
use MMIBundle\Form\Type\StagesEtablisType;



class StageController extends BaseController
{
    public function myInternshipAction(Request $request)
    {
        // On récupère date de début et de fin de stage de l'année courante
        // dans la table de configuration de l'admin

        $config = ConfigQuery::create()
            ->findOneById(1);

        $begining = $config->getInternshipBegining();
        $ending = $config->getInternshipEnding();

        // on indique si on est avant, pendant ou après le stage
        // en fonction du datetime courant

        $current_datetime = new \DateTime(date('Y-m-d h:i:s a', time()));

        $state = null;

        if($current_datetime < $begining){
            $state = 'before';
        }elseif(($begining < $current_datetime) && ($current_datetime < $ending)){
            $state = 'during';
        }elseif($ending < $current_datetime){
            $state = 'after';
        }

        // si on est avant le date de début
        // on affiche la section 'avant'

        // On vérifie si l'utilisateur est connecté (externalisé)
        $response = $this->checkAccess();
        if ($response != null) return $this->redirectToRoute($response);

        // L'utilisateur est connecté, on stocke ses infos en session
        $connectedUser = $this->get('session')->get('Account');
        // On stocke son id dans une variable pour y accéder plus rapidement
        $connectedUserId = $connectedUser->getId();

        if($state == 'before'){

            $this->params['etape'] = 'avant';

            // On essaye de récupérer son stage
            $stage = InternshipQuery::create()->findOneByUserId($connectedUserId);

            $result = null;

            // Si aucun stage n'a jamais été créé on crée un nouvel objet stage
            if($stage == null){
                $stage = new Internship;
            }elseif($stage->getValide() == 1){

                // Si le stage a été validé on affiche un message de confirmation à la place du formulaire
                $result = 'Mme Gil a validé ton stage, Élodie éditera prochainement ta convention.';

                if($stage->getEtabli() == 1){

                    // Si la convention a été établie on affiche un message de confirmation à la place du formulaire
                    $result = 'Élodie a édité ta convention, elle t\'attend au secrétariat !';

                }

                $this->params['result'] = $result;
                return $this->render("MMIBundle:Stage:my_internship.html.twig", $this->params);

            }elseif($stage->getValide() == 2){

                // Si le stage a été refusé on affiche un message d'alerte
                $result = 'Mme Gil a refusé ton stage, nous t\' invitons à aller la voir.';
                $this->params['result'] = $result;
                return $this->render("MMIBundle:Stage:my_internship.html.twig", $this->params);
            }else{

                // Autrement on affiche un message de confirmation à la place du formulaire
                $result = 'Mme Gil a bien reçu ta demande, elle la validera bientôt.';
                $this->params['result'] = $result;
                return $this->render("MMIBundle:Stage:my_internship.html.twig", $this->params);
            }

            // On crée le formulaire de l'étape "before"
            // On l'associe au modèle de formulaire correspondant
            // Et on l'associe à l'objet stage créé précédement

            $beforeForm = $this->createForm(new beforeInternshipType(), $stage);


            // On essaye de récupérer les données du éventuelle requête POST

            $beforeForm->handleRequest($request);

            // Si un formulaire a été soumis et qu'il est valide

            if ($beforeForm->isSubmitted() && $beforeForm->isValid()) {

                // Si c'est le formulaire "before"

                if($beforeForm->getName() == 'beforeInternship'){

                    // On affecte manuellement l'id utilisateur

                    $stage->setUserId($connectedUserId);

                    // On affecte manuellement les dates de début et fin de stage de la table config

                    $stage->setInternshipBegining($begining);
                    $stage->setInternshipEnding($ending);

                    // On sauvegarde l'objet stage dans la BDD

                    $stage->save();

                    // On envoie un message de configuration à la vue

                    $result = 'Mme Gil a bien reçu ta demande, elle la validera bientôt.';
                    $this->params['result'] = $result;

                    // On appelle la vue avec un message de confirmation
                    return $this->render("MMIBundle:Stage:my_internship.html.twig", $this->params);
                }

            }

            // On affiche le formulaire
            $this->params['beforeForm'] = $beforeForm->createView();
            $this->params['result'] = $result;
            return $this->render("MMIBundle:Stage:my_internship.html.twig", $this->params);


        }elseif($state == 'during'){

            // si on est entre la date de début et de fin
            // on affiche la section 'pendant'

            $internship = InternshipQuery::create()
                ->filterByUserId($connectedUserId)
                ->findOne();

            $dateB = $internship->getInternshipBegining();
            $dateE = $internship->getInternshipEnding();

            $dateC = new \DateTime(date('Y-m-d h:i:s a', time()));

            $interval = $dateB->diff($dateE);
            $days = $interval->days;
            $nb_weeks = ($days+3)/7;
            echo 'Nombre de semaines :'.$nb_weeks;

            $c_interval = $dateB->diff($dateC);
            $days = $c_interval->days;
            $curr_week = ceil(($days)/7);
            echo 'Semaine atuelle :'.$curr_week;

            $this->params['etape'] = 'pendant';
            return $this->render("MMIBundle:Stage:my_internship.html.twig", $this->params);

        }elseif($state == 'after'){

            // si on est après la date de fin
            // on affiche la section après

            $this->params['etape'] = 'apres';
            return $this->render("MMIBundle:Stage:my_internship.html.twig", $this->params);

        }

    }

    public function stagesPromoAction(){

        $internships = InternshipQuery::create()
            ->filterByValide(null)
            ->find();

        // On vérifie si l'utilisateur est connecté (externalisé)
        $response = $this->checkAccess();
        if ($response != null) return $this->redirectToRoute($response);

        $this->params['internships'] = $internships;
        return $this->render("MMIBundle:Stage:stages_promo.html.twig", $this->params);
    }

    public function internshipValidationAction($id, $valide){

        $internship = InternshipQuery::create()
            ->findOneById($id);
        $internship->setValide($valide);
        $internship->save();

        return $this->redirectToRoute('mmi_stages_promo');
    }

    public function stagesValidesAction(){

        $internships = InternshipQuery::create()
            ->filterByValide(true)
            ->filterByEtabli(null)
            ->find();

        // On vérifie si l'utilisateur est connecté (externalisé)
        $response = $this->checkAccess();
        if ($response != null) return $this->redirectToRoute($response);

        $this->params['internships'] = $internships;
        return $this->render("MMIBundle:Stage:stages_valides.html.twig", $this->params);
    }

    public function agreementValidationAction($id){

        $internship = InternshipQuery::create()
            ->findOneById($id);
        $internship->setEtabli(true);
        $internship->save();

        return $this->redirectToRoute('mmi_stages_valides');
    }

    public function stagesEtablisAction(Request $request){

        // On vérifie si l'utilisateur est connecté (externalisé)
        $response = $this->checkAccess();
        if ($response != null) return $this->redirectToRoute($response);

        $internships = InternshipQuery::create()
            ->filterByEtabli(true)
            ->find();

        $stagesEtablisForms = [];

        foreach($internships as $internship) {
            $stagesEtablisForm = $this->createForm(new StagesEtablisType(), $internship);
            $stagesEtablisForms[$internship->getId()] = $stagesEtablisForm;
        }

        if ($request->isMethod('POST')) {
            if ($request->request->has('stageEtablis')) {
                $data = $request->request->get('stageEtablis');

                if (isset($data['id'])) {
                    $id = $data['id'];

                    if (isset($stagesEtablisForms[$id])) {
                        $form = $stagesEtablisForms[$id];

                        $form->handleRequest($request);

                        if ($form->isValid()) {
                            $form->getData()->save();
                        }
                    }
                }
            }
        }

        // Si un formulaire a été soumis et qu'il est valide

        /*
        if ($stagesEtablisForm->isSubmitted() && $stagesEtablisForm->isValid()) {

            // Si c'est le formulaire "before"

            if($beforeForm->getName() == 'beforeInternship'){

                // On affecte manuellement l'id utilisateur

                $stage->setUserId($connectedUserId);

                // On affecte manuellement les dates de début et fin de stage de la table config

                $stage->setInternshipBegining($begining);
                $stage->setInternshipEnding($ending);

                // On sauvegarde l'objet stage dans la BDD

                $stage->save();

                // On envoie un message de configuration à la vue

                $result = 'Mme Gil a bien reçu ta demande, elle la validera bientôt.';

                // On appelle la vue avec un message de confirmation
                return $this->render("MMIBundle:Stage:my_internship.html.twig", array(
                    'result' => $result
                ));
            }

        }
*/

        // On affiche le formulaire
        $this->params['internships'] = $internships;
        $this->params['stageEtablisForms'] = $stagesEtablisForms;
        return $this->render("MMIBundle:Stage:stages_etablis.html.twig", $this->params);


    }

    public function stagesTuteurAction(){

        // On vérifie si l'utilisateur est connecté (externalisé)
        $response = $this->checkAccess();
        if ($response != null) return $this->redirectToRoute($response);

        // L'utilisateur est connecté, on stocke ses infos en session
        $connectedUser = $this->get('session')->get('Account');
        // On stocke son id dans une variable pour y accéder plus rapidement
        $connectedUserId = $connectedUser->getId();

        $internships = InternshipQuery::create()
            ->filterByEtabli(true)
            ->filterByTutorId($connectedUserId)
            ->find();

        $this->params['internships'] = $internships;
        return $this->render("MMIBundle:Stage:stages_tuteur.html.twig", $this->params);
    }

    public function internshipAction($id){

        // On vérifie si l'utilisateur est connecté (externalisé)
        $response = $this->checkAccess();
        if ($response != null) return $this->redirectToRoute($response);

        $internship = InternshipQuery::create()
            ->filterById($id)
            ->findOne();

        $this->params['internship'] = $internship;
        return $this->render("MMIBundle:Stage:internship.html.twig", $this->params);
    }
}
