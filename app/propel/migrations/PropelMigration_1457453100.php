<?php

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1457453100.
 * Generated on 2016-03-08 17:05:00 by Romain
 */
class PropelMigration_1457453100
{

    public function preUp($manager)
    {
        // add the pre-migration code here
    }

    public function postUp($manager)
    {
        // add the post-migration code here
    }

    public function preDown($manager)
    {
        // add the pre-migration code here
    }

    public function postDown($manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        return array (
  'default' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

DROP INDEX `address_FI_1` ON `address`;

ALTER TABLE `address`
    ADD `geoloc_id` INTEGER NOT NULL AFTER `city_id`;

ALTER TABLE `address` DROP `latitude`;

ALTER TABLE `address` DROP `longitude`;

CREATE INDEX `address_FI_1` ON `address` (`geoloc_id`);

ALTER TABLE `city`
    ADD `geoloc_id` INTEGER NOT NULL AFTER `country_id`;

CREATE INDEX `city_FI_2` ON `city` (`geoloc_id`);

CREATE TABLE `geoloc`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `latitude` FLOAT,
    `longitude` FLOAT,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        return array (
  'default' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `geoloc`;

DROP INDEX `address_FI_1` ON `address`;

ALTER TABLE `address`
    ADD `latitude` FLOAT AFTER `street`,
    ADD `longitude` FLOAT AFTER `latitude`;

ALTER TABLE `address` DROP `geoloc_id`;

CREATE INDEX `address_FI_1` ON `address` (`city_id`);

DROP INDEX `city_FI_2` ON `city`;

ALTER TABLE `city` DROP `geoloc_id`;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

}