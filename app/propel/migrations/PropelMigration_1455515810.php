<?php

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1455515810.
 * Generated on 2016-02-15 06:56:50 by Romain
 */
class PropelMigration_1455515810
{

    public function preUp($manager)
    {
        // add the pre-migration code here
    }

    public function postUp($manager)
    {
        // add the post-migration code here
    }

    public function preDown($manager)
    {
        // add the pre-migration code here
    }

    public function postDown($manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        return array (
  'default' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

CREATE TABLE `config`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `internship_begining` DATE,
    `internship_ending` DATE,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

CREATE TABLE `user`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `city_id` INTEGER NOT NULL,
    `profile_id` INTEGER NOT NULL,
    `promo_id` INTEGER NOT NULL,
    `email` VARCHAR(64),
    `password` VARCHAR(128),
    `lastname` VARCHAR(32),
    `firstname` VARCHAR(32),
    `description` VARCHAR(140),
    `newsletter` TINYINT(1),
    `birthday` DATE,
    `url_portfolio` VARCHAR(64),
    `url_linkedin` VARCHAR(64),
    `url_facebook` VARCHAR(64),
    `url_google` VARCHAR(64),
    `url_twitter` VARCHAR(64),
    `url_tumblr` VARCHAR(64),
    `url_instagram` VARCHAR(64),
    `url_blog` VARCHAR(64),
    `cv_file` VARCHAR(64),
    `image_profil_file` VARCHAR(64),
    `image_couv_file` VARCHAR(64),
    `telephone` VARCHAR(32),
    `first_connection` TINYINT(1),
    PRIMARY KEY (`id`),
    INDEX `user_FI_1` (`city_id`),
    INDEX `user_FI_2` (`profile_id`),
    INDEX `user_FI_3` (`promo_id`)
) ENGINE=MyISAM;

CREATE TABLE `role`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(32),
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

CREATE TABLE `page`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(32),
    `route` VARCHAR(32),
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

CREATE TABLE `itemMenu`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `route` VARCHAR(32),
    `name` VARCHAR(32),
    `order` INTEGER,
    `logState` VARCHAR(10) DEFAULT \'both\',
    `onlyFor` INTEGER(1) DEFAULT 0,
    `htmlClass` VARCHAR(20),
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

CREATE TABLE `user_role`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NOT NULL,
    `role_id` INTEGER NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `user_role_FI_1` (`user_id`),
    INDEX `user_role_FI_2` (`role_id`)
) ENGINE=MyISAM;

CREATE TABLE `role_page`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `role_id` INTEGER NOT NULL,
    `page_id` INTEGER NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `role_page_FI_1` (`role_id`),
    INDEX `role_page_FI_2` (`page_id`)
) ENGINE=MyISAM;

CREATE TABLE `role_menu`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `menu_id` INTEGER NOT NULL,
    `role_id` INTEGER NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `role_menu_FI_1` (`role_id`),
    INDEX `role_menu_FI_2` (`menu_id`)
) ENGINE=MyISAM;

CREATE TABLE `company`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `address_id` INTEGER NOT NULL,
    `contact_id` INTEGER NOT NULL,
    `industry_id` INTEGER NOT NULL,
    `name` VARCHAR(32),
    `siret` INTEGER(32),
    `password` VARCHAR(128),
    `email` VARCHAR(64),
    `telephone` VARCHAR(16),
    `description` TEXT,
    `workforce` VARCHAR(16),
    `logo_file` VARCHAR(64),
    `url_website` VARCHAR(64),
    `first_connection` INTEGER(1),
    PRIMARY KEY (`id`),
    INDEX `company_FI_1` (`address_id`),
    INDEX `company_FI_2` (`contact_id`),
    INDEX `company_FI_3` (`industry_id`)
) ENGINE=MyISAM;

CREATE TABLE `contact`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `firstname` VARCHAR(32),
    `lastname` VARCHAR(32),
    `profession` VARCHAR(32),
    `telephone` VARCHAR(32),
    `email` VARCHAR(32),
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

CREATE TABLE `address`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `city_id` INTEGER NOT NULL,
    `street` VARCHAR(64),
    PRIMARY KEY (`id`),
    INDEX `address_FI_1` (`city_id`)
) ENGINE=MyISAM;

CREATE TABLE `city`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `country_id` INTEGER NOT NULL,
    `name` VARCHAR(32),
    `cp` INTEGER(5),
    PRIMARY KEY (`id`),
    INDEX `city_FI_1` (`country_id`)
) ENGINE=MyISAM;

CREATE TABLE `country`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(32),
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

CREATE TABLE `offerInternship`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `internship_id` INTEGER NOT NULL,
    `type` INTEGER(1),
    `name` VARCHAR(32),
    `profil` TEXT,
    PRIMARY KEY (`id`),
    INDEX `offerInternship_FI_1` (`internship_id`)
) ENGINE=MyISAM;

CREATE TABLE `internship`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `offer_id` INTEGER NOT NULL,
    `user_id` INTEGER NOT NULL,
    `company_id` INTEGER NOT NULL,
    `tutor_id` INTEGER NOT NULL,
    `work_function` VARCHAR(32),
    `description` TEXT,
    `information` TEXT,
    `url_website_diffusion` VARCHAR(64),
    `signatory_lastname` VARCHAR(32),
    `signatory_firstname` VARCHAR(32),
    `signatory_function` VARCHAR(64),
    `master_lastname` VARCHAR(32),
    `master_firstname` VARCHAR(32),
    `master_email` VARCHAR(64),
    `master_telephone` VARCHAR(32),
    `valide` SMALLINT,
    `etabli` SMALLINT,
    `date_visite` DATE,
    `date_soutenance` DATE,
    `internship_begining` DATE,
    `internship_ending` DATE,
    PRIMARY KEY (`id`),
    INDEX `internship_FI_1` (`offer_id`),
    INDEX `internship_FI_2` (`user_id`),
    INDEX `internship_FI_3` (`company_id`)
) ENGINE=MyISAM;

CREATE TABLE `promo`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `degree_id` INTEGER NOT NULL,
    `year` INTEGER(4) NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `promo_FI_1` (`degree_id`)
) ENGINE=MyISAM;

CREATE TABLE `profession`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(64),
    `description` TEXT,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

CREATE TABLE `user_job`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NOT NULL,
    `profession_id` INTEGER NOT NULL,
    `company_id` INTEGER NOT NULL,
    `year_from` INTEGER(4),
    `year_to` INTEGER(4),
    `description` TEXT,
    PRIMARY KEY (`id`),
    INDEX `user_job_FI_1` (`user_id`),
    INDEX `user_job_FI_2` (`profession_id`),
    INDEX `user_job_FI_3` (`company_id`)
) ENGINE=MyISAM;

CREATE TABLE `degree`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(64),
    `option` VARCHAR(64),
    `summary` TEXT,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

CREATE TABLE `user_obtained_degree`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NOT NULL,
    `degree_id` INTEGER NOT NULL,
    `year_from` INTEGER(4),
    `year_to` INTEGER(4),
    PRIMARY KEY (`id`),
    INDEX `user_obtained_degree_FI_1` (`user_id`),
    INDEX `user_obtained_degree_FI_2` (`degree_id`)
) ENGINE=MyISAM;

CREATE TABLE `univ_project`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` VARCHAR(64),
    `name` VARCHAR(64),
    `need` TEXT,
    `brief` TEXT,
    `website` VARCHAR(64),
    PRIMARY KEY (`id`),
    INDEX `univ_project_FI_1` (`user_id`)
) ENGINE=MyISAM;

CREATE TABLE `user_member_unipro`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_id` INTEGER NOT NULL,
    `unipro_id` INTEGER NOT NULL,
    `profession_id` INTEGER NOT NULL,
    `year_from` INTEGER(4),
    `year_to` INTEGER(4),
    PRIMARY KEY (`id`),
    INDEX `user_member_unipro_FI_1` (`user_id`),
    INDEX `user_member_unipro_FI_2` (`unipro_id`),
    INDEX `user_member_unipro_FI_3` (`profession_id`)
) ENGINE=MyISAM;

CREATE TABLE `industry`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(64),
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        return array (
  'default' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `config`;

DROP TABLE IF EXISTS `user`;

DROP TABLE IF EXISTS `role`;

DROP TABLE IF EXISTS `page`;

DROP TABLE IF EXISTS `itemMenu`;

DROP TABLE IF EXISTS `user_role`;

DROP TABLE IF EXISTS `role_page`;

DROP TABLE IF EXISTS `role_menu`;

DROP TABLE IF EXISTS `company`;

DROP TABLE IF EXISTS `contact`;

DROP TABLE IF EXISTS `address`;

DROP TABLE IF EXISTS `city`;

DROP TABLE IF EXISTS `country`;

DROP TABLE IF EXISTS `offerInternship`;

DROP TABLE IF EXISTS `internship`;

DROP TABLE IF EXISTS `promo`;

DROP TABLE IF EXISTS `profession`;

DROP TABLE IF EXISTS `user_job`;

DROP TABLE IF EXISTS `degree`;

DROP TABLE IF EXISTS `user_obtained_degree`;

DROP TABLE IF EXISTS `univ_project`;

DROP TABLE IF EXISTS `user_member_unipro`;

DROP TABLE IF EXISTS `industry`;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

}