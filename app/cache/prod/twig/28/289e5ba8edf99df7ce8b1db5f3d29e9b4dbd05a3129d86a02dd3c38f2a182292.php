<?php

/* TwigBundle:Exception:error.html.twig */
class __TwigTemplate_6951118f5d140d641e321b6dcd56020815a233d0e2493d7704d8f02c311df04c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"fr\" class=\"no-js\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, user-scalable=no\">
    <title>331 | erreur ";
        // line 6
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : null), "html", null, true);
        echo "</title>
    <link href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/screen.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\"/>
    <link href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/print.css"), "html", null, true);
        echo "\" media=\"print\" rel=\"stylesheet\" type=\"text/css\"/>
    <!--[if IE]>
    <link href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/ie.css"), "html", null, true);
        echo "\" media=\"screen, projection\" rel=\"stylesheet\" type=\"text/css\"/>
    <![endif]-->
    <link rel=\"shortcut icon\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("ui_img/favicon.ico"), "html", null, true);
        echo "\" type=\"image/x-icon\">
    <link rel=\"icon\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("ui_img/favicon.ico"), "html", null, true);
        echo "\" type=\"image/x-icon\">
    <!--<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css\">-->

    <script src=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("assets/jquery-custom.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("assets/modernizr.js"), "html", null, true);
        echo "\"></script>
</head>
<body class=\"lost\">
<header style=\"background-image: none; background-color: rgba(38, 38, 38, 0.46)\">
    <a href=\"";
        // line 21
        echo $this->env->getExtension('routing')->getPath("mmi_homepage");
        echo "\" class=\"logo white transition\">331 Acceuil</a>

    <h1 class=\"super_title\">Erreur ";
        // line 23
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : null), "html", null, true);
        echo "</h1>

    <p>Je crois bien que quelque chose est cassé...
        <br>On va essayer de réparer au plus vite ! Comme vous pouvez le constater nous mettons notre meilleure équipe sur le coup !
        <br>
        <br>Aidez en nous rapportant les bugs que vous avez rencontrés via ce <a class=\"blue\" href=\"https://docs.google.com/forms/d/1tpxZpZmBladdlVFwdieDO39oo4amLHMGSa02eHLm1CE/viewform?c=0&w=1\">formulaire</a> !
        <br>Merci d'avance de la part de toute l'équipe</p>

    <iframe src=\"http://tv.giphy.com/computer\" frameborder=\"0\"></iframe>

</header>
<footer class=\"white\">
    <a href=\"";
        // line 35
        echo $this->env->getExtension('routing')->getPath("mmi_homepage");
        echo "\" class=\"logo\">331 Acceuil</a>
    <ul>
        <li><a href=\"\">À propos</a></li>
        <li><a href=\"\">Mentions légales</a></li>
        <li><a href=\"http://mmimontbeliard.com\" target=\"_blank\">Département MMI</a></li>
    </ul>
</footer>

<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-73230977-1', 'auto');
    ga('send', 'pageview');

</script>

</body>
</html>




";
    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 35,  70 => 23,  65 => 21,  58 => 17,  54 => 16,  48 => 13,  44 => 12,  39 => 10,  34 => 8,  30 => 7,  26 => 6,  19 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html lang="fr" class="no-js">*/
/* <head>*/
/*     <meta charset="UTF-8">*/
/*     <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">*/
/*     <title>331 | erreur {{ status_code }}</title>*/
/*     <link href="{{ asset('css/screen.css') }}" rel="stylesheet" type="text/css"/>*/
/*     <link href="{{ asset('css/print.css') }}" media="print" rel="stylesheet" type="text/css"/>*/
/*     <!--[if IE]>*/
/*     <link href="{{ asset('css/ie.css') }}" media="screen, projection" rel="stylesheet" type="text/css"/>*/
/*     <![endif]-->*/
/*     <link rel="shortcut icon" href="{{ asset('ui_img/favicon.ico') }}" type="image/x-icon">*/
/*     <link rel="icon" href="{{ asset('ui_img/favicon.ico') }}" type="image/x-icon">*/
/*     <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">-->*/
/* */
/*     <script src="{{ asset('assets/jquery-custom.min.js') }}"></script>*/
/*     <script src="{{ asset('assets/modernizr.js') }}"></script>*/
/* </head>*/
/* <body class="lost">*/
/* <header style="background-image: none; background-color: rgba(38, 38, 38, 0.46)">*/
/*     <a href="{{ path("mmi_homepage") }}" class="logo white transition">331 Acceuil</a>*/
/* */
/*     <h1 class="super_title">Erreur {{ status_code }}</h1>*/
/* */
/*     <p>Je crois bien que quelque chose est cassé...*/
/*         <br>On va essayer de réparer au plus vite ! Comme vous pouvez le constater nous mettons notre meilleure équipe sur le coup !*/
/*         <br>*/
/*         <br>Aidez en nous rapportant les bugs que vous avez rencontrés via ce <a class="blue" href="https://docs.google.com/forms/d/1tpxZpZmBladdlVFwdieDO39oo4amLHMGSa02eHLm1CE/viewform?c=0&w=1">formulaire</a> !*/
/*         <br>Merci d'avance de la part de toute l'équipe</p>*/
/* */
/*     <iframe src="http://tv.giphy.com/computer" frameborder="0"></iframe>*/
/* */
/* </header>*/
/* <footer class="white">*/
/*     <a href="{{ path("mmi_homepage") }}" class="logo">331 Acceuil</a>*/
/*     <ul>*/
/*         <li><a href="">À propos</a></li>*/
/*         <li><a href="">Mentions légales</a></li>*/
/*         <li><a href="http://mmimontbeliard.com" target="_blank">Département MMI</a></li>*/
/*     </ul>*/
/* </footer>*/
/* */
/* <script>*/
/*     (function (i, s, o, g, r, a, m) {*/
/*         i['GoogleAnalyticsObject'] = r;*/
/*         i[r] = i[r] || function () {*/
/*                     (i[r].q = i[r].q || []).push(arguments)*/
/*                 }, i[r].l = 1 * new Date();*/
/*         a = s.createElement(o),*/
/*                 m = s.getElementsByTagName(o)[0];*/
/*         a.async = 1;*/
/*         a.src = g;*/
/*         m.parentNode.insertBefore(a, m)*/
/*     })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');*/
/* */
/*     ga('create', 'UA-73230977-1', 'auto');*/
/*     ga('send', 'pageview');*/
/* */
/* </script>*/
/* */
/* </body>*/
/* </html>*/
/* */
/* */
/* */
/* */
/* */
