<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        // mmi_homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'mmi_homepage');
            }

            return array (  '_controller' => 'MMIBundle\\Controller\\DefaultController::indexAction',  '_route' => 'mmi_homepage',);
        }

        // mmi_deconnexion
        if ($pathinfo === '/deconnexion') {
            return array (  '_controller' => 'MMIBundle\\Controller\\DefaultController::deconnexionAction',  '_route' => 'mmi_deconnexion',);
        }

        // mmi_search
        if (0 === strpos($pathinfo, '/search') && preg_match('#^/search(?:/(?P<type>[^/]++))?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'mmi_search')), array (  '_controller' => 'MMIBundle\\Controller\\DefaultController::searchAction',  'type' => 'internships',));
        }

        // mmi_account
        if ($pathinfo === '/account') {
            return array (  '_controller' => 'MMIBundle\\Controller\\DefaultController::accountAction',  '_route' => 'mmi_account',);
        }

        // mmi_blog
        if ($pathinfo === '/blog') {
            return array (  '_controller' => 'MMIBundle\\Controller\\DefaultController::blogAction',  '_route' => 'mmi_blog',);
        }

        // mmi_welcome_student
        if ($pathinfo === '/welcome_student') {
            return array (  '_controller' => 'MMIBundle\\Controller\\UtilisateurController::welcomeAction',  '_route' => 'mmi_welcome_student',);
        }

        // mmi_edit_student
        if ($pathinfo === '/edit_student') {
            return array (  '_controller' => 'MMIBundle\\Controller\\UtilisateurController::edit_studentAction',  '_route' => 'mmi_edit_student',);
        }

        // mmi_student
        if (0 === strpos($pathinfo, '/student') && preg_match('#^/student/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'mmi_student')), array (  '_controller' => 'MMIBundle\\Controller\\UtilisateurController::studentAction',));
        }

        // mmi_pros
        if ($pathinfo === '/pros') {
            return array (  '_controller' => 'MMIBundle\\Controller\\EntrepriseController::indexAction',  '_route' => 'mmi_pros',);
        }

        // mmi_welcome_company
        if ($pathinfo === '/welcome_company') {
            return array (  '_controller' => 'MMIBundle\\Controller\\EntrepriseController::welcomeAction',  '_route' => 'mmi_welcome_company',);
        }

        // mmi_company
        if ($pathinfo === '/company') {
            return array (  '_controller' => 'MMIBundle\\Controller\\EntrepriseController::companyAction',  '_route' => 'mmi_company',);
        }

        // mmi_edit_company
        if ($pathinfo === '/edit_company') {
            return array (  '_controller' => 'MMIBundle\\Controller\\EntrepriseController::edit_companyAction',  '_route' => 'mmi_edit_company',);
        }

        // mmi_univ_project
        if (0 === strpos($pathinfo, '/univ_project') && preg_match('#^/univ_project/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'mmi_univ_project')), array (  '_controller' => 'MMIBundle\\Controller\\ProjetUnivController::indexAction',));
        }

        // mmi_edit_univ_project
        if ($pathinfo === '/edit_univ_project') {
            return array (  '_controller' => 'MMIBundle\\Controller\\ProjetUnivController::edit_univ_projectAction',  '_route' => 'mmi_edit_univ_project',);
        }

        // mmi_edit_add_univ_project
        if ($pathinfo === '/add_univ_project') {
            return array (  '_controller' => 'MMIBundle\\Controller\\ProjetUnivController::add_univ_projectAction',  '_route' => 'mmi_edit_add_univ_project',);
        }

        // mmi_training
        if (0 === strpos($pathinfo, '/training') && preg_match('#^/training/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'mmi_training')), array (  '_controller' => 'MMIBundle\\Controller\\FormationController::indexAction',));
        }

        // mmi_add_training
        if ($pathinfo === '/add_training') {
            return array (  '_controller' => 'MMIBundle\\Controller\\FormationController::add_trainingAction',  '_route' => 'mmi_add_training',);
        }

        // mmi_edit_training
        if ($pathinfo === '/edit_training') {
            return array (  '_controller' => 'MMIBundle\\Controller\\FormationController::edit_trainingAction',  '_route' => 'mmi_edit_training',);
        }

        // mmi_profession
        if (0 === strpos($pathinfo, '/profession') && preg_match('#^/profession/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'mmi_profession')), array (  '_controller' => 'MMIBundle\\Controller\\ProfessionController::indexAction',));
        }

        // mmi_add_profession
        if ($pathinfo === '/add_profession') {
            return array (  '_controller' => 'MMIBundle\\Controller\\ProfessionController::add_professionAction',  '_route' => 'mmi_add_profession',);
        }

        // mmi_edit_profession
        if ($pathinfo === '/edit_profession') {
            return array (  '_controller' => 'MMIBundle\\Controller\\ProfessionController::edit_professionAction',  '_route' => 'mmi_edit_profession',);
        }

        // mmi_my_internship
        if ($pathinfo === '/my_internship') {
            return array (  '_controller' => 'MMIBundle\\Controller\\StageController::myInternshipAction',  '_route' => 'mmi_my_internship',);
        }

        // mmi_stages_promo
        if ($pathinfo === '/stages_promo') {
            return array (  '_controller' => 'MMIBundle\\Controller\\StageController::stagesPromoAction',  '_route' => 'mmi_stages_promo',);
        }

        // mmi_internship_validation
        if (0 === strpos($pathinfo, '/internship_validation') && preg_match('#^/internship_validation/(?P<id>[^/]++)/(?P<valide>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'mmi_internship_validation')), array (  '_controller' => 'MMIBundle\\Controller\\StageController::internshipValidationAction',));
        }

        // mmi_stages_valides
        if ($pathinfo === '/stages_valides') {
            return array (  '_controller' => 'MMIBundle\\Controller\\StageController::stagesValidesAction',  '_route' => 'mmi_stages_valides',);
        }

        if (0 === strpos($pathinfo, '/agreement_')) {
            // mmi_agreement_generation
            if ($pathinfo === '/agreement_generation') {
                return array (  '_controller' => 'MMIBundle\\Controller\\StageController::internshipGenerationAction',  '_route' => 'mmi_agreement_generation',);
            }

            // mmi_agreement_validation
            if (0 === strpos($pathinfo, '/agreement_validation') && preg_match('#^/agreement_validation/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'mmi_agreement_validation')), array (  '_controller' => 'MMIBundle\\Controller\\StageController::agreementValidationAction',));
            }

        }

        if (0 === strpos($pathinfo, '/stages_')) {
            // mmi_stages_etablis
            if ($pathinfo === '/stages_etablis') {
                return array (  '_controller' => 'MMIBundle\\Controller\\StageController::stagesEtablisAction',  '_route' => 'mmi_stages_etablis',);
            }

            // mmi_stages_tuteur
            if ($pathinfo === '/stages_tuteur') {
                return array (  '_controller' => 'MMIBundle\\Controller\\StageController::stagesTuteurAction',  '_route' => 'mmi_stages_tuteur',);
            }

        }

        // mmi_internship
        if (0 === strpos($pathinfo, '/internship') && preg_match('#^/internship/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'mmi_internship')), array (  '_controller' => 'MMIBundle\\Controller\\StageController::internshipAction',));
        }

        if (0 === strpos($pathinfo, '/ajax_')) {
            if (0 === strpos($pathinfo, '/ajax_get_')) {
                // mmi_ajax_get_list_by_type
                if (0 === strpos($pathinfo, '/ajax_get_list_by_type') && preg_match('#^/ajax_get_list_by_type/(?P<type>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'mmi_ajax_get_list_by_type')), array (  '_controller' => 'MMIBundle\\Controller\\AjaxController::getListByTypeAction',));
                }

                // mmi_ajax_get_experience
                if (0 === strpos($pathinfo, '/ajax_get_experience') && preg_match('#^/ajax_get_experience/(?P<type>[^/]++)/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'mmi_ajax_get_experience')), array (  '_controller' => 'MMIBundle\\Controller\\AjaxController::getExperienceAction',));
                }

            }

            // mmi_ajax_delete_experience
            if (0 === strpos($pathinfo, '/ajax_delete_experience') && preg_match('#^/ajax_delete_experience/(?P<type>[^/]++)/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'mmi_ajax_delete_experience')), array (  '_controller' => 'MMIBundle\\Controller\\AjaxController::deleteExperienceAction',));
            }

        }

        // mmi_validate
        if (0 === strpos($pathinfo, '/validate') && preg_match('#^/validate/(?P<code>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'mmi_validate')), array (  '_controller' => 'MMIBundle\\Controller\\ValidationEmailController::validateAction',));
        }

        // homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'homepage');
            }

            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
