<?php

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Psr\Log\LoggerInterface;

/**
 * appProdUrlGenerator
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlGenerator extends Symfony\Component\Routing\Generator\UrlGenerator
{
    private static $declaredRoutes = array(
        'mmi_homepage' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'MMIBundle\\Controller\\DefaultController::indexAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'mmi_deconnexion' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'MMIBundle\\Controller\\DefaultController::deconnexionAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/deconnexion',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'mmi_search' => array (  0 =>   array (    0 => 'type',  ),  1 =>   array (    '_controller' => 'MMIBundle\\Controller\\DefaultController::searchAction',    'type' => 'internships',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'type',    ),    1 =>     array (      0 => 'text',      1 => '/search',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'mmi_account' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'MMIBundle\\Controller\\DefaultController::accountAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/account',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'mmi_blog' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'MMIBundle\\Controller\\DefaultController::blogAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/blog',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'mmi_welcome_student' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'MMIBundle\\Controller\\UtilisateurController::welcomeAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/welcome_student',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'mmi_edit_student' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'MMIBundle\\Controller\\UtilisateurController::edit_studentAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/edit_student',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'mmi_student' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'MMIBundle\\Controller\\UtilisateurController::studentAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/student',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'mmi_pros' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'MMIBundle\\Controller\\EntrepriseController::indexAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/pros',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'mmi_welcome_company' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'MMIBundle\\Controller\\EntrepriseController::welcomeAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/welcome_company',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'mmi_company' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'MMIBundle\\Controller\\EntrepriseController::companyAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/company',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'mmi_edit_company' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'MMIBundle\\Controller\\EntrepriseController::edit_companyAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/edit_company',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'mmi_univ_project' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'MMIBundle\\Controller\\ProjetUnivController::indexAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/univ_project',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'mmi_edit_univ_project' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'MMIBundle\\Controller\\ProjetUnivController::edit_univ_projectAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/edit_univ_project',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'mmi_edit_add_univ_project' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'MMIBundle\\Controller\\ProjetUnivController::add_univ_projectAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/add_univ_project',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'mmi_training' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'MMIBundle\\Controller\\FormationController::indexAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/training',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'mmi_add_training' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'MMIBundle\\Controller\\FormationController::add_trainingAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/add_training',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'mmi_edit_training' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'MMIBundle\\Controller\\FormationController::edit_trainingAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/edit_training',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'mmi_profession' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'MMIBundle\\Controller\\ProfessionController::indexAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/profession',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'mmi_add_profession' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'MMIBundle\\Controller\\ProfessionController::add_professionAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/add_profession',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'mmi_edit_profession' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'MMIBundle\\Controller\\ProfessionController::edit_professionAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/edit_profession',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'mmi_my_internship' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'MMIBundle\\Controller\\StageController::myInternshipAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/my_internship',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'mmi_stages_promo' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'MMIBundle\\Controller\\StageController::stagesPromoAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/stages_promo',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'mmi_internship_validation' => array (  0 =>   array (    0 => 'id',    1 => 'valide',  ),  1 =>   array (    '_controller' => 'MMIBundle\\Controller\\StageController::internshipValidationAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'valide',    ),    1 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    2 =>     array (      0 => 'text',      1 => '/internship_validation',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'mmi_stages_valides' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'MMIBundle\\Controller\\StageController::stagesValidesAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/stages_valides',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'mmi_agreement_generation' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'MMIBundle\\Controller\\StageController::internshipGenerationAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/agreement_generation',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'mmi_agreement_validation' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'MMIBundle\\Controller\\StageController::agreementValidationAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/agreement_validation',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'mmi_stages_etablis' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'MMIBundle\\Controller\\StageController::stagesEtablisAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/stages_etablis',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'mmi_stages_tuteur' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'MMIBundle\\Controller\\StageController::stagesTuteurAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/stages_tuteur',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'mmi_internship' => array (  0 =>   array (    0 => 'id',  ),  1 =>   array (    '_controller' => 'MMIBundle\\Controller\\StageController::internshipAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    1 =>     array (      0 => 'text',      1 => '/internship',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'mmi_ajax_get_list_by_type' => array (  0 =>   array (    0 => 'type',  ),  1 =>   array (    '_controller' => 'MMIBundle\\Controller\\AjaxController::getListByTypeAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'type',    ),    1 =>     array (      0 => 'text',      1 => '/ajax_get_list_by_type',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'mmi_ajax_get_experience' => array (  0 =>   array (    0 => 'type',    1 => 'id',  ),  1 =>   array (    '_controller' => 'MMIBundle\\Controller\\AjaxController::getExperienceAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    1 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'type',    ),    2 =>     array (      0 => 'text',      1 => '/ajax_get_experience',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'mmi_ajax_delete_experience' => array (  0 =>   array (    0 => 'type',    1 => 'id',  ),  1 =>   array (    '_controller' => 'MMIBundle\\Controller\\AjaxController::deleteExperienceAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'id',    ),    1 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'type',    ),    2 =>     array (      0 => 'text',      1 => '/ajax_delete_experience',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'mmi_validate' => array (  0 =>   array (    0 => 'code',  ),  1 =>   array (    '_controller' => 'MMIBundle\\Controller\\ValidationEmailController::validateAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'variable',      1 => '/',      2 => '[^/]++',      3 => 'code',    ),    1 =>     array (      0 => 'text',      1 => '/validate',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
        'homepage' => array (  0 =>   array (  ),  1 =>   array (    '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  ),  2 =>   array (  ),  3 =>   array (    0 =>     array (      0 => 'text',      1 => '/',    ),  ),  4 =>   array (  ),  5 =>   array (  ),),
    );

    /**
     * Constructor.
     */
    public function __construct(RequestContext $context, LoggerInterface $logger = null)
    {
        $this->context = $context;
        $this->logger = $logger;
    }

    public function generate($name, $parameters = array(), $referenceType = self::ABSOLUTE_PATH)
    {
        if (!isset(self::$declaredRoutes[$name])) {
            throw new RouteNotFoundException(sprintf('Unable to generate a URL for the named route "%s" as such route does not exist.', $name));
        }

        list($variables, $defaults, $requirements, $tokens, $hostTokens, $requiredSchemes) = self::$declaredRoutes[$name];

        return $this->doGenerate($variables, $defaults, $requirements, $tokens, $parameters, $name, $referenceType, $hostTokens, $requiredSchemes);
    }
}
