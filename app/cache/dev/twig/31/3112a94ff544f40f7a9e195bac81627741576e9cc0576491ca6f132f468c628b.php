<?php

/* MMIBundle:Default:search.html.twig */
class __TwigTemplate_009cd3f7ca43ee89ab8ce51cc8b89dd473eae1878693130064585efc0ff41bc6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("MMIBundle::template.html.twig", "MMIBundle:Default:search.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'includeHead' => array($this, 'block_includeHead'),
            'bodyClass' => array($this, 'block_bodyClass'),
            'header' => array($this, 'block_header'),
            'headerExtended' => array($this, 'block_headerExtended'),
            'content' => array($this, 'block_content'),
            'includeBody' => array($this, 'block_includeBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "MMIBundle::template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_571e284d7ede3caa26a838dfcbcafe7b61660bc56a2c21dbd902083873591443 = $this->env->getExtension("native_profiler");
        $__internal_571e284d7ede3caa26a838dfcbcafe7b61660bc56a2c21dbd902083873591443->enter($__internal_571e284d7ede3caa26a838dfcbcafe7b61660bc56a2c21dbd902083873591443_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MMIBundle:Default:search.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_571e284d7ede3caa26a838dfcbcafe7b61660bc56a2c21dbd902083873591443->leave($__internal_571e284d7ede3caa26a838dfcbcafe7b61660bc56a2c21dbd902083873591443_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_95ae0cadfc69d6835fabb86724566bbe02c4db9c696749adfa75f803845cbf45 = $this->env->getExtension("native_profiler");
        $__internal_95ae0cadfc69d6835fabb86724566bbe02c4db9c696749adfa75f803845cbf45->enter($__internal_95ae0cadfc69d6835fabb86724566bbe02c4db9c696749adfa75f803845cbf45_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Rechercher";
        
        $__internal_95ae0cadfc69d6835fabb86724566bbe02c4db9c696749adfa75f803845cbf45->leave($__internal_95ae0cadfc69d6835fabb86724566bbe02c4db9c696749adfa75f803845cbf45_prof);

    }

    // line 5
    public function block_includeHead($context, array $blocks = array())
    {
        $__internal_2dba95f63da9075e425249e344e2797ec231dc38b27eca08e196190ce0716d09 = $this->env->getExtension("native_profiler");
        $__internal_2dba95f63da9075e425249e344e2797ec231dc38b27eca08e196190ce0716d09->enter($__internal_2dba95f63da9075e425249e344e2797ec231dc38b27eca08e196190ce0716d09_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "includeHead"));

        
        $__internal_2dba95f63da9075e425249e344e2797ec231dc38b27eca08e196190ce0716d09->leave($__internal_2dba95f63da9075e425249e344e2797ec231dc38b27eca08e196190ce0716d09_prof);

    }

    // line 8
    public function block_bodyClass($context, array $blocks = array())
    {
        $__internal_e3164e80c8fd9f905a50309f234d5a92c6b1308e5edfb3f5f9b057fc1b530cc5 = $this->env->getExtension("native_profiler");
        $__internal_e3164e80c8fd9f905a50309f234d5a92c6b1308e5edfb3f5f9b057fc1b530cc5->enter($__internal_e3164e80c8fd9f905a50309f234d5a92c6b1308e5edfb3f5f9b057fc1b530cc5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "bodyClass"));

        echo "search";
        
        $__internal_e3164e80c8fd9f905a50309f234d5a92c6b1308e5edfb3f5f9b057fc1b530cc5->leave($__internal_e3164e80c8fd9f905a50309f234d5a92c6b1308e5edfb3f5f9b057fc1b530cc5_prof);

    }

    // line 10
    public function block_header($context, array $blocks = array())
    {
        $__internal_f9f7183562d436189ee7ec30100c1bf988e599e68a3a60f4e1568fd0a6a53d7a = $this->env->getExtension("native_profiler");
        $__internal_f9f7183562d436189ee7ec30100c1bf988e599e68a3a60f4e1568fd0a6a53d7a->enter($__internal_f9f7183562d436189ee7ec30100c1bf988e599e68a3a60f4e1568fd0a6a53d7a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        echo "style=\"background-image: url(";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("ui_img/rechercher_01.jpeg"), "html", null, true);
        echo ")\"";
        
        $__internal_f9f7183562d436189ee7ec30100c1bf988e599e68a3a60f4e1568fd0a6a53d7a->leave($__internal_f9f7183562d436189ee7ec30100c1bf988e599e68a3a60f4e1568fd0a6a53d7a_prof);

    }

    // line 12
    public function block_headerExtended($context, array $blocks = array())
    {
        $__internal_ece699a903ec08e8373befe4310f116e624b833aa23b3a18b6a49a37140a52d9 = $this->env->getExtension("native_profiler");
        $__internal_ece699a903ec08e8373befe4310f116e624b833aa23b3a18b6a49a37140a52d9->enter($__internal_ece699a903ec08e8373befe4310f116e624b833aa23b3a18b6a49a37140a52d9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "headerExtended"));

        // line 13
        echo "    <form action=\"\" class=\"search\">
        <h3 class=\"white\">Comment peut-on vous aider ?</h3>
        <input type=\"text\" placeholder=\"\">
        <button type=\"button\">Rechercher</button>
    </form>
";
        
        $__internal_ece699a903ec08e8373befe4310f116e624b833aa23b3a18b6a49a37140a52d9->leave($__internal_ece699a903ec08e8373befe4310f116e624b833aa23b3a18b6a49a37140a52d9_prof);

    }

    // line 20
    public function block_content($context, array $blocks = array())
    {
        $__internal_dd00a55855b87dd761cbc9c801572a0b4c397dbc8837df650b672268364f5acf = $this->env->getExtension("native_profiler");
        $__internal_dd00a55855b87dd761cbc9c801572a0b4c397dbc8837df650b672268364f5acf->enter($__internal_dd00a55855b87dd761cbc9c801572a0b4c397dbc8837df650b672268364f5acf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 21
        echo "    <nav class=\"search\">
        <ul>
            <li><a href=\"";
        // line 23
        echo $this->env->getExtension('routing')->getPath("mmi_search");
        echo "\"
                   ";
        // line 24
        if (((isset($context["search_type"]) ? $context["search_type"] : $this->getContext($context, "search_type")) == "internships")) {
            echo "class=\"active\"";
        }
        echo ">Stage</a></li>
            <li><a href=\"";
        // line 25
        echo $this->env->getExtension('routing')->getPath("mmi_search");
        echo "/projects\" ";
        if (((isset($context["search_type"]) ? $context["search_type"] : $this->getContext($context, "search_type")) == "projects")) {
            echo "class=\"active\"";
        }
        echo ">Projet
                    universitaire</a></li>
            <li><a href=\"";
        // line 27
        echo $this->env->getExtension('routing')->getPath("mmi_search");
        echo "/students\"
                   ";
        // line 28
        if (((isset($context["search_type"]) ? $context["search_type"] : $this->getContext($context, "search_type")) == "students")) {
            echo "class=\"active\"";
        }
        echo ">Profil</a></li>
            <li><a href=\"";
        // line 29
        echo $this->env->getExtension('routing')->getPath("mmi_search");
        echo "/companies\"
                   ";
        // line 30
        if (((isset($context["search_type"]) ? $context["search_type"] : $this->getContext($context, "search_type")) == "companies")) {
            echo "class=\"active\"";
        }
        echo ">Entreprise</a></li>
            <li><a href=\"";
        // line 31
        echo $this->env->getExtension('routing')->getPath("mmi_search");
        echo "/degrees\" ";
        if (((isset($context["search_type"]) ? $context["search_type"] : $this->getContext($context, "search_type")) == "degrees")) {
            echo "class=\"active\"";
        }
        echo ">Formation</a>
            </li>
            <li><a href=\"";
        // line 33
        echo $this->env->getExtension('routing')->getPath("mmi_search");
        echo "/professions\"
                   ";
        // line 34
        if (((isset($context["search_type"]) ? $context["search_type"] : $this->getContext($context, "search_type")) == "professions")) {
            echo "class=\"active\"";
        }
        echo ">Métier</a></li>
        </ul>
    </nav>
    <main>
        <form action=\"\" class=\"filter\">
            ";
        // line 39
        if (((isset($context["search_type"]) ? $context["search_type"] : $this->getContext($context, "search_type")) == "students")) {
            // line 40
            echo "                <select class=\"promo-filter\" placeholder=\"Promo\">
                    <option value=\"ras\">Promo</option>
                    ";
            // line 42
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["listPromos"]) ? $context["listPromos"] : $this->getContext($context, "listPromos")));
            foreach ($context['_seq'] as $context["_key"] => $context["promo"]) {
                // line 43
                echo "                        <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["promo"], "id", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["promo"], "year", array()), "html", null, true);
                echo "</option>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['promo'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 45
            echo "                </select>
                <select class=\"city-filter\" placeholder=\"Ville\">
                    <option value=\"ras\">Ville</option>
                    ";
            // line 48
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["listCity"]) ? $context["listCity"] : $this->getContext($context, "listCity")));
            foreach ($context['_seq'] as $context["_key"] => $context["city"]) {
                // line 49
                echo "                        <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["city"], "id", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["city"], "name", array()), "html", null, true);
                echo "</option>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['city'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 51
            echo "                </select>
                <select class=\"profile-filter\" placeholder=\"Profil\">
                    <option value=\"ras\">Profil</option>
                    ";
            // line 54
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["listProfession"]) ? $context["listProfession"] : $this->getContext($context, "listProfession")));
            foreach ($context['_seq'] as $context["_key"] => $context["profession"]) {
                // line 55
                echo "                        <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["profession"], "id", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["profession"], "name", array()), "html", null, true);
                echo "</option>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['profession'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 57
            echo "                </select>
            ";
        } elseif ((        // line 58
(isset($context["search_type"]) ? $context["search_type"] : $this->getContext($context, "search_type")) == "companies")) {
            // line 59
            echo "                <select class=\"industry-filter\" placeholder=\"Secteur\">
                    <option value=\"ras\">Secteur</option>
                    ";
            // line 61
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["listIndustry"]) ? $context["listIndustry"] : $this->getContext($context, "listIndustry")));
            foreach ($context['_seq'] as $context["_key"] => $context["industry"]) {
                // line 62
                echo "                        <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["industry"], "id", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["industry"], "name", array()), "html", null, true);
                echo "</option>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['industry'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 64
            echo "                </select>
                <select class=\"workforce-filter\" placeholder=\"Effectif\">
                    <option value=\"ras\">Effectif</option>
                    ";
            // line 67
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["listCity"]) ? $context["listCity"] : $this->getContext($context, "listCity")));
            foreach ($context['_seq'] as $context["_key"] => $context["city"]) {
                // line 68
                echo "                        <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["city"], "id", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["city"], "name", array()), "html", null, true);
                echo "</option>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['city'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 70
            echo "                </select>
                <select class=\"city-filter\" placeholder=\"Ville\">
                    <option value=\"ras\">Ville</option>
                    ";
            // line 73
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["listCity"]) ? $context["listCity"] : $this->getContext($context, "listCity")));
            foreach ($context['_seq'] as $context["_key"] => $context["city"]) {
                // line 74
                echo "                        <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["city"], "id", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["city"], "name", array()), "html", null, true);
                echo "</option>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['city'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 76
            echo "                </select>
            ";
        }
        // line 78
        echo "        </form>

        <input type=\"hidden\" id=\"search_type\" value=\"";
        // line 80
        echo twig_escape_filter($this->env, (isset($context["search_type"]) ? $context["search_type"] : $this->getContext($context, "search_type")), "html", null, true);
        echo "\">

        <section class=\"tile_wrapper wrapper\">
            ";
        // line 83
        if (((isset($context["search_type"]) ? $context["search_type"] : $this->getContext($context, "search_type")) == "students")) {
            // line 84
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["data_list"]) ? $context["data_list"] : $this->getContext($context, "data_list")));
            foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
                // line 85
                echo "                    ";
                $context["class"] = "";
                // line 86
                echo "                    ";
                if ( !(null === $this->getAttribute($context["user"], "promo", array()))) {
                    // line 87
                    echo "                        ";
                    $context["class"] = (((isset($context["class"]) ? $context["class"] : $this->getContext($context, "class")) . " promo-") . $this->getAttribute($this->getAttribute($context["user"], "promo", array()), "id", array()));
                    // line 88
                    echo "                    ";
                }
                // line 89
                echo "                    ";
                if ( !(null === $this->getAttribute($context["user"], "city", array()))) {
                    // line 90
                    echo "                        ";
                    $context["class"] = (((isset($context["class"]) ? $context["class"] : $this->getContext($context, "class")) . " city-") . $this->getAttribute($this->getAttribute($context["user"], "city", array()), "id", array()));
                    // line 91
                    echo "                    ";
                }
                // line 92
                echo "                    ";
                if (($this->getAttribute($context["user"], "profileId", array()) != 0)) {
                    // line 93
                    echo "                        ";
                    $context["class"] = (((isset($context["class"]) ? $context["class"] : $this->getContext($context, "class")) . " profile-") . $this->getAttribute($context["user"], "profileId", array()));
                    // line 94
                    echo "                    ";
                }
                // line 95
                echo "                    <article class=\"tile profile_picture_enabled";
                echo twig_escape_filter($this->env, (isset($context["class"]) ? $context["class"] : $this->getContext($context, "class")), "html", null, true);
                echo "\">
                        <h3><a href=\"\">";
                // line 96
                echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "firstname", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "lastname", array()), "html", null, true);
                echo "</a></h3>

                        <div class=\"profilepicture search center\"
                             style=\"background-image: url(";
                // line 99
                echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("images/profil_picture/"), "html", null, true);
                echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "imageProfilFile", array()), "html", null, true);
                echo ")\">
                            <a href=\"";
                // line 100
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("mmi_student", array("id" => $this->getAttribute($context["user"], "id", array()))), "html", null, true);
                echo "\"></a></div>
                        ";
                // line 101
                if ( !(null === $this->getAttribute($context["user"], "profession", array()))) {
                    // line 102
                    echo "                            <h6>";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["user"], "profession", array()), "name", array()), "html", null, true);
                    echo "</h6>
                        ";
                }
                // line 104
                echo "
                        <p>";
                // line 105
                echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "description", array()), "html", null, true);
                echo "</p>
                    </article>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 108
            echo "            ";
        } elseif (((isset($context["search_type"]) ? $context["search_type"] : $this->getContext($context, "search_type")) == "companies")) {
            // line 109
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["data_list"]) ? $context["data_list"] : $this->getContext($context, "data_list")));
            foreach ($context['_seq'] as $context["_key"] => $context["company"]) {
                // line 110
                echo "                    ";
                $context["class"] = "";
                // line 111
                echo "                    ";
                if ( !(null === $this->getAttribute($context["company"], "industry", array()))) {
                    // line 112
                    echo "                        ";
                    $context["class"] = (((isset($context["class"]) ? $context["class"] : $this->getContext($context, "class")) . " industry-") . $this->getAttribute($this->getAttribute($context["company"], "industry", array()), "id", array()));
                    // line 113
                    echo "                    ";
                }
                // line 114
                echo "                    ";
                if ( !(null === $this->getAttribute($context["company"], "workforce", array()))) {
                    // line 115
                    echo "                        ";
                    $context["class"] = (((isset($context["class"]) ? $context["class"] : $this->getContext($context, "class")) . " workforce-") . $this->getAttribute($context["company"], "workforce", array()));
                    // line 116
                    echo "                    ";
                }
                // line 117
                echo "                    ";
                if (($this->getAttribute($context["company"], "addressId", array()) != 0)) {
                    // line 118
                    echo "                        ";
                    $context["class"] = (((isset($context["class"]) ? $context["class"] : $this->getContext($context, "class")) . " city-") . $this->getAttribute($context["company"], "addressId", array()));
                    // line 119
                    echo "                    ";
                }
                // line 120
                echo "                    <article class=\"tile profile_picture_enabled";
                echo twig_escape_filter($this->env, (isset($context["class"]) ? $context["class"] : $this->getContext($context, "class")), "html", null, true);
                echo "\">
                        <h3><a href=\"\">";
                // line 121
                echo twig_escape_filter($this->env, $this->getAttribute($context["company"], "name", array()), "html", null, true);
                echo "</a></h3>

                        <div class=\"profilepicture search center\"
                             style=\"background-image: url(";
                // line 124
                echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("images/company/"), "html", null, true);
                echo twig_escape_filter($this->env, $this->getAttribute($context["company"], "logoFile", array()), "html", null, true);
                echo ")\">
                            <a href=\"";
                // line 125
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("mmi_company", array("id" => $this->getAttribute($context["company"], "id", array()))), "html", null, true);
                echo "\"></a></div>

                        ";
                // line 127
                if ( !(null === $this->getAttribute($context["company"], "industry", array()))) {
                    // line 128
                    echo "                            <h6>";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["company"], "industry", array()), "name", array()), "html", null, true);
                    echo "</h6>
                        ";
                }
                // line 130
                echo "
                        <p>";
                // line 131
                echo twig_escape_filter($this->env, $this->getAttribute($context["company"], "description", array()), "html", null, true);
                echo "</p>
                    </article>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['company'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 134
            echo "            ";
        } elseif (((isset($context["search_type"]) ? $context["search_type"] : $this->getContext($context, "search_type")) == "projects")) {
            // line 135
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["data_list"]) ? $context["data_list"] : $this->getContext($context, "data_list")));
            foreach ($context['_seq'] as $context["_key"] => $context["project"]) {
                // line 136
                echo "                    <article class=\"tile profile_picture_enabled\">
                        <h3><a href=\"\">";
                // line 137
                echo twig_escape_filter($this->env, $this->getAttribute($context["project"], "name", array()), "html", null, true);
                echo "</a></h3>
                        <a href=\"";
                // line 138
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("mmi_univ_project", array("id" => $this->getAttribute($context["project"], "id", array()))), "html", null, true);
                echo "\"></a></div>

                        <h6>";
                // line 140
                echo twig_escape_filter($this->env, $this->getAttribute($context["project"], "website", array()), "html", null, true);
                echo "</h6>

                        <p>";
                // line 142
                echo twig_escape_filter($this->env, $this->getAttribute($context["project"], "brief", array()), "html", null, true);
                echo "</p>
                    </article>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['project'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 145
            echo "            ";
        } elseif (((isset($context["search_type"]) ? $context["search_type"] : $this->getContext($context, "search_type")) == "internships")) {
            // line 146
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["data_list"]) ? $context["data_list"] : $this->getContext($context, "data_list")));
            foreach ($context['_seq'] as $context["_key"] => $context["internship"]) {
                // line 147
                echo "                    <article class=\"tile profile_picture_enabled\">
                        <h3><a href=\"\">";
                // line 148
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["internship"], "company", array()), "name", array()), "html", null, true);
                echo "</a></h3>
                        <a href=\"#\"></a></div>

                        ";
                // line 151
                if ( !(null === $this->getAttribute($context["internship"], "user", array()))) {
                    // line 152
                    echo "                            <h6>";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["internship"], "user", array()), "firstname", array()), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["internship"], "user", array()), "lastname", array()), "html", null, true);
                    echo "</h6>
                        ";
                }
                // line 154
                echo "
                        <p>";
                // line 155
                echo twig_escape_filter($this->env, $this->getAttribute($context["internship"], "workFunction", array()), "html", null, true);
                echo "</p>
                    </article>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['internship'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 158
            echo "            ";
        } elseif (((isset($context["search_type"]) ? $context["search_type"] : $this->getContext($context, "search_type")) == "degrees")) {
            // line 159
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["data_list"]) ? $context["data_list"] : $this->getContext($context, "data_list")));
            foreach ($context['_seq'] as $context["_key"] => $context["degree"]) {
                // line 160
                echo "                    <article class=\"tile profile_picture_enabled\">
                        <h3><a href=\"\">";
                // line 161
                echo twig_escape_filter($this->env, $this->getAttribute($context["degree"], "name", array()), "html", null, true);
                echo "</a></h3>
                        <a href=\"#\"></a></div>

                        ";
                // line 164
                if ( !(null === $this->getAttribute($context["degree"], "option", array()))) {
                    // line 165
                    echo "                            <h6>";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["degree"], "option", array()), "html", null, true);
                    echo "</h6>
                        ";
                }
                // line 167
                echo "
                        <p>";
                // line 168
                echo twig_escape_filter($this->env, $this->getAttribute($context["degree"], "summary", array()), "html", null, true);
                echo "</p>
                    </article>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['degree'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 171
            echo "            ";
        } elseif (((isset($context["search_type"]) ? $context["search_type"] : $this->getContext($context, "search_type")) == "professions")) {
            // line 172
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["data_list"]) ? $context["data_list"] : $this->getContext($context, "data_list")));
            foreach ($context['_seq'] as $context["_key"] => $context["profession"]) {
                // line 173
                echo "                    <article class=\"tile profile_picture_enabled\">
                        <h3><a href=\"\">";
                // line 174
                echo twig_escape_filter($this->env, $this->getAttribute($context["profession"], "name", array()), "html", null, true);
                echo "</a></h3>
                        <a href=\"#\"></a></div>

                        <p>";
                // line 177
                echo twig_escape_filter($this->env, $this->getAttribute($context["profession"], "description", array()), "html", null, true);
                echo "</p>
                    </article>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['profession'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 180
            echo "            ";
        }
        // line 181
        echo "        </section>
    </main>
";
        
        $__internal_dd00a55855b87dd761cbc9c801572a0b4c397dbc8837df650b672268364f5acf->leave($__internal_dd00a55855b87dd761cbc9c801572a0b4c397dbc8837df650b672268364f5acf_prof);

    }

    // line 185
    public function block_includeBody($context, array $blocks = array())
    {
        $__internal_52b5f963f46bad5af585c62e31efcabf5c7c1e11becdbfa2c89cba3434eecf6f = $this->env->getExtension("native_profiler");
        $__internal_52b5f963f46bad5af585c62e31efcabf5c7c1e11becdbfa2c89cba3434eecf6f->enter($__internal_52b5f963f46bad5af585c62e31efcabf5c7c1e11becdbfa2c89cba3434eecf6f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "includeBody"));

        // line 186
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/caroussel.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 187
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/menu.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 188
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/custom_select.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 189
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("assets/jquery-latest.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 190
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/search.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_52b5f963f46bad5af585c62e31efcabf5c7c1e11becdbfa2c89cba3434eecf6f->leave($__internal_52b5f963f46bad5af585c62e31efcabf5c7c1e11becdbfa2c89cba3434eecf6f_prof);

    }

    public function getTemplateName()
    {
        return "MMIBundle:Default:search.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  647 => 190,  643 => 189,  639 => 188,  635 => 187,  630 => 186,  624 => 185,  615 => 181,  612 => 180,  603 => 177,  597 => 174,  594 => 173,  589 => 172,  586 => 171,  577 => 168,  574 => 167,  568 => 165,  566 => 164,  560 => 161,  557 => 160,  552 => 159,  549 => 158,  540 => 155,  537 => 154,  529 => 152,  527 => 151,  521 => 148,  518 => 147,  513 => 146,  510 => 145,  501 => 142,  496 => 140,  491 => 138,  487 => 137,  484 => 136,  479 => 135,  476 => 134,  467 => 131,  464 => 130,  458 => 128,  456 => 127,  451 => 125,  446 => 124,  440 => 121,  435 => 120,  432 => 119,  429 => 118,  426 => 117,  423 => 116,  420 => 115,  417 => 114,  414 => 113,  411 => 112,  408 => 111,  405 => 110,  400 => 109,  397 => 108,  388 => 105,  385 => 104,  379 => 102,  377 => 101,  373 => 100,  368 => 99,  360 => 96,  355 => 95,  352 => 94,  349 => 93,  346 => 92,  343 => 91,  340 => 90,  337 => 89,  334 => 88,  331 => 87,  328 => 86,  325 => 85,  320 => 84,  318 => 83,  312 => 80,  308 => 78,  304 => 76,  293 => 74,  289 => 73,  284 => 70,  273 => 68,  269 => 67,  264 => 64,  253 => 62,  249 => 61,  245 => 59,  243 => 58,  240 => 57,  229 => 55,  225 => 54,  220 => 51,  209 => 49,  205 => 48,  200 => 45,  189 => 43,  185 => 42,  181 => 40,  179 => 39,  169 => 34,  165 => 33,  156 => 31,  150 => 30,  146 => 29,  140 => 28,  136 => 27,  127 => 25,  121 => 24,  117 => 23,  113 => 21,  107 => 20,  95 => 13,  89 => 12,  75 => 10,  63 => 8,  52 => 5,  40 => 3,  11 => 1,);
    }
}
/* {% extends "MMIBundle::template.html.twig" %}*/
/* */
/* {% block title %}Rechercher{% endblock %}*/
/* */
/* {% block includeHead %}*/
/* {% endblock %}*/
/* */
/* {% block bodyClass %}search{% endblock %}*/
/* */
/* {% block header %}style="background-image: url({{ asset("ui_img/rechercher_01.jpeg") }})"{% endblock %}*/
/* */
/* {% block headerExtended %}*/
/*     <form action="" class="search">*/
/*         <h3 class="white">Comment peut-on vous aider ?</h3>*/
/*         <input type="text" placeholder="">*/
/*         <button type="button">Rechercher</button>*/
/*     </form>*/
/* {% endblock %}*/
/* */
/* {% block content %}*/
/*     <nav class="search">*/
/*         <ul>*/
/*             <li><a href="{{ path('mmi_search') }}"*/
/*                    {% if search_type == 'internships' %}class="active"{% endif %}>Stage</a></li>*/
/*             <li><a href="{{ path('mmi_search') }}/projects" {% if search_type == 'projects' %}class="active"{% endif %}>Projet*/
/*                     universitaire</a></li>*/
/*             <li><a href="{{ path('mmi_search') }}/students"*/
/*                    {% if search_type == 'students' %}class="active"{% endif %}>Profil</a></li>*/
/*             <li><a href="{{ path('mmi_search') }}/companies"*/
/*                    {% if search_type == 'companies' %}class="active"{% endif %}>Entreprise</a></li>*/
/*             <li><a href="{{ path('mmi_search') }}/degrees" {% if search_type == 'degrees' %}class="active"{% endif %}>Formation</a>*/
/*             </li>*/
/*             <li><a href="{{ path('mmi_search') }}/professions"*/
/*                    {% if search_type == 'professions' %}class="active"{% endif %}>Métier</a></li>*/
/*         </ul>*/
/*     </nav>*/
/*     <main>*/
/*         <form action="" class="filter">*/
/*             {% if search_type == 'students' %}*/
/*                 <select class="promo-filter" placeholder="Promo">*/
/*                     <option value="ras">Promo</option>*/
/*                     {% for promo in listPromos %}*/
/*                         <option value="{{ promo.id }}">{{ promo.year }}</option>*/
/*                     {% endfor %}*/
/*                 </select>*/
/*                 <select class="city-filter" placeholder="Ville">*/
/*                     <option value="ras">Ville</option>*/
/*                     {% for city in listCity %}*/
/*                         <option value="{{ city.id }}">{{ city.name }}</option>*/
/*                     {% endfor %}*/
/*                 </select>*/
/*                 <select class="profile-filter" placeholder="Profil">*/
/*                     <option value="ras">Profil</option>*/
/*                     {% for profession in listProfession %}*/
/*                         <option value="{{ profession.id }}">{{ profession.name }}</option>*/
/*                     {% endfor %}*/
/*                 </select>*/
/*             {% elseif search_type == 'companies' %}*/
/*                 <select class="industry-filter" placeholder="Secteur">*/
/*                     <option value="ras">Secteur</option>*/
/*                     {% for industry in listIndustry %}*/
/*                         <option value="{{ industry.id }}">{{ industry.name }}</option>*/
/*                     {% endfor %}*/
/*                 </select>*/
/*                 <select class="workforce-filter" placeholder="Effectif">*/
/*                     <option value="ras">Effectif</option>*/
/*                     {% for city in listCity %}*/
/*                         <option value="{{ city.id }}">{{ city.name }}</option>*/
/*                     {% endfor %}*/
/*                 </select>*/
/*                 <select class="city-filter" placeholder="Ville">*/
/*                     <option value="ras">Ville</option>*/
/*                     {% for city in listCity %}*/
/*                         <option value="{{ city.id }}">{{ city.name }}</option>*/
/*                     {% endfor %}*/
/*                 </select>*/
/*             {% endif %}*/
/*         </form>*/
/* */
/*         <input type="hidden" id="search_type" value="{{ search_type }}">*/
/* */
/*         <section class="tile_wrapper wrapper">*/
/*             {% if search_type == 'students' %}*/
/*                 {% for user in data_list %}*/
/*                     {% set class = '' %}*/
/*                     {% if user.promo is not null %}*/
/*                         {% set class = class ~ ' promo-' ~ user.promo.id %}*/
/*                     {% endif %}*/
/*                     {% if user.city is not null %}*/
/*                         {% set class = class ~ ' city-' ~ user.city.id %}*/
/*                     {% endif %}*/
/*                     {% if user.profileId != 0 %}*/
/*                         {% set class = class ~ ' profile-' ~ user.profileId %}*/
/*                     {% endif %}*/
/*                     <article class="tile profile_picture_enabled{{ class }}">*/
/*                         <h3><a href="">{{ user.firstname }} {{ user.lastname }}</a></h3>*/
/* */
/*                         <div class="profilepicture search center"*/
/*                              style="background-image: url({{ asset("images/profil_picture/") }}{{ user.imageProfilFile }})">*/
/*                             <a href="{{ path('mmi_student', { id: user.id }) }}"></a></div>*/
/*                         {% if (user.profession is not null) %}*/
/*                             <h6>{{ user.profession.name }}</h6>*/
/*                         {% endif %}*/
/* */
/*                         <p>{{ user.description }}</p>*/
/*                     </article>*/
/*                 {% endfor %}*/
/*             {% elseif search_type == 'companies' %}*/
/*                 {% for company in data_list %}*/
/*                     {% set class = '' %}*/
/*                     {% if company.industry is not null %}*/
/*                         {% set class = class ~ ' industry-' ~ company.industry.id %}*/
/*                     {% endif %}*/
/*                     {% if company.workforce is not null %}*/
/*                         {% set class = class ~ ' workforce-' ~ company.workforce %}*/
/*                     {% endif %}*/
/*                     {% if company.addressId != 0 %}*/
/*                         {% set class = class ~ ' city-' ~ company.addressId %}*/
/*                     {% endif %}*/
/*                     <article class="tile profile_picture_enabled{{ class }}">*/
/*                         <h3><a href="">{{ company.name }}</a></h3>*/
/* */
/*                         <div class="profilepicture search center"*/
/*                              style="background-image: url({{ asset("images/company/") }}{{ company.logoFile }})">*/
/*                             <a href="{{ path('mmi_company', { id: company.id }) }}"></a></div>*/
/* */
/*                         {% if company.industry is not null %}*/
/*                             <h6>{{ company.industry.name }}</h6>*/
/*                         {% endif %}*/
/* */
/*                         <p>{{ company.description }}</p>*/
/*                     </article>*/
/*                 {% endfor %}*/
/*             {% elseif search_type == 'projects' %}*/
/*                 {% for project in data_list %}*/
/*                     <article class="tile profile_picture_enabled">*/
/*                         <h3><a href="">{{ project.name }}</a></h3>*/
/*                         <a href="{{ path('mmi_univ_project', { id: project.id }) }}"></a></div>*/
/* */
/*                         <h6>{{ project.website }}</h6>*/
/* */
/*                         <p>{{ project.brief }}</p>*/
/*                     </article>*/
/*                 {% endfor %}*/
/*             {% elseif search_type == 'internships' %}*/
/*                 {% for internship in data_list %}*/
/*                     <article class="tile profile_picture_enabled">*/
/*                         <h3><a href="">{{ internship.company.name }}</a></h3>*/
/*                         <a href="#"></a></div>*/
/* */
/*                         {% if internship.user is not null %}*/
/*                             <h6>{{ internship.user.firstname }} {{ internship.user.lastname }}</h6>*/
/*                         {% endif %}*/
/* */
/*                         <p>{{ internship.workFunction }}</p>*/
/*                     </article>*/
/*                 {% endfor %}*/
/*             {% elseif search_type == 'degrees' %}*/
/*                 {% for degree in data_list %}*/
/*                     <article class="tile profile_picture_enabled">*/
/*                         <h3><a href="">{{ degree.name }}</a></h3>*/
/*                         <a href="#"></a></div>*/
/* */
/*                         {% if degree.option is not null %}*/
/*                             <h6>{{ degree.option }}</h6>*/
/*                         {% endif %}*/
/* */
/*                         <p>{{ degree.summary }}</p>*/
/*                     </article>*/
/*                 {% endfor %}*/
/*             {% elseif search_type == 'professions' %}*/
/*                 {% for profession in data_list %}*/
/*                     <article class="tile profile_picture_enabled">*/
/*                         <h3><a href="">{{ profession.name }}</a></h3>*/
/*                         <a href="#"></a></div>*/
/* */
/*                         <p>{{ profession.description }}</p>*/
/*                     </article>*/
/*                 {% endfor %}*/
/*             {% endif %}*/
/*         </section>*/
/*     </main>*/
/* {% endblock %}*/
/* */
/* {% block includeBody %}*/
/*     <script src="{{ asset("js/caroussel.js") }}"></script>*/
/*     <script src="{{ asset("js/menu.js") }}"></script>*/
/*     <script src="{{ asset("js/custom_select.js") }}"></script>*/
/*     <script src="{{ asset("assets/jquery-latest.min.js") }}"></script>*/
/*     <script src="{{ asset("js/search.js") }}"></script>*/
/* {% endblock %}*/
/* */
