<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_24f7f75ad12574cc581a1c75d46329f6d5f3b1c20459cf49e46e2f1b86868555 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("TwigBundle::layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "TwigBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f06fa078a5158ada4c8388d3c009deb37708af3d2581e0a8fb3b73b5afc8c775 = $this->env->getExtension("native_profiler");
        $__internal_f06fa078a5158ada4c8388d3c009deb37708af3d2581e0a8fb3b73b5afc8c775->enter($__internal_f06fa078a5158ada4c8388d3c009deb37708af3d2581e0a8fb3b73b5afc8c775_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f06fa078a5158ada4c8388d3c009deb37708af3d2581e0a8fb3b73b5afc8c775->leave($__internal_f06fa078a5158ada4c8388d3c009deb37708af3d2581e0a8fb3b73b5afc8c775_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_4832d1937cba94d6b04184f993fb515048b3673992ca8698e8d5a50562a8fbd9 = $this->env->getExtension("native_profiler");
        $__internal_4832d1937cba94d6b04184f993fb515048b3673992ca8698e8d5a50562a8fbd9->enter($__internal_4832d1937cba94d6b04184f993fb515048b3673992ca8698e8d5a50562a8fbd9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_4832d1937cba94d6b04184f993fb515048b3673992ca8698e8d5a50562a8fbd9->leave($__internal_4832d1937cba94d6b04184f993fb515048b3673992ca8698e8d5a50562a8fbd9_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_1bdafcb8d758f921b20bda90a1add9f518ec726c849f877f129d27685c64ced8 = $this->env->getExtension("native_profiler");
        $__internal_1bdafcb8d758f921b20bda90a1add9f518ec726c849f877f129d27685c64ced8->enter($__internal_1bdafcb8d758f921b20bda90a1add9f518ec726c849f877f129d27685c64ced8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_1bdafcb8d758f921b20bda90a1add9f518ec726c849f877f129d27685c64ced8->leave($__internal_1bdafcb8d758f921b20bda90a1add9f518ec726c849f877f129d27685c64ced8_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_52e4bb65333536036ad3200ef14d7fc0f09e4a90cac57a0cd34977770e9d42e1 = $this->env->getExtension("native_profiler");
        $__internal_52e4bb65333536036ad3200ef14d7fc0f09e4a90cac57a0cd34977770e9d42e1->enter($__internal_52e4bb65333536036ad3200ef14d7fc0f09e4a90cac57a0cd34977770e9d42e1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("TwigBundle:Exception:exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 12)->display($context);
        
        $__internal_52e4bb65333536036ad3200ef14d7fc0f09e4a90cac57a0cd34977770e9d42e1->leave($__internal_52e4bb65333536036ad3200ef14d7fc0f09e4a90cac57a0cd34977770e9d42e1_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'TwigBundle::layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include 'TwigBundle:Exception:exception.html.twig' %}*/
/* {% endblock %}*/
/* */
