<?php

/* MMIBundle:Utilisateur:student.html.twig */
class __TwigTemplate_99bb4b0b4f6a0cce51409710a04e98cb19ac170cc7d2383e8f6801fb3e05668a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("MMIBundle::template.html.twig", "MMIBundle:Utilisateur:student.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'includeHead' => array($this, 'block_includeHead'),
            'bodyClass' => array($this, 'block_bodyClass'),
            'header' => array($this, 'block_header'),
            'headerExtended' => array($this, 'block_headerExtended'),
            'content' => array($this, 'block_content'),
            'includeBody' => array($this, 'block_includeBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "MMIBundle::template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3c4bb0707aa37067d9c306fccc29fbbb6ef80dd8899db2e58fd16f366b2abf00 = $this->env->getExtension("native_profiler");
        $__internal_3c4bb0707aa37067d9c306fccc29fbbb6ef80dd8899db2e58fd16f366b2abf00->enter($__internal_3c4bb0707aa37067d9c306fccc29fbbb6ef80dd8899db2e58fd16f366b2abf00_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MMIBundle:Utilisateur:student.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3c4bb0707aa37067d9c306fccc29fbbb6ef80dd8899db2e58fd16f366b2abf00->leave($__internal_3c4bb0707aa37067d9c306fccc29fbbb6ef80dd8899db2e58fd16f366b2abf00_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_d3e99bef15a7698087f0c058e689f9e44d47fdf4877c67ad21400a99c7441afb = $this->env->getExtension("native_profiler");
        $__internal_d3e99bef15a7698087f0c058e689f9e44d47fdf4877c67ad21400a99c7441afb->enter($__internal_d3e99bef15a7698087f0c058e689f9e44d47fdf4877c67ad21400a99c7441afb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Profil de ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["student"]) ? $context["student"] : $this->getContext($context, "student")), "firstname", array()), "html", null, true);
        
        $__internal_d3e99bef15a7698087f0c058e689f9e44d47fdf4877c67ad21400a99c7441afb->leave($__internal_d3e99bef15a7698087f0c058e689f9e44d47fdf4877c67ad21400a99c7441afb_prof);

    }

    // line 5
    public function block_includeHead($context, array $blocks = array())
    {
        $__internal_c913c032873886dbb1f00dc016d6cdf4518799777b11fa586235f9a8d12dafa5 = $this->env->getExtension("native_profiler");
        $__internal_c913c032873886dbb1f00dc016d6cdf4518799777b11fa586235f9a8d12dafa5->enter($__internal_c913c032873886dbb1f00dc016d6cdf4518799777b11fa586235f9a8d12dafa5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "includeHead"));

        // line 6
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("assets/jquery-ui-1.11.4/jquery-ui.min.css"), "html", null, true);
        echo "\">
    <script src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("assets/jquery.scrollTo.min.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_c913c032873886dbb1f00dc016d6cdf4518799777b11fa586235f9a8d12dafa5->leave($__internal_c913c032873886dbb1f00dc016d6cdf4518799777b11fa586235f9a8d12dafa5_prof);

    }

    // line 10
    public function block_bodyClass($context, array $blocks = array())
    {
        $__internal_43abb41b80f1cf40417e76efab566efcdaf256e16cc039774a20d54e5c9de17e = $this->env->getExtension("native_profiler");
        $__internal_43abb41b80f1cf40417e76efab566efcdaf256e16cc039774a20d54e5c9de17e->enter($__internal_43abb41b80f1cf40417e76efab566efcdaf256e16cc039774a20d54e5c9de17e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "bodyClass"));

        echo "student";
        
        $__internal_43abb41b80f1cf40417e76efab566efcdaf256e16cc039774a20d54e5c9de17e->leave($__internal_43abb41b80f1cf40417e76efab566efcdaf256e16cc039774a20d54e5c9de17e_prof);

    }

    // line 12
    public function block_header($context, array $blocks = array())
    {
        $__internal_4786c3d5f3fcff3d46f0cb364ec19419c603c779945c67624c1c29451081edae = $this->env->getExtension("native_profiler");
        $__internal_4786c3d5f3fcff3d46f0cb364ec19419c603c779945c67624c1c29451081edae->enter($__internal_4786c3d5f3fcff3d46f0cb364ec19419c603c779945c67624c1c29451081edae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 13
        echo "    style=\"background-image: linear-gradient(transparent, white), url(";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("images/profil_cover/"), "html", null, true);
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["student"]) ? $context["student"] : $this->getContext($context, "student")), "imageCouvFile", array()), "html", null, true);
        echo ")\">
";
        
        $__internal_4786c3d5f3fcff3d46f0cb364ec19419c603c779945c67624c1c29451081edae->leave($__internal_4786c3d5f3fcff3d46f0cb364ec19419c603c779945c67624c1c29451081edae_prof);

    }

    // line 16
    public function block_headerExtended($context, array $blocks = array())
    {
        $__internal_23d3754fedd6c4f248a8d2d9653a4758e3fa53073e71ecd9692c7b8049ff0da6 = $this->env->getExtension("native_profiler");
        $__internal_23d3754fedd6c4f248a8d2d9653a4758e3fa53073e71ecd9692c7b8049ff0da6->enter($__internal_23d3754fedd6c4f248a8d2d9653a4758e3fa53073e71ecd9692c7b8049ff0da6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "headerExtended"));

        // line 17
        echo "    <div class=\"profil_header\">
        <img src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("images/profil_picture/"), "html", null, true);
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["student"]) ? $context["student"] : $this->getContext($context, "student")), "imageProfilFile", array()), "html", null, true);
        echo "\" class=\"profilepicture profil\">

        <h1>";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["student"]) ? $context["student"] : $this->getContext($context, "student")), "firstname", array()), "html", null, true);
        echo " <span class=\"blue\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["student"]) ? $context["student"] : $this->getContext($context, "student")), "lastname", array()), "html", null, true);
        echo "</span></h1>

        ";
        // line 22
        if ( !(null === $this->getAttribute((isset($context["student"]) ? $context["student"] : $this->getContext($context, "student")), "profession", array()))) {
            // line 23
            echo "            <h2>";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["student"]) ? $context["student"] : $this->getContext($context, "student")), "profession", array()), "name", array()), "html", null, true);
            echo "</h2>
        ";
        }
        // line 25
        echo "
        ";
        // line 26
        if ( !(null === $this->getAttribute((isset($context["student"]) ? $context["student"] : $this->getContext($context, "student")), "promo", array()))) {
            // line 27
            echo "            <p>Diplomé en ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["student"]) ? $context["student"] : $this->getContext($context, "student")), "promo", array()), "year", array()), "html", null, true);
            echo "</p>
        ";
        }
        // line 29
        echo "        <a href=\"";
        echo $this->env->getExtension('routing')->getPath("mmi_edit_student");
        echo "\">Modifier mes informations <i class=\"fa fa-pencil\"></i>
        </a>
    </div>
";
        
        $__internal_23d3754fedd6c4f248a8d2d9653a4758e3fa53073e71ecd9692c7b8049ff0da6->leave($__internal_23d3754fedd6c4f248a8d2d9653a4758e3fa53073e71ecd9692c7b8049ff0da6_prof);

    }

    // line 34
    public function block_content($context, array $blocks = array())
    {
        $__internal_fa98662072683b038a12ff07760837a905a8fb8a466ed12a0dcfa04bb32167bc = $this->env->getExtension("native_profiler");
        $__internal_fa98662072683b038a12ff07760837a905a8fb8a466ed12a0dcfa04bb32167bc->enter($__internal_fa98662072683b038a12ff07760837a905a8fb8a466ed12a0dcfa04bb32167bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 35
        echo "    <main>
        <section class=\"presentation\">
            <!--<div>
                <h4>Lier mon profil :</h4>

                <p>Ton profil se remplira tout seul et tu pourras te connecter en clic !</p>
                <button class=\"big white\" type=\"button\" style=\"margin-top: 20px; margin-right: 10px\">Facebook</button>
                <button class=\"big white\" type=\"button\" style=\"margin-top: 20px\">LinkedIn</button>
            </div>-->
            <div>
                <h4>Infos personnelles</h4>

                <p>
                    Âge : <span>";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(twig_date_converter($this->env, "now"), "diff", array(0 => twig_date_converter($this->env, $this->getAttribute((isset($context["student"]) ? $context["student"] : $this->getContext($context, "student")), "birthday", array()))), "method"), "y", array()), "html", null, true);
        echo "</span><br>
                    Habite à <span>";
        // line 49
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["student"]) ? $context["student"] : $this->getContext($context, "student")), "city", array()), "name", array()), "html", null, true);
        echo "</span><br>
                    Email : <span>";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["student"]) ? $context["student"] : $this->getContext($context, "student")), "email", array()), "html", null, true);
        echo "</span><br>
                    ";
        // line 51
        if ( !(null === $this->getAttribute((isset($context["student"]) ? $context["student"] : $this->getContext($context, "student")), "telephone", array()))) {
            // line 52
            echo "                        Téléphone : <span>";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["student"]) ? $context["student"] : $this->getContext($context, "student")), "telephone", array()), "html", null, true);
            echo "</span><br>
                    ";
        }
        // line 54
        echo "                    ";
        if ( !(null === $this->getAttribute((isset($context["student"]) ? $context["student"] : $this->getContext($context, "student")), "urlPortfolio", array()))) {
            // line 55
            echo "                        Portfolio : <span>";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["student"]) ? $context["student"] : $this->getContext($context, "student")), "urlPortfolio", array()), "html", null, true);
            echo "</span><br>
                    ";
        }
        // line 57
        echo "                    ";
        if ( !(null === $this->getAttribute((isset($context["student"]) ? $context["student"] : $this->getContext($context, "student")), "urlBlog", array()))) {
            // line 58
            echo "                        Blog : <span>";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["student"]) ? $context["student"] : $this->getContext($context, "student")), "urlBlog", array()), "html", null, true);
            echo "</span><br>
                    ";
        }
        // line 60
        echo "                    ";
        if ( !(null === $this->getAttribute((isset($context["student"]) ? $context["student"] : $this->getContext($context, "student")), "cvFile", array()))) {
            // line 61
            echo "                        CV : <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("cv"), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["student"]) ? $context["student"] : $this->getContext($context, "student")), "cvFile", array()), "html", null, true);
            echo "\">Voir le CV</a>
                    ";
        }
        // line 63
        echo "                </p>
            </div>
            <div>
                <h4>Social</h4>

                <p>
                    ";
        // line 69
        if (($this->getAttribute((isset($context["student"]) ? $context["student"] : $this->getContext($context, "student")), "urlFacebook", array()) != null)) {
            // line 70
            echo "                        <a href=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["student"]) ? $context["student"] : $this->getContext($context, "student")), "urlFacebook", array()), "html", null, true);
            echo "\">Facebook</a><br>
                    ";
        }
        // line 72
        echo "                    ";
        if (($this->getAttribute((isset($context["student"]) ? $context["student"] : $this->getContext($context, "student")), "urlLinkedin", array()) != null)) {
            // line 73
            echo "                        <a href=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["student"]) ? $context["student"] : $this->getContext($context, "student")), "urlLinkedin", array()), "html", null, true);
            echo "\">LinkedIn</a><br>
                    ";
        }
        // line 75
        echo "                    ";
        if (($this->getAttribute((isset($context["student"]) ? $context["student"] : $this->getContext($context, "student")), "urlTwitter", array()) != null)) {
            // line 76
            echo "                        <a href=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["student"]) ? $context["student"] : $this->getContext($context, "student")), "urlTwitter", array()), "html", null, true);
            echo "\">Twitter</a><br>
                    ";
        }
        // line 78
        echo "                    ";
        if (($this->getAttribute((isset($context["student"]) ? $context["student"] : $this->getContext($context, "student")), "urlTumblr", array()) != null)) {
            // line 79
            echo "                        <a href=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["student"]) ? $context["student"] : $this->getContext($context, "student")), "urlTumblr", array()), "html", null, true);
            echo "\">Tumblr</a><br>
                    ";
        }
        // line 81
        echo "                    ";
        if (($this->getAttribute((isset($context["student"]) ? $context["student"] : $this->getContext($context, "student")), "urlInstagram", array()) != null)) {
            // line 82
            echo "                        <a href=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["student"]) ? $context["student"] : $this->getContext($context, "student")), "urlInstagram", array()), "html", null, true);
            echo "\">Instagram</a><br>
                    ";
        }
        // line 84
        echo "                </p>
            </div>
        </section>
        <section class=\"parcours\">
            <h1>Son parcours</h1>

            ";
        // line 90
        if ((twig_length_filter($this->env, (isset($context["after"]) ? $context["after"] : $this->getContext($context, "after"))) > 0)) {
            // line 91
            echo "                <h2>Après MMI</h2>
                <ul>
                    ";
            // line 93
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["after"]) ? $context["after"] : $this->getContext($context, "after")));
            foreach ($context['_seq'] as $context["_key"] => $context["experience"]) {
                // line 94
                echo "                        <li class=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["experience"], "type", array()), "html", null, true);
                echo "_";
                echo twig_escape_filter($this->env, $this->getAttribute($context["experience"], "id", array()), "html", null, true);
                echo "\">
                            <div>
                                ";
                // line 96
                echo twig_escape_filter($this->env, $this->getAttribute($context["experience"], "from", array()), "html", null, true);
                echo "<!--
                            ";
                // line 97
                if ((($this->getAttribute($context["experience"], "to", array()) != null) && ($this->getAttribute($context["experience"], "to", array()) != $this->getAttribute($context["experience"], "from", array())))) {
                    // line 98
                    echo "                                -->-";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["experience"], "to", array()), "html", null, true);
                    echo "
                                ";
                }
                // line 100
                echo "                                <!-- -->
                            </div>
                            ";
                // line 102
                if (($this->getAttribute($context["experience"], "image", array(), "any", true, true) && ($this->getAttribute($context["experience"], "image", array()) != ""))) {
                    // line 103
                    echo "                                <img src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("images/company/"), "html", null, true);
                    echo twig_escape_filter($this->env, $this->getAttribute($context["experience"], "image", array()), "html", null, true);
                    echo "\" class=\"picture\">
                            ";
                }
                // line 105
                echo "
                            <div>
                                <h3>";
                // line 107
                echo twig_escape_filter($this->env, $this->getAttribute($context["experience"], "name", array()), "html", null, true);
                echo "<span class=\"experience_category\"> | ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["experience"], "display", array()), "html", null, true);
                echo "</span></h3>

                                ";
                // line 109
                if ($this->getAttribute($context["experience"], "desc", array(), "any", true, true)) {
                    // line 110
                    echo "                                    <p>";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["experience"], "desc", array()), "html", null, true);
                    echo "
                                        <i class=\"fa fa-pencil targeted\" data-target=\"#edit\"></i>
                                    </p>
                                ";
                }
                // line 114
                echo "                            </div>
                        </li>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['experience'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 117
            echo "                </ul>
            ";
        }
        // line 119
        echo "            ";
        if ((twig_length_filter($this->env, (isset($context["during"]) ? $context["during"] : $this->getContext($context, "during"))) > 0)) {
            // line 120
            echo "                <h2>Pendant MMI</h2>
                <ul>
                    ";
            // line 122
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["during"]) ? $context["during"] : $this->getContext($context, "during")));
            foreach ($context['_seq'] as $context["_key"] => $context["experience"]) {
                // line 123
                echo "                        <li class=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["experience"], "type", array()), "html", null, true);
                echo "_";
                echo twig_escape_filter($this->env, $this->getAttribute($context["experience"], "id", array()), "html", null, true);
                echo "\">
                            <div>
                                ";
                // line 125
                echo twig_escape_filter($this->env, $this->getAttribute($context["experience"], "from", array()), "html", null, true);
                echo "<!--
                            ";
                // line 126
                if ((($this->getAttribute($context["experience"], "to", array()) != null) && ($this->getAttribute($context["experience"], "to", array()) != $this->getAttribute($context["experience"], "from", array())))) {
                    // line 127
                    echo "                                -->-";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["experience"], "to", array()), "html", null, true);
                    echo "
                                ";
                }
                // line 129
                echo "                                <!-- -->
                            </div>
                            ";
                // line 131
                if (($this->getAttribute($context["experience"], "image", array(), "any", true, true) && ($this->getAttribute($context["experience"], "image", array()) != ""))) {
                    // line 132
                    echo "                                <img src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("images/company/"), "html", null, true);
                    echo twig_escape_filter($this->env, $this->getAttribute($context["experience"], "image", array()), "html", null, true);
                    echo "\" class=\"picture\">
                            ";
                }
                // line 134
                echo "
                            <div>
                                <h3>";
                // line 136
                echo twig_escape_filter($this->env, $this->getAttribute($context["experience"], "name", array()), "html", null, true);
                echo "<span class=\"experience_category\"> | ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["experience"], "display", array()), "html", null, true);
                echo "</span></h3>

                                ";
                // line 138
                if ($this->getAttribute($context["experience"], "desc", array(), "any", true, true)) {
                    // line 139
                    echo "                                    <p>";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["experience"], "desc", array()), "html", null, true);
                    echo "
                                        <i class=\"fa fa-pencil targeted\" data-target=\"#edit\"></i>
                                    </p>
                                ";
                }
                // line 143
                echo "                            </div>
                        </li>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['experience'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 146
            echo "                </ul>
            ";
        }
        // line 148
        echo "            ";
        if ((twig_length_filter($this->env, (isset($context["before"]) ? $context["before"] : $this->getContext($context, "before"))) > 0)) {
            // line 149
            echo "                <h2>Avant MMI</h2>
                <ul>
                    ";
            // line 151
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["before"]) ? $context["before"] : $this->getContext($context, "before")));
            foreach ($context['_seq'] as $context["_key"] => $context["experience"]) {
                // line 152
                echo "                        <li class=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["experience"], "type", array()), "html", null, true);
                echo "_";
                echo twig_escape_filter($this->env, $this->getAttribute($context["experience"], "id", array()), "html", null, true);
                echo "\">
                            <div>
                                ";
                // line 154
                echo twig_escape_filter($this->env, $this->getAttribute($context["experience"], "from", array()), "html", null, true);
                echo "<!--
                            ";
                // line 155
                if ((($this->getAttribute($context["experience"], "to", array()) != null) && ($this->getAttribute($context["experience"], "to", array()) != $this->getAttribute($context["experience"], "from", array())))) {
                    // line 156
                    echo "                                -->-";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["experience"], "to", array()), "html", null, true);
                    echo "
                                ";
                }
                // line 158
                echo "                                <!-- -->
                            </div>
                            ";
                // line 160
                if (($this->getAttribute($context["experience"], "image", array(), "any", true, true) && ($this->getAttribute($context["experience"], "image", array()) != ""))) {
                    // line 161
                    echo "                                <img src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("images/company/"), "html", null, true);
                    echo twig_escape_filter($this->env, $this->getAttribute($context["experience"], "image", array()), "html", null, true);
                    echo "\" class=\"picture\">
                            ";
                }
                // line 163
                echo "
                            <div>
                                <h3>";
                // line 165
                echo twig_escape_filter($this->env, $this->getAttribute($context["experience"], "name", array()), "html", null, true);
                echo "<span class=\"experience_category\"> | ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["experience"], "display", array()), "html", null, true);
                echo "</span></h3>

                                ";
                // line 167
                if ($this->getAttribute($context["experience"], "desc", array(), "any", true, true)) {
                    // line 168
                    echo "                                    <p>";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["experience"], "desc", array()), "html", null, true);
                    echo "
                                        <i class=\"fa fa-pencil targeted\" data-target=\"#edit\"></i>
                                    </p>
                                ";
                }
                // line 172
                echo "                            </div>
                        </li>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['experience'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 175
            echo "                </ul>
            ";
        }
        // line 177
        echo "        </section>

        ";
        // line 179
        if (( !(null === (isset($context["account"]) ? $context["account"] : $this->getContext($context, "account"))) && ($this->getAttribute((isset($context["student"]) ? $context["student"] : $this->getContext($context, "student")), "id", array()) == $this->getAttribute((isset($context["account"]) ? $context["account"] : $this->getContext($context, "account")), "id", array())))) {
            // line 180
            echo "            <section class=\"edit\" id=\"edit\">

                <h2>Tenez-nous au courant !</h2>
                <h4>Ajoutez ou modifer vos expériences</h4>
                <select class=\"experiences\">
                    <option value=\"add\">Ajouter</option>
                    ";
            // line 186
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["experiences"]) ? $context["experiences"] : $this->getContext($context, "experiences")));
            foreach ($context['_seq'] as $context["_key"] => $context["experience"]) {
                // line 187
                echo "                        <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["experience"], "type", array()), "html", null, true);
                echo ":";
                echo twig_escape_filter($this->env, $this->getAttribute($context["experience"], "id", array()), "html", null, true);
                echo "\"
                                class=\"";
                // line 188
                echo twig_escape_filter($this->env, $this->getAttribute($context["experience"], "type", array()), "html", null, true);
                echo "_";
                echo twig_escape_filter($this->env, $this->getAttribute($context["experience"], "id", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["experience"], "name", array()), "html", null, true);
                echo "</option>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['experience'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 190
            echo "                </select>
                <select class=\"type\">
                    <option value=\"Job\">Emploi</option>
                    <option value=\"Degree\">Formation</option>
                    <option value=\"Project\">Projet</option>
                </select>

                <!-- Emploi -->
                ";
            // line 198
            echo             $this->env->getExtension('form')->renderer->renderBlock((isset($context["addUserJob"]) ? $context["addUserJob"] : $this->getContext($context, "addUserJob")), 'form_start', array("attr" => array("class" => "experience Job")));
            // line 200
            echo "
                <p>Nom de votre profession :</p>
                ";
            // line 202
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["addUserJob"]) ? $context["addUserJob"] : $this->getContext($context, "addUserJob")), "profession_id", array()), 'widget', array("attr" => array("class" => "big")));
            // line 204
            echo "
                <a data-toggle=\"modal\" data-target=\"#addProfessionModal\">Il n'est pas dans la liste ?</a>

                <p>Nom de l'entreprise :</p>
                ";
            // line 208
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["addUserJob"]) ? $context["addUserJob"] : $this->getContext($context, "addUserJob")), "company_id", array()), 'widget', array("attr" => array("class" => "big")));
            // line 210
            echo "
                <a data-toggle=\"modal\" data-target=\"#addCompanyModal\">Elle n'est pas dans la liste ?</a>

                <p>Période</p>
                De
                ";
            // line 215
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["addUserJob"]) ? $context["addUserJob"] : $this->getContext($context, "addUserJob")), "yearFrom", array()), 'widget', array("attr" => array("class" => "")));
            // line 217
            echo "
                à
                ";
            // line 219
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["addUserJob"]) ? $context["addUserJob"] : $this->getContext($context, "addUserJob")), "yearTo", array()), 'widget', array("attr" => array("class" => "")));
            // line 221
            echo "

                <p>Description de votre poste :</p>
                ";
            // line 224
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["addUserJob"]) ? $context["addUserJob"] : $this->getContext($context, "addUserJob")), "description", array()), 'widget', array("attr" => array("placeholder" => "Quelques mots suffisent")));
            // line 227
            echo "

                <button type=\"submit\" name=\"submitJob\" class=\"big addUpdate\">Ajouter</button>
                <button type=\"button\" class=\"big delete\">Supprimer</button>
                ";
            // line 231
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["addUserJob"]) ? $context["addUserJob"] : $this->getContext($context, "addUserJob")), 'rest');
            echo "
                ";
            // line 232
            echo             $this->env->getExtension('form')->renderer->renderBlock((isset($context["addUserJob"]) ? $context["addUserJob"] : $this->getContext($context, "addUserJob")), 'form_end');
            echo "

                <!-- Formation -->
                ";
            // line 235
            echo             $this->env->getExtension('form')->renderer->renderBlock((isset($context["addUserDegree"]) ? $context["addUserDegree"] : $this->getContext($context, "addUserDegree")), 'form_start', array("attr" => array("class" => "experience Degree")));
            // line 237
            echo "
                <p>Nom de votre formation :</p>
                ";
            // line 239
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["addUserDegree"]) ? $context["addUserDegree"] : $this->getContext($context, "addUserDegree")), "degree_id", array()), 'widget', array("attr" => array("class" => "big")));
            // line 241
            echo "
                <a data-toggle=\"modal\" data-target=\"#addDegreeModal\">Elle n'est pas dans la liste ?</a>

                <p>Période</p>

                <div>
                    De
                    ";
            // line 248
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["addUserDegree"]) ? $context["addUserDegree"] : $this->getContext($context, "addUserDegree")), "yearFrom", array()), 'widget', array("attr" => array("class" => "")));
            // line 250
            echo "
                    à
                    ";
            // line 252
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["addUserDegree"]) ? $context["addUserDegree"] : $this->getContext($context, "addUserDegree")), "yearTo", array()), 'widget', array("attr" => array("class" => "")));
            // line 254
            echo "
                </div>

                <button type=\"submit\" name=\"submitDegree\" class=\"big addUpdate\">Ajouter</button>
                <button type=\"button\" class=\"big delete\">Supprimer</button>
                ";
            // line 259
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["addUserDegree"]) ? $context["addUserDegree"] : $this->getContext($context, "addUserDegree")), 'rest');
            echo "
                ";
            // line 260
            echo             $this->env->getExtension('form')->renderer->renderBlock((isset($context["addUserDegree"]) ? $context["addUserDegree"] : $this->getContext($context, "addUserDegree")), 'form_end');
            echo "

                <!-- Projet -->
                ";
            // line 263
            echo             $this->env->getExtension('form')->renderer->renderBlock((isset($context["addUserProject"]) ? $context["addUserProject"] : $this->getContext($context, "addUserProject")), 'form_start', array("attr" => array("class" => "experience Project")));
            // line 265
            echo "
                <p>Nom de votre projet :</p>
                ";
            // line 267
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["addUserProject"]) ? $context["addUserProject"] : $this->getContext($context, "addUserProject")), "unipro_id", array()), 'widget', array("attr" => array("class" => "big")));
            // line 269
            echo "
                <a data-toggle=\"modal\" data-target=\"#addProjectModal\">Il n'est pas dans la liste ?</a>

                <p>Nom de votre rôle :</p>
                ";
            // line 273
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["addUserProject"]) ? $context["addUserProject"] : $this->getContext($context, "addUserProject")), "profession_id", array()), 'widget', array("attr" => array("class" => "big")));
            // line 275
            echo "
                <a data-toggle=\"modal\" data-target=\"#addProfessionModal\">Il n'est pas dans la liste ?</a>

                <p>Période</p>

                <div>
                    De
                    ";
            // line 282
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["addUserProject"]) ? $context["addUserProject"] : $this->getContext($context, "addUserProject")), "yearFrom", array()), 'widget', array("attr" => array("class" => "")));
            // line 284
            echo "
                    à
                    ";
            // line 286
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["addUserProject"]) ? $context["addUserProject"] : $this->getContext($context, "addUserProject")), "yearTo", array()), 'widget', array("attr" => array("class" => "")));
            // line 288
            echo "
                </div>

                <button type=\"submit\" name=\"submitProject\" class=\"big addUpdate\">Ajouter</button>
                <button type=\"button\" class=\"big delete\">Supprimer</button>
                ";
            // line 293
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["addUserProject"]) ? $context["addUserProject"] : $this->getContext($context, "addUserProject")), 'rest');
            echo "
                ";
            // line 294
            echo             $this->env->getExtension('form')->renderer->renderBlock((isset($context["addUserProject"]) ? $context["addUserProject"] : $this->getContext($context, "addUserProject")), 'form_end');
            echo "
            </section>
        ";
        }
        // line 297
        echo "    </main>

    <!-- modal add profession -->
    <div class=\"modal fade\" id=\"addProfessionModal\" tabindex=\"-1\" role=\"dialog\"
         aria-labelledby=\"addProfessionModalLabel\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                ";
        // line 304
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["addProfession"]) ? $context["addProfession"] : $this->getContext($context, "addProfession")), 'form_start');
        echo "
                <div class=\"modal-header\">
                    <h4 class=\"modal-title\" id=\"addProfessionModalLabel\">Ajouter une profession</h4>
                </div>
                <div class=\"modal-body\">
                    <p>Nom de la profession :</p>
                    ";
        // line 310
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["addProfession"]) ? $context["addProfession"] : $this->getContext($context, "addProfession")), "name", array()), 'widget', array("attr" => array("class" => "big")));
        // line 312
        echo "

                    <p>Description :</p>
                    ";
        // line 315
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["addProfession"]) ? $context["addProfession"] : $this->getContext($context, "addProfession")), "description", array()), 'widget', array("attr" => array("placeholder" => "Quelques mots suffisent")));
        // line 317
        echo "
                    ";
        // line 318
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["addProfession"]) ? $context["addProfession"] : $this->getContext($context, "addProfession")), 'rest');
        echo "
                </div>
                <div class=\"modal-footer\">
                    <button type=\"submit\" id=\"addProfessionModalButton\" class=\"big\">Ajouter</button>
                </div>
                ";
        // line 323
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["addProfession"]) ? $context["addProfession"] : $this->getContext($context, "addProfession")), 'form_end');
        echo "
            </div>
        </div>
    </div>

    <!-- modal add company -->
    <div class=\"modal fade\" id=\"addCompanyModal\" tabindex=\"-1\" role=\"dialog\"
         aria-labelledby=\"addCompanyModalLabel\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                ";
        // line 333
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["addCompany"]) ? $context["addCompany"] : $this->getContext($context, "addCompany")), 'form_start');
        echo "
                <div class=\"modal-header\">
                    <h4 class=\"modal-title\" id=\"addCompanyModalLabel\">Ajouter une entreprise</h4>
                </div>
                <div class=\"modal-body\">
                    <p>Nom de l'entreprise :</p>
                    ";
        // line 339
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["addCompany"]) ? $context["addCompany"] : $this->getContext($context, "addCompany")), "name", array()), 'widget', array("attr" => array("class" => "big")));
        // line 341
        echo "

                    <p>Logo de l'entreprise :</p>
                    ";
        // line 344
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["addCompany"]) ? $context["addCompany"] : $this->getContext($context, "addCompany")), "logo_file", array()), 'widget', array("attr" => array("class" => "big grey custom-input-file", "placeholder" => "Ajouter son logo", "data-title" => "Sélectionner")));
        // line 348
        echo "
                    ";
        // line 349
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["addCompany"]) ? $context["addCompany"] : $this->getContext($context, "addCompany")), 'rest');
        echo "
                </div>
                <div class=\"modal-footer\">
                    <button type=\"submit\" id=\"addCompanyModalButton\" class=\"big\">Ajouter</button>
                </div>
                ";
        // line 354
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["addCompany"]) ? $context["addCompany"] : $this->getContext($context, "addCompany")), 'form_end');
        echo "
            </div>
        </div>
    </div>

    <!-- modal add degree -->
    <div class=\"modal fade\" id=\"addDegreeModal\" tabindex=\"-1\" role=\"dialog\"
         aria-labelledby=\"addDegreeModalLabel\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                ";
        // line 364
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["addDegree"]) ? $context["addDegree"] : $this->getContext($context, "addDegree")), 'form_start');
        echo "
                <div class=\"modal-header\">
                    <h4 class=\"modal-title\" id=\"addDegreeModalLabel\">Ajouter une formation</h4>
                </div>
                <div class=\"modal-body\">
                    <p>Nom de la formation :</p>
                    ";
        // line 370
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["addDegree"]) ? $context["addDegree"] : $this->getContext($context, "addDegree")), "name", array()), 'widget', array("attr" => array("class" => "big")));
        // line 372
        echo "

                    <p>Option :</p>
                    ";
        // line 375
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["addDegree"]) ? $context["addDegree"] : $this->getContext($context, "addDegree")), "option", array()), 'widget', array("attr" => array("class" => "big")));
        // line 377
        echo "

                    <p>Description :</p>
                    ";
        // line 380
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["addDegree"]) ? $context["addDegree"] : $this->getContext($context, "addDegree")), "summary", array()), 'widget');
        echo "
                    ";
        // line 381
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["addDegree"]) ? $context["addDegree"] : $this->getContext($context, "addDegree")), 'rest');
        echo "
                </div>
                <div class=\"modal-footer\">
                    <button type=\"submit\" id=\"addDegreeModalButton\" class=\"big\">Ajouter</button>
                </div>
                ";
        // line 386
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["addDegree"]) ? $context["addDegree"] : $this->getContext($context, "addDegree")), 'form_end');
        echo "
            </div>
        </div>
    </div>

    <!-- modal add project -->
    <div class=\"modal fade\" id=\"addProjectModal\" tabindex=\"-1\" role=\"dialog\"
         aria-labelledby=\"addProjectModalLabel\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                ";
        // line 396
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["addDegree"]) ? $context["addDegree"] : $this->getContext($context, "addDegree")), 'form_start');
        echo "
                <div class=\"modal-header\">
                    <h4 class=\"modal-title\" id=\"addProjectModalLabel\">Ajouter un projet universitaire</h4>
                </div>
                <div class=\"modal-body\">
                    <p>Nom du projet :</p>
                    ";
        // line 402
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["addProject"]) ? $context["addProject"] : $this->getContext($context, "addProject")), "name", array()), 'widget', array("attr" => array("class" => "big")));
        // line 404
        echo "

                    <p>Lien vers le site :</p>
                    ";
        // line 407
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["addProject"]) ? $context["addProject"] : $this->getContext($context, "addProject")), "website", array()), 'widget', array("attr" => array("class" => "big")));
        // line 409
        echo "

                    ";
        // line 411
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["addProject"]) ? $context["addProject"] : $this->getContext($context, "addProject")), 'rest');
        echo "
                </div>
                <div class=\"modal-footer\">
                    <button type=\"submit\" id=\"addProjectModalButton\" class=\"big\">Ajouter</button>
                </div>
                ";
        // line 416
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["addProject"]) ? $context["addProject"] : $this->getContext($context, "addProject")), 'form_end');
        echo "
            </div>
        </div>
    </div>
";
        
        $__internal_fa98662072683b038a12ff07760837a905a8fb8a466ed12a0dcfa04bb32167bc->leave($__internal_fa98662072683b038a12ff07760837a905a8fb8a466ed12a0dcfa04bb32167bc_prof);

    }

    // line 422
    public function block_includeBody($context, array $blocks = array())
    {
        $__internal_3c123a1a1c8d043982eea3f5231b4e4d6f941fd4e73c69f50f8442c47b2e10c8 = $this->env->getExtension("native_profiler");
        $__internal_3c123a1a1c8d043982eea3f5231b4e4d6f941fd4e73c69f50f8442c47b2e10c8->enter($__internal_3c123a1a1c8d043982eea3f5231b4e4d6f941fd4e73c69f50f8442c47b2e10c8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "includeBody"));

        // line 423
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("assets/jquery-custom.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 424
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("assets/jquery-ui-1.11.4/jquery-ui.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 425
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/menu.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 426
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/custom_input_file.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 427
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/custom_select.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 428
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/student.js"), "html", null, true);
        echo "\"></script>

    <script src=\"";
        // line 430
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/ajax/ajaxGetByType.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\">
        \$(document).ready(function () {

            // Job
            ajaxListGetByType('Profession', \$('input#UserAddJobType_profession_id'));
            ajaxListGetByType('Company', \$('input#UserAddJobType_company_id'));

            // Degree
            ajaxListGetByType('Degree', \$('input#UserAddDegreeType_degree_id'));

            // Project
            ajaxListGetByType('Project', \$('input#UserAddProjectType_unipro_id'));
            ajaxListGetByType('Profession', \$('input#UserAddProjectType_profession_id'));

        });
    </script>
";
        
        $__internal_3c123a1a1c8d043982eea3f5231b4e4d6f941fd4e73c69f50f8442c47b2e10c8->leave($__internal_3c123a1a1c8d043982eea3f5231b4e4d6f941fd4e73c69f50f8442c47b2e10c8_prof);

    }

    public function getTemplateName()
    {
        return "MMIBundle:Utilisateur:student.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  923 => 430,  918 => 428,  914 => 427,  910 => 426,  906 => 425,  902 => 424,  897 => 423,  891 => 422,  879 => 416,  871 => 411,  867 => 409,  865 => 407,  860 => 404,  858 => 402,  849 => 396,  836 => 386,  828 => 381,  824 => 380,  819 => 377,  817 => 375,  812 => 372,  810 => 370,  801 => 364,  788 => 354,  780 => 349,  777 => 348,  775 => 344,  770 => 341,  768 => 339,  759 => 333,  746 => 323,  738 => 318,  735 => 317,  733 => 315,  728 => 312,  726 => 310,  717 => 304,  708 => 297,  702 => 294,  698 => 293,  691 => 288,  689 => 286,  685 => 284,  683 => 282,  674 => 275,  672 => 273,  666 => 269,  664 => 267,  660 => 265,  658 => 263,  652 => 260,  648 => 259,  641 => 254,  639 => 252,  635 => 250,  633 => 248,  624 => 241,  622 => 239,  618 => 237,  616 => 235,  610 => 232,  606 => 231,  600 => 227,  598 => 224,  593 => 221,  591 => 219,  587 => 217,  585 => 215,  578 => 210,  576 => 208,  570 => 204,  568 => 202,  564 => 200,  562 => 198,  552 => 190,  540 => 188,  533 => 187,  529 => 186,  521 => 180,  519 => 179,  515 => 177,  511 => 175,  503 => 172,  495 => 168,  493 => 167,  486 => 165,  482 => 163,  475 => 161,  473 => 160,  469 => 158,  463 => 156,  461 => 155,  457 => 154,  449 => 152,  445 => 151,  441 => 149,  438 => 148,  434 => 146,  426 => 143,  418 => 139,  416 => 138,  409 => 136,  405 => 134,  398 => 132,  396 => 131,  392 => 129,  386 => 127,  384 => 126,  380 => 125,  372 => 123,  368 => 122,  364 => 120,  361 => 119,  357 => 117,  349 => 114,  341 => 110,  339 => 109,  332 => 107,  328 => 105,  321 => 103,  319 => 102,  315 => 100,  309 => 98,  307 => 97,  303 => 96,  295 => 94,  291 => 93,  287 => 91,  285 => 90,  277 => 84,  271 => 82,  268 => 81,  262 => 79,  259 => 78,  253 => 76,  250 => 75,  244 => 73,  241 => 72,  235 => 70,  233 => 69,  225 => 63,  217 => 61,  214 => 60,  208 => 58,  205 => 57,  199 => 55,  196 => 54,  190 => 52,  188 => 51,  184 => 50,  180 => 49,  176 => 48,  161 => 35,  155 => 34,  143 => 29,  137 => 27,  135 => 26,  132 => 25,  126 => 23,  124 => 22,  117 => 20,  111 => 18,  108 => 17,  102 => 16,  91 => 13,  85 => 12,  73 => 10,  64 => 7,  59 => 6,  53 => 5,  40 => 3,  11 => 1,);
    }
}
/* {% extends "MMIBundle::template.html.twig" %}*/
/* */
/* {% block title %}Profil de {{ student.firstname }}{% endblock %}*/
/* */
/* {% block includeHead %}*/
/*     <link rel="stylesheet" href="{{ asset("assets/jquery-ui-1.11.4/jquery-ui.min.css") }}">*/
/*     <script src="{{ asset('assets/jquery.scrollTo.min.js') }}"></script>*/
/* {% endblock %}*/
/* */
/* {% block bodyClass %}student{% endblock %}*/
/* */
/* {% block header %}*/
/*     style="background-image: linear-gradient(transparent, white), url({{ asset('images/profil_cover/') }}{{ student.imageCouvFile }})">*/
/* {% endblock %}*/
/* */
/* {% block headerExtended %}*/
/*     <div class="profil_header">*/
/*         <img src="{{ asset('images/profil_picture/') }}{{ student.imageProfilFile }}" class="profilepicture profil">*/
/* */
/*         <h1>{{ student.firstname }} <span class="blue">{{ student.lastname }}</span></h1>*/
/* */
/*         {% if (student.profession is not null) %}*/
/*             <h2>{{ student.profession.name }}</h2>*/
/*         {% endif %}*/
/* */
/*         {% if student.promo is not null %}*/
/*             <p>Diplomé en {{ student.promo.year }}</p>*/
/*         {% endif %}*/
/*         <a href="{{ path('mmi_edit_student') }}">Modifier mes informations <i class="fa fa-pencil"></i>*/
/*         </a>*/
/*     </div>*/
/* {% endblock %}*/
/* */
/* {% block content %}*/
/*     <main>*/
/*         <section class="presentation">*/
/*             <!--<div>*/
/*                 <h4>Lier mon profil :</h4>*/
/* */
/*                 <p>Ton profil se remplira tout seul et tu pourras te connecter en clic !</p>*/
/*                 <button class="big white" type="button" style="margin-top: 20px; margin-right: 10px">Facebook</button>*/
/*                 <button class="big white" type="button" style="margin-top: 20px">LinkedIn</button>*/
/*             </div>-->*/
/*             <div>*/
/*                 <h4>Infos personnelles</h4>*/
/* */
/*                 <p>*/
/*                     Âge : <span>{{ date('now').diff(date(student.birthday)).y }}</span><br>*/
/*                     Habite à <span>{{ student.city.name }}</span><br>*/
/*                     Email : <span>{{ student.email }}</span><br>*/
/*                     {% if student.telephone is not null %}*/
/*                         Téléphone : <span>{{ student.telephone }}</span><br>*/
/*                     {% endif %}*/
/*                     {% if student.urlPortfolio is not null %}*/
/*                         Portfolio : <span>{{ student.urlPortfolio }}</span><br>*/
/*                     {% endif %}*/
/*                     {% if student.urlBlog is not null %}*/
/*                         Blog : <span>{{ student.urlBlog }}</span><br>*/
/*                     {% endif %}*/
/*                     {% if student.cvFile is not null %}*/
/*                         CV : <a href="{{ asset('cv') }}/{{ student.cvFile }}">Voir le CV</a>*/
/*                     {% endif %}*/
/*                 </p>*/
/*             </div>*/
/*             <div>*/
/*                 <h4>Social</h4>*/
/* */
/*                 <p>*/
/*                     {% if student.urlFacebook != null %}*/
/*                         <a href="{{ student.urlFacebook }}">Facebook</a><br>*/
/*                     {% endif %}*/
/*                     {% if student.urlLinkedin != null %}*/
/*                         <a href="{{ student.urlLinkedin }}">LinkedIn</a><br>*/
/*                     {% endif %}*/
/*                     {% if student.urlTwitter != null %}*/
/*                         <a href="{{ student.urlTwitter }}">Twitter</a><br>*/
/*                     {% endif %}*/
/*                     {% if student.urlTumblr != null %}*/
/*                         <a href="{{ student.urlTumblr }}">Tumblr</a><br>*/
/*                     {% endif %}*/
/*                     {% if student.urlInstagram != null %}*/
/*                         <a href="{{ student.urlInstagram }}">Instagram</a><br>*/
/*                     {% endif %}*/
/*                 </p>*/
/*             </div>*/
/*         </section>*/
/*         <section class="parcours">*/
/*             <h1>Son parcours</h1>*/
/* */
/*             {% if after | length > 0 %}*/
/*                 <h2>Après MMI</h2>*/
/*                 <ul>*/
/*                     {% for experience in after %}*/
/*                         <li class="{{ experience.type }}_{{ experience.id }}">*/
/*                             <div>*/
/*                                 {{ experience.from }}<!--*/
/*                             {% if experience.to != null and experience.to != experience.from %}*/
/*                                 -->-{{ experience.to }}*/
/*                                 {% endif %}*/
/*                                 <!-- -->*/
/*                             </div>*/
/*                             {% if experience.image is defined and experience.image != "" %}*/
/*                                 <img src="{{ asset('images/company/') }}{{ experience.image }}" class="picture">*/
/*                             {% endif %}*/
/* */
/*                             <div>*/
/*                                 <h3>{{ experience.name }}<span class="experience_category"> | {{ experience.display }}</span></h3>*/
/* */
/*                                 {% if experience.desc is defined %}*/
/*                                     <p>{{ experience.desc }}*/
/*                                         <i class="fa fa-pencil targeted" data-target="#edit"></i>*/
/*                                     </p>*/
/*                                 {% endif %}*/
/*                             </div>*/
/*                         </li>*/
/*                     {% endfor %}*/
/*                 </ul>*/
/*             {% endif %}*/
/*             {% if during | length > 0 %}*/
/*                 <h2>Pendant MMI</h2>*/
/*                 <ul>*/
/*                     {% for experience in during %}*/
/*                         <li class="{{ experience.type }}_{{ experience.id }}">*/
/*                             <div>*/
/*                                 {{ experience.from }}<!--*/
/*                             {% if experience.to != null and experience.to != experience.from %}*/
/*                                 -->-{{ experience.to }}*/
/*                                 {% endif %}*/
/*                                 <!-- -->*/
/*                             </div>*/
/*                             {% if experience.image is defined and experience.image != "" %}*/
/*                                 <img src="{{ asset('images/company/') }}{{ experience.image }}" class="picture">*/
/*                             {% endif %}*/
/* */
/*                             <div>*/
/*                                 <h3>{{ experience.name }}<span class="experience_category"> | {{ experience.display }}</span></h3>*/
/* */
/*                                 {% if experience.desc is defined %}*/
/*                                     <p>{{ experience.desc }}*/
/*                                         <i class="fa fa-pencil targeted" data-target="#edit"></i>*/
/*                                     </p>*/
/*                                 {% endif %}*/
/*                             </div>*/
/*                         </li>*/
/*                     {% endfor %}*/
/*                 </ul>*/
/*             {% endif %}*/
/*             {% if before | length > 0 %}*/
/*                 <h2>Avant MMI</h2>*/
/*                 <ul>*/
/*                     {% for experience in before %}*/
/*                         <li class="{{ experience.type }}_{{ experience.id }}">*/
/*                             <div>*/
/*                                 {{ experience.from }}<!--*/
/*                             {% if experience.to != null and experience.to != experience.from %}*/
/*                                 -->-{{ experience.to }}*/
/*                                 {% endif %}*/
/*                                 <!-- -->*/
/*                             </div>*/
/*                             {% if experience.image is defined and experience.image != "" %}*/
/*                                 <img src="{{ asset('images/company/') }}{{ experience.image }}" class="picture">*/
/*                             {% endif %}*/
/* */
/*                             <div>*/
/*                                 <h3>{{ experience.name }}<span class="experience_category"> | {{ experience.display }}</span></h3>*/
/* */
/*                                 {% if experience.desc is defined %}*/
/*                                     <p>{{ experience.desc }}*/
/*                                         <i class="fa fa-pencil targeted" data-target="#edit"></i>*/
/*                                     </p>*/
/*                                 {% endif %}*/
/*                             </div>*/
/*                         </li>*/
/*                     {% endfor %}*/
/*                 </ul>*/
/*             {% endif %}*/
/*         </section>*/
/* */
/*         {% if account is not null and student.id == account.id %}*/
/*             <section class="edit" id="edit">*/
/* */
/*                 <h2>Tenez-nous au courant !</h2>*/
/*                 <h4>Ajoutez ou modifer vos expériences</h4>*/
/*                 <select class="experiences">*/
/*                     <option value="add">Ajouter</option>*/
/*                     {% for experience in experiences %}*/
/*                         <option value="{{ experience.type }}:{{ experience.id }}"*/
/*                                 class="{{ experience.type }}_{{ experience.id }}">{{ experience.name }}</option>*/
/*                     {% endfor %}*/
/*                 </select>*/
/*                 <select class="type">*/
/*                     <option value="Job">Emploi</option>*/
/*                     <option value="Degree">Formation</option>*/
/*                     <option value="Project">Projet</option>*/
/*                 </select>*/
/* */
/*                 <!-- Emploi -->*/
/*                 {{ form_start(addUserJob,{*/
/*                     "attr":{*/
/*                         "class": "experience Job"}}) }}*/
/*                 <p>Nom de votre profession :</p>*/
/*                 {{ form_widget(addUserJob.profession_id,{*/
/*                     "attr": {*/
/*                         "class": "big"}}) }}*/
/*                 <a data-toggle="modal" data-target="#addProfessionModal">Il n'est pas dans la liste ?</a>*/
/* */
/*                 <p>Nom de l'entreprise :</p>*/
/*                 {{ form_widget(addUserJob.company_id,{*/
/*                     "attr": {*/
/*                         "class": "big"}}) }}*/
/*                 <a data-toggle="modal" data-target="#addCompanyModal">Elle n'est pas dans la liste ?</a>*/
/* */
/*                 <p>Période</p>*/
/*                 De*/
/*                 {{ form_widget(addUserJob.yearFrom,{*/
/*                     "attr": {*/
/*                         "class": ""}}) }}*/
/*                 à*/
/*                 {{ form_widget(addUserJob.yearTo,{*/
/*                     "attr": {*/
/*                         "class": ""}}) }}*/
/* */
/*                 <p>Description de votre poste :</p>*/
/*                 {{ form_widget(addUserJob.description,{*/
/*                     "attr": {*/
/*                         "placeholder": "Quelques mots suffisent"*/
/*                     }}) }}*/
/* */
/*                 <button type="submit" name="submitJob" class="big addUpdate">Ajouter</button>*/
/*                 <button type="button" class="big delete">Supprimer</button>*/
/*                 {{ form_rest(addUserJob) }}*/
/*                 {{ form_end(addUserJob) }}*/
/* */
/*                 <!-- Formation -->*/
/*                 {{ form_start(addUserDegree,{*/
/*                     "attr":{*/
/*                         "class": "experience Degree"}}) }}*/
/*                 <p>Nom de votre formation :</p>*/
/*                 {{ form_widget(addUserDegree.degree_id,{*/
/*                     "attr": {*/
/*                         "class": "big"}}) }}*/
/*                 <a data-toggle="modal" data-target="#addDegreeModal">Elle n'est pas dans la liste ?</a>*/
/* */
/*                 <p>Période</p>*/
/* */
/*                 <div>*/
/*                     De*/
/*                     {{ form_widget(addUserDegree.yearFrom,{*/
/*                         "attr": {*/
/*                             "class": ""}}) }}*/
/*                     à*/
/*                     {{ form_widget(addUserDegree.yearTo,{*/
/*                         "attr": {*/
/*                             "class": ""}}) }}*/
/*                 </div>*/
/* */
/*                 <button type="submit" name="submitDegree" class="big addUpdate">Ajouter</button>*/
/*                 <button type="button" class="big delete">Supprimer</button>*/
/*                 {{ form_rest(addUserDegree) }}*/
/*                 {{ form_end(addUserDegree) }}*/
/* */
/*                 <!-- Projet -->*/
/*                 {{ form_start(addUserProject,{*/
/*                     "attr":{*/
/*                         "class": "experience Project"}}) }}*/
/*                 <p>Nom de votre projet :</p>*/
/*                 {{ form_widget(addUserProject.unipro_id,{*/
/*                     "attr": {*/
/*                         "class": "big"}}) }}*/
/*                 <a data-toggle="modal" data-target="#addProjectModal">Il n'est pas dans la liste ?</a>*/
/* */
/*                 <p>Nom de votre rôle :</p>*/
/*                 {{ form_widget(addUserProject.profession_id,{*/
/*                     "attr": {*/
/*                         "class": "big"}}) }}*/
/*                 <a data-toggle="modal" data-target="#addProfessionModal">Il n'est pas dans la liste ?</a>*/
/* */
/*                 <p>Période</p>*/
/* */
/*                 <div>*/
/*                     De*/
/*                     {{ form_widget(addUserProject.yearFrom,{*/
/*                         "attr": {*/
/*                             "class": ""}}) }}*/
/*                     à*/
/*                     {{ form_widget(addUserProject.yearTo,{*/
/*                         "attr": {*/
/*                             "class": ""}}) }}*/
/*                 </div>*/
/* */
/*                 <button type="submit" name="submitProject" class="big addUpdate">Ajouter</button>*/
/*                 <button type="button" class="big delete">Supprimer</button>*/
/*                 {{ form_rest(addUserProject) }}*/
/*                 {{ form_end(addUserProject) }}*/
/*             </section>*/
/*         {% endif %}*/
/*     </main>*/
/* */
/*     <!-- modal add profession -->*/
/*     <div class="modal fade" id="addProfessionModal" tabindex="-1" role="dialog"*/
/*          aria-labelledby="addProfessionModalLabel">*/
/*         <div class="modal-dialog" role="document">*/
/*             <div class="modal-content">*/
/*                 {{ form_start(addProfession) }}*/
/*                 <div class="modal-header">*/
/*                     <h4 class="modal-title" id="addProfessionModalLabel">Ajouter une profession</h4>*/
/*                 </div>*/
/*                 <div class="modal-body">*/
/*                     <p>Nom de la profession :</p>*/
/*                     {{ form_widget(addProfession.name,{*/
/*                         "attr": {*/
/*                             "class": "big"}}) }}*/
/* */
/*                     <p>Description :</p>*/
/*                     {{ form_widget(addProfession.description,{*/
/*                         "attr": {*/
/*                             "placeholder": "Quelques mots suffisent"}}) }}*/
/*                     {{ form_rest(addProfession) }}*/
/*                 </div>*/
/*                 <div class="modal-footer">*/
/*                     <button type="submit" id="addProfessionModalButton" class="big">Ajouter</button>*/
/*                 </div>*/
/*                 {{ form_end(addProfession) }}*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* */
/*     <!-- modal add company -->*/
/*     <div class="modal fade" id="addCompanyModal" tabindex="-1" role="dialog"*/
/*          aria-labelledby="addCompanyModalLabel">*/
/*         <div class="modal-dialog" role="document">*/
/*             <div class="modal-content">*/
/*                 {{ form_start(addCompany) }}*/
/*                 <div class="modal-header">*/
/*                     <h4 class="modal-title" id="addCompanyModalLabel">Ajouter une entreprise</h4>*/
/*                 </div>*/
/*                 <div class="modal-body">*/
/*                     <p>Nom de l'entreprise :</p>*/
/*                     {{ form_widget(addCompany.name,{*/
/*                         "attr": {*/
/*                             "class": "big"}}) }}*/
/* */
/*                     <p>Logo de l'entreprise :</p>*/
/*                     {{ form_widget(addCompany.logo_file,{*/
/*                         "attr": {*/
/*                             "class": "big grey custom-input-file",*/
/*                             "placeholder": "Ajouter son logo",*/
/*                             "data-title": "Sélectionner"}}) }}*/
/*                     {{ form_rest(addCompany) }}*/
/*                 </div>*/
/*                 <div class="modal-footer">*/
/*                     <button type="submit" id="addCompanyModalButton" class="big">Ajouter</button>*/
/*                 </div>*/
/*                 {{ form_end(addCompany) }}*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* */
/*     <!-- modal add degree -->*/
/*     <div class="modal fade" id="addDegreeModal" tabindex="-1" role="dialog"*/
/*          aria-labelledby="addDegreeModalLabel">*/
/*         <div class="modal-dialog" role="document">*/
/*             <div class="modal-content">*/
/*                 {{ form_start(addDegree) }}*/
/*                 <div class="modal-header">*/
/*                     <h4 class="modal-title" id="addDegreeModalLabel">Ajouter une formation</h4>*/
/*                 </div>*/
/*                 <div class="modal-body">*/
/*                     <p>Nom de la formation :</p>*/
/*                     {{ form_widget(addDegree.name,{*/
/*                         "attr": {*/
/*                             "class": "big"}}) }}*/
/* */
/*                     <p>Option :</p>*/
/*                     {{ form_widget(addDegree.option,{*/
/*                         "attr": {*/
/*                             "class": "big"}}) }}*/
/* */
/*                     <p>Description :</p>*/
/*                     {{ form_widget(addDegree.summary) }}*/
/*                     {{ form_rest(addDegree) }}*/
/*                 </div>*/
/*                 <div class="modal-footer">*/
/*                     <button type="submit" id="addDegreeModalButton" class="big">Ajouter</button>*/
/*                 </div>*/
/*                 {{ form_end(addDegree) }}*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* */
/*     <!-- modal add project -->*/
/*     <div class="modal fade" id="addProjectModal" tabindex="-1" role="dialog"*/
/*          aria-labelledby="addProjectModalLabel">*/
/*         <div class="modal-dialog" role="document">*/
/*             <div class="modal-content">*/
/*                 {{ form_start(addDegree) }}*/
/*                 <div class="modal-header">*/
/*                     <h4 class="modal-title" id="addProjectModalLabel">Ajouter un projet universitaire</h4>*/
/*                 </div>*/
/*                 <div class="modal-body">*/
/*                     <p>Nom du projet :</p>*/
/*                     {{ form_widget(addProject.name,{*/
/*                         "attr": {*/
/*                             "class": "big"}}) }}*/
/* */
/*                     <p>Lien vers le site :</p>*/
/*                     {{ form_widget(addProject.website,{*/
/*                         "attr": {*/
/*                             "class": "big"}}) }}*/
/* */
/*                     {{ form_rest(addProject) }}*/
/*                 </div>*/
/*                 <div class="modal-footer">*/
/*                     <button type="submit" id="addProjectModalButton" class="big">Ajouter</button>*/
/*                 </div>*/
/*                 {{ form_end(addProject) }}*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
/* {% block includeBody %}*/
/*     <script src="{{ asset("assets/jquery-custom.min.js") }}"></script>*/
/*     <script src="{{ asset("assets/jquery-ui-1.11.4/jquery-ui.min.js") }}"></script>*/
/*     <script src="{{ asset('js/menu.js') }}"></script>*/
/*     <script src="{{ asset("js/custom_input_file.js") }}"></script>*/
/*     <script src="{{ asset('js/custom_select.js') }}"></script>*/
/*     <script src="{{ asset('js/student.js') }}"></script>*/
/* */
/*     <script src="{{ asset('js/ajax/ajaxGetByType.js') }}"></script>*/
/*     <script type="text/javascript">*/
/*         $(document).ready(function () {*/
/* */
/*             // Job*/
/*             ajaxListGetByType('Profession', $('input#UserAddJobType_profession_id'));*/
/*             ajaxListGetByType('Company', $('input#UserAddJobType_company_id'));*/
/* */
/*             // Degree*/
/*             ajaxListGetByType('Degree', $('input#UserAddDegreeType_degree_id'));*/
/* */
/*             // Project*/
/*             ajaxListGetByType('Project', $('input#UserAddProjectType_unipro_id'));*/
/*             ajaxListGetByType('Profession', $('input#UserAddProjectType_profession_id'));*/
/* */
/*         });*/
/*     </script>*/
/* {% endblock %}*/
/* */
