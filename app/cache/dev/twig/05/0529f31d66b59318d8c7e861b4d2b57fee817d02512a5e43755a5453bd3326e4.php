<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_adb88ebf48a3a1631fdb900d219ee46e4e0fdc87d899c9813a914ce6169cc988 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("TwigBundle::layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "TwigBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6d3dbeb30f62838474d036e92ae6d36197bf6b6ed203f2220aa9271cce7fd7f5 = $this->env->getExtension("native_profiler");
        $__internal_6d3dbeb30f62838474d036e92ae6d36197bf6b6ed203f2220aa9271cce7fd7f5->enter($__internal_6d3dbeb30f62838474d036e92ae6d36197bf6b6ed203f2220aa9271cce7fd7f5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6d3dbeb30f62838474d036e92ae6d36197bf6b6ed203f2220aa9271cce7fd7f5->leave($__internal_6d3dbeb30f62838474d036e92ae6d36197bf6b6ed203f2220aa9271cce7fd7f5_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_44c3fba370c5e8df964598dffdd154caf9b525c38101de0cc759c793974c0a97 = $this->env->getExtension("native_profiler");
        $__internal_44c3fba370c5e8df964598dffdd154caf9b525c38101de0cc759c793974c0a97->enter($__internal_44c3fba370c5e8df964598dffdd154caf9b525c38101de0cc759c793974c0a97_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_44c3fba370c5e8df964598dffdd154caf9b525c38101de0cc759c793974c0a97->leave($__internal_44c3fba370c5e8df964598dffdd154caf9b525c38101de0cc759c793974c0a97_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_51523cde7ef386e9c2cbd8650b9c810d144746b2ebf3b1b394e9d1323621b9cb = $this->env->getExtension("native_profiler");
        $__internal_51523cde7ef386e9c2cbd8650b9c810d144746b2ebf3b1b394e9d1323621b9cb->enter($__internal_51523cde7ef386e9c2cbd8650b9c810d144746b2ebf3b1b394e9d1323621b9cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_51523cde7ef386e9c2cbd8650b9c810d144746b2ebf3b1b394e9d1323621b9cb->leave($__internal_51523cde7ef386e9c2cbd8650b9c810d144746b2ebf3b1b394e9d1323621b9cb_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_dcfcbc3efddd2c8547f971189ae8301e43f24958bb8e69e9f7b36d2fcf3a682c = $this->env->getExtension("native_profiler");
        $__internal_dcfcbc3efddd2c8547f971189ae8301e43f24958bb8e69e9f7b36d2fcf3a682c->enter($__internal_dcfcbc3efddd2c8547f971189ae8301e43f24958bb8e69e9f7b36d2fcf3a682c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("TwigBundle:Exception:exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 12)->display($context);
        
        $__internal_dcfcbc3efddd2c8547f971189ae8301e43f24958bb8e69e9f7b36d2fcf3a682c->leave($__internal_dcfcbc3efddd2c8547f971189ae8301e43f24958bb8e69e9f7b36d2fcf3a682c_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'TwigBundle::layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include 'TwigBundle:Exception:exception.html.twig' %}*/
/* {% endblock %}*/
/* */
