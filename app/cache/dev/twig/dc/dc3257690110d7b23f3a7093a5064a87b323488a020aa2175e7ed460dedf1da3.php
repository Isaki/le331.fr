<?php

/* MMIBundle:Default:index.html.twig */
class __TwigTemplate_4e24c8fcd78e5c2e778f389c8f0561feabf27de2330ee1da42c17479ab9629d6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("MMIBundle::template.html.twig", "MMIBundle:Default:index.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'includeHead' => array($this, 'block_includeHead'),
            'bodyClass' => array($this, 'block_bodyClass'),
            'header' => array($this, 'block_header'),
            'headerExtended' => array($this, 'block_headerExtended'),
            'content' => array($this, 'block_content'),
            'includeBody' => array($this, 'block_includeBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "MMIBundle::template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_67b0d048882150a2fa3d3c1f65bbf5f846d9559a083933ba019a857efdb68b63 = $this->env->getExtension("native_profiler");
        $__internal_67b0d048882150a2fa3d3c1f65bbf5f846d9559a083933ba019a857efdb68b63->enter($__internal_67b0d048882150a2fa3d3c1f65bbf5f846d9559a083933ba019a857efdb68b63_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MMIBundle:Default:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_67b0d048882150a2fa3d3c1f65bbf5f846d9559a083933ba019a857efdb68b63->leave($__internal_67b0d048882150a2fa3d3c1f65bbf5f846d9559a083933ba019a857efdb68b63_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_0ac4e563564c8a3a4f20a889906bdc230079e45e66d28ececfe6184f698b5f1a = $this->env->getExtension("native_profiler");
        $__internal_0ac4e563564c8a3a4f20a889906bdc230079e45e66d28ececfe6184f698b5f1a->enter($__internal_0ac4e563564c8a3a4f20a889906bdc230079e45e66d28ececfe6184f698b5f1a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Accueil";
        
        $__internal_0ac4e563564c8a3a4f20a889906bdc230079e45e66d28ececfe6184f698b5f1a->leave($__internal_0ac4e563564c8a3a4f20a889906bdc230079e45e66d28ececfe6184f698b5f1a_prof);

    }

    // line 5
    public function block_includeHead($context, array $blocks = array())
    {
        $__internal_fc8837297242137ef713dca2007a409ed31be0af112212337984fb61e14cb235 = $this->env->getExtension("native_profiler");
        $__internal_fc8837297242137ef713dca2007a409ed31be0af112212337984fb61e14cb235->enter($__internal_fc8837297242137ef713dca2007a409ed31be0af112212337984fb61e14cb235_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "includeHead"));

        // line 6
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/form_regex.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_fc8837297242137ef713dca2007a409ed31be0af112212337984fb61e14cb235->leave($__internal_fc8837297242137ef713dca2007a409ed31be0af112212337984fb61e14cb235_prof);

    }

    // line 9
    public function block_bodyClass($context, array $blocks = array())
    {
        $__internal_e690ce3e257ad7ff56c6af64f9753653caf3ea57c41d140988f53769ec1583cd = $this->env->getExtension("native_profiler");
        $__internal_e690ce3e257ad7ff56c6af64f9753653caf3ea57c41d140988f53769ec1583cd->enter($__internal_e690ce3e257ad7ff56c6af64f9753653caf3ea57c41d140988f53769ec1583cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "bodyClass"));

        echo "home";
        
        $__internal_e690ce3e257ad7ff56c6af64f9753653caf3ea57c41d140988f53769ec1583cd->leave($__internal_e690ce3e257ad7ff56c6af64f9753653caf3ea57c41d140988f53769ec1583cd_prof);

    }

    // line 11
    public function block_header($context, array $blocks = array())
    {
        $__internal_aeddf1bb7b3332bdbd6b9df49e4310ccf3a646ee24bfbf1529fae45f5f240faa = $this->env->getExtension("native_profiler");
        $__internal_aeddf1bb7b3332bdbd6b9df49e4310ccf3a646ee24bfbf1529fae45f5f240faa->enter($__internal_aeddf1bb7b3332bdbd6b9df49e4310ccf3a646ee24bfbf1529fae45f5f240faa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        echo "class=\"home\"";
        
        $__internal_aeddf1bb7b3332bdbd6b9df49e4310ccf3a646ee24bfbf1529fae45f5f240faa->leave($__internal_aeddf1bb7b3332bdbd6b9df49e4310ccf3a646ee24bfbf1529fae45f5f240faa_prof);

    }

    // line 13
    public function block_headerExtended($context, array $blocks = array())
    {
        $__internal_af4c3fa4d4f5d8f15230c5d1865df4d76ff1e2b3d9caad8a88fe4e9ae985ad0d = $this->env->getExtension("native_profiler");
        $__internal_af4c3fa4d4f5d8f15230c5d1865df4d76ff1e2b3d9caad8a88fe4e9ae985ad0d->enter($__internal_af4c3fa4d4f5d8f15230c5d1865df4d76ff1e2b3d9caad8a88fe4e9ae985ad0d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "headerExtended"));

        // line 14
        echo "    <div id=\"caroussel\" class=\"caroussel\">
        <img src=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("ui_img/home_01.jpg"), "html", null, true);
        echo "\" alt=\"\">
        <img src=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("ui_img/home_02.jpg"), "html", null, true);
        echo "\" alt=\"\">
        <img src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("ui_img/home_03.gif"), "html", null, true);
        echo "\" alt=\"\">
        <img src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("ui_img/home_04.jpg"), "html", null, true);
        echo "\" alt=\"\">
        <img src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("ui_img/home_05.jpg"), "html", null, true);
        echo "\" alt=\"\">
        <img src=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("ui_img/home_06.jpg"), "html", null, true);
        echo "\" alt=\"\">
    </div>
    <div id=\"caroussel_text\" class=\"caroussel_text\">
        <div class=\"home\">
            <h2>Nous sommes<span class=\"super_title\">&nbsp;&nbsp;&nbsp;&nbsp;331</span></h2>

            <p style=\"margin-left: 100px; width: 300px\">Construis ton avenir avec 331, La communauté des anciens
                étudiants de MMi Montbéliard</p>
        </div>
        <div class=\"home\">
            <h2 class=\"dark\"><span class=\"super_title\">Participe</span> aux événements de la communauté</h2>
        </div>
        <div class=\"home\">
            <h2>Profite d'un grand<span class=\"super_title\">Réseau</span></h2>

            <p style=\"margin-left: 30%;\">de professionnels et d’étudiants</p>
        </div>
        <div class=\"home\">
            <h2>Trouve ton<span class=\"super_title\">Stage</span></h2>

            <p style=\"margin-left: 55%\">ou un emploi grâce aux offres de nos partenaires</p>
        </div>
        <div class=\"home\">
            <h2>Lance ton<span class=\"super_title\">Projet</span></h2>

            <p style=\"margin: -40px 0 0 50%\">grâce à l’incubateur</p>
        </div>
        <div class=\"home\">
            <h2 class=\"\">Trouve la formation et le<span class=\"super_title\">Métier</span></h2>

            <p class=\"\" style=\"margin: -50px 0 0 30%\">de tes rêves</p>
        </div>
    </div>
";
        
        $__internal_af4c3fa4d4f5d8f15230c5d1865df4d76ff1e2b3d9caad8a88fe4e9ae985ad0d->leave($__internal_af4c3fa4d4f5d8f15230c5d1865df4d76ff1e2b3d9caad8a88fe4e9ae985ad0d_prof);

    }

    // line 55
    public function block_content($context, array $blocks = array())
    {
        $__internal_dbce1047dca0eef98e40b7031a00cae153a86ee220af2003fb50bddeeddb16a2 = $this->env->getExtension("native_profiler");
        $__internal_dbce1047dca0eef98e40b7031a00cae153a86ee220af2003fb50bddeeddb16a2->enter($__internal_dbce1047dca0eef98e40b7031a00cae153a86ee220af2003fb50bddeeddb16a2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 56
        echo "    <main>
        ";
        // line 57
        if ( !array_key_exists("account", $context)) {
            // line 58
            echo "            <section class=\"wrapper\">
                <article class=\"log\">
                    <h1 class=\"right\">S'inscrire</h1>

                    ";
            // line 62
            echo             $this->env->getExtension('form')->renderer->renderBlock((isset($context["register"]) ? $context["register"] : $this->getContext($context, "register")), 'form_start', array("attr" => array("class" => "verif_regex")));
            // line 64
            echo "
                    ";
            // line 65
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["register"]) ? $context["register"] : $this->getContext($context, "register")), "firstname", array()), 'widget', array("attr" => array("class" => "big right verif", "data-regex" => "nom")));
            // line 68
            echo "
                    ";
            // line 69
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["register"]) ? $context["register"] : $this->getContext($context, "register")), "lastname", array()), 'widget', array("attr" => array("class" => "big right verif", "data-regex" => "nom")));
            // line 72
            echo "
                    ";
            // line 73
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["register"]) ? $context["register"] : $this->getContext($context, "register")), "email", array()), 'widget', array("attr" => array("class" => "big right verif", "data-regex" => "email")));
            // line 76
            echo "
                    ";
            // line 77
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["register"]) ? $context["register"] : $this->getContext($context, "register")), "password", array()), 'widget', array("attr" => array("class" => "big right verif")));
            // line 79
            echo "
                    <button type=\"submit\" name=\"submit\" class=\"big right\">S'inscrire</button>
                    ";
            // line 81
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["register"]) ? $context["register"] : $this->getContext($context, "register")), 'rest');
            echo "
                    ";
            // line 82
            echo             $this->env->getExtension('form')->renderer->renderBlock((isset($context["register"]) ? $context["register"] : $this->getContext($context, "register")), 'form_end');
            echo "

                    <a href=\"";
            // line 84
            echo $this->env->getExtension('routing')->getPath("mmi_pros");
            echo "\" style=\"display: block; margin-top: 30px; float: right\">Vous êtes
                        une entreprise
                        ?</a>
                </article>
                <article class=\"log\" id=\"login\">
                    <h1>Se connecter</h1>

                    ";
            // line 91
            echo             $this->env->getExtension('form')->renderer->renderBlock((isset($context["connection"]) ? $context["connection"] : $this->getContext($context, "connection")), 'form_start', array("attr" => array("class" => "verif_regex")));
            // line 93
            echo "
                    ";
            // line 94
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["connection"]) ? $context["connection"] : $this->getContext($context, "connection")), "email", array()), 'widget', array("attr" => array("class" => "big verif", "data-regex" => "email")));
            // line 97
            echo "
                    ";
            // line 98
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["connection"]) ? $context["connection"] : $this->getContext($context, "connection")), "password", array()), 'widget', array("attr" => array("class" => "big verif")));
            // line 100
            echo "
                    <button type=\"submit\" name=\"submit\" class=\"big\">Se connecter</button>
                    ";
            // line 102
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["connection"]) ? $context["connection"] : $this->getContext($context, "connection")), 'rest');
            echo "
                    ";
            // line 103
            echo             $this->env->getExtension('form')->renderer->renderBlock((isset($context["connection"]) ? $context["connection"] : $this->getContext($context, "connection")), 'form_end');
            echo "

                    <!--<h4>Ou encore plus rapide !</h4>
                    <button class=\"big white\" type=\"button\" style=\"margin-top: 20px; margin-right: 10px\">Facebook</button>
                    <button class=\"big white\" type=\"button\" style=\"margin-top: 20px\">LinkedIn</button>-->
                </article>
            </section>
        ";
        }
        // line 111
        echo "        <section class=\"illustration\">
            <h1 class=\"quarter\">Avec <span class=\"blue\">331</span>&nbsp;:</h1>

            <section class=\"wrapper\">
                <img src=\"";
        // line 115
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("ui_img/drawing_home_01.svg"), "html", null, true);
        echo "\" alt=\"Améliorez votre visibilité\">
                <div class=\"droite\">
                    <h2><span>Rejoins</span> notre communauté d'étudiants et d'anciens</h2>
                    <p>Consulte leur profils et leur témoignages</p>
                </div>
            </section>
            <section class=\"wrapper\">
                <div class=\"gauche\">
                    <h2><span>Profite</span> d'un précieux réseau d'entreprises</h2>
                    <p>Consulte nos fiches entreprises</p>
                </div>
                <img src=\"";
        // line 126
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("ui_img/drawing_home_02.svg"), "html", null, true);
        echo "\" alt=\"Améliorez votre visibilité\">
            </section>
            <section class=\"wrapper\">
                <img src=\"";
        // line 129
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("ui_img/drawing_home_03.svg"), "html", null, true);
        echo "\" alt=\"Améliorez votre visibilité\">
                <div class=\"droite\">
                    <h2><span>Participe</span> aux événements de la communauté</h2>
                    <p>Lis les articles de notre blog</p>
                </div>
            </section>
            <section class=\"wrapper\">
                <div class=\"gauche\">
                    <h2><span>Partage</span> tes connaissances avec la communauté</h2>

                    <p>Publie un article</p>

                </div>
                <img src=\"";
        // line 142
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("ui_img/drawing_home_04.svg"), "html", null, true);
        echo "\" alt=\"Améliorez votre visibilité\">
            </section>

        </section>

        <!--
        <section class=\"wrapper link_to_pro\">
            <div class=\"agency_logos quarter_wide\" style=\"margin-right: 30px\"></div>
            <div class=\"quarter_wide\">
                <h1>Devenez partenaires</h1>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio ratione tempora ut. Ab explicabo
                    fugiat,
                    illo labore necessitatibus neque pariatur reiciendis tempora veritatis voluptas voluptates. Lorem
                    ipsum
                    dolor sit amet, consectetur adipisicing elit. Adipisci asperiores, minima nobis non officia repellat
                    repellendus veniam vero. Dicta labore nisi reprehenderit soluta voluptas. Debitis deleniti earum
                    expedita nesciunt voluptatibus!</p>
                <a href=\"";
        // line 160
        echo $this->env->getExtension('routing')->getPath("mmi_pros");
        echo "\">
                    <button class=\"big white\" style=\"margin: 15px 0\">Espace pro</button>
                </a>
            </div>
        </section>
        <section class=\"very_light_grey inset_shadow\">
            <h1 class=\"quarter\">331 c'est aussi&nbsp;:</h1>

            <div class=\"wrapper\">
                <article class=\"tile_post\">
                    <h3><a href=\"\">Talking about something</a></h3>

                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab esse eveniet excepturi facilis
                        fugiat illo
                        laborum molestias nesciunt quasi velit.</p>
                    <a href=\"\">Suivant</a>
                </article>
                <article class=\"tile_post\">
                    <h3><a href=\"\">L’association 331</a></h3>

                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci architecto aspernatur aut
                        cumque
                        deserunt, ea excepturi exercitationem illo laboriosam magnam molestiae natus, omnis porro, quasi
                        quo
                        quod ratione repudiandae saepe.</p>
                </article>
            </div>
        </section>
        -->
    </main>
";
        
        $__internal_dbce1047dca0eef98e40b7031a00cae153a86ee220af2003fb50bddeeddb16a2->leave($__internal_dbce1047dca0eef98e40b7031a00cae153a86ee220af2003fb50bddeeddb16a2_prof);

    }

    // line 192
    public function block_includeBody($context, array $blocks = array())
    {
        $__internal_3e0b6b7d228574ccb777cc25d1d19ead97c67280072d30cb23a840b0f81bddf1 = $this->env->getExtension("native_profiler");
        $__internal_3e0b6b7d228574ccb777cc25d1d19ead97c67280072d30cb23a840b0f81bddf1->enter($__internal_3e0b6b7d228574ccb777cc25d1d19ead97c67280072d30cb23a840b0f81bddf1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "includeBody"));

        // line 193
        echo "    <div id=\"loader\"></div>
    <script src=\"";
        // line 194
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/transition.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 195
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/caroussel.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 196
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/menu.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 197
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("assets/bodymovin.js"), "html", null, true);
        echo "\"></script>
    <script>

        \$(document).ready(function(){
            var animData = {
                container: document.getElementById('loader'),
                renderer: 'svg',
                loop: true,
                autoplay: true,
                path: '";
        // line 206
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("assets/data_anim.json"), "html", null, true);
        echo "'
            };

            var anim = bodymovin.loadAnimation(animData);

            \$(window).load(function(){

                var modal_content = \$('#errorModal .no_errors').length;

                if (modal_content == 0){
                    \$('#errorModal').modal('show')
                }
            });

        });

    </script>
";
        
        $__internal_3e0b6b7d228574ccb777cc25d1d19ead97c67280072d30cb23a840b0f81bddf1->leave($__internal_3e0b6b7d228574ccb777cc25d1d19ead97c67280072d30cb23a840b0f81bddf1_prof);

    }

    public function getTemplateName()
    {
        return "MMIBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  389 => 206,  377 => 197,  373 => 196,  369 => 195,  365 => 194,  362 => 193,  356 => 192,  318 => 160,  297 => 142,  281 => 129,  275 => 126,  261 => 115,  255 => 111,  244 => 103,  240 => 102,  236 => 100,  234 => 98,  231 => 97,  229 => 94,  226 => 93,  224 => 91,  214 => 84,  209 => 82,  205 => 81,  201 => 79,  199 => 77,  196 => 76,  194 => 73,  191 => 72,  189 => 69,  186 => 68,  184 => 65,  181 => 64,  179 => 62,  173 => 58,  171 => 57,  168 => 56,  162 => 55,  121 => 20,  117 => 19,  113 => 18,  109 => 17,  105 => 16,  101 => 15,  98 => 14,  92 => 13,  80 => 11,  68 => 9,  58 => 6,  52 => 5,  40 => 3,  11 => 1,);
    }
}
/* {% extends "MMIBundle::template.html.twig" %}*/
/* */
/* {% block title %}Accueil{% endblock %}*/
/* */
/* {% block includeHead %}*/
/*     <script src="{{ asset('js/form_regex.js') }}"></script>*/
/* {% endblock %}*/
/* */
/* {% block bodyClass %}home{% endblock %}*/
/* */
/* {% block header %}class="home"{% endblock %}*/
/* */
/* {% block headerExtended %}*/
/*     <div id="caroussel" class="caroussel">*/
/*         <img src="{{ asset("ui_img/home_01.jpg") }}" alt="">*/
/*         <img src="{{ asset("ui_img/home_02.jpg") }}" alt="">*/
/*         <img src="{{ asset("ui_img/home_03.gif") }}" alt="">*/
/*         <img src="{{ asset("ui_img/home_04.jpg") }}" alt="">*/
/*         <img src="{{ asset("ui_img/home_05.jpg") }}" alt="">*/
/*         <img src="{{ asset("ui_img/home_06.jpg") }}" alt="">*/
/*     </div>*/
/*     <div id="caroussel_text" class="caroussel_text">*/
/*         <div class="home">*/
/*             <h2>Nous sommes<span class="super_title">&nbsp;&nbsp;&nbsp;&nbsp;331</span></h2>*/
/* */
/*             <p style="margin-left: 100px; width: 300px">Construis ton avenir avec 331, La communauté des anciens*/
/*                 étudiants de MMi Montbéliard</p>*/
/*         </div>*/
/*         <div class="home">*/
/*             <h2 class="dark"><span class="super_title">Participe</span> aux événements de la communauté</h2>*/
/*         </div>*/
/*         <div class="home">*/
/*             <h2>Profite d'un grand<span class="super_title">Réseau</span></h2>*/
/* */
/*             <p style="margin-left: 30%;">de professionnels et d’étudiants</p>*/
/*         </div>*/
/*         <div class="home">*/
/*             <h2>Trouve ton<span class="super_title">Stage</span></h2>*/
/* */
/*             <p style="margin-left: 55%">ou un emploi grâce aux offres de nos partenaires</p>*/
/*         </div>*/
/*         <div class="home">*/
/*             <h2>Lance ton<span class="super_title">Projet</span></h2>*/
/* */
/*             <p style="margin: -40px 0 0 50%">grâce à l’incubateur</p>*/
/*         </div>*/
/*         <div class="home">*/
/*             <h2 class="">Trouve la formation et le<span class="super_title">Métier</span></h2>*/
/* */
/*             <p class="" style="margin: -50px 0 0 30%">de tes rêves</p>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
/* {% block content %}*/
/*     <main>*/
/*         {% if account is not defined %}*/
/*             <section class="wrapper">*/
/*                 <article class="log">*/
/*                     <h1 class="right">S'inscrire</h1>*/
/* */
/*                     {{ form_start(register,*/
/*                     {"attr": {*/
/*                         "class": "verif_regex"}}) }}*/
/*                     {{ form_widget(register.firstname,*/
/*                     {"attr": {*/
/*                         "class": "big right verif",*/
/*                         "data-regex": "nom"}}) }}*/
/*                     {{ form_widget(register.lastname,*/
/*                     {"attr": {*/
/*                         "class": "big right verif",*/
/*                         "data-regex": "nom"}}) }}*/
/*                     {{ form_widget(register.email,*/
/*                     {"attr": {*/
/*                         "class": "big right verif",*/
/*                         "data-regex": "email"}}) }}*/
/*                     {{ form_widget(register.password,*/
/*                     {"attr": {*/
/*                         "class": "big right verif"}}) }}*/
/*                     <button type="submit" name="submit" class="big right">S'inscrire</button>*/
/*                     {{ form_rest(register) }}*/
/*                     {{ form_end(register) }}*/
/* */
/*                     <a href="{{ path("mmi_pros") }}" style="display: block; margin-top: 30px; float: right">Vous êtes*/
/*                         une entreprise*/
/*                         ?</a>*/
/*                 </article>*/
/*                 <article class="log" id="login">*/
/*                     <h1>Se connecter</h1>*/
/* */
/*                     {{ form_start(connection,*/
/*                     {"attr": {*/
/*                         "class": "verif_regex"}}) }}*/
/*                     {{ form_widget(connection.email,*/
/*                     {"attr": {*/
/*                         "class": "big verif",*/
/*                         "data-regex": "email"}}) }}*/
/*                     {{ form_widget(connection.password,*/
/*                     {"attr": {*/
/*                         "class": "big verif"}}) }}*/
/*                     <button type="submit" name="submit" class="big">Se connecter</button>*/
/*                     {{ form_rest(connection) }}*/
/*                     {{ form_end(connection) }}*/
/* */
/*                     <!--<h4>Ou encore plus rapide !</h4>*/
/*                     <button class="big white" type="button" style="margin-top: 20px; margin-right: 10px">Facebook</button>*/
/*                     <button class="big white" type="button" style="margin-top: 20px">LinkedIn</button>-->*/
/*                 </article>*/
/*             </section>*/
/*         {% endif %}*/
/*         <section class="illustration">*/
/*             <h1 class="quarter">Avec <span class="blue">331</span>&nbsp;:</h1>*/
/* */
/*             <section class="wrapper">*/
/*                 <img src="{{ asset("ui_img/drawing_home_01.svg") }}" alt="Améliorez votre visibilité">*/
/*                 <div class="droite">*/
/*                     <h2><span>Rejoins</span> notre communauté d'étudiants et d'anciens</h2>*/
/*                     <p>Consulte leur profils et leur témoignages</p>*/
/*                 </div>*/
/*             </section>*/
/*             <section class="wrapper">*/
/*                 <div class="gauche">*/
/*                     <h2><span>Profite</span> d'un précieux réseau d'entreprises</h2>*/
/*                     <p>Consulte nos fiches entreprises</p>*/
/*                 </div>*/
/*                 <img src="{{ asset("ui_img/drawing_home_02.svg") }}" alt="Améliorez votre visibilité">*/
/*             </section>*/
/*             <section class="wrapper">*/
/*                 <img src="{{ asset("ui_img/drawing_home_03.svg") }}" alt="Améliorez votre visibilité">*/
/*                 <div class="droite">*/
/*                     <h2><span>Participe</span> aux événements de la communauté</h2>*/
/*                     <p>Lis les articles de notre blog</p>*/
/*                 </div>*/
/*             </section>*/
/*             <section class="wrapper">*/
/*                 <div class="gauche">*/
/*                     <h2><span>Partage</span> tes connaissances avec la communauté</h2>*/
/* */
/*                     <p>Publie un article</p>*/
/* */
/*                 </div>*/
/*                 <img src="{{ asset("ui_img/drawing_home_04.svg") }}" alt="Améliorez votre visibilité">*/
/*             </section>*/
/* */
/*         </section>*/
/* */
/*         <!--*/
/*         <section class="wrapper link_to_pro">*/
/*             <div class="agency_logos quarter_wide" style="margin-right: 30px"></div>*/
/*             <div class="quarter_wide">*/
/*                 <h1>Devenez partenaires</h1>*/
/* */
/*                 <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio ratione tempora ut. Ab explicabo*/
/*                     fugiat,*/
/*                     illo labore necessitatibus neque pariatur reiciendis tempora veritatis voluptas voluptates. Lorem*/
/*                     ipsum*/
/*                     dolor sit amet, consectetur adipisicing elit. Adipisci asperiores, minima nobis non officia repellat*/
/*                     repellendus veniam vero. Dicta labore nisi reprehenderit soluta voluptas. Debitis deleniti earum*/
/*                     expedita nesciunt voluptatibus!</p>*/
/*                 <a href="{{ path("mmi_pros") }}">*/
/*                     <button class="big white" style="margin: 15px 0">Espace pro</button>*/
/*                 </a>*/
/*             </div>*/
/*         </section>*/
/*         <section class="very_light_grey inset_shadow">*/
/*             <h1 class="quarter">331 c'est aussi&nbsp;:</h1>*/
/* */
/*             <div class="wrapper">*/
/*                 <article class="tile_post">*/
/*                     <h3><a href="">Talking about something</a></h3>*/
/* */
/*                     <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab esse eveniet excepturi facilis*/
/*                         fugiat illo*/
/*                         laborum molestias nesciunt quasi velit.</p>*/
/*                     <a href="">Suivant</a>*/
/*                 </article>*/
/*                 <article class="tile_post">*/
/*                     <h3><a href="">L’association 331</a></h3>*/
/* */
/*                     <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci architecto aspernatur aut*/
/*                         cumque*/
/*                         deserunt, ea excepturi exercitationem illo laboriosam magnam molestiae natus, omnis porro, quasi*/
/*                         quo*/
/*                         quod ratione repudiandae saepe.</p>*/
/*                 </article>*/
/*             </div>*/
/*         </section>*/
/*         -->*/
/*     </main>*/
/* {% endblock %}*/
/* */
/* {% block includeBody %}*/
/*     <div id="loader"></div>*/
/*     <script src="{{ asset('js/transition.js') }}"></script>*/
/*     <script src="{{ asset("js/caroussel.js") }}"></script>*/
/*     <script src="{{ asset("js/menu.js") }}"></script>*/
/*     <script src="{{ asset('assets/bodymovin.js') }}"></script>*/
/*     <script>*/
/* */
/*         $(document).ready(function(){*/
/*             var animData = {*/
/*                 container: document.getElementById('loader'),*/
/*                 renderer: 'svg',*/
/*                 loop: true,*/
/*                 autoplay: true,*/
/*                 path: '{{ asset('assets/data_anim.json') }}'*/
/*             };*/
/* */
/*             var anim = bodymovin.loadAnimation(animData);*/
/* */
/*             $(window).load(function(){*/
/* */
/*                 var modal_content = $('#errorModal .no_errors').length;*/
/* */
/*                 if (modal_content == 0){*/
/*                     $('#errorModal').modal('show')*/
/*                 }*/
/*             });*/
/* */
/*         });*/
/* */
/*     </script>*/
/* {% endblock %}*/
/* */
