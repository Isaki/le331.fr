<?php

/* MMIBundle:Utilisateur:edit_student.html.twig */
class __TwigTemplate_94eb5629f2ea5794d3614b021e135d3b1d38357dce23e34ba2c72df9c605d1c2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("MMIBundle::template.html.twig", "MMIBundle:Utilisateur:edit_student.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'includeHead' => array($this, 'block_includeHead'),
            'bodyClass' => array($this, 'block_bodyClass'),
            'header' => array($this, 'block_header'),
            'headerExtended' => array($this, 'block_headerExtended'),
            'content' => array($this, 'block_content'),
            'includeBody' => array($this, 'block_includeBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "MMIBundle::template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a79b468ace682a53541cfadd07150c2ead86e0d1111e091e7d72b6a143c339ce = $this->env->getExtension("native_profiler");
        $__internal_a79b468ace682a53541cfadd07150c2ead86e0d1111e091e7d72b6a143c339ce->enter($__internal_a79b468ace682a53541cfadd07150c2ead86e0d1111e091e7d72b6a143c339ce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MMIBundle:Utilisateur:edit_student.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a79b468ace682a53541cfadd07150c2ead86e0d1111e091e7d72b6a143c339ce->leave($__internal_a79b468ace682a53541cfadd07150c2ead86e0d1111e091e7d72b6a143c339ce_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_76d7dfe4aa8eec721eac273b72837324332e966301cc57b57f5116d3cb969696 = $this->env->getExtension("native_profiler");
        $__internal_76d7dfe4aa8eec721eac273b72837324332e966301cc57b57f5116d3cb969696->enter($__internal_76d7dfe4aa8eec721eac273b72837324332e966301cc57b57f5116d3cb969696_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Paramètres de votre profil";
        
        $__internal_76d7dfe4aa8eec721eac273b72837324332e966301cc57b57f5116d3cb969696->leave($__internal_76d7dfe4aa8eec721eac273b72837324332e966301cc57b57f5116d3cb969696_prof);

    }

    // line 5
    public function block_includeHead($context, array $blocks = array())
    {
        $__internal_559ea5cdc4bcfcf1023cc57498d3b3b5ec2302d9991a0b31560fd561284152cd = $this->env->getExtension("native_profiler");
        $__internal_559ea5cdc4bcfcf1023cc57498d3b3b5ec2302d9991a0b31560fd561284152cd->enter($__internal_559ea5cdc4bcfcf1023cc57498d3b3b5ec2302d9991a0b31560fd561284152cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "includeHead"));

        // line 6
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("assets/jquery-ui-1.11.4/jquery-ui.min.css"), "html", null, true);
        echo "\">
    <script type=\"text/javascript\" src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/form_regex.js"), "html", null, true);
        echo "\"></script>
    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/welcome_sender.css"), "html", null, true);
        echo "\">
";
        
        $__internal_559ea5cdc4bcfcf1023cc57498d3b3b5ec2302d9991a0b31560fd561284152cd->leave($__internal_559ea5cdc4bcfcf1023cc57498d3b3b5ec2302d9991a0b31560fd561284152cd_prof);

    }

    // line 11
    public function block_bodyClass($context, array $blocks = array())
    {
        $__internal_1d38fd2f0dff08f9bf652ebb9f9e44508d685ca87cd8cc16c0071bf933a0f3b9 = $this->env->getExtension("native_profiler");
        $__internal_1d38fd2f0dff08f9bf652ebb9f9e44508d685ca87cd8cc16c0071bf933a0f3b9->enter($__internal_1d38fd2f0dff08f9bf652ebb9f9e44508d685ca87cd8cc16c0071bf933a0f3b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "bodyClass"));

        echo "edit";
        
        $__internal_1d38fd2f0dff08f9bf652ebb9f9e44508d685ca87cd8cc16c0071bf933a0f3b9->leave($__internal_1d38fd2f0dff08f9bf652ebb9f9e44508d685ca87cd8cc16c0071bf933a0f3b9_prof);

    }

    // line 13
    public function block_header($context, array $blocks = array())
    {
        $__internal_9dca9121d037f59154a9c7f6f87b4fdc11e4679f6e05a748fb52617994a888fe = $this->env->getExtension("native_profiler");
        $__internal_9dca9121d037f59154a9c7f6f87b4fdc11e4679f6e05a748fb52617994a888fe->enter($__internal_9dca9121d037f59154a9c7f6f87b4fdc11e4679f6e05a748fb52617994a888fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        
        $__internal_9dca9121d037f59154a9c7f6f87b4fdc11e4679f6e05a748fb52617994a888fe->leave($__internal_9dca9121d037f59154a9c7f6f87b4fdc11e4679f6e05a748fb52617994a888fe_prof);

    }

    // line 15
    public function block_headerExtended($context, array $blocks = array())
    {
        $__internal_13c144979d58aaf761c08f6ff2298c083392dc73d6baba3e1238d64165873cef = $this->env->getExtension("native_profiler");
        $__internal_13c144979d58aaf761c08f6ff2298c083392dc73d6baba3e1238d64165873cef->enter($__internal_13c144979d58aaf761c08f6ff2298c083392dc73d6baba3e1238d64165873cef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "headerExtended"));

        // line 16
        echo "    <h1>Paramètres de votre profil</h1>
";
        
        $__internal_13c144979d58aaf761c08f6ff2298c083392dc73d6baba3e1238d64165873cef->leave($__internal_13c144979d58aaf761c08f6ff2298c083392dc73d6baba3e1238d64165873cef_prof);

    }

    // line 19
    public function block_content($context, array $blocks = array())
    {
        $__internal_a7574d4078132ad37456714efe3ade9ba386a72c3c250179954bf08c0d95734e = $this->env->getExtension("native_profiler");
        $__internal_a7574d4078132ad37456714efe3ade9ba386a72c3c250179954bf08c0d95734e->enter($__internal_a7574d4078132ad37456714efe3ade9ba386a72c3c250179954bf08c0d95734e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 20
        echo "    <main>
        <a href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("mmi_student", array("id" => $this->getAttribute((isset($context["account"]) ? $context["account"] : $this->getContext($context, "account")), "id", array()))), "html", null, true);
        echo "\">Voir un aperçu de mon profil</a>
        ";
        // line 22
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit"]) ? $context["edit"] : $this->getContext($context, "edit")), 'form_start', array("attr" => array("enctype" => "multipart/form-data")));
        // line 24
        echo "
        <section>
            <h2>Description</h2>
            ";
        // line 27
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit"]) ? $context["edit"] : $this->getContext($context, "edit")), "firstname", array()), 'widget', array("attr" => array("class" => "big stretch", "data-regex" => "nom")));
        // line 30
        echo "
            ";
        // line 31
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit"]) ? $context["edit"] : $this->getContext($context, "edit")), "lastname", array()), 'widget', array("attr" => array("class" => "big stretch", "data-regex" => "nom")));
        // line 34
        echo "
            ";
        // line 35
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit"]) ? $context["edit"] : $this->getContext($context, "edit")), "description", array()), 'widget');
        echo "
            ";
        // line 36
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit"]) ? $context["edit"] : $this->getContext($context, "edit")), "profession", array()), 'widget', array("attr" => array("class" => "custom-select sources selector_type")));
        // line 38
        echo "
            ";
        // line 39
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit"]) ? $context["edit"] : $this->getContext($context, "edit")), "promo", array()), 'widget', array("attr" => array("class" => "custom-select sources selector_type")));
        // line 41
        echo "
        </section>

        <section>
            <h2>Photo de profil</h2>
            ";
        // line 46
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit"]) ? $context["edit"] : $this->getContext($context, "edit")), "image_profil_file", array()), 'widget', array("attr" => array("class" => "big grey custom-input-file", "data-title" => "Sélectionner")));
        // line 49
        echo "
            <p style=\"margin-bottom: 30px\">La photo doit faire moins de 2MB et doit être au format jpeg, png ou gif</p>

            <h2>Photo de couverture</h2>
            ";
        // line 53
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit"]) ? $context["edit"] : $this->getContext($context, "edit")), "image_couverture_file", array()), 'widget', array("attr" => array("class" => "big grey custom-input-file", "data-title" => "Sélectionner")));
        // line 56
        echo "

            <p>La photo doit faire moins de 2MB et doit être au format jpeg, png ou gif</p>
        </section>

        <section>
            <h2>Informations</h2>

            <div class=\"dateSelector\" style=\"margin: 25px 0 10px 0\">
                ";
        // line 65
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit"]) ? $context["edit"] : $this->getContext($context, "edit")), "birthday", array()), 'widget', array("attr" => array("class" => "big stretch"), "id" => "datePicker"));
        // line 68
        echo "
            </div>
            ";
        // line 70
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit"]) ? $context["edit"] : $this->getContext($context, "edit")), "telephone", array()), 'widget', array("attr" => array("class" => "big stretch")));
        // line 72
        echo "
            ";
        // line 73
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit"]) ? $context["edit"] : $this->getContext($context, "edit")), "url_portfolio", array()), 'widget', array("attr" => array("class" => "big stretch", "data-regex" => "lien")));
        // line 76
        echo "
        </section>

        <section>
            <h2>Votre CV</h2>
            ";
        // line 81
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit"]) ? $context["edit"] : $this->getContext($context, "edit")), "cv_file", array()), 'widget', array("attr" => array("class" => "big grey custom-input-file", "data-title" => "Sélectionner")));
        // line 84
        echo "
            <p>2MB max au format pdf</p>
        </section>

        <section>
            <h2>L'adresse</h2>
            ";
        // line 90
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit"]) ? $context["edit"] : $this->getContext($context, "edit")), "cp", array()), 'widget', array("attr" => array("class" => "big stretch", "data-regex" => "cp")));
        // line 93
        echo "
            ";
        // line 94
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit"]) ? $context["edit"] : $this->getContext($context, "edit")), "city_id", array()), 'widget', array("attr" => array("class" => "big stretch", "data-regex" => "nom")));
        // line 97
        echo "
        </section>

        <section>
            <h2>Social <span style=\"font-size: 16px;\">(facultatif)</span></h2>
            ";
        // line 102
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit"]) ? $context["edit"] : $this->getContext($context, "edit")), "url_blog", array()), 'widget', array("attr" => array("class" => "big stretch", "data-regex" => "lien")));
        // line 105
        echo "
            ";
        // line 106
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit"]) ? $context["edit"] : $this->getContext($context, "edit")), "url_facebook", array()), 'widget', array("attr" => array("class" => "big stretch", "data-regex" => "lien")));
        // line 109
        echo "
            ";
        // line 110
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit"]) ? $context["edit"] : $this->getContext($context, "edit")), "url_twitter", array()), 'widget', array("attr" => array("class" => "big stretch", "data-regex" => "lien")));
        // line 113
        echo "
            ";
        // line 114
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit"]) ? $context["edit"] : $this->getContext($context, "edit")), "url_linkedin", array()), 'widget', array("attr" => array("class" => "big stretch", "data-regex" => "lien")));
        // line 117
        echo "
            ";
        // line 118
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit"]) ? $context["edit"] : $this->getContext($context, "edit")), "url_instagram", array()), 'widget', array("attr" => array("class" => "big stretch", "data-regex" => "lien")));
        // line 121
        echo "
            ";
        // line 122
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["edit"]) ? $context["edit"] : $this->getContext($context, "edit")), "url_tumblr", array()), 'widget', array("attr" => array("class" => "big stretch", "data-regex" => "lien")));
        // line 125
        echo "
        </section>

        <button type=\"submit\" name=\"submit\" style=\"margin-bottom: 30px\" class=\"big\">Modifier</button>
        ";
        // line 129
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["edit"]) ? $context["edit"] : $this->getContext($context, "edit")), 'rest');
        echo "
        ";
        // line 130
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit"]) ? $context["edit"] : $this->getContext($context, "edit")), 'form_end');
        echo "
    </main>
";
        
        $__internal_a7574d4078132ad37456714efe3ade9ba386a72c3c250179954bf08c0d95734e->leave($__internal_a7574d4078132ad37456714efe3ade9ba386a72c3c250179954bf08c0d95734e_prof);

    }

    // line 134
    public function block_includeBody($context, array $blocks = array())
    {
        $__internal_505005c43cabc2193b940ca2586430ba6910992bf1f6d5b0a2231c57f8e6beb3 = $this->env->getExtension("native_profiler");
        $__internal_505005c43cabc2193b940ca2586430ba6910992bf1f6d5b0a2231c57f8e6beb3->enter($__internal_505005c43cabc2193b940ca2586430ba6910992bf1f6d5b0a2231c57f8e6beb3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "includeBody"));

        // line 135
        echo "    <script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("assets/jquery-latest.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 136
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("assets/jquery-ui-1.11.4/jquery-ui.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 137
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("assets/datePicker.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 138
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/welcome_sender.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 139
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/custom_select.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 140
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/custom_input_file.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 141
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/ajax/ajaxGetByType.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\">
        \$(document).ready(function () {
            ajaxListGetByType('City', \$('input#UserEditType_city_id'));
        });
    </script>
";
        
        $__internal_505005c43cabc2193b940ca2586430ba6910992bf1f6d5b0a2231c57f8e6beb3->leave($__internal_505005c43cabc2193b940ca2586430ba6910992bf1f6d5b0a2231c57f8e6beb3_prof);

    }

    public function getTemplateName()
    {
        return "MMIBundle:Utilisateur:edit_student.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  304 => 141,  300 => 140,  296 => 139,  292 => 138,  288 => 137,  284 => 136,  279 => 135,  273 => 134,  263 => 130,  259 => 129,  253 => 125,  251 => 122,  248 => 121,  246 => 118,  243 => 117,  241 => 114,  238 => 113,  236 => 110,  233 => 109,  231 => 106,  228 => 105,  226 => 102,  219 => 97,  217 => 94,  214 => 93,  212 => 90,  204 => 84,  202 => 81,  195 => 76,  193 => 73,  190 => 72,  188 => 70,  184 => 68,  182 => 65,  171 => 56,  169 => 53,  163 => 49,  161 => 46,  154 => 41,  152 => 39,  149 => 38,  147 => 36,  143 => 35,  140 => 34,  138 => 31,  135 => 30,  133 => 27,  128 => 24,  126 => 22,  122 => 21,  119 => 20,  113 => 19,  105 => 16,  99 => 15,  88 => 13,  76 => 11,  67 => 8,  63 => 7,  58 => 6,  52 => 5,  40 => 3,  11 => 1,);
    }
}
/* {% extends "MMIBundle::template.html.twig" %}*/
/* */
/* {% block title %}Paramètres de votre profil{% endblock %}*/
/* */
/* {% block includeHead %}*/
/*     <link rel="stylesheet" href="{{ asset("assets/jquery-ui-1.11.4/jquery-ui.min.css") }}">*/
/*     <script type="text/javascript" src="{{ asset('js/form_regex.js') }}"></script>*/
/*     <link type="text/css" rel="stylesheet" href="{{ asset("css/welcome_sender.css") }}">*/
/* {% endblock %}*/
/* */
/* {% block bodyClass %}edit{% endblock %}*/
/* */
/* {% block header %}{% endblock %}*/
/* */
/* {% block headerExtended %}*/
/*     <h1>Paramètres de votre profil</h1>*/
/* {% endblock %}*/
/* */
/* {% block content %}*/
/*     <main>*/
/*         <a href="{{ path('mmi_student',{id: account.id}) }}">Voir un aperçu de mon profil</a>*/
/*         {{ form_start(edit,*/
/*         {"attr": {*/
/*             "enctype": "multipart/form-data"}}) }}*/
/*         <section>*/
/*             <h2>Description</h2>*/
/*             {{ form_widget(edit.firstname,*/
/*             {"attr": {*/
/*                 "class": "big stretch",*/
/*                 "data-regex": "nom"}}) }}*/
/*             {{ form_widget(edit.lastname,*/
/*             {"attr": {*/
/*                 "class": "big stretch",*/
/*                 "data-regex": "nom"}}) }}*/
/*             {{ form_widget(edit.description) }}*/
/*             {{ form_widget(edit.profession,*/
/*             {"attr": {*/
/*                 "class": "custom-select sources selector_type"}}) }}*/
/*             {{ form_widget(edit.promo,*/
/*             {"attr": {*/
/*                 "class": "custom-select sources selector_type"}}) }}*/
/*         </section>*/
/* */
/*         <section>*/
/*             <h2>Photo de profil</h2>*/
/*             {{ form_widget(edit.image_profil_file,*/
/*             {"attr": {*/
/*                 "class": "big grey custom-input-file",*/
/*                 "data-title": "Sélectionner"}}) }}*/
/*             <p style="margin-bottom: 30px">La photo doit faire moins de 2MB et doit être au format jpeg, png ou gif</p>*/
/* */
/*             <h2>Photo de couverture</h2>*/
/*             {{ form_widget(edit.image_couverture_file,*/
/*             {"attr": {*/
/*                 "class": "big grey custom-input-file",*/
/*                 "data-title": "Sélectionner"}}) }}*/
/* */
/*             <p>La photo doit faire moins de 2MB et doit être au format jpeg, png ou gif</p>*/
/*         </section>*/
/* */
/*         <section>*/
/*             <h2>Informations</h2>*/
/* */
/*             <div class="dateSelector" style="margin: 25px 0 10px 0">*/
/*                 {{ form_widget(edit.birthday,*/
/*                 {"attr": {*/
/*                     "class": "big stretch"},*/
/*                     "id": "datePicker"}) }}*/
/*             </div>*/
/*             {{ form_widget(edit.telephone,*/
/*             {"attr": {*/
/*                 "class": "big stretch"}}) }}*/
/*             {{ form_widget(edit.url_portfolio,*/
/*             {"attr": {*/
/*                 "class": "big stretch",*/
/*                 "data-regex": "lien"}}) }}*/
/*         </section>*/
/* */
/*         <section>*/
/*             <h2>Votre CV</h2>*/
/*             {{ form_widget(edit.cv_file,*/
/*             {"attr": {*/
/*                 "class": "big grey custom-input-file",*/
/*                 "data-title": "Sélectionner"}}) }}*/
/*             <p>2MB max au format pdf</p>*/
/*         </section>*/
/* */
/*         <section>*/
/*             <h2>L'adresse</h2>*/
/*             {{ form_widget(edit.cp,*/
/*             {"attr": {*/
/*                 "class": "big stretch",*/
/*                 "data-regex": "cp"}}) }}*/
/*             {{ form_widget(edit.city_id,*/
/*             {"attr": {*/
/*                 "class": "big stretch",*/
/*                 "data-regex": "nom"}}) }}*/
/*         </section>*/
/* */
/*         <section>*/
/*             <h2>Social <span style="font-size: 16px;">(facultatif)</span></h2>*/
/*             {{ form_widget(edit.url_blog,*/
/*             {"attr": {*/
/*                 "class": "big stretch",*/
/*                 "data-regex": "lien"}}) }}*/
/*             {{ form_widget(edit.url_facebook,*/
/*             {"attr": {*/
/*                 "class": "big stretch",*/
/*                 "data-regex": "lien"}}) }}*/
/*             {{ form_widget(edit.url_twitter,*/
/*             {"attr": {*/
/*                 "class": "big stretch",*/
/*                 "data-regex": "lien"}}) }}*/
/*             {{ form_widget(edit.url_linkedin,*/
/*             {"attr": {*/
/*                 "class": "big stretch",*/
/*                 "data-regex": "lien"}}) }}*/
/*             {{ form_widget(edit.url_instagram,*/
/*             {"attr": {*/
/*                 "class": "big stretch",*/
/*                 "data-regex": "lien"}}) }}*/
/*             {{ form_widget(edit.url_tumblr,*/
/*             {"attr": {*/
/*                 "class": "big stretch",*/
/*                 "data-regex": "lien"}}) }}*/
/*         </section>*/
/* */
/*         <button type="submit" name="submit" style="margin-bottom: 30px" class="big">Modifier</button>*/
/*         {{ form_rest(edit) }}*/
/*         {{ form_end(edit) }}*/
/*     </main>*/
/* {% endblock %}*/
/* */
/* {% block includeBody %}*/
/*     <script type="text/javascript" src="{{ asset("assets/jquery-latest.min.js") }}"></script>*/
/*     <script type="text/javascript" src="{{ asset("assets/jquery-ui-1.11.4/jquery-ui.min.js") }}"></script>*/
/*     <script type="text/javascript" src="{{ asset("assets/datePicker.js") }}"></script>*/
/*     <script type="text/javascript" src="{{ asset("js/welcome_sender.js") }}"></script>*/
/*     <script type="text/javascript" src="{{ asset("js/custom_select.js") }}"></script>*/
/*     <script type="text/javascript" src="{{ asset("js/custom_input_file.js") }}"></script>*/
/*     <script type="text/javascript" src="{{ asset('js/ajax/ajaxGetByType.js') }}"></script>*/
/*     <script type="text/javascript">*/
/*         $(document).ready(function () {*/
/*             ajaxListGetByType('City', $('input#UserEditType_city_id'));*/
/*         });*/
/*     </script>*/
/* {% endblock %}*/
/* */
