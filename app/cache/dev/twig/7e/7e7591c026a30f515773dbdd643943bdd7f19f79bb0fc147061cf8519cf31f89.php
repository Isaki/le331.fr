<?php

/* MMIBundle:Entreprise:welcome_company.html.twig */
class __TwigTemplate_615547a6b57eced52527438bdd213128c007cf5c94c99a9faa894cb0e8c8fdaf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("MMIBundle::template.html.twig", "MMIBundle:Entreprise:welcome_company.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'includeHead' => array($this, 'block_includeHead'),
            'bodyClass' => array($this, 'block_bodyClass'),
            'headerExtended' => array($this, 'block_headerExtended'),
            'content' => array($this, 'block_content'),
            'includeBody' => array($this, 'block_includeBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "MMIBundle::template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8de7ef51c64a2072f8ffc8c5840e66a43088912a0ce3a2d81049d01e39abe081 = $this->env->getExtension("native_profiler");
        $__internal_8de7ef51c64a2072f8ffc8c5840e66a43088912a0ce3a2d81049d01e39abe081->enter($__internal_8de7ef51c64a2072f8ffc8c5840e66a43088912a0ce3a2d81049d01e39abe081_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MMIBundle:Entreprise:welcome_company.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8de7ef51c64a2072f8ffc8c5840e66a43088912a0ce3a2d81049d01e39abe081->leave($__internal_8de7ef51c64a2072f8ffc8c5840e66a43088912a0ce3a2d81049d01e39abe081_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_53582d13e44ba6f6037feccafebecc03b021ec227394bda8342850176f647ccd = $this->env->getExtension("native_profiler");
        $__internal_53582d13e44ba6f6037feccafebecc03b021ec227394bda8342850176f647ccd->enter($__internal_53582d13e44ba6f6037feccafebecc03b021ec227394bda8342850176f647ccd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Bienvenue ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["account"]) ? $context["account"] : $this->getContext($context, "account")), "name", array()), "html", null, true);
        echo " !";
        
        $__internal_53582d13e44ba6f6037feccafebecc03b021ec227394bda8342850176f647ccd->leave($__internal_53582d13e44ba6f6037feccafebecc03b021ec227394bda8342850176f647ccd_prof);

    }

    // line 5
    public function block_includeHead($context, array $blocks = array())
    {
        $__internal_4018da180454a5b12ac6f80c5744ee772abc811c9a0a896cd4569026efeeda60 = $this->env->getExtension("native_profiler");
        $__internal_4018da180454a5b12ac6f80c5744ee772abc811c9a0a896cd4569026efeeda60->enter($__internal_4018da180454a5b12ac6f80c5744ee772abc811c9a0a896cd4569026efeeda60_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "includeHead"));

        // line 6
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/form_regex.js"), "html", null, true);
        echo "\"></script>
    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/welcome_sender.css"), "html", null, true);
        echo "\">
";
        
        $__internal_4018da180454a5b12ac6f80c5744ee772abc811c9a0a896cd4569026efeeda60->leave($__internal_4018da180454a5b12ac6f80c5744ee772abc811c9a0a896cd4569026efeeda60_prof);

    }

    // line 10
    public function block_bodyClass($context, array $blocks = array())
    {
        $__internal_6650800cd1bc47ac63324211d2995a49f68eafaa3159f3f10a34d42cc693744d = $this->env->getExtension("native_profiler");
        $__internal_6650800cd1bc47ac63324211d2995a49f68eafaa3159f3f10a34d42cc693744d->enter($__internal_6650800cd1bc47ac63324211d2995a49f68eafaa3159f3f10a34d42cc693744d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "bodyClass"));

        echo "welcome wcCompany";
        
        $__internal_6650800cd1bc47ac63324211d2995a49f68eafaa3159f3f10a34d42cc693744d->leave($__internal_6650800cd1bc47ac63324211d2995a49f68eafaa3159f3f10a34d42cc693744d_prof);

    }

    // line 12
    public function block_headerExtended($context, array $blocks = array())
    {
        $__internal_401029ebf7a5738fabc0619deff806e8694f6aee9deccc92499f08e563f4b5db = $this->env->getExtension("native_profiler");
        $__internal_401029ebf7a5738fabc0619deff806e8694f6aee9deccc92499f08e563f4b5db->enter($__internal_401029ebf7a5738fabc0619deff806e8694f6aee9deccc92499f08e563f4b5db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "headerExtended"));

        // line 13
        echo "    <div>
        <h1>Bienvenue parmis nous ";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["account"]) ? $context["account"] : $this->getContext($context, "account")), "name", array()), "html", null, true);
        echo " !</h1>

        <p>La communauté 331 est impatiente de mieux vous connaître.</p>
    </div>
";
        
        $__internal_401029ebf7a5738fabc0619deff806e8694f6aee9deccc92499f08e563f4b5db->leave($__internal_401029ebf7a5738fabc0619deff806e8694f6aee9deccc92499f08e563f4b5db_prof);

    }

    // line 20
    public function block_content($context, array $blocks = array())
    {
        $__internal_27d17b87b578992e3a9a91e96304a1dcc78c5f696698aa01eaf7beba65ffe123 = $this->env->getExtension("native_profiler");
        $__internal_27d17b87b578992e3a9a91e96304a1dcc78c5f696698aa01eaf7beba65ffe123->enter($__internal_27d17b87b578992e3a9a91e96304a1dcc78c5f696698aa01eaf7beba65ffe123_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 21
        echo "    <main>
        <h2>Informations pratiques</h2>

        ";
        // line 24
        if (array_key_exists("result", $context)) {
            echo twig_escape_filter($this->env, (isset($context["result"]) ? $context["result"] : $this->getContext($context, "result")), "html", null, true);
        }
        // line 25
        echo "
        ";
        // line 26
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["welcome"]) ? $context["welcome"] : $this->getContext($context, "welcome")), 'form_start', array("attr" => array("enctype" => "multipart/form-data")));
        // line 27
        echo "
        <section>
            <h3>Description</h3>
            ";
        // line 30
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["welcome"]) ? $context["welcome"] : $this->getContext($context, "welcome")), "description", array()), 'widget');
        echo "
        </section>

        <section>
            <h3>Adresse</h3>
            ";
        // line 35
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["welcome"]) ? $context["welcome"] : $this->getContext($context, "welcome")), "address", array()), 'widget', array("attr" => array("class" => "big stretch")));
        // line 37
        echo "
            ";
        // line 38
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["welcome"]) ? $context["welcome"] : $this->getContext($context, "welcome")), "city", array()), 'widget', array("attr" => array("class" => "big stretch_inline")));
        // line 40
        echo "
            ";
        // line 41
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["welcome"]) ? $context["welcome"] : $this->getContext($context, "welcome")), "cp", array()), 'widget', array("attr" => array("class" => "big stretch_inline")));
        // line 43
        echo "
        </section>

        <section class=\"inline\">
            <h3>Contact</h3>
            ";
        // line 48
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["welcome"]) ? $context["welcome"] : $this->getContext($context, "welcome")), "firstname", array()), 'widget', array("attr" => array("class" => "big stretch", "data-regex" => "nom")));
        // line 51
        echo "
            ";
        // line 52
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["welcome"]) ? $context["welcome"] : $this->getContext($context, "welcome")), "lastname", array()), 'widget', array("attr" => array("class" => "big stretch", "data-regex" => "nom")));
        // line 55
        echo "
            ";
        // line 56
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["welcome"]) ? $context["welcome"] : $this->getContext($context, "welcome")), "profession", array()), 'widget', array("attr" => array("class" => "big stretch")));
        // line 58
        echo "
            ";
        // line 59
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["welcome"]) ? $context["welcome"] : $this->getContext($context, "welcome")), "telephone", array()), 'widget', array("attr" => array("class" => "big stretch")));
        // line 61
        echo "
            ";
        // line 62
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["welcome"]) ? $context["welcome"] : $this->getContext($context, "welcome")), "email", array()), 'widget', array("attr" => array("class" => "big stretch", "data-regex" => "email")));
        // line 65
        echo "
        </section>

        <section class=\"inline\">
            <h3>Entreprise</h3>

            <div class=\"button_border\">
                Votre secteur d’activité ?
                <select name=\"secteur\" class=\"\" placeholder=\"Sélectionner\">
                    <option value=\"Choix 1\">Choix 1</option>
                    <option value=\"Choix 2\">Choix 2</option>
                    <option value=\"Choix 3\">Choix 3</option>
                </select>
            </div>
            ";
        // line 79
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["welcome"]) ? $context["welcome"] : $this->getContext($context, "welcome")), "url_website", array()), 'widget', array("attr" => array("class" => "big stretch", "data-regex" => "lien")));
        // line 82
        echo "
            <div class=\"button_border\">
                Combien d’employés ?
                <select name=\"secteur\" class=\"\" placeholder=\"Sélectionner\">
                    <option value=\"1 à 10\">0 à 10</option>
                    <option value=\"10 à 25\">10 à 25</option>
                    <option value=\"25 à 50\">25 à 50</option>
                    <option value=\"50 à 100\">50 à 100</option>
                    <option value=\"100 & +\">100 et +</option>
                </select>
            </div>
        </section>

        ";
        // line 104
        echo "
        <p>Notre outils technologique ultra avancé nous enverra vos réponses, lancez-le à la fin du questionnaire
            !</p>

        <div class=\"welcome_sender\">
            <button type=\"submit\" id=\"plane\">Go !</button>
        </div>
        ";
        // line 111
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["welcome"]) ? $context["welcome"] : $this->getContext($context, "welcome")), 'rest');
        echo "
        ";
        // line 112
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["welcome"]) ? $context["welcome"] : $this->getContext($context, "welcome")), 'form_end');
        echo "
    </main>
";
        
        $__internal_27d17b87b578992e3a9a91e96304a1dcc78c5f696698aa01eaf7beba65ffe123->leave($__internal_27d17b87b578992e3a9a91e96304a1dcc78c5f696698aa01eaf7beba65ffe123_prof);

    }

    // line 116
    public function block_includeBody($context, array $blocks = array())
    {
        $__internal_db4d7e2e9e046df7c1a4c94668b9332d568113600c1bf3582746669d6b42b9b9 = $this->env->getExtension("native_profiler");
        $__internal_db4d7e2e9e046df7c1a4c94668b9332d568113600c1bf3582746669d6b42b9b9->enter($__internal_db4d7e2e9e046df7c1a4c94668b9332d568113600c1bf3582746669d6b42b9b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "includeBody"));

        // line 117
        echo "    <script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("assets/jquery-custom.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 118
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("assets/datePicker.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 119
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/welcome_sender.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 120
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/custom_select.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_db4d7e2e9e046df7c1a4c94668b9332d568113600c1bf3582746669d6b42b9b9->leave($__internal_db4d7e2e9e046df7c1a4c94668b9332d568113600c1bf3582746669d6b42b9b9_prof);

    }

    public function getTemplateName()
    {
        return "MMIBundle:Entreprise:welcome_company.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  255 => 120,  251 => 119,  247 => 118,  242 => 117,  236 => 116,  226 => 112,  222 => 111,  213 => 104,  198 => 82,  196 => 79,  180 => 65,  178 => 62,  175 => 61,  173 => 59,  170 => 58,  168 => 56,  165 => 55,  163 => 52,  160 => 51,  158 => 48,  151 => 43,  149 => 41,  146 => 40,  144 => 38,  141 => 37,  139 => 35,  131 => 30,  126 => 27,  124 => 26,  121 => 25,  117 => 24,  112 => 21,  106 => 20,  94 => 14,  91 => 13,  85 => 12,  73 => 10,  64 => 7,  59 => 6,  53 => 5,  39 => 3,  11 => 1,);
    }
}
/* {% extends "MMIBundle::template.html.twig" %}*/
/* */
/* {% block title %}Bienvenue {{ account.name }} !{% endblock %}*/
/* */
/* {% block includeHead %}*/
/*     <script src="{{ asset('js/form_regex.js') }}"></script>*/
/*     <link type="text/css" rel="stylesheet" href="{{ asset("css/welcome_sender.css") }}">*/
/* {% endblock %}*/
/* */
/* {% block bodyClass %}welcome wcCompany{% endblock %}*/
/* */
/* {% block headerExtended %}*/
/*     <div>*/
/*         <h1>Bienvenue parmis nous {{ account.name }} !</h1>*/
/* */
/*         <p>La communauté 331 est impatiente de mieux vous connaître.</p>*/
/*     </div>*/
/* {% endblock %}*/
/* */
/* {% block content %}*/
/*     <main>*/
/*         <h2>Informations pratiques</h2>*/
/* */
/*         {% if result is defined %}{{ result }}{% endif %}*/
/* */
/*         {{ form_start(welcome,*/
/*         {"attr": {"enctype": "multipart/form-data"}}) }}*/
/*         <section>*/
/*             <h3>Description</h3>*/
/*             {{ form_widget(welcome.description) }}*/
/*         </section>*/
/* */
/*         <section>*/
/*             <h3>Adresse</h3>*/
/*             {{ form_widget(welcome.address,*/
/*             {"attr": {*/
/*                 "class": "big stretch"}}) }}*/
/*             {{ form_widget(welcome.city,*/
/*             {"attr": {*/
/*                 "class": "big stretch_inline"}}) }}*/
/*             {{ form_widget(welcome.cp,*/
/*             {"attr": {*/
/*                 "class": "big stretch_inline"}}) }}*/
/*         </section>*/
/* */
/*         <section class="inline">*/
/*             <h3>Contact</h3>*/
/*             {{ form_widget(welcome.firstname,*/
/*             {"attr": {*/
/*                 "class": "big stretch",*/
/*                 "data-regex": "nom"}}) }}*/
/*             {{ form_widget(welcome.lastname,*/
/*             {"attr": {*/
/*                 "class": "big stretch",*/
/*                 "data-regex": "nom"}}) }}*/
/*             {{ form_widget(welcome.profession,*/
/*             {"attr": {*/
/*                 "class": "big stretch"}}) }}*/
/*             {{ form_widget(welcome.telephone,*/
/*             {"attr": {*/
/*                 "class": "big stretch"}}) }}*/
/*             {{ form_widget(welcome.email,*/
/*             {"attr": {*/
/*                 "class": "big stretch",*/
/*                 "data-regex": "email"}}) }}*/
/*         </section>*/
/* */
/*         <section class="inline">*/
/*             <h3>Entreprise</h3>*/
/* */
/*             <div class="button_border">*/
/*                 Votre secteur d’activité ?*/
/*                 <select name="secteur" class="" placeholder="Sélectionner">*/
/*                     <option value="Choix 1">Choix 1</option>*/
/*                     <option value="Choix 2">Choix 2</option>*/
/*                     <option value="Choix 3">Choix 3</option>*/
/*                 </select>*/
/*             </div>*/
/*             {{ form_widget(welcome.url_website,*/
/*             {"attr": {*/
/*                 "class": "big stretch",*/
/*                 "data-regex": "lien"}}) }}*/
/*             <div class="button_border">*/
/*                 Combien d’employés ?*/
/*                 <select name="secteur" class="" placeholder="Sélectionner">*/
/*                     <option value="1 à 10">0 à 10</option>*/
/*                     <option value="10 à 25">10 à 25</option>*/
/*                     <option value="25 à 50">25 à 50</option>*/
/*                     <option value="50 à 100">50 à 100</option>*/
/*                     <option value="100 & +">100 et +</option>*/
/*                 </select>*/
/*             </div>*/
/*         </section>*/
/* */
/*         {#<section class="inline">*/
/*             <h3>Social</h3>*/
/*             <input type="text" class="big stretch" name="facebook"*/
/*                    placeholder="Quelle est le lien vers votre Facebook ?">*/
/*             <input type="text" class="big stretch" name="twitter"*/
/*                    placeholder="Quelle est le lien vers votre Twitter ?">*/
/*             <input type="text" class="big stretch" name="linkedin"*/
/*                    placeholder="Quelle est le lien vers votre LinkedIn ?">*/
/*         </section>#}*/
/* */
/*         <p>Notre outils technologique ultra avancé nous enverra vos réponses, lancez-le à la fin du questionnaire*/
/*             !</p>*/
/* */
/*         <div class="welcome_sender">*/
/*             <button type="submit" id="plane">Go !</button>*/
/*         </div>*/
/*         {{ form_rest(welcome) }}*/
/*         {{ form_end(welcome) }}*/
/*     </main>*/
/* {% endblock %}*/
/* */
/* {% block includeBody %}*/
/*     <script type="text/javascript" src="{{ asset("assets/jquery-custom.min.js") }}"></script>*/
/*     <script type="text/javascript" src="{{ asset("assets/datePicker.js") }}"></script>*/
/*     <script type="text/javascript" src="{{ asset("js/welcome_sender.js") }}"></script>*/
/*     <script type="text/javascript" src="{{ asset("js/custom_select.js") }}"></script>*/
/* {% endblock %}*/
/* */
