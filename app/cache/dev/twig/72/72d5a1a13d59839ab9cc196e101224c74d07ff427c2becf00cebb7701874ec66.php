<?php

/* MMIBundle:Default:pros.html.twig */
class __TwigTemplate_89fb2b3bfd5c9a9a2c053167dd9412c21a2b1c48c50c8dec1f12f46a7877e6f2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("MMIBundle::template.html.twig", "MMIBundle:Default:pros.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'includeHead' => array($this, 'block_includeHead'),
            'bodyClass' => array($this, 'block_bodyClass'),
            'header' => array($this, 'block_header'),
            'headerExtended' => array($this, 'block_headerExtended'),
            'content' => array($this, 'block_content'),
            'includeBody' => array($this, 'block_includeBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "MMIBundle::template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b6ae4fe0a4e38a0034d88d364eb685451afea8658896026df62d5dd36db6d8ea = $this->env->getExtension("native_profiler");
        $__internal_b6ae4fe0a4e38a0034d88d364eb685451afea8658896026df62d5dd36db6d8ea->enter($__internal_b6ae4fe0a4e38a0034d88d364eb685451afea8658896026df62d5dd36db6d8ea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MMIBundle:Default:pros.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b6ae4fe0a4e38a0034d88d364eb685451afea8658896026df62d5dd36db6d8ea->leave($__internal_b6ae4fe0a4e38a0034d88d364eb685451afea8658896026df62d5dd36db6d8ea_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_50e1eab699837d2b6a4644ecf2727bfa8ba646eb12312a78d13814f2dbdcc810 = $this->env->getExtension("native_profiler");
        $__internal_50e1eab699837d2b6a4644ecf2727bfa8ba646eb12312a78d13814f2dbdcc810->enter($__internal_50e1eab699837d2b6a4644ecf2727bfa8ba646eb12312a78d13814f2dbdcc810_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Espace pro";
        
        $__internal_50e1eab699837d2b6a4644ecf2727bfa8ba646eb12312a78d13814f2dbdcc810->leave($__internal_50e1eab699837d2b6a4644ecf2727bfa8ba646eb12312a78d13814f2dbdcc810_prof);

    }

    // line 5
    public function block_includeHead($context, array $blocks = array())
    {
        $__internal_92521d4327e28b112310b69e8124d9b04ec08342d95c756b5f978f1c14c6e774 = $this->env->getExtension("native_profiler");
        $__internal_92521d4327e28b112310b69e8124d9b04ec08342d95c756b5f978f1c14c6e774->enter($__internal_92521d4327e28b112310b69e8124d9b04ec08342d95c756b5f978f1c14c6e774_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "includeHead"));

        // line 6
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/form_regex.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_92521d4327e28b112310b69e8124d9b04ec08342d95c756b5f978f1c14c6e774->leave($__internal_92521d4327e28b112310b69e8124d9b04ec08342d95c756b5f978f1c14c6e774_prof);

    }

    // line 9
    public function block_bodyClass($context, array $blocks = array())
    {
        $__internal_f7972d212b38acec1e3b1429ca816a632382c37267de7d01f3391e62b013fd33 = $this->env->getExtension("native_profiler");
        $__internal_f7972d212b38acec1e3b1429ca816a632382c37267de7d01f3391e62b013fd33->enter($__internal_f7972d212b38acec1e3b1429ca816a632382c37267de7d01f3391e62b013fd33_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "bodyClass"));

        echo "home_pro";
        
        $__internal_f7972d212b38acec1e3b1429ca816a632382c37267de7d01f3391e62b013fd33->leave($__internal_f7972d212b38acec1e3b1429ca816a632382c37267de7d01f3391e62b013fd33_prof);

    }

    // line 11
    public function block_header($context, array $blocks = array())
    {
        $__internal_93d005cf6395930ca37dda030d37269da89cf25d56dd66cef329d1487f9e6868 = $this->env->getExtension("native_profiler");
        $__internal_93d005cf6395930ca37dda030d37269da89cf25d56dd66cef329d1487f9e6868->enter($__internal_93d005cf6395930ca37dda030d37269da89cf25d56dd66cef329d1487f9e6868_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        echo "style=\"background-image: url(";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("ui_img/header_home_pro.jpg"), "html", null, true);
        echo "); background-position: 50% 80%\"";
        
        $__internal_93d005cf6395930ca37dda030d37269da89cf25d56dd66cef329d1487f9e6868->leave($__internal_93d005cf6395930ca37dda030d37269da89cf25d56dd66cef329d1487f9e6868_prof);

    }

    // line 13
    public function block_headerExtended($context, array $blocks = array())
    {
        $__internal_fc02143a50f433380544038032a43e5ca2c52cfb0151f8d726badb6defb814cb = $this->env->getExtension("native_profiler");
        $__internal_fc02143a50f433380544038032a43e5ca2c52cfb0151f8d726badb6defb814cb->enter($__internal_fc02143a50f433380544038032a43e5ca2c52cfb0151f8d726badb6defb814cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "headerExtended"));

        // line 14
        echo "    <div class=\"caroussel_text\">
        <h2>Espace Pro <span class=\"super_title\">&nbsp;&nbsp;&nbsp;&nbsp;331</span></h2>

        <div class=\"pro\" style=\"width: 300px;\">
            <h3>Recrutez les meilleurs talents du web avec 331.</h3>

            <p>Le réseau professionel des étudiants et diplômés de MMI Montbéliard</p>
        </div>
    </div>
";
        
        $__internal_fc02143a50f433380544038032a43e5ca2c52cfb0151f8d726badb6defb814cb->leave($__internal_fc02143a50f433380544038032a43e5ca2c52cfb0151f8d726badb6defb814cb_prof);

    }

    // line 25
    public function block_content($context, array $blocks = array())
    {
        $__internal_46d6b26110a53f35165412ec5137d6a6ef21fc6951c2adecb0c47ca034c5c4f9 = $this->env->getExtension("native_profiler");
        $__internal_46d6b26110a53f35165412ec5137d6a6ef21fc6951c2adecb0c47ca034c5c4f9->enter($__internal_46d6b26110a53f35165412ec5137d6a6ef21fc6951c2adecb0c47ca034c5c4f9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 26
        echo "    <main>
        <section class=\"illustration\">
            <section class=\"wrapper\">
                <img src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("ui_img/drawing_pro_01.svg"), "html", null, true);
        echo "\" alt=\"Améliorez votre visibilité\">

                <div class=\"droite\">
                    <h2><span>Améliorez</span> votre visibilité auprès de nos étudiants</h2>

                    <p>Créer votre page entreprise</p>
                </div>
            </section>
            <section class=\"wrapper\">
                <div class=\"gauche\">
                    <h2><span>Recrutez</span> vos stagiaires, apprentis et salariés parmis nos étudiants</h2>

                    <p>Déposez une annonce ou rechercher et consulter les profils étudiant.</p>
                </div>
                <img src=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("ui_img/drawing_pro_02.svg"), "html", null, true);
        echo "\" alt=\"Améliorez votre visibilité\">
            </section>
            <section class=\"wrapper\">
                <img src=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("ui_img/drawing_pro_03.svg"), "html", null, true);
        echo "\" alt=\"Améliorez votre visibilité\">

                <div class=\"droite\">
                    <h2><span>Rencontrez</span> nos étudiants pour discuter</h2>

                    <p>Participez aux événements 331</p>
                </div>
            </section>
            <section class=\"wrapper\">
                <div class=\"gauche\">
                    <h2><span>Soutenez</span> les projets étudiant de notre incubateur</h2>
                </div>
                <img src=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("ui_img/drawing_pro_04.svg"), "html", null, true);
        echo "\" alt=\"Améliorez votre visibilité\">
            </section>
        </section>
        <section id=\"connexion\" class=\"wrapper light\">
            <article class=\"log\">
                <h1 class=\"right\">S'inscrire</h1>

                ";
        // line 65
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["inscription"]) ? $context["inscription"] : $this->getContext($context, "inscription")), 'form_start', array("attr" => array("class" => "verif_regex")));
        // line 67
        echo "
                ";
        // line 68
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["inscription"]) ? $context["inscription"] : $this->getContext($context, "inscription")), "name", array()), 'widget', array("attr" => array("class" => "big right verif", "data-regex" => "nom")));
        // line 71
        echo "
                ";
        // line 72
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["inscription"]) ? $context["inscription"] : $this->getContext($context, "inscription")), "email", array()), 'widget', array("attr" => array("class" => "big right verif", "placeholder" => "Adresse email", "data-regex" => "email")));
        // line 76
        echo "
                ";
        // line 77
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["inscription"]) ? $context["inscription"] : $this->getContext($context, "inscription")), "password", array()), 'widget', array("attr" => array("class" => "big right verif", "placeholder" => "Mot de passe")));
        // line 80
        echo "
                <button type=\"submit\" class=\"big right\">S'inscrire</button>
                ";
        // line 82
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["inscription"]) ? $context["inscription"] : $this->getContext($context, "inscription")), 'rest');
        echo "
                ";
        // line 83
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["inscription"]) ? $context["inscription"] : $this->getContext($context, "inscription")), 'form_end');
        echo "

                <a href=\"";
        // line 85
        echo $this->env->getExtension('routing')->getPath("mmi_homepage");
        echo "\" style=\"display: block; margin-top: 30px; float: right\">Vous êtes un étudiant ?</a>
            </article>
            <article class=\"log\" id=\"login\">
                <h1>Se connecter</h1>

                ";
        // line 90
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["connexion"]) ? $context["connexion"] : $this->getContext($context, "connexion")), 'form_start', array("attr" => array("class" => "verif_regex")));
        // line 92
        echo "
                ";
        // line 93
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["connexion"]) ? $context["connexion"] : $this->getContext($context, "connexion")), "email", array()), 'widget', array("attr" => array("class" => "big verif", "placeholder" => "Adresse email", "data-regex" => "email")));
        // line 97
        echo "
                ";
        // line 98
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["connexion"]) ? $context["connexion"] : $this->getContext($context, "connexion")), "password", array()), 'widget', array("attr" => array("class" => "big verif", "placeholder" => "Mot de passe")));
        // line 101
        echo "
                <button type=\"submit\" class=\"big\">Se connecter</button>
                ";
        // line 103
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["connexion"]) ? $context["connexion"] : $this->getContext($context, "connexion")), 'rest');
        echo "
                ";
        // line 104
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["connexion"]) ? $context["connexion"] : $this->getContext($context, "connexion")), 'form_end');
        echo "

                <!--<h4>Ou encore plus rapide !</h4>
                <button class=\"big white\" type=\"button\" style=\"margin-top: 20px; margin-right: 10px\">Facebook</button>
                <button class=\"big white\" type=\"button\" style=\"margin-top: 20px\">LinkedIn</button>-->
            </article>
        </section>
    </main>
";
        
        $__internal_46d6b26110a53f35165412ec5137d6a6ef21fc6951c2adecb0c47ca034c5c4f9->leave($__internal_46d6b26110a53f35165412ec5137d6a6ef21fc6951c2adecb0c47ca034c5c4f9_prof);

    }

    // line 114
    public function block_includeBody($context, array $blocks = array())
    {
        $__internal_708bf2c56a75513cda54dfeac4a8d721642e91c2454b4d32a23a3054312a2f43 = $this->env->getExtension("native_profiler");
        $__internal_708bf2c56a75513cda54dfeac4a8d721642e91c2454b4d32a23a3054312a2f43->enter($__internal_708bf2c56a75513cda54dfeac4a8d721642e91c2454b4d32a23a3054312a2f43_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "includeBody"));

        // line 115
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("assets/jquery.scrollTo.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 116
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/caroussel.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 117
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/menu.js"), "html", null, true);
        echo "\"></script>
    <script>
        \$(document).ready(function(){

            \$(window).load(function(){

                var modal_content = \$('#errorModal .no_errors').length;

                if (modal_content == 0){
                    \$('#errorModal').modal('show')
                }
            });

        });
    </script>
";
        
        $__internal_708bf2c56a75513cda54dfeac4a8d721642e91c2454b4d32a23a3054312a2f43->leave($__internal_708bf2c56a75513cda54dfeac4a8d721642e91c2454b4d32a23a3054312a2f43_prof);

    }

    public function getTemplateName()
    {
        return "MMIBundle:Default:pros.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  264 => 117,  260 => 116,  255 => 115,  249 => 114,  233 => 104,  229 => 103,  225 => 101,  223 => 98,  220 => 97,  218 => 93,  215 => 92,  213 => 90,  205 => 85,  200 => 83,  196 => 82,  192 => 80,  190 => 77,  187 => 76,  185 => 72,  182 => 71,  180 => 68,  177 => 67,  175 => 65,  165 => 58,  150 => 46,  144 => 43,  127 => 29,  122 => 26,  116 => 25,  100 => 14,  94 => 13,  80 => 11,  68 => 9,  58 => 6,  52 => 5,  40 => 3,  11 => 1,);
    }
}
/* {% extends "MMIBundle::template.html.twig" %}*/
/* */
/* {% block title %}Espace pro{% endblock %}*/
/* */
/* {% block includeHead %}*/
/*     <script src="{{ asset('js/form_regex.js') }}"></script>*/
/* {% endblock %}*/
/* */
/* {% block bodyClass %}home_pro{% endblock %}*/
/* */
/* {% block header %}style="background-image: url({{ asset("ui_img/header_home_pro.jpg") }}); background-position: 50% 80%"{% endblock %}*/
/* */
/* {% block headerExtended %}*/
/*     <div class="caroussel_text">*/
/*         <h2>Espace Pro <span class="super_title">&nbsp;&nbsp;&nbsp;&nbsp;331</span></h2>*/
/* */
/*         <div class="pro" style="width: 300px;">*/
/*             <h3>Recrutez les meilleurs talents du web avec 331.</h3>*/
/* */
/*             <p>Le réseau professionel des étudiants et diplômés de MMI Montbéliard</p>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
/* {% block content %}*/
/*     <main>*/
/*         <section class="illustration">*/
/*             <section class="wrapper">*/
/*                 <img src="{{ asset("ui_img/drawing_pro_01.svg") }}" alt="Améliorez votre visibilité">*/
/* */
/*                 <div class="droite">*/
/*                     <h2><span>Améliorez</span> votre visibilité auprès de nos étudiants</h2>*/
/* */
/*                     <p>Créer votre page entreprise</p>*/
/*                 </div>*/
/*             </section>*/
/*             <section class="wrapper">*/
/*                 <div class="gauche">*/
/*                     <h2><span>Recrutez</span> vos stagiaires, apprentis et salariés parmis nos étudiants</h2>*/
/* */
/*                     <p>Déposez une annonce ou rechercher et consulter les profils étudiant.</p>*/
/*                 </div>*/
/*                 <img src="{{ asset("ui_img/drawing_pro_02.svg") }}" alt="Améliorez votre visibilité">*/
/*             </section>*/
/*             <section class="wrapper">*/
/*                 <img src="{{ asset("ui_img/drawing_pro_03.svg") }}" alt="Améliorez votre visibilité">*/
/* */
/*                 <div class="droite">*/
/*                     <h2><span>Rencontrez</span> nos étudiants pour discuter</h2>*/
/* */
/*                     <p>Participez aux événements 331</p>*/
/*                 </div>*/
/*             </section>*/
/*             <section class="wrapper">*/
/*                 <div class="gauche">*/
/*                     <h2><span>Soutenez</span> les projets étudiant de notre incubateur</h2>*/
/*                 </div>*/
/*                 <img src="{{ asset("ui_img/drawing_pro_04.svg") }}" alt="Améliorez votre visibilité">*/
/*             </section>*/
/*         </section>*/
/*         <section id="connexion" class="wrapper light">*/
/*             <article class="log">*/
/*                 <h1 class="right">S'inscrire</h1>*/
/* */
/*                 {{ form_start(inscription,*/
/*                 {"attr": {*/
/*                     "class": "verif_regex"}}) }}*/
/*                 {{ form_widget(inscription.name,*/
/*                 {"attr": {*/
/*                     "class": "big right verif",*/
/*                     "data-regex": "nom"}}) }}*/
/*                 {{ form_widget(inscription.email,*/
/*                 {"attr": {*/
/*                     "class": "big right verif",*/
/*                     "placeholder": "Adresse email",*/
/*                     "data-regex": "email"}}) }}*/
/*                 {{ form_widget(inscription.password,*/
/*                 {"attr": {*/
/*                     "class": "big right verif",*/
/*                     "placeholder": "Mot de passe"}}) }}*/
/*                 <button type="submit" class="big right">S'inscrire</button>*/
/*                 {{ form_rest(inscription) }}*/
/*                 {{ form_end(inscription) }}*/
/* */
/*                 <a href="{{ path("mmi_homepage") }}" style="display: block; margin-top: 30px; float: right">Vous êtes un étudiant ?</a>*/
/*             </article>*/
/*             <article class="log" id="login">*/
/*                 <h1>Se connecter</h1>*/
/* */
/*                 {{ form_start(connexion,*/
/*                 {"attr": {*/
/*                     "class": "verif_regex"}}) }}*/
/*                 {{ form_widget(connexion.email,*/
/*                 {"attr": {*/
/*                     "class": "big verif",*/
/*                     "placeholder": "Adresse email",*/
/*                     "data-regex": "email"}}) }}*/
/*                 {{ form_widget(connexion.password,*/
/*                 {"attr": {*/
/*                     "class": "big verif",*/
/*                     "placeholder": "Mot de passe"}}) }}*/
/*                 <button type="submit" class="big">Se connecter</button>*/
/*                 {{ form_rest(connexion) }}*/
/*                 {{ form_end(connexion) }}*/
/* */
/*                 <!--<h4>Ou encore plus rapide !</h4>*/
/*                 <button class="big white" type="button" style="margin-top: 20px; margin-right: 10px">Facebook</button>*/
/*                 <button class="big white" type="button" style="margin-top: 20px">LinkedIn</button>-->*/
/*             </article>*/
/*         </section>*/
/*     </main>*/
/* {% endblock %}*/
/* */
/* {% block includeBody %}*/
/*     <script src="{{ asset("assets/jquery.scrollTo.min.js") }}"></script>*/
/*     <script src="{{ asset("js/caroussel.js") }}"></script>*/
/*     <script src="{{ asset("js/menu.js") }}"></script>*/
/*     <script>*/
/*         $(document).ready(function(){*/
/* */
/*             $(window).load(function(){*/
/* */
/*                 var modal_content = $('#errorModal .no_errors').length;*/
/* */
/*                 if (modal_content == 0){*/
/*                     $('#errorModal').modal('show')*/
/*                 }*/
/*             });*/
/* */
/*         });*/
/*     </script>*/
/* {% endblock %}*/
/* */
