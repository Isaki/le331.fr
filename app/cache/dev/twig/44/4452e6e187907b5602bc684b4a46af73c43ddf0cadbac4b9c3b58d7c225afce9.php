<?php

/* MMIBundle:Entreprise:edit_company.html.twig */
class __TwigTemplate_5bac2da17dbdf066bfb3d73858161aa768b249a831c0a87363e1f5a54b7fabbd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("MMIBundle::template.html.twig", "MMIBundle:Entreprise:edit_company.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'includeHead' => array($this, 'block_includeHead'),
            'bodyClass' => array($this, 'block_bodyClass'),
            'header' => array($this, 'block_header'),
            'content' => array($this, 'block_content'),
            'includeBody' => array($this, 'block_includeBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "MMIBundle::template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5113e486f667a1d3db2de1a31f225aa6f76f799398f567f86b98ccb26199dbbe = $this->env->getExtension("native_profiler");
        $__internal_5113e486f667a1d3db2de1a31f225aa6f76f799398f567f86b98ccb26199dbbe->enter($__internal_5113e486f667a1d3db2de1a31f225aa6f76f799398f567f86b98ccb26199dbbe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MMIBundle:Entreprise:edit_company.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5113e486f667a1d3db2de1a31f225aa6f76f799398f567f86b98ccb26199dbbe->leave($__internal_5113e486f667a1d3db2de1a31f225aa6f76f799398f567f86b98ccb26199dbbe_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_3412b465e2a8c0c3c5edb33d3adb558b23fcb651772d5f7b20cf45bffe5addee = $this->env->getExtension("native_profiler");
        $__internal_3412b465e2a8c0c3c5edb33d3adb558b23fcb651772d5f7b20cf45bffe5addee->enter($__internal_3412b465e2a8c0c3c5edb33d3adb558b23fcb651772d5f7b20cf45bffe5addee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Paramètres de votre profil pro";
        
        $__internal_3412b465e2a8c0c3c5edb33d3adb558b23fcb651772d5f7b20cf45bffe5addee->leave($__internal_3412b465e2a8c0c3c5edb33d3adb558b23fcb651772d5f7b20cf45bffe5addee_prof);

    }

    // line 5
    public function block_includeHead($context, array $blocks = array())
    {
        $__internal_4ed7ab3861ca9991340a8414f4717b67f399d1cb9229a2a2e79e7b49c7022c82 = $this->env->getExtension("native_profiler");
        $__internal_4ed7ab3861ca9991340a8414f4717b67f399d1cb9229a2a2e79e7b49c7022c82->enter($__internal_4ed7ab3861ca9991340a8414f4717b67f399d1cb9229a2a2e79e7b49c7022c82_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "includeHead"));

        // line 6
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/form_regex.js"), "html", null, true);
        echo "\"></script>
    <link type=\"text/css\" rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/welcome_sender.css"), "html", null, true);
        echo "\">
";
        
        $__internal_4ed7ab3861ca9991340a8414f4717b67f399d1cb9229a2a2e79e7b49c7022c82->leave($__internal_4ed7ab3861ca9991340a8414f4717b67f399d1cb9229a2a2e79e7b49c7022c82_prof);

    }

    // line 10
    public function block_bodyClass($context, array $blocks = array())
    {
        $__internal_d99981aa6441e71737257db3cf9f60f9653577ddfbd65efbcda7580b3703b707 = $this->env->getExtension("native_profiler");
        $__internal_d99981aa6441e71737257db3cf9f60f9653577ddfbd65efbcda7580b3703b707->enter($__internal_d99981aa6441e71737257db3cf9f60f9653577ddfbd65efbcda7580b3703b707_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "bodyClass"));

        echo "edit";
        
        $__internal_d99981aa6441e71737257db3cf9f60f9653577ddfbd65efbcda7580b3703b707->leave($__internal_d99981aa6441e71737257db3cf9f60f9653577ddfbd65efbcda7580b3703b707_prof);

    }

    // line 12
    public function block_header($context, array $blocks = array())
    {
        $__internal_a33ccf96c5bc85e4fbb283fa6dca0c6a602f1252b84535a357156df43a0ba8a2 = $this->env->getExtension("native_profiler");
        $__internal_a33ccf96c5bc85e4fbb283fa6dca0c6a602f1252b84535a357156df43a0ba8a2->enter($__internal_a33ccf96c5bc85e4fbb283fa6dca0c6a602f1252b84535a357156df43a0ba8a2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 13
        echo "    <header>
        <div id=\"hamburger\" class=\"hamburger dark\">
            <div></div>
            <div></div>
            <div></div>
        </div>
        <nav class=\"black\">
            <ul>
                <li><a class=\"transition blue\" href=\"search.html\">Rechercher</a></li>
                <li><a class=\"transition \" href=\"\">Mon profil</a></li>
                <li><a class=\"transition thin\" href=\"\">Mon stage</a></li>
                <li><a class=\"transition thin\" href=\"\">Incubateur</a></li>
                <li><a class=\"transition thin\" href=\"\">Mes articles</a></li>
                <li><a class=\"transition thin\" href=\"\"><i class=\"fa fa-cog\"></i></a></li>
            </ul>
        </nav>
        <a href=\"";
        // line 29
        echo $this->env->getExtension('routing')->getPath("mmi_homepage");
        echo "\" class=\"logo transition\">331 Acceuil</a>
        <h1>Paramètres de votre profil pro</h1>
    </header>
";
        
        $__internal_a33ccf96c5bc85e4fbb283fa6dca0c6a602f1252b84535a357156df43a0ba8a2->leave($__internal_a33ccf96c5bc85e4fbb283fa6dca0c6a602f1252b84535a357156df43a0ba8a2_prof);

    }

    // line 34
    public function block_content($context, array $blocks = array())
    {
        $__internal_68bbff8a2cd8d8adcf4874b3414bd1d07a92916272040f605607deaa6bc816ce = $this->env->getExtension("native_profiler");
        $__internal_68bbff8a2cd8d8adcf4874b3414bd1d07a92916272040f605607deaa6bc816ce->enter($__internal_68bbff8a2cd8d8adcf4874b3414bd1d07a92916272040f605607deaa6bc816ce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 35
        echo "    <main>
        <form method=\"post\" action=\"\" class=\"verif_regex\">
            <section>
                <h2>Description</h2>
                <input type=\"text\" class=\"big stretch verif\" name=\"tittle\" placeholder=\"Nom de votre entreprise*\">
                <textarea name=\"slogan\" placeholder=\"Slogan ( <140 caractères )*\"></textarea>
                <textarea name=\"description\" placeholder=\"Description*\"></textarea>
                <select name=\"select\" class=\"custom-select sources selector_type\" placeholder=\"Secteur*\">
                    <option value=\"value1\">1</option>
                    <option value=\"value2\">2</option>
                    <option value=\"value3\">3</option>
                    <option value=\"value4\">4</option>
                </select>
                <input style=\"margin-top: 15px\" type=\"text\" class=\"big stretch verif\" name=\"url\"
                       placeholder=\"Site internet\">
                <select name=\"select\" class=\"custom-select sources selector_type\" placeholder=\"Effectif*\">
                    <option value=\"value1\">1</option>
                    <option value=\"value2\">2</option>
                    <option value=\"value3\">3</option>
                    <option value=\"value4\">4</option>
                </select>
            </section>
            <section>
                <h2>Photo de profil</h2>
                <input class=\"big\" type=\"file\">
                <p style=\"margin-bottom: 30px\">La photo doit faire moins de 2MB et doit être au format jpeg, png ou gif</p>
                <h2>Photo de couverture</h2>
                <input class=\"big\" type=\"file\">
                <p>La photo doit faire moins de 2MB et doit être au format jpeg, png ou gif</p>
            </section>
            <section>
                <h2>Contact</h2>
                <input type=\"text\" class=\"big stretch\" name=\"cp\" placeholder=\"Téléphone*\">
                <input type=\"text\" class=\"big stretch\" name=\"cp\" placeholder=\"Nom*\">
                <input type=\"text\" class=\"big stretch\" name=\"cp\" placeholder=\"Prénom*\">
                <input type=\"text\" class=\"big stretch\" name=\"cp\" placeholder=\"Poste occupé*\">
                <input type=\"text\" class=\"big stretch\" name=\"cp\" placeholder=\"Adresse email*\">
            </section>
            <section>
                <h2>L'adresse</h2>
                <input type=\"text\" class=\"big stretch\" name=\"cp\" placeholder=\"Adresse*\">
                <input type=\"text\" class=\"big stretch\" name=\"cp\" placeholder=\"Code postal*\">
                <input type=\"text\" class=\"big stretch\" name=\"cp\" placeholder=\"Ville*\">
            </section>
            <section>
                <h2>Social</h2>
                <input type=\"text\" class=\"big stretch\" name=\"cp\" placeholder=\"Facebook\">
                <input type=\"text\" class=\"big stretch\" name=\"cp\" placeholder=\"Twitter\">
                <input type=\"text\" class=\"big stretch\" name=\"cp\" placeholder=\"LinkedIn\">
                <input type=\"text\" class=\"big stretch\" name=\"cp\" placeholder=\"Instagram\">
                <input type=\"text\" class=\"big stretch\" name=\"cp\" placeholder=\"Tumblr\">
                <p>* Champs oblgatoires</p>
            </section>
            <button style=\"margin-bottom: 30px\" class=\"big\" type=\"submit\">Valider</button>
        </form>
    </main>
";
        
        $__internal_68bbff8a2cd8d8adcf4874b3414bd1d07a92916272040f605607deaa6bc816ce->leave($__internal_68bbff8a2cd8d8adcf4874b3414bd1d07a92916272040f605607deaa6bc816ce_prof);

    }

    // line 93
    public function block_includeBody($context, array $blocks = array())
    {
        $__internal_c7c438d39a5ac18d39c88e6320194ad7670321f1c41df1f2a58fa08f8c696b65 = $this->env->getExtension("native_profiler");
        $__internal_c7c438d39a5ac18d39c88e6320194ad7670321f1c41df1f2a58fa08f8c696b65->enter($__internal_c7c438d39a5ac18d39c88e6320194ad7670321f1c41df1f2a58fa08f8c696b65_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "includeBody"));

        // line 94
        echo "    <script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("assets/jquery-custom.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 95
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("assets/datePicker.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 96
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/welcome_sender.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 97
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/custom_select.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 98
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/custom_input_file.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_c7c438d39a5ac18d39c88e6320194ad7670321f1c41df1f2a58fa08f8c696b65->leave($__internal_c7c438d39a5ac18d39c88e6320194ad7670321f1c41df1f2a58fa08f8c696b65_prof);

    }

    public function getTemplateName()
    {
        return "MMIBundle:Entreprise:edit_company.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  210 => 98,  206 => 97,  202 => 96,  198 => 95,  193 => 94,  187 => 93,  124 => 35,  118 => 34,  107 => 29,  89 => 13,  83 => 12,  71 => 10,  62 => 7,  57 => 6,  51 => 5,  39 => 3,  11 => 1,);
    }
}
/* {% extends "MMIBundle::template.html.twig" %}*/
/* */
/* {% block title %}Paramètres de votre profil pro{% endblock %}*/
/* */
/* {% block includeHead %}*/
/*     <script src="{{ asset('js/form_regex.js') }}"></script>*/
/*     <link type="text/css" rel="stylesheet" href="{{ asset("css/welcome_sender.css") }}">*/
/* {% endblock %}*/
/* */
/* {% block bodyClass %}edit{% endblock %}*/
/* */
/* {% block header %}*/
/*     <header>*/
/*         <div id="hamburger" class="hamburger dark">*/
/*             <div></div>*/
/*             <div></div>*/
/*             <div></div>*/
/*         </div>*/
/*         <nav class="black">*/
/*             <ul>*/
/*                 <li><a class="transition blue" href="search.html">Rechercher</a></li>*/
/*                 <li><a class="transition " href="">Mon profil</a></li>*/
/*                 <li><a class="transition thin" href="">Mon stage</a></li>*/
/*                 <li><a class="transition thin" href="">Incubateur</a></li>*/
/*                 <li><a class="transition thin" href="">Mes articles</a></li>*/
/*                 <li><a class="transition thin" href=""><i class="fa fa-cog"></i></a></li>*/
/*             </ul>*/
/*         </nav>*/
/*         <a href="{{ path("mmi_homepage") }}" class="logo transition">331 Acceuil</a>*/
/*         <h1>Paramètres de votre profil pro</h1>*/
/*     </header>*/
/* {% endblock %}*/
/* */
/* {% block content %}*/
/*     <main>*/
/*         <form method="post" action="" class="verif_regex">*/
/*             <section>*/
/*                 <h2>Description</h2>*/
/*                 <input type="text" class="big stretch verif" name="tittle" placeholder="Nom de votre entreprise*">*/
/*                 <textarea name="slogan" placeholder="Slogan ( <140 caractères )*"></textarea>*/
/*                 <textarea name="description" placeholder="Description*"></textarea>*/
/*                 <select name="select" class="custom-select sources selector_type" placeholder="Secteur*">*/
/*                     <option value="value1">1</option>*/
/*                     <option value="value2">2</option>*/
/*                     <option value="value3">3</option>*/
/*                     <option value="value4">4</option>*/
/*                 </select>*/
/*                 <input style="margin-top: 15px" type="text" class="big stretch verif" name="url"*/
/*                        placeholder="Site internet">*/
/*                 <select name="select" class="custom-select sources selector_type" placeholder="Effectif*">*/
/*                     <option value="value1">1</option>*/
/*                     <option value="value2">2</option>*/
/*                     <option value="value3">3</option>*/
/*                     <option value="value4">4</option>*/
/*                 </select>*/
/*             </section>*/
/*             <section>*/
/*                 <h2>Photo de profil</h2>*/
/*                 <input class="big" type="file">*/
/*                 <p style="margin-bottom: 30px">La photo doit faire moins de 2MB et doit être au format jpeg, png ou gif</p>*/
/*                 <h2>Photo de couverture</h2>*/
/*                 <input class="big" type="file">*/
/*                 <p>La photo doit faire moins de 2MB et doit être au format jpeg, png ou gif</p>*/
/*             </section>*/
/*             <section>*/
/*                 <h2>Contact</h2>*/
/*                 <input type="text" class="big stretch" name="cp" placeholder="Téléphone*">*/
/*                 <input type="text" class="big stretch" name="cp" placeholder="Nom*">*/
/*                 <input type="text" class="big stretch" name="cp" placeholder="Prénom*">*/
/*                 <input type="text" class="big stretch" name="cp" placeholder="Poste occupé*">*/
/*                 <input type="text" class="big stretch" name="cp" placeholder="Adresse email*">*/
/*             </section>*/
/*             <section>*/
/*                 <h2>L'adresse</h2>*/
/*                 <input type="text" class="big stretch" name="cp" placeholder="Adresse*">*/
/*                 <input type="text" class="big stretch" name="cp" placeholder="Code postal*">*/
/*                 <input type="text" class="big stretch" name="cp" placeholder="Ville*">*/
/*             </section>*/
/*             <section>*/
/*                 <h2>Social</h2>*/
/*                 <input type="text" class="big stretch" name="cp" placeholder="Facebook">*/
/*                 <input type="text" class="big stretch" name="cp" placeholder="Twitter">*/
/*                 <input type="text" class="big stretch" name="cp" placeholder="LinkedIn">*/
/*                 <input type="text" class="big stretch" name="cp" placeholder="Instagram">*/
/*                 <input type="text" class="big stretch" name="cp" placeholder="Tumblr">*/
/*                 <p>* Champs oblgatoires</p>*/
/*             </section>*/
/*             <button style="margin-bottom: 30px" class="big" type="submit">Valider</button>*/
/*         </form>*/
/*     </main>*/
/* {% endblock %}*/
/* */
/* {% block includeBody %}*/
/*     <script type="text/javascript" src="{{ asset("assets/jquery-custom.min.js") }}"></script>*/
/*     <script type="text/javascript" src="{{ asset("assets/datePicker.js") }}"></script>*/
/*     <script type="text/javascript" src="{{ asset("js/welcome_sender.js") }}"></script>*/
/*     <script type="text/javascript" src="{{ asset("js/custom_select.js") }}"></script>*/
/*     <script type="text/javascript" src="{{ asset("js/custom_input_file.js") }}"></script>*/
/* {% endblock %}*/
/* */
