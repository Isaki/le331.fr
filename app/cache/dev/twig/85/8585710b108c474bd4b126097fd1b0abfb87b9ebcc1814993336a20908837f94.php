<?php

/* MMIBundle::template.html.twig */
class __TwigTemplate_14e77bb56f6e3c04a5b98d9398bca69bb9cdfc85a81f29b9ad3991359422ae88 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'includeHead' => array($this, 'block_includeHead'),
            'bodyClass' => array($this, 'block_bodyClass'),
            'header' => array($this, 'block_header'),
            'headerExtended' => array($this, 'block_headerExtended'),
            'content' => array($this, 'block_content'),
            'includeBody' => array($this, 'block_includeBody'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a5ba30cad16aa566a795ab411d5454f448d1f45040903466fd0bcb29a9c8d526 = $this->env->getExtension("native_profiler");
        $__internal_a5ba30cad16aa566a795ab411d5454f448d1f45040903466fd0bcb29a9c8d526->enter($__internal_a5ba30cad16aa566a795ab411d5454f448d1f45040903466fd0bcb29a9c8d526_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MMIBundle::template.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html class=\"light\" lang=\"fr\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <title>331 | ";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo "</title>

    <link href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/screen.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\"/>
    <link href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/print.css"), "html", null, true);
        echo "\" media=\"print\" rel=\"stylesheet\" type=\"text/css\"/>
    <!--[if IE]>
    <link href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("css/ie.css"), "html", null, true);
        echo "\" media=\"screen, projection\" rel=\"stylesheet\" type=\"text/css\"/>
    <![endif]-->
    <link rel=\"shortcut icon\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("ui_img/favicon.ico"), "html", null, true);
        echo "\" type=\"image/x-icon\">
    <link rel=\"icon\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("ui_img/favicon.ico"), "html", null, true);
        echo "\" type=\"image/x-icon\">
    <!--<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css\">-->
    <link rel=\"stylesheet\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("assets/bootstrap-3.3.6-dist/css/bootstrap.modal.css"), "html", null, true);
        echo "\">

    <script src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("assets/jquery-custom.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("assets/modernizr.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("assets/bootstrap-3.3.6-dist/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>

    ";
        // line 22
        $this->displayBlock('includeHead', $context, $blocks);
        // line 23
        echo "</head>
<body class=\"";
        // line 24
        $this->displayBlock('bodyClass', $context, $blocks);
        echo "\">
<div style=\"padding: 15px; text-align:center\">
    Le site web est en bêta test. <a href=\"http://goo.gl/forms/Fv5gVjDZ95\" target=\"_blank\"
                                     style=\"text-decoration: underline\">Ce formulaire</a> vous permettra de signaler un
    bug ou suggérer une amélioration. - la team dev 331
</div>
<header ";
        // line 30
        $this->displayBlock('header', $context, $blocks);
        echo ">
    <a href=\"";
        // line 31
        echo $this->env->getExtension('routing')->getPath("mmi_homepage");
        echo "\" class=\"logo ";
        if (((isset($context["menu_color"]) ? $context["menu_color"] : $this->getContext($context, "menu_color")) != "dark")) {
            echo "white";
        }
        echo " transition\">331
        Accueil</a>

    <div id=\"hamburger\" class=\"hamburger ";
        // line 34
        echo twig_escape_filter($this->env, (isset($context["menu_color"]) ? $context["menu_color"] : $this->getContext($context, "menu_color")), "html", null, true);
        echo "\">
        <div></div>
        <div></div>
        <div></div>
    </div>
    <nav";
        // line 39
        if (((isset($context["menu_color"]) ? $context["menu_color"] : $this->getContext($context, "menu_color")) == "dark")) {
            echo " class=\"black\"";
        }
        echo ">
        <ul>
            ";
        // line 41
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["menu_items"]) ? $context["menu_items"] : $this->getContext($context, "menu_items")));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 42
            echo "                <li><a class=\"transition thin ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "active", array()), "html", null, true);
            echo "\" href=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "path", array()), "html", null, true);
            echo "\">";
            echo $this->getAttribute($context["item"], "name", array());
            echo "</a></li>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 44
        echo "        </ul>
    </nav>
    ";
        // line 46
        $this->displayBlock('headerExtended', $context, $blocks);
        // line 47
        echo "</header>

";
        // line 49
        $this->displayBlock('content', $context, $blocks);
        // line 50
        echo "
<footer class=\"white\">
    <a href=\"";
        // line 52
        echo $this->env->getExtension('routing')->getPath("mmi_homepage");
        echo "\" class=\"logo\">331 Acceuil</a>
    <ul>
        <li><a href=\"\">À propos</a></li>
        <li><a href=\"\">Mentions légales</a></li>
        <li><a href=\"http://mmimontbeliard.com\" target=\"_blank\">Département MMI</a></li>
    </ul>
</footer>

<!-- modal des messages d'erreur -->
<div class=\"modal fade\" id=\"errorModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"errorModalLabel\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            ";
        // line 64
        if (array_key_exists("result", $context)) {
            // line 65
            echo "                <p>";
            echo twig_escape_filter($this->env, (isset($context["result"]) ? $context["result"] : $this->getContext($context, "result")), "html", null, true);
            echo "</p>
            ";
        } else {
            // line 67
            echo "                <p class=\"no_errors\">all clear</p>
            ";
        }
        // line 69
        echo "            <button type=\"button\" class=\"close\" aria-label=\"Close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">&times;</span></button>
        </div>
    </div>
</div>

";
        // line 74
        $this->displayBlock('includeBody', $context, $blocks);
        // line 75
        echo "
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-73230977-1', 'auto');
    ga('send', 'pageview');

</script>

</body>
</html>";
        
        $__internal_a5ba30cad16aa566a795ab411d5454f448d1f45040903466fd0bcb29a9c8d526->leave($__internal_a5ba30cad16aa566a795ab411d5454f448d1f45040903466fd0bcb29a9c8d526_prof);

    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        $__internal_250f385147e21efeceacce03c42c54e50682815ca57f8115a2d45c0a915a5938 = $this->env->getExtension("native_profiler");
        $__internal_250f385147e21efeceacce03c42c54e50682815ca57f8115a2d45c0a915a5938->enter($__internal_250f385147e21efeceacce03c42c54e50682815ca57f8115a2d45c0a915a5938_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Accueil";
        
        $__internal_250f385147e21efeceacce03c42c54e50682815ca57f8115a2d45c0a915a5938->leave($__internal_250f385147e21efeceacce03c42c54e50682815ca57f8115a2d45c0a915a5938_prof);

    }

    // line 22
    public function block_includeHead($context, array $blocks = array())
    {
        $__internal_ab5382b8640f20ed87a48a1f8e22403101a698edc304b7da4d75b4cdbbbb8b7f = $this->env->getExtension("native_profiler");
        $__internal_ab5382b8640f20ed87a48a1f8e22403101a698edc304b7da4d75b4cdbbbb8b7f->enter($__internal_ab5382b8640f20ed87a48a1f8e22403101a698edc304b7da4d75b4cdbbbb8b7f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "includeHead"));

        
        $__internal_ab5382b8640f20ed87a48a1f8e22403101a698edc304b7da4d75b4cdbbbb8b7f->leave($__internal_ab5382b8640f20ed87a48a1f8e22403101a698edc304b7da4d75b4cdbbbb8b7f_prof);

    }

    // line 24
    public function block_bodyClass($context, array $blocks = array())
    {
        $__internal_b5fe00b7baf5e79ff21fa264bad1079736e22c44777de9b0f42548d8e80e8542 = $this->env->getExtension("native_profiler");
        $__internal_b5fe00b7baf5e79ff21fa264bad1079736e22c44777de9b0f42548d8e80e8542->enter($__internal_b5fe00b7baf5e79ff21fa264bad1079736e22c44777de9b0f42548d8e80e8542_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "bodyClass"));

        
        $__internal_b5fe00b7baf5e79ff21fa264bad1079736e22c44777de9b0f42548d8e80e8542->leave($__internal_b5fe00b7baf5e79ff21fa264bad1079736e22c44777de9b0f42548d8e80e8542_prof);

    }

    // line 30
    public function block_header($context, array $blocks = array())
    {
        $__internal_8724da0c7f89471ef0dc66025b150ada8110ba3ec0a451458db2c2745475ce28 = $this->env->getExtension("native_profiler");
        $__internal_8724da0c7f89471ef0dc66025b150ada8110ba3ec0a451458db2c2745475ce28->enter($__internal_8724da0c7f89471ef0dc66025b150ada8110ba3ec0a451458db2c2745475ce28_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        
        $__internal_8724da0c7f89471ef0dc66025b150ada8110ba3ec0a451458db2c2745475ce28->leave($__internal_8724da0c7f89471ef0dc66025b150ada8110ba3ec0a451458db2c2745475ce28_prof);

    }

    // line 46
    public function block_headerExtended($context, array $blocks = array())
    {
        $__internal_3b3d23eea720d99625dd224103b76db0614111253675f1cc3537c39745fb9c0e = $this->env->getExtension("native_profiler");
        $__internal_3b3d23eea720d99625dd224103b76db0614111253675f1cc3537c39745fb9c0e->enter($__internal_3b3d23eea720d99625dd224103b76db0614111253675f1cc3537c39745fb9c0e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "headerExtended"));

        
        $__internal_3b3d23eea720d99625dd224103b76db0614111253675f1cc3537c39745fb9c0e->leave($__internal_3b3d23eea720d99625dd224103b76db0614111253675f1cc3537c39745fb9c0e_prof);

    }

    // line 49
    public function block_content($context, array $blocks = array())
    {
        $__internal_055577b253d015fa2cedde9d78a79654bcccae9cfd95fe5a0586397bc9f084eb = $this->env->getExtension("native_profiler");
        $__internal_055577b253d015fa2cedde9d78a79654bcccae9cfd95fe5a0586397bc9f084eb->enter($__internal_055577b253d015fa2cedde9d78a79654bcccae9cfd95fe5a0586397bc9f084eb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        
        $__internal_055577b253d015fa2cedde9d78a79654bcccae9cfd95fe5a0586397bc9f084eb->leave($__internal_055577b253d015fa2cedde9d78a79654bcccae9cfd95fe5a0586397bc9f084eb_prof);

    }

    // line 74
    public function block_includeBody($context, array $blocks = array())
    {
        $__internal_6ee19165ac6438165c9e75a80d03b7e4d9f3acca07a308f7ca4d2694ca972335 = $this->env->getExtension("native_profiler");
        $__internal_6ee19165ac6438165c9e75a80d03b7e4d9f3acca07a308f7ca4d2694ca972335->enter($__internal_6ee19165ac6438165c9e75a80d03b7e4d9f3acca07a308f7ca4d2694ca972335_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "includeBody"));

        
        $__internal_6ee19165ac6438165c9e75a80d03b7e4d9f3acca07a308f7ca4d2694ca972335->leave($__internal_6ee19165ac6438165c9e75a80d03b7e4d9f3acca07a308f7ca4d2694ca972335_prof);

    }

    public function getTemplateName()
    {
        return "MMIBundle::template.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  288 => 74,  277 => 49,  266 => 46,  255 => 30,  244 => 24,  233 => 22,  221 => 6,  194 => 75,  192 => 74,  185 => 69,  181 => 67,  175 => 65,  173 => 64,  158 => 52,  154 => 50,  152 => 49,  148 => 47,  146 => 46,  142 => 44,  129 => 42,  125 => 41,  118 => 39,  110 => 34,  100 => 31,  96 => 30,  87 => 24,  84 => 23,  82 => 22,  77 => 20,  73 => 19,  69 => 18,  64 => 16,  59 => 14,  55 => 13,  50 => 11,  45 => 9,  41 => 8,  36 => 6,  29 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html class="light" lang="fr">*/
/* <head>*/
/*     <meta charset="UTF-8">*/
/*     <meta name="viewport" content="width=device-width, initial-scale=1">*/
/*     <title>331 | {% block title %}Accueil{% endblock %}</title>*/
/* */
/*     <link href="{{ asset('css/screen.css') }}" rel="stylesheet" type="text/css"/>*/
/*     <link href="{{ asset('css/print.css') }}" media="print" rel="stylesheet" type="text/css"/>*/
/*     <!--[if IE]>*/
/*     <link href="{{ asset('css/ie.css') }}" media="screen, projection" rel="stylesheet" type="text/css"/>*/
/*     <![endif]-->*/
/*     <link rel="shortcut icon" href="{{ asset('ui_img/favicon.ico') }}" type="image/x-icon">*/
/*     <link rel="icon" href="{{ asset('ui_img/favicon.ico') }}" type="image/x-icon">*/
/*     <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">-->*/
/*     <link rel="stylesheet" href="{{ asset("assets/bootstrap-3.3.6-dist/css/bootstrap.modal.css") }}">*/
/* */
/*     <script src="{{ asset('assets/jquery-custom.min.js') }}"></script>*/
/*     <script src="{{ asset('assets/modernizr.js') }}"></script>*/
/*     <script src="{{ asset("assets/bootstrap-3.3.6-dist/js/bootstrap.min.js") }}"></script>*/
/* */
/*     {% block includeHead %}{% endblock %}*/
/* </head>*/
/* <body class="{% block bodyClass %}{% endblock %}">*/
/* <div style="padding: 15px; text-align:center">*/
/*     Le site web est en bêta test. <a href="http://goo.gl/forms/Fv5gVjDZ95" target="_blank"*/
/*                                      style="text-decoration: underline">Ce formulaire</a> vous permettra de signaler un*/
/*     bug ou suggérer une amélioration. - la team dev 331*/
/* </div>*/
/* <header {% block header %}{% endblock %}>*/
/*     <a href="{{ path("mmi_homepage") }}" class="logo {% if menu_color != 'dark' %}white{% endif %} transition">331*/
/*         Accueil</a>*/
/* */
/*     <div id="hamburger" class="hamburger {{ menu_color }}">*/
/*         <div></div>*/
/*         <div></div>*/
/*         <div></div>*/
/*     </div>*/
/*     <nav{% if menu_color == 'dark' %} class="black"{% endif %}>*/
/*         <ul>*/
/*             {% for item in menu_items %}*/
/*                 <li><a class="transition thin {{ item.active }}" href="{{ item.path }}">{{ item.name | raw }}</a></li>*/
/*             {% endfor %}*/
/*         </ul>*/
/*     </nav>*/
/*     {% block headerExtended %}{% endblock %}*/
/* </header>*/
/* */
/* {% block content %}{% endblock %}*/
/* */
/* <footer class="white">*/
/*     <a href="{{ path("mmi_homepage") }}" class="logo">331 Acceuil</a>*/
/*     <ul>*/
/*         <li><a href="">À propos</a></li>*/
/*         <li><a href="">Mentions légales</a></li>*/
/*         <li><a href="http://mmimontbeliard.com" target="_blank">Département MMI</a></li>*/
/*     </ul>*/
/* </footer>*/
/* */
/* <!-- modal des messages d'erreur -->*/
/* <div class="modal fade" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="errorModalLabel">*/
/*     <div class="modal-dialog" role="document">*/
/*         <div class="modal-content">*/
/*             {% if result is defined %}*/
/*                 <p>{{ result }}</p>*/
/*             {% else %}*/
/*                 <p class="no_errors">all clear</p>*/
/*             {% endif %}*/
/*             <button type="button" class="close" aria-label="Close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* */
/* {% block includeBody %}{% endblock %}*/
/* */
/* <script>*/
/*     (function (i, s, o, g, r, a, m) {*/
/*         i['GoogleAnalyticsObject'] = r;*/
/*         i[r] = i[r] || function () {*/
/*                     (i[r].q = i[r].q || []).push(arguments)*/
/*                 }, i[r].l = 1 * new Date();*/
/*         a = s.createElement(o),*/
/*                 m = s.getElementsByTagName(o)[0];*/
/*         a.async = 1;*/
/*         a.src = g;*/
/*         m.parentNode.insertBefore(a, m)*/
/*     })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');*/
/* */
/*     ga('create', 'UA-73230977-1', 'auto');*/
/*     ga('send', 'pageview');*/
/* */
/* </script>*/
/* */
/* </body>*/
/* </html>*/
