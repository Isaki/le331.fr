<?php

/* MMIBundle:Entreprise:company.html.twig */
class __TwigTemplate_f9c99645f712412c75b47b6cab2048ba29ac22bf86304055cfa9f3679cdecefa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("MMIBundle::template.html.twig", "MMIBundle:Entreprise:company.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'includeHead' => array($this, 'block_includeHead'),
            'bodyClass' => array($this, 'block_bodyClass'),
            'headerExtended' => array($this, 'block_headerExtended'),
            'content' => array($this, 'block_content'),
            'includeBody' => array($this, 'block_includeBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "MMIBundle::template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_501a2a48bc564fda860dfc2b67011d46a6779a09ccf1e27154a156fb2b87ad6f = $this->env->getExtension("native_profiler");
        $__internal_501a2a48bc564fda860dfc2b67011d46a6779a09ccf1e27154a156fb2b87ad6f->enter($__internal_501a2a48bc564fda860dfc2b67011d46a6779a09ccf1e27154a156fb2b87ad6f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MMIBundle:Entreprise:company.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_501a2a48bc564fda860dfc2b67011d46a6779a09ccf1e27154a156fb2b87ad6f->leave($__internal_501a2a48bc564fda860dfc2b67011d46a6779a09ccf1e27154a156fb2b87ad6f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_0a032d7ad2bb907dcd4f0c8633b56fe0ed2f58923f50f1c54bad07be9f677eb3 = $this->env->getExtension("native_profiler");
        $__internal_0a032d7ad2bb907dcd4f0c8633b56fe0ed2f58923f50f1c54bad07be9f677eb3->enter($__internal_0a032d7ad2bb907dcd4f0c8633b56fe0ed2f58923f50f1c54bad07be9f677eb3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Profil ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["company"]) ? $context["company"] : $this->getContext($context, "company")), "name", array()), "html", null, true);
        
        $__internal_0a032d7ad2bb907dcd4f0c8633b56fe0ed2f58923f50f1c54bad07be9f677eb3->leave($__internal_0a032d7ad2bb907dcd4f0c8633b56fe0ed2f58923f50f1c54bad07be9f677eb3_prof);

    }

    // line 5
    public function block_includeHead($context, array $blocks = array())
    {
        $__internal_5a22bc83b43668c581b329edf8151c68d825cbdd04b1e72f48a919565d2553bb = $this->env->getExtension("native_profiler");
        $__internal_5a22bc83b43668c581b329edf8151c68d825cbdd04b1e72f48a919565d2553bb->enter($__internal_5a22bc83b43668c581b329edf8151c68d825cbdd04b1e72f48a919565d2553bb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "includeHead"));

        
        $__internal_5a22bc83b43668c581b329edf8151c68d825cbdd04b1e72f48a919565d2553bb->leave($__internal_5a22bc83b43668c581b329edf8151c68d825cbdd04b1e72f48a919565d2553bb_prof);

    }

    // line 8
    public function block_bodyClass($context, array $blocks = array())
    {
        $__internal_375b5d46c94cb380d2dea154d3234497b2e4778f7221a5717d95cc787a912117 = $this->env->getExtension("native_profiler");
        $__internal_375b5d46c94cb380d2dea154d3234497b2e4778f7221a5717d95cc787a912117->enter($__internal_375b5d46c94cb380d2dea154d3234497b2e4778f7221a5717d95cc787a912117_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "bodyClass"));

        echo "company";
        
        $__internal_375b5d46c94cb380d2dea154d3234497b2e4778f7221a5717d95cc787a912117->leave($__internal_375b5d46c94cb380d2dea154d3234497b2e4778f7221a5717d95cc787a912117_prof);

    }

    // line 10
    public function block_headerExtended($context, array $blocks = array())
    {
        $__internal_54fe7b7314c4a391b7a9f2f82c92975a69cd6e518b58cc9e3a8bed32a32b59cc = $this->env->getExtension("native_profiler");
        $__internal_54fe7b7314c4a391b7a9f2f82c92975a69cd6e518b58cc9e3a8bed32a32b59cc->enter($__internal_54fe7b7314c4a391b7a9f2f82c92975a69cd6e518b58cc9e3a8bed32a32b59cc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "headerExtended"));

        // line 11
        echo "    <div class=\"profil_header white company\">
        <img src=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("images/company/"), "html", null, true);
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["company"]) ? $context["company"] : $this->getContext($context, "company")), "logoFile", array()), "html", null, true);
        echo "\" class=\"profilepicture profil\">

        <h1>";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["company"]) ? $context["company"] : $this->getContext($context, "company")), "name", array()), "html", null, true);
        echo "</h1>

        ";
        // line 18
        echo "    </div>
";
        
        $__internal_54fe7b7314c4a391b7a9f2f82c92975a69cd6e518b58cc9e3a8bed32a32b59cc->leave($__internal_54fe7b7314c4a391b7a9f2f82c92975a69cd6e518b58cc9e3a8bed32a32b59cc_prof);

    }

    // line 21
    public function block_content($context, array $blocks = array())
    {
        $__internal_83c999db22a8fdbd0b8724a28b3233d141d1d2d2ca8c14e0dfffa29ec04c2951 = $this->env->getExtension("native_profiler");
        $__internal_83c999db22a8fdbd0b8724a28b3233d141d1d2d2ca8c14e0dfffa29ec04c2951->enter($__internal_83c999db22a8fdbd0b8724a28b3233d141d1d2d2ca8c14e0dfffa29ec04c2951_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 22
        echo "    <main>
        <div>
            ";
        // line 24
        if ( !(null === $this->getAttribute((isset($context["company"]) ? $context["company"] : $this->getContext($context, "company")), "description", array()))) {
            // line 25
            echo "                <article class=\"description\">
                    <h2>Description</h2>

                    <p>";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["company"]) ? $context["company"] : $this->getContext($context, "company")), "description", array()), "html", null, true);
            echo "</p>
                </article>
            ";
        }
        // line 31
        echo "            <article class=\"infos\">
                <h2>Infos</h2>
                ";
        // line 33
        if ( !(null === $this->getAttribute((isset($context["company"]) ? $context["company"] : $this->getContext($context, "company")), "industry", array()))) {
            echo "Secteur d’activité : <span>";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["company"]) ? $context["company"] : $this->getContext($context, "company")), "industry", array()), "name", array()), "html", null, true);
            echo "</span>
                    <br/>";
        }
        // line 35
        echo "                Effectif : <span>";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["company"]) ? $context["company"] : $this->getContext($context, "company")), "workforce", array()), "html", null, true);
        echo "</span><br/>
                Site : <span>";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["company"]) ? $context["company"] : $this->getContext($context, "company")), "email", array()), "html", null, true);
        echo "</span>
            </article>
            ";
        // line 38
        if ( !(null === $this->getAttribute((isset($context["company"]) ? $context["company"] : $this->getContext($context, "company")), "address", array()))) {
            // line 39
            echo "                <article class=\"adresse\">
                    <h2>Adresse</h2>
                    ";
            // line 41
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["company"]) ? $context["company"] : $this->getContext($context, "company")), "address", array()), "street", array()), "html", null, true);
            echo "
                    ";
            // line 42
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["company"]) ? $context["company"] : $this->getContext($context, "company")), "address", array()), "city", array()), "cp", array()), "html", null, true);
            echo "
                    ";
            // line 43
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["company"]) ? $context["company"] : $this->getContext($context, "company")), "address", array()), "city", array()), "name", array()), "html", null, true);
            echo "
                </article>
            ";
        }
        // line 46
        echo "            ";
        if ( !(null === $this->getAttribute((isset($context["company"]) ? $context["company"] : $this->getContext($context, "company")), "contact", array()))) {
            // line 47
            echo "                <article class=\"contact\">
                    <h2>Contact</h2>
                    Prénom : ";
            // line 49
            if ( !(null === $this->getAttribute($this->getAttribute((isset($context["company"]) ? $context["company"] : $this->getContext($context, "company")), "contact", array()), "firstname", array()))) {
                echo "<span>";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["company"]) ? $context["company"] : $this->getContext($context, "company")), "contact", array()), "firstname", array()), "html", null, true);
                echo "</span>
                        <br/>";
            }
            // line 51
            echo "                    Nom : ";
            if ( !(null === $this->getAttribute($this->getAttribute((isset($context["company"]) ? $context["company"] : $this->getContext($context, "company")), "contact", array()), "lastname", array()))) {
                echo "<span>";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["company"]) ? $context["company"] : $this->getContext($context, "company")), "contact", array()), "lastname", array()), "html", null, true);
                echo "</span>
                        <br/>";
            }
            // line 53
            echo "                    Poste occupé : ";
            if ( !(null === $this->getAttribute($this->getAttribute((isset($context["company"]) ? $context["company"] : $this->getContext($context, "company")), "contact", array()), "profession", array()))) {
                // line 54
                echo "                        <span>";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["company"]) ? $context["company"] : $this->getContext($context, "company")), "contact", array()), "profession", array()), "name", array()), "html", null, true);
                echo "</span><br/>";
            }
            // line 55
            echo "                    Téléphone : ";
            if ( !(null === $this->getAttribute($this->getAttribute((isset($context["company"]) ? $context["company"] : $this->getContext($context, "company")), "contact", array()), "telephone", array()))) {
                // line 56
                echo "                        <span>";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["company"]) ? $context["company"] : $this->getContext($context, "company")), "contact", array()), "telephone", array()), "html", null, true);
                echo "</span><br/>";
            }
            // line 57
            echo "                    Email : ";
            if ( !(null === $this->getAttribute($this->getAttribute((isset($context["company"]) ? $context["company"] : $this->getContext($context, "company")), "contact", array()), "email", array()))) {
                echo "<span>";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["company"]) ? $context["company"] : $this->getContext($context, "company")), "contact", array()), "email", array()), "html", null, true);
                echo "</span>
                        <br/>";
            }
            // line 59
            echo "                </article>
            ";
        }
        // line 61
        echo "            <article class=\"membres\">
                <h2>Nos membres y ont travaillé</h2>
                <a href=\"\"><img src=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("images/profil_picture/louis.jpg"), "html", null, true);
        echo "\" class=\"image_profil\"></a>
                <a href=\"\"><img src=\"";
        // line 64
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("images/profil_picture/fiona.jpg"), "html", null, true);
        echo "\" class=\"image_profil\"></a>
            </article>
            <article class=\"config\">
                ";
        // line 67
        if ((array_key_exists("account", $context) && ($this->getAttribute((isset($context["account"]) ? $context["account"] : $this->getContext($context, "account")), "id", array()) == $this->getAttribute((isset($context["company"]) ? $context["company"] : $this->getContext($context, "company")), "id", array())))) {
            // line 68
            echo "                    <a href=\"";
            echo $this->env->getExtension('routing')->getPath("mmi_edit_company");
            echo "\">Modifier les informations administratives</a><br>
                ";
        } else {
            // line 70
            echo "                    <a href=\"\">Cette entreprise est en cessation d’activité ?</a>
                ";
        }
        // line 72
        echo "            </article>
        </div>
        <section>
            <input type=\"hidden\">

            <div id=\"map\"></div>
        </section>
        <div id=\"maptoggle\" class=\"\">
            <svg x=\"0px\" y=\"0px\" viewBox=\"0 0 100 100\" style=\"enable-background:new 0 0 100 100;\" xml:space=\"preserve\">
                <g id=\"Circle\">
                    <circle class=\"st0\" cx=\"50\" cy=\"50\" r=\"46.1\"/>
                </g>
                <g id=\"Picker\">
                    <path class=\"st1\" d=\"M49.9,77.2c-21-32.5-21.1-54.4,0.2-54.4S71.2,44.7,49.9,77.2z\"/>
                </g>
                <g id=\"Cross\">
                    <line class=\"st2\" x1=\"71.1\" y1=\"71.1\" x2=\"28.9\" y2=\"28.9\"/>
                    <line class=\"st2\" x1=\"71.1\" y1=\"28.9\" x2=\"28.9\" y2=\"71.1\"/>
                </g>
            </svg>
        </div>
    </main>
";
        
        $__internal_83c999db22a8fdbd0b8724a28b3233d141d1d2d2ca8c14e0dfffa29ec04c2951->leave($__internal_83c999db22a8fdbd0b8724a28b3233d141d1d2d2ca8c14e0dfffa29ec04c2951_prof);

    }

    // line 96
    public function block_includeBody($context, array $blocks = array())
    {
        $__internal_6be84b91dc80da6dc9ae95d77224f48773d4055692fd6589197d3b9c349aa1e2 = $this->env->getExtension("native_profiler");
        $__internal_6be84b91dc80da6dc9ae95d77224f48773d4055692fd6589197d3b9c349aa1e2->enter($__internal_6be84b91dc80da6dc9ae95d77224f48773d4055692fd6589197d3b9c349aa1e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "includeBody"));

        // line 97
        echo "    <script type=\"javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/map.js"), "html", null, true);
        echo "\"></script>
    <script type=\"javascript\" async defer
            src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyBpJzOJLQuJUianW27QLEzMo9Y-YWp7aW4&callback=initMap\"></script>
";
        
        $__internal_6be84b91dc80da6dc9ae95d77224f48773d4055692fd6589197d3b9c349aa1e2->leave($__internal_6be84b91dc80da6dc9ae95d77224f48773d4055692fd6589197d3b9c349aa1e2_prof);

    }

    public function getTemplateName()
    {
        return "MMIBundle:Entreprise:company.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  278 => 97,  272 => 96,  243 => 72,  239 => 70,  233 => 68,  231 => 67,  225 => 64,  221 => 63,  217 => 61,  213 => 59,  205 => 57,  200 => 56,  197 => 55,  192 => 54,  189 => 53,  181 => 51,  174 => 49,  170 => 47,  167 => 46,  161 => 43,  157 => 42,  153 => 41,  149 => 39,  147 => 38,  142 => 36,  137 => 35,  130 => 33,  126 => 31,  120 => 28,  115 => 25,  113 => 24,  109 => 22,  103 => 21,  95 => 18,  90 => 14,  84 => 12,  81 => 11,  75 => 10,  63 => 8,  52 => 5,  39 => 3,  11 => 1,);
    }
}
/* {% extends "MMIBundle::template.html.twig" %}*/
/* */
/* {% block title %}Profil {{ company.name }}{% endblock %}*/
/* */
/* {% block includeHead %}*/
/* {% endblock %}*/
/* */
/* {% block bodyClass %}company{% endblock %}*/
/* */
/* {% block headerExtended %}*/
/*     <div class="profil_header white company">*/
/*         <img src="{{ asset("images/company/") }}{{ company.logoFile }}" class="profilepicture profil">*/
/* */
/*         <h1>{{ company.name }}</h1>*/
/* */
/*         {#<p>Remplissez votre fiche en deux clics en important les données de votre entreprise depuis LinkedIn</p>*/
/*         <button type="button" class="white big">LinkedIn</button>#}*/
/*     </div>*/
/* {% endblock %}*/
/* */
/* {% block content %}*/
/*     <main>*/
/*         <div>*/
/*             {% if company.description is not null %}*/
/*                 <article class="description">*/
/*                     <h2>Description</h2>*/
/* */
/*                     <p>{{ company.description }}</p>*/
/*                 </article>*/
/*             {% endif %}*/
/*             <article class="infos">*/
/*                 <h2>Infos</h2>*/
/*                 {% if company.industry is not null %}Secteur d’activité : <span>{{ company.industry.name }}</span>*/
/*                     <br/>{% endif %}*/
/*                 Effectif : <span>{{ company.workforce }}</span><br/>*/
/*                 Site : <span>{{ company.email }}</span>*/
/*             </article>*/
/*             {% if company.address is not null %}*/
/*                 <article class="adresse">*/
/*                     <h2>Adresse</h2>*/
/*                     {{ company.address.street }}*/
/*                     {{ company.address.city.cp }}*/
/*                     {{ company.address.city.name }}*/
/*                 </article>*/
/*             {% endif %}*/
/*             {% if company.contact is not null %}*/
/*                 <article class="contact">*/
/*                     <h2>Contact</h2>*/
/*                     Prénom : {% if company.contact.firstname is not null %}<span>{{ company.contact.firstname }}</span>*/
/*                         <br/>{% endif %}*/
/*                     Nom : {% if company.contact.lastname is not null %}<span>{{ company.contact.lastname }}</span>*/
/*                         <br/>{% endif %}*/
/*                     Poste occupé : {% if company.contact.profession is not null %}*/
/*                         <span>{{ company.contact.profession.name }}</span><br/>{% endif %}*/
/*                     Téléphone : {% if company.contact.telephone is not null %}*/
/*                         <span>{{ company.contact.telephone }}</span><br/>{% endif %}*/
/*                     Email : {% if company.contact.email is not null %}<span>{{ company.contact.email }}</span>*/
/*                         <br/>{% endif %}*/
/*                 </article>*/
/*             {% endif %}*/
/*             <article class="membres">*/
/*                 <h2>Nos membres y ont travaillé</h2>*/
/*                 <a href=""><img src="{{ asset("images/profil_picture/louis.jpg") }}" class="image_profil"></a>*/
/*                 <a href=""><img src="{{ asset("images/profil_picture/fiona.jpg") }}" class="image_profil"></a>*/
/*             </article>*/
/*             <article class="config">*/
/*                 {% if account is defined and account.id == company.id %}*/
/*                     <a href="{{ path('mmi_edit_company') }}">Modifier les informations administratives</a><br>*/
/*                 {% else %}*/
/*                     <a href="">Cette entreprise est en cessation d’activité ?</a>*/
/*                 {% endif %}*/
/*             </article>*/
/*         </div>*/
/*         <section>*/
/*             <input type="hidden">*/
/* */
/*             <div id="map"></div>*/
/*         </section>*/
/*         <div id="maptoggle" class="">*/
/*             <svg x="0px" y="0px" viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;" xml:space="preserve">*/
/*                 <g id="Circle">*/
/*                     <circle class="st0" cx="50" cy="50" r="46.1"/>*/
/*                 </g>*/
/*                 <g id="Picker">*/
/*                     <path class="st1" d="M49.9,77.2c-21-32.5-21.1-54.4,0.2-54.4S71.2,44.7,49.9,77.2z"/>*/
/*                 </g>*/
/*                 <g id="Cross">*/
/*                     <line class="st2" x1="71.1" y1="71.1" x2="28.9" y2="28.9"/>*/
/*                     <line class="st2" x1="71.1" y1="28.9" x2="28.9" y2="71.1"/>*/
/*                 </g>*/
/*             </svg>*/
/*         </div>*/
/*     </main>*/
/* {% endblock %}*/
/* */
/* {% block includeBody %}*/
/*     <script type="javascript" src="{{ asset("js/map.js") }}"></script>*/
/*     <script type="javascript" async defer*/
/*             src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBpJzOJLQuJUianW27QLEzMo9Y-YWp7aW4&callback=initMap"></script>*/
/* {% endblock %}*/
/* */
