<?php

/* MMIBundle:Default:account.html.twig */
class __TwigTemplate_8754f4d0d99ea93bfcc958313b9303edd76f119b1c4935ed8fbc2c52333df879 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("MMIBundle::template.html.twig", "MMIBundle:Default:account.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'includeHead' => array($this, 'block_includeHead'),
            'bodyClass' => array($this, 'block_bodyClass'),
            'header' => array($this, 'block_header'),
            'headerExtended' => array($this, 'block_headerExtended'),
            'content' => array($this, 'block_content'),
            'includeBody' => array($this, 'block_includeBody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "MMIBundle::template.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_081b575b6e6bae04d846c3b7d0f4d38e0428b53670bcb8abfd537d6f4be6a153 = $this->env->getExtension("native_profiler");
        $__internal_081b575b6e6bae04d846c3b7d0f4d38e0428b53670bcb8abfd537d6f4be6a153->enter($__internal_081b575b6e6bae04d846c3b7d0f4d38e0428b53670bcb8abfd537d6f4be6a153_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "MMIBundle:Default:account.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_081b575b6e6bae04d846c3b7d0f4d38e0428b53670bcb8abfd537d6f4be6a153->leave($__internal_081b575b6e6bae04d846c3b7d0f4d38e0428b53670bcb8abfd537d6f4be6a153_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_05ed43a6d7d932f5fef39362a733d627f6334351c73e078743a7e0761b351439 = $this->env->getExtension("native_profiler");
        $__internal_05ed43a6d7d932f5fef39362a733d627f6334351c73e078743a7e0761b351439->enter($__internal_05ed43a6d7d932f5fef39362a733d627f6334351c73e078743a7e0761b351439_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Gestion du compte";
        
        $__internal_05ed43a6d7d932f5fef39362a733d627f6334351c73e078743a7e0761b351439->leave($__internal_05ed43a6d7d932f5fef39362a733d627f6334351c73e078743a7e0761b351439_prof);

    }

    // line 5
    public function block_includeHead($context, array $blocks = array())
    {
        $__internal_45307037cf8a1eaf8d3496524677339c3475cbfe02b07d9d9c7015c14cb36e53 = $this->env->getExtension("native_profiler");
        $__internal_45307037cf8a1eaf8d3496524677339c3475cbfe02b07d9d9c7015c14cb36e53->enter($__internal_45307037cf8a1eaf8d3496524677339c3475cbfe02b07d9d9c7015c14cb36e53_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "includeHead"));

        // line 6
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/form_regex.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_45307037cf8a1eaf8d3496524677339c3475cbfe02b07d9d9c7015c14cb36e53->leave($__internal_45307037cf8a1eaf8d3496524677339c3475cbfe02b07d9d9c7015c14cb36e53_prof);

    }

    // line 9
    public function block_bodyClass($context, array $blocks = array())
    {
        $__internal_cc7ad042126c5fefdda1f04ad785d1c56d00044294b1c6caa2183edbfa217f35 = $this->env->getExtension("native_profiler");
        $__internal_cc7ad042126c5fefdda1f04ad785d1c56d00044294b1c6caa2183edbfa217f35->enter($__internal_cc7ad042126c5fefdda1f04ad785d1c56d00044294b1c6caa2183edbfa217f35_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "bodyClass"));

        echo "account";
        
        $__internal_cc7ad042126c5fefdda1f04ad785d1c56d00044294b1c6caa2183edbfa217f35->leave($__internal_cc7ad042126c5fefdda1f04ad785d1c56d00044294b1c6caa2183edbfa217f35_prof);

    }

    // line 11
    public function block_header($context, array $blocks = array())
    {
        $__internal_55587a3162ff4eeedc82ab111c5a779c44c4140e89ca83988095693185bb8875 = $this->env->getExtension("native_profiler");
        $__internal_55587a3162ff4eeedc82ab111c5a779c44c4140e89ca83988095693185bb8875->enter($__internal_55587a3162ff4eeedc82ab111c5a779c44c4140e89ca83988095693185bb8875_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        
        $__internal_55587a3162ff4eeedc82ab111c5a779c44c4140e89ca83988095693185bb8875->leave($__internal_55587a3162ff4eeedc82ab111c5a779c44c4140e89ca83988095693185bb8875_prof);

    }

    // line 13
    public function block_headerExtended($context, array $blocks = array())
    {
        $__internal_94c13751e2e6556f606e49ad3ca6b5f88c7ee47b6412ae39007614417816a3ef = $this->env->getExtension("native_profiler");
        $__internal_94c13751e2e6556f606e49ad3ca6b5f88c7ee47b6412ae39007614417816a3ef->enter($__internal_94c13751e2e6556f606e49ad3ca6b5f88c7ee47b6412ae39007614417816a3ef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "headerExtended"));

        
        $__internal_94c13751e2e6556f606e49ad3ca6b5f88c7ee47b6412ae39007614417816a3ef->leave($__internal_94c13751e2e6556f606e49ad3ca6b5f88c7ee47b6412ae39007614417816a3ef_prof);

    }

    // line 15
    public function block_content($context, array $blocks = array())
    {
        $__internal_18e63020a56bac4a5014d4c93c1024b144cf619c69b00c21ce13332fd725cd71 = $this->env->getExtension("native_profiler");
        $__internal_18e63020a56bac4a5014d4c93c1024b144cf619c69b00c21ce13332fd725cd71->enter($__internal_18e63020a56bac4a5014d4c93c1024b144cf619c69b00c21ce13332fd725cd71_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 16
        echo "    <main class=\"manage_account\">
        <h1>Gestion du compte</h1>
        ";
        // line 18
        if (array_key_exists("result", $context)) {
            echo twig_escape_filter($this->env, (isset($context["result"]) ? $context["result"] : $this->getContext($context, "result")), "html", null, true);
        }
        // line 19
        echo "        <section class=\"wrapper\">
            <section>

                ";
        // line 23
        echo "                ";
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["modifyEmail"]) ? $context["modifyEmail"] : $this->getContext($context, "modifyEmail")), 'form_start', array("attr" => array("class" => "verif_regex")));
        // line 25
        echo "
                <h2 class=\"dark\">Adresse email</h2>
                ";
        // line 27
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["modifyEmail"]) ? $context["modifyEmail"] : $this->getContext($context, "modifyEmail")), "email", array()), 'widget', array("attr" => array("class" => "big verif", "data-regex" => "email")));
        // line 30
        echo "
                <button type=\"submit\" class=\"big grey\">Modifier</button>
                ";
        // line 32
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["modifyEmail"]) ? $context["modifyEmail"] : $this->getContext($context, "modifyEmail")), 'rest');
        echo "
                ";
        // line 33
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["modifyEmail"]) ? $context["modifyEmail"] : $this->getContext($context, "modifyEmail")), 'form_end');
        echo "

                ";
        // line 36
        echo "                ";
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["modifyPassword"]) ? $context["modifyPassword"] : $this->getContext($context, "modifyPassword")), 'form_start', array("attr" => array("class" => "verif_regex")));
        // line 38
        echo "
                <h2 class=\"dark\">Mot de passe</h2>
                ";
        // line 40
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["modifyPassword"]) ? $context["modifyPassword"] : $this->getContext($context, "modifyPassword")), "pass1", array()), 'widget', array("attr" => array("class" => "big verif")));
        // line 42
        echo "
                ";
        // line 43
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["modifyPassword"]) ? $context["modifyPassword"] : $this->getContext($context, "modifyPassword")), "pass2", array()), 'widget', array("attr" => array("class" => "big verif")));
        // line 45
        echo "
                ";
        // line 46
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["modifyPassword"]) ? $context["modifyPassword"] : $this->getContext($context, "modifyPassword")), "pass3", array()), 'widget', array("attr" => array("class" => "big verif")));
        // line 48
        echo "
                <button type=\"submit\" class=\"big grey\">Modifier</button>
                ";
        // line 50
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["modifyPassword"]) ? $context["modifyPassword"] : $this->getContext($context, "modifyPassword")), 'rest');
        echo "
                ";
        // line 51
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["modifyPassword"]) ? $context["modifyPassword"] : $this->getContext($context, "modifyPassword")), 'form_end');
        echo "

            </section>
            <section>

                ";
        // line 57
        echo "                ";
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["toggleNewsletter"]) ? $context["toggleNewsletter"] : $this->getContext($context, "toggleNewsletter")), 'form_start');
        echo "
                <h2 class=\"dark\">Newsletter</h2>
                ";
        // line 59
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["toggleNewsletter"]) ? $context["toggleNewsletter"] : $this->getContext($context, "toggleNewsletter")), 'rest');
        echo "
                ";
        // line 60
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["toggleNewsletter"]) ? $context["toggleNewsletter"] : $this->getContext($context, "toggleNewsletter")), 'form_end');
        echo "

                ";
        // line 63
        echo "                ";
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["deleteUser"]) ? $context["deleteUser"] : $this->getContext($context, "deleteUser")), 'form_start', array("attr" => array("class" => "verif_regex")));
        // line 65
        echo "
                <h2 class=\"dark\">Suppression du compte</h2>
                ";
        // line 67
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["deleteUser"]) ? $context["deleteUser"] : $this->getContext($context, "deleteUser")), "password", array()), 'widget', array("attr" => array("class" => "big verif", "placeholder" => "Confirmation du mot de passe")));
        // line 70
        echo "
                <button type=\"submit\" class=\"big\">Supprimer mon compte</button>
                ";
        // line 72
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["deleteUser"]) ? $context["deleteUser"] : $this->getContext($context, "deleteUser")), 'rest');
        echo "
                ";
        // line 73
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["deleteUser"]) ? $context["deleteUser"] : $this->getContext($context, "deleteUser")), 'form_end');
        echo "

            </section>
        </section>
    </main>
";
        
        $__internal_18e63020a56bac4a5014d4c93c1024b144cf619c69b00c21ce13332fd725cd71->leave($__internal_18e63020a56bac4a5014d4c93c1024b144cf619c69b00c21ce13332fd725cd71_prof);

    }

    // line 80
    public function block_includeBody($context, array $blocks = array())
    {
        $__internal_05360d8408e4d94b44343f000cacb03f15c272d966f336faf836404b56581e04 = $this->env->getExtension("native_profiler");
        $__internal_05360d8408e4d94b44343f000cacb03f15c272d966f336faf836404b56581e04->enter($__internal_05360d8408e4d94b44343f000cacb03f15c272d966f336faf836404b56581e04_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "includeBody"));

        // line 81
        echo "    <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("js/menu.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_05360d8408e4d94b44343f000cacb03f15c272d966f336faf836404b56581e04->leave($__internal_05360d8408e4d94b44343f000cacb03f15c272d966f336faf836404b56581e04_prof);

    }

    public function getTemplateName()
    {
        return "MMIBundle:Default:account.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  229 => 81,  223 => 80,  210 => 73,  206 => 72,  202 => 70,  200 => 67,  196 => 65,  193 => 63,  188 => 60,  184 => 59,  178 => 57,  170 => 51,  166 => 50,  162 => 48,  160 => 46,  157 => 45,  155 => 43,  152 => 42,  150 => 40,  146 => 38,  143 => 36,  138 => 33,  134 => 32,  130 => 30,  128 => 27,  124 => 25,  121 => 23,  116 => 19,  112 => 18,  108 => 16,  102 => 15,  91 => 13,  80 => 11,  68 => 9,  58 => 6,  52 => 5,  40 => 3,  11 => 1,);
    }
}
/* {% extends "MMIBundle::template.html.twig" %}*/
/* */
/* {% block title %}Gestion du compte{% endblock %}*/
/* */
/* {% block includeHead %}*/
/*     <script src="{{ asset('js/form_regex.js') }}"></script>*/
/* {% endblock %}*/
/* */
/* {% block bodyClass %}account{% endblock %}*/
/* */
/* {% block header %}{% endblock %}*/
/* */
/* {% block headerExtended %}{% endblock %}*/
/* */
/* {% block content %}*/
/*     <main class="manage_account">*/
/*         <h1>Gestion du compte</h1>*/
/*         {% if result is defined %}{{ result }}{% endif %}*/
/*         <section class="wrapper">*/
/*             <section>*/
/* */
/*                 {# Modification de l'email #}*/
/*                 {{ form_start(modifyEmail,*/
/*                 {"attr": {*/
/*                     "class": "verif_regex"}}) }}*/
/*                 <h2 class="dark">Adresse email</h2>*/
/*                 {{ form_widget(modifyEmail.email,*/
/*                 {"attr": {*/
/*                     "class": "big verif",*/
/*                     "data-regex": "email"}}) }}*/
/*                 <button type="submit" class="big grey">Modifier</button>*/
/*                 {{ form_rest(modifyEmail) }}*/
/*                 {{ form_end(modifyEmail) }}*/
/* */
/*                 {# Modification du mot de passe #}*/
/*                 {{ form_start(modifyPassword,*/
/*                 {"attr": {*/
/*                     "class": "verif_regex"}}) }}*/
/*                 <h2 class="dark">Mot de passe</h2>*/
/*                 {{ form_widget(modifyPassword.pass1,*/
/*                 {"attr": {*/
/*                     "class": "big verif"}}) }}*/
/*                 {{ form_widget(modifyPassword.pass2,*/
/*                 {"attr": {*/
/*                     "class": "big verif"}}) }}*/
/*                 {{ form_widget(modifyPassword.pass3,*/
/*                 {"attr": {*/
/*                     "class": "big verif"}}) }}*/
/*                 <button type="submit" class="big grey">Modifier</button>*/
/*                 {{ form_rest(modifyPassword) }}*/
/*                 {{ form_end(modifyPassword) }}*/
/* */
/*             </section>*/
/*             <section>*/
/* */
/*                 {# Toggle newsletter #}*/
/*                 {{ form_start(toggleNewsletter) }}*/
/*                 <h2 class="dark">Newsletter</h2>*/
/*                 {{ form_rest(toggleNewsletter) }}*/
/*                 {{ form_end(toggleNewsletter) }}*/
/* */
/*                 {# Supprimer compte #}*/
/*                 {{ form_start(deleteUser,*/
/*                 {"attr": {*/
/*                     "class": "verif_regex"}}) }}*/
/*                 <h2 class="dark">Suppression du compte</h2>*/
/*                 {{ form_widget(deleteUser.password,*/
/*                 {"attr": {*/
/*                     "class": "big verif",*/
/*                     "placeholder": "Confirmation du mot de passe"}}) }}*/
/*                 <button type="submit" class="big">Supprimer mon compte</button>*/
/*                 {{ form_rest(deleteUser) }}*/
/*                 {{ form_end(deleteUser) }}*/
/* */
/*             </section>*/
/*         </section>*/
/*     </main>*/
/* {% endblock %}*/
/* */
/* {% block includeBody %}*/
/*     <script src="{{ asset("js/menu.js") }}"></script>*/
/* {% endblock %}*/
/* */
