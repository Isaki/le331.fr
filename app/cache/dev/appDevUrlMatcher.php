<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_purge
                if ($pathinfo === '/_profiler/purge') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:purgeAction',  '_route' => '_profiler_purge',);
                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            if (0 === strpos($pathinfo, '/_configurator')) {
                // _configurator_home
                if (rtrim($pathinfo, '/') === '/_configurator') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_configurator_home');
                    }

                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::checkAction',  '_route' => '_configurator_home',);
                }

                // _configurator_step
                if (0 === strpos($pathinfo, '/_configurator/step') && preg_match('#^/_configurator/step/(?P<index>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_configurator_step')), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::stepAction',));
                }

                // _configurator_final
                if ($pathinfo === '/_configurator/final') {
                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::finalAction',  '_route' => '_configurator_final',);
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        // mmi_homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'mmi_homepage');
            }

            return array (  '_controller' => 'MMIBundle\\Controller\\DefaultController::indexAction',  '_route' => 'mmi_homepage',);
        }

        // mmi_deconnexion
        if ($pathinfo === '/deconnexion') {
            return array (  '_controller' => 'MMIBundle\\Controller\\DefaultController::deconnexionAction',  '_route' => 'mmi_deconnexion',);
        }

        // mmi_search
        if (0 === strpos($pathinfo, '/search') && preg_match('#^/search(?:/(?P<type>[^/]++))?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'mmi_search')), array (  '_controller' => 'MMIBundle\\Controller\\DefaultController::searchAction',  'type' => 'students',));
        }

        // mmi_account
        if ($pathinfo === '/account') {
            return array (  '_controller' => 'MMIBundle\\Controller\\DefaultController::accountAction',  '_route' => 'mmi_account',);
        }

        // mmi_blog
        if ($pathinfo === '/blog') {
            return array (  '_controller' => 'MMIBundle\\Controller\\DefaultController::blogAction',  '_route' => 'mmi_blog',);
        }

        // mmi_welcome_student
        if ($pathinfo === '/welcome_student') {
            return array (  '_controller' => 'MMIBundle\\Controller\\UtilisateurController::welcomeAction',  '_route' => 'mmi_welcome_student',);
        }

        // mmi_edit_student
        if ($pathinfo === '/edit_student') {
            return array (  '_controller' => 'MMIBundle\\Controller\\UtilisateurController::edit_studentAction',  '_route' => 'mmi_edit_student',);
        }

        // mmi_student
        if (0 === strpos($pathinfo, '/student') && preg_match('#^/student/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'mmi_student')), array (  '_controller' => 'MMIBundle\\Controller\\UtilisateurController::studentAction',));
        }

        // mmi_pros
        if ($pathinfo === '/pros') {
            return array (  '_controller' => 'MMIBundle\\Controller\\EntrepriseController::indexAction',  '_route' => 'mmi_pros',);
        }

        // mmi_welcome_company
        if ($pathinfo === '/welcome_company') {
            return array (  '_controller' => 'MMIBundle\\Controller\\EntrepriseController::welcomeAction',  '_route' => 'mmi_welcome_company',);
        }

        // mmi_company
        if (0 === strpos($pathinfo, '/company') && preg_match('#^/company/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'mmi_company')), array (  '_controller' => 'MMIBundle\\Controller\\EntrepriseController::companyAction',));
        }

        // mmi_edit_company
        if ($pathinfo === '/edit_company') {
            return array (  '_controller' => 'MMIBundle\\Controller\\EntrepriseController::edit_companyAction',  '_route' => 'mmi_edit_company',);
        }

        // mmi_univ_project
        if (0 === strpos($pathinfo, '/univ_project') && preg_match('#^/univ_project/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'mmi_univ_project')), array (  '_controller' => 'MMIBundle\\Controller\\ProjetUnivController::indexAction',));
        }

        // mmi_edit_univ_project
        if ($pathinfo === '/edit_univ_project') {
            return array (  '_controller' => 'MMIBundle\\Controller\\ProjetUnivController::edit_univ_projectAction',  '_route' => 'mmi_edit_univ_project',);
        }

        // mmi_edit_add_univ_project
        if ($pathinfo === '/add_univ_project') {
            return array (  '_controller' => 'MMIBundle\\Controller\\ProjetUnivController::add_univ_projectAction',  '_route' => 'mmi_edit_add_univ_project',);
        }

        // mmi_training
        if (0 === strpos($pathinfo, '/training') && preg_match('#^/training/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'mmi_training')), array (  '_controller' => 'MMIBundle\\Controller\\FormationController::indexAction',));
        }

        // mmi_add_training
        if ($pathinfo === '/add_training') {
            return array (  '_controller' => 'MMIBundle\\Controller\\FormationController::add_trainingAction',  '_route' => 'mmi_add_training',);
        }

        // mmi_edit_training
        if ($pathinfo === '/edit_training') {
            return array (  '_controller' => 'MMIBundle\\Controller\\FormationController::edit_trainingAction',  '_route' => 'mmi_edit_training',);
        }

        // mmi_profession
        if (0 === strpos($pathinfo, '/profession') && preg_match('#^/profession/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'mmi_profession')), array (  '_controller' => 'MMIBundle\\Controller\\ProfessionController::indexAction',));
        }

        // mmi_add_profession
        if ($pathinfo === '/add_profession') {
            return array (  '_controller' => 'MMIBundle\\Controller\\ProfessionController::add_professionAction',  '_route' => 'mmi_add_profession',);
        }

        // mmi_edit_profession
        if ($pathinfo === '/edit_profession') {
            return array (  '_controller' => 'MMIBundle\\Controller\\ProfessionController::edit_professionAction',  '_route' => 'mmi_edit_profession',);
        }

        // mmi_my_internship
        if ($pathinfo === '/my_internship') {
            return array (  '_controller' => 'MMIBundle\\Controller\\StageController::myInternshipAction',  '_route' => 'mmi_my_internship',);
        }

        // mmi_stages_promo
        if ($pathinfo === '/stages_promo') {
            return array (  '_controller' => 'MMIBundle\\Controller\\StageController::stagesPromoAction',  '_route' => 'mmi_stages_promo',);
        }

        // mmi_internship_validation
        if (0 === strpos($pathinfo, '/internship_validation') && preg_match('#^/internship_validation/(?P<id>[^/]++)/(?P<valide>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'mmi_internship_validation')), array (  '_controller' => 'MMIBundle\\Controller\\StageController::internshipValidationAction',));
        }

        // mmi_stages_valides
        if ($pathinfo === '/stages_valides') {
            return array (  '_controller' => 'MMIBundle\\Controller\\StageController::stagesValidesAction',  '_route' => 'mmi_stages_valides',);
        }

        if (0 === strpos($pathinfo, '/agreement_')) {
            // mmi_agreement_generation
            if ($pathinfo === '/agreement_generation') {
                return array (  '_controller' => 'MMIBundle\\Controller\\StageController::internshipGenerationAction',  '_route' => 'mmi_agreement_generation',);
            }

            // mmi_agreement_validation
            if (0 === strpos($pathinfo, '/agreement_validation') && preg_match('#^/agreement_validation/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'mmi_agreement_validation')), array (  '_controller' => 'MMIBundle\\Controller\\StageController::agreementValidationAction',));
            }

        }

        if (0 === strpos($pathinfo, '/stages_')) {
            // mmi_stages_etablis
            if ($pathinfo === '/stages_etablis') {
                return array (  '_controller' => 'MMIBundle\\Controller\\StageController::stagesEtablisAction',  '_route' => 'mmi_stages_etablis',);
            }

            // mmi_stages_tuteur
            if ($pathinfo === '/stages_tuteur') {
                return array (  '_controller' => 'MMIBundle\\Controller\\StageController::stagesTuteurAction',  '_route' => 'mmi_stages_tuteur',);
            }

        }

        // mmi_internship
        if (0 === strpos($pathinfo, '/internship') && preg_match('#^/internship/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'mmi_internship')), array (  '_controller' => 'MMIBundle\\Controller\\StageController::internshipAction',));
        }

        if (0 === strpos($pathinfo, '/ajax_')) {
            if (0 === strpos($pathinfo, '/ajax_get_')) {
                // mmi_ajax_get_list_by_type
                if (0 === strpos($pathinfo, '/ajax_get_list_by_type') && preg_match('#^/ajax_get_list_by_type/(?P<type>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'mmi_ajax_get_list_by_type')), array (  '_controller' => 'MMIBundle\\Controller\\AjaxController::getListByTypeAction',));
                }

                // mmi_ajax_get_experience
                if (0 === strpos($pathinfo, '/ajax_get_experience') && preg_match('#^/ajax_get_experience/(?P<type>[^/]++)/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'mmi_ajax_get_experience')), array (  '_controller' => 'MMIBundle\\Controller\\AjaxController::getExperienceAction',));
                }

            }

            // mmi_ajax_delete_experience
            if (0 === strpos($pathinfo, '/ajax_delete_experience') && preg_match('#^/ajax_delete_experience/(?P<type>[^/]++)/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'mmi_ajax_delete_experience')), array (  '_controller' => 'MMIBundle\\Controller\\AjaxController::deleteExperienceAction',));
            }

        }

        // mmi_validate
        if (0 === strpos($pathinfo, '/validate') && preg_match('#^/validate/(?P<code>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'mmi_validate')), array (  '_controller' => 'MMIBundle\\Controller\\ValidationEmailController::validateAction',));
        }

        // homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'homepage');
            }

            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
